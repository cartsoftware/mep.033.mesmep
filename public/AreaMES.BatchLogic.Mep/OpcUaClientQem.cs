﻿using AreaM2M.RealTime.Dto;
using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Server.Utils;
using log4net;
using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMES.BatchLogic.Mep
{
    public class OpcUaClientQem: IOpcUaClient
    {
        private string _baseFolder;
        private string driverName = "QEM";
        private string _deviceId;
        private string _endpoint;
        private string _password;
        private string _username;
        private Session _sessionOpcUaClient;
        private Subscription _subscription;
        private List<string> _subscribeTo = new List<string>();
        private Opc.Ua.ApplicationConfiguration _config;
        private Opc.Ua.Configuration.ApplicationInstance _application;
        private Timer _timerTryToConnect;
        private object _lock_timerTryToConnect = new object();
        private object _lock_write = new object();
        private bool _isOn_timerTryToConnect = false;
        private OpcUaNodeEntryCollection _items;
        private bool _isConnected = false;
        private int state = 0;
        public string SerialNumber { get; set; }

        public Action<string, StatoMacchinaDriver> OnClientStatusChanged { get; set ; }

        public OpcUaClientQem(string deviceId, string endpointOpc)
        {
            _deviceId = deviceId;
            _endpoint = endpointOpc;
            _baseFolder = "";
            _username = "";
            _password = "";
        }


        public Session Connect()
        {
            // vengono assegnati i sottoscrittori
            _items = this.AddVariableQem();

            SystemVariablesDataContainer.Instance.Create(SystemVariables.Items, _deviceId);

            #region Configurazione client  OPC-UA
            _config = new Opc.Ua.ApplicationConfiguration()
            {
                ApplicationName = "Opc",
                ApplicationUri = Utils.Format(@"urn:{0}:MyHome", System.Net.Dns.GetHostName()),
                ApplicationType = ApplicationType.Client,
                SecurityConfiguration = new SecurityConfiguration
                {
                    ApplicationCertificate = new CertificateIdentifier { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\MachineDefault", SubjectName = Utils.Format(@"CN={0}, DC={1}", "MyHomework", System.Net.Dns.GetHostName()) },
                    TrustedIssuerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Certificate Authorities" },
                    TrustedPeerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Applications" },
                    RejectedCertificateStore = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\RejectedCertificates" },
                    AutoAcceptUntrustedCertificates = true,
                    AddAppCertToTrustedStore = true,
                    SendCertificateChain = false,
                    RejectSHA1SignedCertificates = true,


                },
                TransportConfigurations = new TransportConfigurationCollection { },
                TransportQuotas = new TransportQuotas { OperationTimeout = 10000 },
                ClientConfiguration = new ClientConfiguration { DefaultSessionTimeout = 10000, },
                TraceConfiguration = new TraceConfiguration()
            };
            _config.Validate(ApplicationType.Client).GetAwaiter().GetResult();
            if (_config.SecurityConfiguration.AutoAcceptUntrustedCertificates)
                _config.CertificateValidator.CertificateValidation += (s, e) => {
                    e.Accept = (e.Error.StatusCode == StatusCodes.BadCertificateUntrusted);
                };

            _application = new ApplicationInstance
            {
                ApplicationName = "AreaMESLight",
                ApplicationType = ApplicationType.Client,
                ApplicationConfiguration = _config,
            };
            //_application.CheckApplicationInstanceCertificate(false, 2048).GetAwaiter().GetResult();
            #endregion

            _timerTryToConnect = new Timer((_) => {
                try
                {
                    lock (_lock_timerTryToConnect)
                    {
                        if (_isOn_timerTryToConnect) return;
                        _isOn_timerTryToConnect = true;
                        if (_isConnected) return;
                    }

                    _isConnected = false;
                    TryToConnect();
                }
                catch (Exception exc)
                {
                    // errore di connessione
                    string s = exc.Message;
                }
                finally
                {
                    lock (_lock_timerTryToConnect)
                        _isOn_timerTryToConnect = false;
                }
            }, null, 1000, 6000);

            return _sessionOpcUaClient;
        }

        private void TryToConnect()
        {
            //select start endpoint 
            var endpointDescription = CoreClientUtils.SelectEndpoint(_endpoint, useSecurity: false, operationTimeout: 5000);

            UserIdentity user = new UserIdentity(new AnonymousIdentityToken());
            if (!String.IsNullOrWhiteSpace(_username))
                user = new UserIdentity(_username, _password);

            var endpointConfiguration = EndpointConfiguration.Create(_config);
            var configuredEndpoint = new ConfiguredEndpoint(null, endpointDescription, endpointConfiguration);


            //create a new session from server 
            // _sessionOpcUaClient = Session.Create(_config, configuredEndpoint, true, "test", 60000, user, null).GetAwaiter().GetResult();
            _sessionOpcUaClient = Session.Create(_config, configuredEndpoint, true, "", 60000, user, null).GetAwaiter().GetResult();
            {
               
                //Logger.Default.Info("Step 3 - Browse the server namespace.");
                //ReferenceDescriptionCollection refs;
                //refs = _sessionOpcUaClient.FetchReferences(ObjectIds.ObjectsFolder);
            }
            _sessionOpcUaClient.KeepAliveInterval = 2000;
            _sessionOpcUaClient.KeepAlive += _sessionOpcUaClient_KeepAlive;
            _sessionOpcUaClient.SessionClosing += _sessionOpcUaClient_SessionClosing;


            lock (_lock_timerTryToConnect)
                _isConnected = true;

            Thread.Sleep(5000);
            ToSubscribe(_items);
        }

        private void _sessionOpcUaClient_SessionClosing(object sender, EventArgs e)
        {
            Console.WriteLine("SessionClosing");
        }

        private void _sessionOpcUaClient_KeepAlive(Session session, KeepAliveEventArgs e)
        {
            lock (_lock_timerTryToConnect)
                if (_isConnected == false)
                    return;

            var a = e.CurrentState;
            //Console.WriteLine(e.CurrentState + ";" + ServiceResult.IsGood(e.Status));
            //Console.WriteLine(e.CurrentState);

            if (e.CurrentState.ToString() != "Running") { state++; }

            if (state >= 5)
            {
                state = 0;
                _sessionOpcUaClient.Close();
                //_sessionOpcUaClient.Reconnect();
                lock (_lock_timerTryToConnect)
                    _isConnected = false;
            }

        }


        private Subscription ToSubscribe(OpcUaNodeEntryCollection variables)
        {
            _subscription = new Subscription(_sessionOpcUaClient.DefaultSubscription) { PublishingInterval = 1000 };

            var list = new List<MonitoredItem>();

            foreach (var item in variables)
            {
                var nameFixed =  "MEP_QEM."+item.Name;
                var mi = new MonitoredItem()
                {
                    DisplayName = item.Name,
                    StartNodeId = new NodeId(nameFixed, 2),// new NodeId(item.Node.Identifier.ToString()), // new NodeId(String.Format(_baseFolder+"/{0}",item.Name), 3), //new NodeId(Convert.ToUInt32(item.ValueVariantId), 2),
                    AttributeId = Attributes.Value,
                    MonitoringMode = MonitoringMode.Reporting,
                    SamplingInterval = 250,
                    QueueSize = 1,
                };
                mi.Notification += OnNotification;
                _subscription.AddItem(mi);
            }

            _sessionOpcUaClient.AddSubscription(_subscription);
            _subscription.Create();

            return _subscription;
        }//close read

        private void OnNotification(MonitoredItem item, MonitoredItemNotificationEventArgs e)
        {
            MonitoredItem items;
            items = item;
            var messageBag = new List<SystemVariablesDefinitionValue>();

            foreach (var value in item.DequeueValues())
            {
                var systemName = SystemVariables.GetMatch("QEM", item.DisplayName);
                messageBag.Add(new SystemVariablesDefinitionValue(systemName, value.Value));
                //File.AppendAllText("C:\\testQem.txt", String.Format("{0}={1}" + Environment.NewLine, systemName.Name , value.Value));
                SystemVariablesDataContainer.Instance.Fill(messageBag, _deviceId, this.SerialNumber);
            }

            
            

            // SystemVariablesDataContainer.Instance.Fill(messageBag, _deviceId);

        }//close notification


        public void Write(string name, object value, ILog logger)
        {
           
        }

    
        public OpcUaNodeEntryCollection AddVariableQem()
        {
            var variableOPC = new OpcUaNodeEntryCollection();

            SystemVariables.AddDriver(driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "swOre" }, SystemVariables.TempoTaglioSingoloH, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "sbMinuti" }, SystemVariables.TempoTaglioSingoloM, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "sbSecondi" }, SystemVariables.TempoTaglioSingoloS, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "slOreTR" }, SystemVariables.TempoTotaleTagliH, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "sbMinutiTR" }, SystemVariables.TempoTotaleTagliM, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "slOreT" }, SystemVariables.TempoVitaMacchinaH, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "sbMinutiT" }, SystemVariables.TempoVitaMacchinaM, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "glVelDiscY" }, SystemVariables.FeedRateSetPointmm, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "VelYVis" }, SystemVariables.FeedRateAttuale, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "AccValEncS" }, SystemVariables.BladeSpeedSetPointm, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "VisCampoA" }, SystemVariables.CorrenteAssorbita, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "agbAllAux" }, SystemVariables.AllarmiMacchina1, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "swNumeTgSA" }, SystemVariables.TagliProgrammatiSemiautomatico, driverName);

            SystemVariables.Match(new OpaUaNodeEntry { Name = "NumTaEseCicl" }, SystemVariables.TagliEseguitiSemiautomatico, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "MisPrgSing" }, SystemVariables.TaglioSingoloMisura, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "Lama.outUmf" }, SystemVariables.BladeSpeedAttuale, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "NrTagliPrgSing" }, SystemVariables.TaglioSingoloProgrammati, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "NumTaEseCiclo[TipoCiclo]" }, SystemVariables.TaglioSingoloEseguiti, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "MisProgCiclo [TipoCiclo]" }, SystemVariables.MisuraProgrammata, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "NumTaProgCic [TipoCiclo]" }, SystemVariables.PezziProdurre, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "NumTaEseCicl [TipoCiclo]" }, SystemVariables.PezziProdotti, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "PROGRAM.AuxCodProg" }, SystemVariables.PartNumber, driverName);


            SystemVariables.Match(new OpaUaNodeEntry { Name = "VisProg" }, SystemVariables.Programma, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "gwNrLoopPrg" }, SystemVariables.LoopProgrammati, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "NrLoopEseg" }, SystemVariables.LoopEseguiti, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "glPosLinH" }, SystemVariables.ParametroX, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "PosAsseYVis" }, SystemVariables.ParametroY, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "VarOutTesLa" }, SystemVariables.TesaturaLama, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "gbDevLama" }, SystemVariables.DeviazioneLama, driverName);


            SystemVariables.Match(new OpaUaNodeEntry { Name = "AspTotale" }, SystemVariables.AsportazioneTotale, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "posMorsaTa" }, SystemVariables.PosizioneMorsaTaglio, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "PosMorsaAl" }, SystemVariables.PosizioneMorsaAlimentatore, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "PRESSOST.PressBildaVisualizzare" }, SystemVariables.PressBilanciamentoTesta, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "PAG_OP_1.AngAttualeGr" }, SystemVariables.AngoloRotazioneGradi, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "PAG_OP_1.AngAttualePr" }, SystemVariables.AngoloRotazionePrimi, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "LarghPzoRot" }, SystemVariables.LarghezzaPezzo, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "VersA, VersB, VersC, VersD" }, SystemVariables.VersioneSoftwarePLC, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "PLC.frwversion, PLC.frwmajrel, PLC.frwminrel" }, SystemVariables.VersioneFirmware, driverName);


            foreach (var item in SystemVariables.GetMatches(driverName))
                variableOPC.Add(item.Key);
            

            return variableOPC;
        }


        public void Close()
        {
            this._sessionOpcUaClient.RemoveSubscription(_subscription);
            this._sessionOpcUaClient.Close();
            this._sessionOpcUaClient.Dispose();
        }
    }

    

}
