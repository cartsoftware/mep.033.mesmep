﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMES.BatchLogic.Mep
{
    public class OpaUaNodeEntry
    {
        public SystemVariablesDefinition SystemVariable { get; set; }
        public string Name { get; set; }

        public int ValueVariantId { get; set; }

        public int? LockedId { get; set; }

        public OpcUaVariantValueType Type { get; set; }

    }

    public enum OpcUaVariantValueType
    {
        Int32,
        UInt32,
        Double,
        Boolean
    }

    public class OpcUaNodeEntryCollection : List<OpaUaNodeEntry>
    {

    }
}
