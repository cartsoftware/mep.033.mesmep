﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Server.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AreaMES.BatchLogic.Mep
{
    public class SupervisorBatch : SupervisorBatchLogicBase
    {

        private static Dictionary<string, IOpcUaClient> _opUaClients = new Dictionary<string, IOpcUaClient>();

        public static IOpcUaClient GetOpcUaClient(string machineId)
        {
            lock (_opUaClients)
                return _opUaClients[machineId];
            
        }

        public override void Init() {
            // vengono prelevate tutte le macchine dalla persistenza
            var macchine = Repository.All<Macchina>().Result.FindAll(o => o.IsDisable == false);

            // vengono creati gli n-client
            foreach (var item in macchine)
                lock (_opUaClients)
                    AddOpcUaClient(item.Id);
            

        }


        /// <summary>
        /// Metodo per avviare il client OPCUa per una macchina le cui informaizoni sono passate come parametro
        /// </summary>
        /// <param name="machineInfo"></param>
        public void AddOpcUaClient(string macchinaId)
        {
            // viene verificata se la chiave esiste nel dizionario, in caso affermativo viene chiuso 
            // il client OPC-UA e viene rimosso l'oggetto dal dizionario
            RemoveOpcUaClient(macchinaId);

            // viene prelevato l'oggetto completo dalla persistenza
            var macchina = Repository.Get<Macchina>(macchinaId);
            Resolve(macchina.Tipologia, Repository);

            // creazione del driver in base alla tipologia della macchina
            IOpcUaClient client;
            if (macchina.Tipologia.Item.Driver.Contains("QEM") == true)
                client = new OpcUaClientQem(macchinaId, "opc.tcp://" + macchina.IndirizzoIp + ":4840");
            else
                client = new OpcUaClientIsac(macchinaId, "opc.tcp://" + macchina.IndirizzoIp + ":4840");


            client.SerialNumber = macchina.SerialNumber;

            // viene aggiunto l'elemento al dizionario
            client.OnClientStatusChanged += (id, status) => {
                MessageServices.Instance.Publish(StatoMacchinaChangedMessage.Name, new StatoMacchinaChangedMessage { Data = status, MacchinaId = id });
            };        

            _opUaClients.Add(macchina.Id, client);
            // viene avviata la procedura di connessione
            client.Connect();
        }


        /// <summary>
        /// Metodo per rimuovere il client OpcUa dalla lista, chiudendo la relativa sessione
        /// </summary>
        /// <param name="macchinaId"></param>
        public void RemoveOpcUaClient(string macchinaId)
        {
            // viene verificata se la chiave esiste nel dizionario, in caso affermativo viene chiuso 
            // il client OPC-UA e viene rimosso l'oggetto dal dizionario
            IOpcUaClient client;
            if (_opUaClients.TryGetValue(macchinaId, out client))
            {
                client.OnClientStatusChanged = null;
                client.Close();
                _opUaClients.Remove(macchinaId);
            }
        }



        public  void Resolve<T>( ExternalDoc<T> item, IRepository repository) where T : IDoc, new()
        {
            if (!String.IsNullOrWhiteSpace(item.Id))
                item.Item = repository.GetAsync<T>(item.Id).Result;
        }
    }
}
