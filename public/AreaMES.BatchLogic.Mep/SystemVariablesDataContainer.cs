﻿using AreaM2M.RealTime.Dto;
using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Server.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMES.BatchLogic.Mep
{
    public class SystemVariablesDataContainer : IDataContainer
    {
        public ConcurrentDictionary<string, DeviceItems> Data { get; private set; }
        private object _lock = new object();

        private static SystemVariablesDataContainer instance;
        public static SystemVariablesDataContainer Instance { get { return instance; } }

        static SystemVariablesDataContainer()
        {
            instance = new SystemVariablesDataContainer();
        }

        public SystemVariablesDataContainer()
        {
            Data = new ConcurrentDictionary<string, DeviceItems>();
        }

        public void Create(List<SystemVariablesDefinition> values, string deviceId)
        {
            lock (_lock)
            {
                var di = new DeviceItems();

                if (Data.ContainsKey(deviceId))
                    Data.TryRemove(deviceId, out di);

                di.Items = new Dictionary<string, DataItem>();
                di.Device = new Device { Code = deviceId };
                DateTime dt = DateTime.Now;
                foreach (var obj in values)
                {
                    di.Items.Add(obj.Name, new DataItem
                    {
                        Name = obj.Name,
                        Value = null,
                        OldValue = null,
                        ChangeAt = dt,
                        IsChanged = false
                    });
                }


                Data.TryAdd(deviceId, di);
            }
        }

        public void Fill(List<SystemVariablesDefinitionValue> item, string deviceId, string serialNumber)
        {
            lock (_lock)
            {
                DeviceItems deviceItem = null;
                if (Data.TryGetValue(deviceId, out deviceItem))
                {
                    var message = new DeviceValueChangedMessage();
                    message.DeviceSerialNumber = serialNumber;
                    message.DeviceId = deviceId;

                    var messageAllarm = new DeviceValueChangedMessageAlarm();
                    messageAllarm.DeviceSerialNumber = serialNumber;
                    messageAllarm.DeviceId = deviceId;


                    foreach (var tr in item)
                    {
                        DataItem mem = null;

                        deviceItem.Items.TryGetValue(tr.SystemVariable.Name, out mem); //.FirstOrDefault(x => Convert.ToString(x.Registry.Path) == Convert.ToString(tr.Path));
                        if (mem != null)
                        {
                            mem.OldValue = mem.Value;
                            mem.Value = tr.Value;
                            mem.IsAllarm = tr.SystemVariable.IsAllarm;
                            mem.IsChanged = Convert.ToString(mem.OldValue) != Convert.ToString(mem.Value);

                            if (mem.IsChanged ) 
                                if ( tr.SystemVariable.Delta == 0)
                                {
                                    mem.ChangeAt = DateTime.Now;
                                    if ((tr.SystemVariable.IsAllarm) && (Convert.ToBoolean(mem.Value) == true))
                                        messageAllarm.Add(mem);
                                    message.Add(mem);
                                    //File.AppendAllText("C:\\test.txt", String.Format("{0}={1}" + Environment.NewLine, mem.Name, mem.Value));

                                }
                            //var diffValue = oldNumber - newNumber
                            //    var percDiff = (diffValue / oldNumber) * 100
                            //    if (percDiff > mem.System.Delta) ….
                                else 
                                {
                                    if (mem.OldValue == null) mem.OldValue = 0;
                                     var dOldValue = Convert.ToDouble(mem.OldValue);
                                    var dNewValue = Convert.ToDouble(mem.Value);

                                    var diffValue = dOldValue - dNewValue;
                                    var percDiff = (Math.Abs(diffValue) / dOldValue) * 100;
                                    if(percDiff > tr.SystemVariable.Delta) { 

                                    //var perc = dOldValue * (tr.SystemVariable.Delta/100);
                                    //if (dNewValue > (dOldValue + perc) || dNewValue < (dOldValue - perc))
                                    //{
                                        mem.ChangeAt = DateTime.Now;
                                        message.Add(mem);
                                        File.AppendAllText("C:\\test.txt", String.Format("{0}={1}, old={2}, perc={3}" + Environment.NewLine, mem.Name, dNewValue, dOldValue, percDiff));
                                    }
                                }

                            //if(mem.IsChanged && (mem.Name == "TesaturaLama") && mem.OldValue!=null)
                            //{
                            //    Double perc =Convert.ToInt32(mem.OldValue) * 0.02;
                            //    if(Convert.ToInt32(mem.Value)> (Convert.ToInt32(mem.OldValue)+perc)|| Convert.ToInt32(mem.Value) < (Convert.ToInt32(mem.OldValue)- perc))
                            //        {
                            //        mem.ChangeAt = DateTime.Now;
                            //        message.Add(mem);
                            //    }
                            //}

                            //else if (mem.IsChanged)
                            //{
                            //    mem.ChangeAt = DateTime.Now;
                            //    message.Add(mem);
                            //    //File.AppendAllText("C:\\test.txt", String.Format("{0}={1}" + Environment.NewLine, mem.Name, mem.Value));
                            //}
                        }

                    }
                    if (message.Any())
                        MessageServices.Instance.Publish(DeviceValueChangedMessage.Name, message);

                    if (messageAllarm.Any())
                        MessageServices.Instance.Publish(DeviceValueChangedMessageAlarm.Name, messageAllarm);

                }
            }
        }

        public Dictionary<string, DeviceItems> Get()
        {
            return Data.ToDictionary(o => o.Key, y => y.Value);
        }

        public DeviceItems Get(string pDevice)
        {
            return Data[pDevice];
        }
    }


    public class SystemVariablesDefinitionValue
    {
        public SystemVariablesDefinitionValue(SystemVariablesDefinition systemVariable, object value)
        {
            SystemVariable = systemVariable;
            Value = value;
        }

        public SystemVariablesDefinition SystemVariable { get; set; }

        public object Value { get; set; }
    }


}
