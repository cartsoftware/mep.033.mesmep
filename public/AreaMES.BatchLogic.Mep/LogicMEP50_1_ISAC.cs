﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Runtime;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using AreaMes.Server.Utils;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using static AreaMES.BatchLogic.Mep.Logic;

namespace AreaMES.BatchLogic.Mep
{
    public class LogicMEP50_1_ISAC : ILogic
    {
        public Macchina Machine { get; set; }
        public string DeviceDriver { get; set; }
        public string DeviceId { get; set; }
        public string DeviceSerialNumber { get; set; }

        private object _perc_lock = new object();

        public static object _lock = new object();

        public void OnDeviceValueChangedMessage(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, DeviceValueChangedMessage message)
        {
            //acquisisco le variabili con un messaggio quando cambiano
            var MMI_FILE_QUEUE_ACCEPTED = message.FirstOrDefault(o => o.Name == SystemVariables.LavorazioneAccettata.Name);
            var MMI_FILE_QUEUE_REFUSED = message.FirstOrDefault(o => o.Name == SystemVariables.LavorazioneRiufiutata.Name);
            var ST_ACT_CUT_QUEUE_DONE = message.FirstOrDefault(o => o.Name == SystemVariables.PezziProdotti.Name);

            //acquisisco le variabili dalla cache interna 
            var ST_QUEUE_PART_NUMBER = deviceItems.Items[SystemVariables.PartNumber.Name];
            var STATO_MACCHINA = deviceItems.Items[SystemVariables.StatoMacchina.Name];

            // -------------------------------------------------------------------------------------------
            //dato un numero calcolo il currentStatus della  macchina 
            var currentStatus = GetStatus(STATO_MACCHINA);

            // -------------------------------------------------------------------------------------------
            // Fase di SETUP,  accettata o rifiutata
            if (fMEP_00.Stato == AreaMes.Model.Enums.StatoFasi.InLavorazione)
            {
                var OPC_QUEUEXML_RELOAD = message.FirstOrDefault(o => o.Name == SystemVariables.RiletturaQueuexml.Name);

                if ((MMI_FILE_QUEUE_ACCEPTED != null) && (Convert.ToBoolean(MMI_FILE_QUEUE_ACCEPTED.Value) == true))
                {
                    logger.InfoFormat("Variable {0}  = '{1}'", "MMI_FILE_QUEUE_ACCEPTED", true);
                    logger.InfoFormat("Set (before) {0}.locked = '{1}'", "OPC_QUEUEXML_BUSY", false);
                    var d = SupervisorBatch.GetOpcUaClient(DeviceId) as OpcUaClientIsac;
                    d.Write("mainTsk$OPC_QUEUEXML_BUSY", false, logger);

                    fMEP_00.Stato = AreaMes.Model.Enums.StatoFasi.Terminato;
                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_00 });
                    runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
                    //dopo aver caricato  una nuova lavorazione scarico tutte le lavorazioni della macchina e le metto in pausa
                    var listBatch = runtimeContext.GetSfo("InLavorazione", "").FindAll(o => o.DistintaBase.Fasi[0].Macchine[0].Macchina.Id == batch.DistintaBase.Fasi[0].Macchine[0].Macchina.Id);
                    int i = 0;
                    for (i = 0; i < listBatch.Count(); i++)
                    {
                        if (listBatch[i].Id != batch.Id)
                        {
                            listBatch[i].DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_PZ_PROD").ValoreDefault = listBatch[i].PezziProdotti;
                            fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.InAttesa;
                            runtimeContext.SetFase(new SetFaseInfo { idBatch = listBatch[i].Id, fase = fMEP_LAV });
                            fMEP_00.Stato = AreaMes.Model.Enums.StatoFasi.Terminato;
                            runtimeContext.SetFase(new SetFaseInfo { idBatch = listBatch[i].Id, fase = fMEP_00 });
                            runtimeContext.SetBatch(new SetBatchInfo { idBatch = listBatch[i].Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazioneInPausa });
                        }
                    }

                    if (batch.QueueId != null)
                    {
                        var listAllBatch = runtimeContext.GetSfo("-1", "sa").FindAll(o => o.DistintaBase.Fasi[0].Macchine[0].Macchina.Id == batch.DistintaBase.Fasi[0].Macchine[0].Macchina.Id);
                        listAllBatch.FindAll(x => x.Stato != AreaMes.Model.Enums.StatoBatch.Terminato && x.Stato != AreaMes.Model.Enums.StatoBatch.TerminatoAbortito);
                        var batchqueue = listAllBatch.FindAll(x => x.QueueId.Id == batch.QueueId.Id);
                        foreach (var item in batchqueue)
                        {
                            fMEP_00.Stato = AreaMes.Model.Enums.StatoFasi.Terminato;
                            runtimeContext.SetFase(new SetFaseInfo { idBatch = item.Id, fase = fMEP_00 });
                            runtimeContext.SetBatch(new SetBatchInfo { idBatch = item.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
                        }
                    }


                }

                if ((MMI_FILE_QUEUE_REFUSED != null) && (Convert.ToBoolean(MMI_FILE_QUEUE_REFUSED.Value) == true))
                {
                    logger.InfoFormat("Variable {0}  = '{1}'", "MMI_FILE_QUEUE_REFUSED", true);
                    logger.InfoFormat("Set (before) {0}.locked = '{1}'", "OPC_QUEUEXML_BUSY", false);

                    var d = SupervisorBatch.GetOpcUaClient(DeviceId) as OpcUaClientIsac;
                    d.Write("mainTsk$OPC_QUEUEXML_BUSY", false, logger);

                    fMEP_00.Stato = AreaMes.Model.Enums.StatoFasi.InLavorazioneInPausa;
                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_00 });
                    fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.InLavorazioneInPausa;
                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                }
            }
            // -------------------------------------------------------------------------------------------
            // Fase di SETUP completata in automatico
            //if (fMEP_00.Stato == AreaMes.Model.Enums.StatoFasi.Terminato)
            //{

            //    if ((currentStatus != null) &&
            //        (currentStatus.CicloMacchinaInCorso) &&
            //        (!(BatchUtils.IsInLavorazione(fMEP_LAV))))
            //    {
            //        // Risulta completata la fase di setup macchina
            //        batch.PezziProdotti = 0;
            //    }
            //}
            // --------------------------------------------------------------------------------------------

            if (batch.Stato == AreaMes.Model.Enums.StatoBatch.InLavorazioneInPausa) return;
            //se il partnumber non coincide con il odp non modifico nnt 
            if (Convert.ToInt32(batch.OdP) == Convert.ToInt32(ST_QUEUE_PART_NUMBER.Value) && (batch.Stato != AreaMes.Model.Enums.StatoBatch.Terminato))
            {
                // -------------------------------------------------------------------------------------------
                // Gestione Allarmi  Pausa e Lavorazione Tramite il current Statur 
                if (currentStatus != null)
                {
                    //se la macchina è in allarme metto la fase in pausa e il batch in allarme 
                    if (currentStatus.MacchinaInAllarme == true)
                    {
                        runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazioneInEmergenza });
                        fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.InPausa;
                        runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                    }

                    //nel momento in cui l'allarme viene tolto riporto il batch in lavorazione  
                    else if (batch.Stato == AreaMes.Model.Enums.StatoBatch.InLavorazioneInEmergenza)
                        runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });

                    //scatta Hold metto la fase in pausa 
                    else if (currentStatus.CicloMacchinaInHold == true)
                    {
                        fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.InLavorazioneInPausa;
                        runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                        runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
                    }

                    //riporto lo stato in lavorazione dopo la pausa
                    else if ((batch.Stato == AreaMes.Model.Enums.StatoBatch.InLavorazioneInPausa) && currentStatus.CicloMacchinaInCorso == true)
                    {
                        fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.InLavorazione;
                        runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                        runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
                    }
                    // macchina in lavorazione 
                    else if (currentStatus.CicloMacchinaInCorso == true && batch.Stato != AreaMes.Model.Enums.StatoBatch.Terminato)
                    {
                        fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.InLavorazione;
                        runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                        runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
                    }
                }

                // ----------------------------------------------------------------------------------------------
                // Fase di LAVORAZIONE
                if (BatchUtils.IsInLavorazione(fMEP_LAV))
                {
                    if (ST_QUEUE_PART_NUMBER == null) return;
                    if (Convert.ToString(ST_QUEUE_PART_NUMBER.Value) != Convert.ToString(batch.OdPParziale))
                        return;

                    if (ST_ACT_CUT_QUEUE_DONE != null)
                    {
                        try
                        {
                            lock (_lock)
                            {
                                logger.InfoFormat("Batch: {0}, Start calcoli PRODUZIONE", batch.OdP);


                                var PezziProdotti = Convert.ToInt32(batch.DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_PZ_PROD").ValoreDefault);
                                // log
                                logger.InfoFormat("Batch: {0}, Pezzi prodotti(appoggio): {1}, Pezzi prodotti attuali: {2}", batch.OdP, PezziProdotti, batch.PezziProdotti);
                                var Pezzimacchina = Convert.ToInt32(ST_ACT_CUT_QUEUE_DONE.Value);
                                // log
                                logger.InfoFormat("Batch: {0}, Pezzi Coda da macchina: {1}", batch.OdP, Pezzimacchina);
                                batch.PezziProdotti = Pezzimacchina + PezziProdotti;
                                logger.InfoFormat("Batch: {0}, Pezzi prodotti aggiornati: {1}", batch.OdP, batch.PezziProdotti);

                                try
                                {
                                    logger.InfoFormat("Batch: {0}, Start calcoli OEE", batch.OdP);

                                    // viene effettuato il calcolo della percentuale di produzione e OEE
                                    int current = batch.PercCompletamento;
                                    double d0 = batch.PezziDaProdurre > 0 ? ((double)batch.PezziProdotti / (double)batch.PezziDaProdurre) : 0;
                                    batch.PercCompletamento = (d0 <= 1) ? Convert.ToInt32(d0 * 100) : 100;
                                    batch.OEE = BatchUtils.CalculateRuntimeEfficiency(batch);

                                    var mm = Convert.ToInt32(deviceItems.Items[SystemVariables.TempoTaglioSingoloM.Name].Value);
                                    var ss = Convert.ToInt32(deviceItems.Items[SystemVariables.TempoTaglioSingoloS.Name].Value);
                                    TimeSpan ts = new TimeSpan(00, mm, ss);
                                    TimeSpan tr = new TimeSpan(00, 00, 06);
                                    int pezziRimasti = batch.PezziDaProdurre - batch.PezziProdotti;
                                    DateTime tt = DateTime.Now;
                                    for (int i = 0; i < pezziRimasti; i++)
                                        tt = tt + ts + tr; // Addition with DateTime  

                                    batch.DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_TEMPO_STIMATO").ValoreDefault = tt.ToString("HH:mm:ss");

                                    logger.InfoFormat("Batch: {0}, Start calcoli OEE, completed!", batch.OdP);

                                }
                                catch (Exception exc)
                                {
                                    logger.ErrorFormat("Batch: {0}, Start calcoli OEE, on errore, cause: {1}", batch.OdP, exc.Message);
                                }


                                // Viene verificato se è necessario chiudere in automatico in batch
                                if (batch.PezziDaProdurre <= batch.PezziProdotti)
                                {
                                    logger.InfoFormat("Batch: {0}, PEZZI DA PRODURRE <= PEZZI PRODOTTI, start!", batch.OdP);
                                    fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.Terminato;
                                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                                    runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.Terminato });
                                    logger.InfoFormat("Batch: {0}, PEZZI DA PRODURRE <= PEZZI PRODOTTI, completed!", batch.OdP);
                                }

                                logger.InfoFormat("Batch: {0}, Start calcoli PRODUZIONE, completed!", batch.OdP);


                            }
                        }
                        catch (Exception exc)
                        {
                            // log
                            logger.ErrorFormat("Batch: {0}, Start calcoli PRODUZIONE, on errore, cause:{1}", batch.OdP, exc.Message);
                         
                        }
                    }


                }
            }
        }

        public void OnFaseChanged(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, FaseChangedInfo message)
        {
            try
            {
                var d = SupervisorBatch.GetOpcUaClient(DeviceId) as OpcUaClientIsac;


                if (message.Current.CodiceFase == "MEP-00")
                {

                    if (message.NoSetupQueue == true) return;

                    if (message.Current.Stato == AreaMes.Model.Enums.StatoFasi.InLavorazione)
                    {
                        string tempFileName = Path.Combine(Path.GetTempPath(), PurifyFileName(fMEP_00.Macchine.FirstOrDefault().Macchina.Codice) + ".xml");
                        logger.InfoFormat("Creazione file xml '{0}'", tempFileName);

                        var listOfBatch = new List<Batch>();

                        // TODO: se il batch appartiene ad una coda, allora prelevare tutti i batch della coda e creare il file xml
                        if (batch.QueueId == null)
                        {
                            listOfBatch.Add(batch);
                            // l'operatore ha richiesto la fase di setup macchina

                            var xmlDoc = CreateXmlDocument(listOfBatch);
                            xmlDoc.Save(tempFileName);
                            d.WriteFile(tempFileName, logger);
                        }
                        else
                        {
                            var allbatch = runtimeContext.GetSfo("-1", "sa");


                            // prelevare elenco batch, o da mongoDb, o in memory con il runtimeContext
                            foreach (var item in batch.QueueId.Item.Batch)
                            {

                                var selectbatch = allbatch.FirstOrDefault(x => x.Id == item.Id);
                                listOfBatch.Add(selectbatch);
                                selectbatch = null;
                            }

                            // l'operatore ha richiesto la fase di setup macchina

                            var xmlDoc = CreateXmlDocument(listOfBatch);
                            xmlDoc.Save(tempFileName);
                            d.WriteFile(tempFileName, logger);
                        }




                        // se il batch appartiene ad una coda, allora notificare il setup in lavorazione per tutti i batch cdella coda
                        foreach (var item in listOfBatch.Where(o => o.Id != batch.Id))
                        {
                            fMEP_00.Stato = AreaMes.Model.Enums.StatoFasi.InLavorazione;
                            runtimeContext.SetFase(new SetFaseInfo { idBatch = item.Id, fase = fMEP_00, NoSetupQueue = true });

                        }

                    }
                }

            }

            catch (Exception exc)
            {
                logger.Error(exc);
            }
        }

        //private List<Batch> GetAllBatchInQueue(string queueId)
        //{
        //    return ...;
        //}

        private MepStatus GetStatus(DataItem valuee)
        {
            if (valuee == null) return null;
            int value = Convert.ToInt32(valuee.Value);
            MepStatus s = new MepStatus();
            var binario = Convert.ToInt32(value);
            bool[] convertito = new bool[20];

            var i = 0;
            while (binario > 0)
            {
                convertito[i] = (binario % 2 != 0);
                i++;
                binario = binario / 2;
            }

            s.MacchinaInRun = convertito[0];
            s.MacchinaInAllarme = convertito[1];
            s.MacchinaInWarning = convertito[2];
            s.CicloMacchinaInHold = convertito[3];
            s.CicloMacchinaTerminato = convertito[4];
            s.CicloMacchinaInCorso = convertito[5];

            byte[] by = new byte[1];
            BitArray b = new BitArray(new bool[] { convertito[16], convertito[17], convertito[18], convertito[19] });
            b.CopyTo(by, 0);

            switch (Convert.ToInt16(by[0]))
            {
                case 1:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.Manuale;
                    break;
                case 2:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.SemiAutomaticoDinamico;
                    break;
                case 3:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.SemiAutomatico;
                    break;
                case 4:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.ProgrammaSingolo;
                    break;
                case 5:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.CodeDiProgrammi;
                    break;
            }//Close Switch

            return s;
        }

        private XmlDocument CreateXmlDocument(List<Batch> batch)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
            doc.AppendChild(docNode);

            XmlNode queueNode = doc.CreateElement("queue");
            doc.AppendChild(queueNode);

            XmlNode listNode = doc.CreateElement("list");
            queueNode.AppendChild(listNode);
            int batchCount = 0;
            if (batch.Count() < 7)
                batchCount = 7;
            else
                batchCount = batch.Count();

            for (int i = 1; i <= batchCount; i++)
            {
                Batch currentBatch = null;
                //if (batch.Count < i)
                //{
                try
                { currentBatch = batch[i - 1]; }
                catch (Exception exc)
                { }

                //}


                XmlNode itemNode = doc.CreateElement("item");
                listNode.AppendChild(itemNode);

                var catNode = doc.CreateElement("cat");
                var catPrg = doc.CreateElement("prg");
                var catPn = doc.CreateElement("pn");
                var catLen = doc.CreateElement("len");
                var catNpz = doc.CreateElement("npz");
                var catEnb = doc.CreateElement("enb");

                catNode.InnerText = String.Format("QUEUE{0}", i);
                catPrg.InnerText = String.Format("{0}", i);

                if (currentBatch != null)
                {
                    var pezziProdotti = Convert.ToInt32(currentBatch.DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_PZ_PROD").ValoreDefault);
                    var pezziRichiesti = Convert.ToInt32(currentBatch.PezziDaProdurre);
                    var PezziDaProdurre = (pezziRichiesti - pezziProdotti).ToString();
                    catPn.InnerText = currentBatch.OdPParziale;
                    catLen.InnerText = Convert.ToInt32(currentBatch.DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_SETUP_LUNGH").ValoreDefault).ToString();
                    // catNpz.InnerText = batch.PezziDaProdurre.ToString();
                    catNpz.InnerText = PezziDaProdurre.ToString();
                    catEnb.InnerText = "1";
                }
                else
                {
                    //tPn.InnerText = String.Empty;
                    catPn.InnerText = "0";
                    catLen.InnerText = "0";
                    catNpz.InnerText = "0";
                    catEnb.InnerText = "0";
                }

                itemNode.AppendChild(catNode);
                itemNode.AppendChild(catPrg);
                itemNode.AppendChild(catPn);
                itemNode.AppendChild(catLen);
                itemNode.AppendChild(catNpz);
                itemNode.AppendChild(catEnb);
            }

            return doc;
        }

        private string PurifyFileName(string originalFileName)
        {
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            foreach (char c in invalid)
                originalFileName = originalFileName.Replace(c.ToString(), "");


            return originalFileName;
        }

    }
}
