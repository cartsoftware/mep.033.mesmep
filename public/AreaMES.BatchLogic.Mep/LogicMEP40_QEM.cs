﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Runtime;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using static AreaMES.BatchLogic.Mep.Logic;

namespace AreaMES.BatchLogic.Mep
{
    public class LogicMEP40_QEM : ILogic
    {
        public Macchina Machine { get; set; }
        public string DeviceDriver { get; set; }
        public string DeviceId { get; set; }
        public string DeviceSerialNumber { get; set; }

        public static object _lock = new object();

        public void OnDeviceValueChangedMessage(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, DeviceValueChangedMessage message)
        {

        }
        public void OnFaseChanged(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, FaseChangedInfo message)
        {
            try
            {
                var d = SupervisorBatch.GetOpcUaClient(DeviceId) as OpcUaClientQem;

                if (message.Current.CodiceFase == "MEP-00")
                {
                    if (message.Current.Stato == AreaMes.Model.Enums.StatoFasi.InLavorazione)
                    {
                        // l'operatore ha richiesto la fase di setup macchina
                        int Npezzi = Convert.ToInt32(batch.PezziDaProdurre);
                        int LunghezzaPezzo = Convert.ToInt32(batch.DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_SETUP_LUNGH").ValoreDefault);
                        String OdpParziale= batch.OdPParziale;
                        //d.Write("nomeVariabile",)
                    }
                }

            }

            catch (Exception exc)
            {
                logger.Error(exc);
            }
        }

        private MepStatus GetStatus(DataItem valuee)
        {
            //var cacheItem = deviceItems.Items["mainTsk$ST_MAC_STATUS_DW"];
            //var cacheItem = deviceItems.Items["StatoMacchina"];

            //if (cacheItem == null) return null;
            if (valuee == null) return null;

            int value = Convert.ToInt32(valuee.Value);
            //int value = Convert.ToInt32(cacheItem.Value);
            MepStatus s = new MepStatus();
            var binario = Convert.ToInt32(value);
            bool[] convertito = new bool[20];

            var i = 0;
            while (binario > 0)
            {
                convertito[i] = (binario % 2 != 0);
                i++;
                binario = binario / 2;
            }

            s.MacchinaInRun = convertito[0];
            s.MacchinaInAllarme = convertito[1];
            s.MacchinaInWarning = convertito[2];
            s.CicloMacchinaInHold = convertito[3];
            s.CicloMacchinaTerminato = convertito[4];
            s.CicloMacchinaInCorso = convertito[5];

            byte[] by = new byte[1];
            BitArray b = new BitArray(new bool[] { convertito[16], convertito[17], convertito[18], convertito[19] });
            b.CopyTo(by, 0);

            switch (Convert.ToInt16(by[0]))
            {
                case 1:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.Manuale;
                    break;
                case 2:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.SemiAutomaticoDinamico;
                    break;
                case 3:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.SemiAutomatico;
                    break;
                case 4:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.ProgrammaSingolo;
                    break;
                case 5:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.CodeDiProgrammi;
                    break;
            }//Close Switch

            return s;
        }


    }
}
