﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Server.Utils;
using log4net;
using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace AreaMES.BatchLogic.Mep
{
    public class OpcUaClientIsac: IOpcUaClient
    {
        public static Dictionary<string, OpcUaClientIsac> Devices { get; } = new Dictionary<string, OpcUaClientIsac>();
        public Action<string, StatoMacchinaDriver> OnClientStatusChanged { get; set; }
        public string SerialNumber { get; set; }

        private string driverName = "ISAC";
        private string _baseFolder;
        private string _deviceId;
        private string _endpoint;
        private string _password;
        private string _username;
        private Session _sessionOpcUaClient;
        private Subscription _subscription;
        private List<string> _subscribeTo = new List<string>();
        private Opc.Ua.ApplicationConfiguration _config;
        private Opc.Ua.Configuration.ApplicationInstance _application;
        private Timer _timerTryToConnect;
        private object _lock_timerTryToConnect = new object();
        private bool _isOn_timerTryToConnect = false;
        private OpcUaNodeEntryCollection _items;
        private bool _isConnected = false;
        private object _lock_write = new object();
        private int state = 0;
        private ConcurrentDictionary<string, object> _cache = new ConcurrentDictionary<string, object>();


        private NodeId _nodeId_localVariables = null; //new NodeId(3838, 2); // = new NodeId(3940, 2);
        private NodeId _nodeId_update_call = null; //new NodeId(3840, 2); //= new NodeId(3942, 2);
        private NodeId _nodeId_Queue_file = null;
        private NodeId _nodeId_Queue_file_open = null;
        private NodeId _nodeId_Queue_file_write = null;
        private NodeId _nodeId_Queue_file_close = null;

        //--------------------------------------------------------------------------------------------------
        //MODIFICA PER ISAC DA RIMUOVERE!!!!!!
        //private int ISAC_KeepAliveInterval = 2000;
        //private int ISAC_SamplingInterval = 3000;
        private ILog logger = log4net.LogManager.GetLogger("OPC-UA");
        //--------------------------------------------------------------------------------------------------

            

        public OpcUaClientIsac(string deviceId, string endpointOpc)
        {
            logger.InfoFormat("{0}| Creating client ...", this._deviceId);
            _deviceId = deviceId;
            _endpoint = endpointOpc;
            _baseFolder = "";
            _username = "MEP_SPA";
            _password = "SerieAutomaticheMep";

           logger.InfoFormat("{0}| Creating client ... completed", this._deviceId);
        }


        public Session Connect()
        {
            //--------------------------------------------------------------------------------------------------
            //MODIFICA PER ISAC DA RIMUOVERE!!!!!!
            //try {
            //    var Path_ISAC_KeepAliveInterval = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ISAC_KeepAliveInterval.txt");
            //    var Path_ISAC_SamplingInterval = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ISAC_SamplingInterval.txt");
            //    if (File.Exists(Path_ISAC_KeepAliveInterval) && File.Exists(Path_ISAC_SamplingInterval))
            //    {
            //        StreamReader fileRead__KeepAliveInterval = File.OpenText(Path_ISAC_KeepAliveInterval);
            //        StreamReader fileRead__SamplingInterval = File.OpenText(Path_ISAC_SamplingInterval);
            //        ISAC_KeepAliveInterval = Convert.ToInt32(fileRead__KeepAliveInterval.ReadLine());
            //        ISAC_SamplingInterval = Convert.ToInt32(fileRead__SamplingInterval.ReadLine());
            //    }

            //}
            //catch (Exception e)
            //{
            //    Logger.Error("Errore di Configurazione file ISAC {0}", e);
            //}
            //--------------------------------------------------------------------------------------------------

            logger.InfoFormat("{0}| Connecting...", this._deviceId);

            // viene notificato il cambio di stato
            OnClientStatusChanged?.BeginInvoke(this._deviceId, StatoMacchinaDriver.InConnessione, null, null);

            // vengono assegnati i sottoscrittori
            this.AddVariableIsac();

            SystemVariablesDataContainer.Instance.Create(SystemVariables.Items, _deviceId);

            #region Configurazione client  OPC-UA
            _config = new Opc.Ua.ApplicationConfiguration()
            {
                ApplicationName = "Opc",
                ApplicationUri = Utils.Format(@"urn:{0}:MyHome", System.Net.Dns.GetHostName()),
                ApplicationType = ApplicationType.Client,
                SecurityConfiguration = new SecurityConfiguration
                {
                    ApplicationCertificate = new CertificateIdentifier { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\MachineDefault", SubjectName = Utils.Format(@"CN={0}, DC={1}", "MyHomework", System.Net.Dns.GetHostName()) },
                    TrustedIssuerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Certificate Authorities" },
                    TrustedPeerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Applications" },
                    RejectedCertificateStore = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\RejectedCertificates" },
                    AutoAcceptUntrustedCertificates = true,
                    AddAppCertToTrustedStore = true,
                    SendCertificateChain = false,
                    RejectSHA1SignedCertificates = true,


                },
                TransportConfigurations = new TransportConfigurationCollection { },
                TransportQuotas = new TransportQuotas { OperationTimeout = 10000 },
                ClientConfiguration = new ClientConfiguration { DefaultSessionTimeout = 10000, },
                TraceConfiguration = new TraceConfiguration()
            };
            _config.Validate(ApplicationType.Client).GetAwaiter().GetResult();
            if (_config.SecurityConfiguration.AutoAcceptUntrustedCertificates)
                _config.CertificateValidator.CertificateValidation += (s, e) => {
                    e.Accept = (e.Error.StatusCode == StatusCodes.BadCertificateUntrusted);
                };

            _application = new ApplicationInstance
            {
                ApplicationName = "AreaMESLight",
                ApplicationType = ApplicationType.Client,
                ApplicationConfiguration = _config,
            };
            //_application.CheckApplicationInstanceCertificate(false, 2048).GetAwaiter().GetResult();
            #endregion

            _timerTryToConnect = new Timer((_) => {
                try
                {
                    lock (_lock_timerTryToConnect)
                    {
                        if (_isOn_timerTryToConnect) return;
                        _isOn_timerTryToConnect = true;
                        if (_isConnected) return;
                    }
                    _isConnected = false;
                    TryToConnect();
                }
                catch (Exception exc)
                {
                    // errore di connessione
                    string s = exc.Message;
                    logger.ErrorFormat("{0}| TryToConnect() on error, cause: {1}", this._deviceId, exc.Message);

                }
                finally
                {
                    lock (_lock_timerTryToConnect)
                        _isOn_timerTryToConnect = false;
                }
            }, null, 1000, 6000);

            return _sessionOpcUaClient;
        }

        private void TryToConnect()
        {
            logger.InfoFormat("{0}| TryToConnect() ...", this._deviceId);
         
            //select start endpoint 
            var endpointDescription = CoreClientUtils.SelectEndpoint(_endpoint, useSecurity: true, operationTimeout: 5000);

            UserIdentity user = new UserIdentity(new AnonymousIdentityToken());
            if (!String.IsNullOrWhiteSpace(_username))
                user = new UserIdentity(_username, _password);

            var endpointConfiguration = EndpointConfiguration.Create(_config);
            var configuredEndpoint = new ConfiguredEndpoint(null, endpointDescription, endpointConfiguration);


            //create a new session from server 
            _sessionOpcUaClient = Session.Create(_config, configuredEndpoint, true, "", 60000, user, null).GetAwaiter().GetResult();
            {
                //Logger.Default.Info("Step 3 - Browse the server namespace.");
                //ReferenceDescriptionCollection refs;
                //refs = _sessionOpcUaClient.FetchReferences(ObjectIds.ObjectsFolder);
            }
            _sessionOpcUaClient.KeepAliveInterval = 2000;
            _sessionOpcUaClient.KeepAlive += _sessionOpcUaClient_KeepAlive;
            _sessionOpcUaClient.SessionClosing += _sessionOpcUaClient_SessionClosing;

            logger.InfoFormat("{0}| TryToConnect(), sleep ...", this._deviceId);
            lock (_lock_timerTryToConnect)
                _isConnected = true;
            Thread.Sleep(5000);
            logger.InfoFormat("{0}| TryToConnect(), sleep ... completed", this._deviceId);


            // ---------------------------------------------------------------------------------
            ToSubscribe();


            // viene notificato il cambio di stato
            OnClientStatusChanged?.BeginInvoke(this._deviceId, StatoMacchinaDriver.Connesso, null, null);
        }



        private static void notificatest()
        {

            var message = new DeviceValueChangedMessage();
             message.DeviceId = "f63fa193-b5a1-4f3f-af7b-dfd9b03d0e22";
            message.DeviceSerialNumber = "000100020003005";

            var messageAllarm = new DeviceValueChangedMessageAlarm();
            messageAllarm.DeviceSerialNumber = "f63fa193-b5a1-4f3f-af7b-dfd9b03d0e22";
            messageAllarm.DeviceId = "f63fa193-b5a1-4f3f-af7b-dfd9b03d0e22";

            Random random = new Random();
            //var mem= new DataItem {  Value =  random.Next(0, 100),OldValue = 6, ChangeAt = DateTime.Now, IsChanged = true,Name="FeedRateAttuale"  };
            //message.Add(mem);

            //var mem1 = new DataItem { Value = random.Next(0, 100), OldValue = 6, ChangeAt = DateTime.Now, IsChanged = true, Name = "BladeSpeedAttuale"  };
            //message.Add(mem1);

            var mem2 = new DataItem { Value = random.Next(0, 100), OldValue = 6, ChangeAt = DateTime.Now, IsChanged = true, Name = "CorrenteAssorbita" };
            message.Add(mem2);

            //var mem3 = new DataItem { Value = random.Next(0, 100), OldValue = 6, ChangeAt = DateTime.Now, IsChanged = true, Name ="ParametroX" };
            //message.Add(mem3);

            //var mem4 = new DataItem { Value = random.Next(0, 100), OldValue = 6, ChangeAt=DateTime.Now, IsChanged = true, Name = "ParametroY"  };
            //message.Add(mem4);

            //if (Convert.ToInt32(mem4.Value)>80)
            //{
            var mem5 = new DataItem { Value = true, OldValue = false, ChangeAt = DateTime.Now, IsChanged = true, Name = "Allarme1_PremereReset" };
            messageAllarm.Add(mem5);

                 var mem6 = new DataItem { Value = true, OldValue = false, ChangeAt = DateTime.Now, IsChanged = true, Name = "Allarme6_FungoPremuto" };
            messageAllarm.Add(mem5);
            //}


            //else if(Convert.ToInt32(mem4.Value) <30)
            //{
            //    var mem5 = new DataItem { Value = true, OldValue = false, ChangeAt = DateTime.Now, IsChanged = true, Name = "Warning1_AbilitareMotoreLama" };
            //    message.Add(mem5);

           // }

            //else
            //{
            //    var mem5 = new DataItem { Value = false, OldValue = false, ChangeAt = DateTime.Now, IsChanged = true, Name = "Allarme1_PremereReset" };
            //    message.Add(mem5);

            //}
           

            MessageServices.Instance.Publish(DeviceValueChangedMessage.Name, message);
            //MessageServices.Instance.Publish(DeviceValueChangedMessageAlarm.Name, messageAllarm);
        }


        private void _sessionOpcUaClient_SessionClosing(object sender, EventArgs e)
        {
            // viene notificato il cambio di stato
            OnClientStatusChanged?.BeginInvoke(this._deviceId, StatoMacchinaDriver.Disconnesso, null, null);
        }

        private void _sessionOpcUaClient_KeepAlive(Session session, KeepAliveEventArgs e)
        {
            lock (_lock_timerTryToConnect)
                if (_isConnected == false)
                    return;

            var a = e.CurrentState;

            if (e.CurrentState.ToString() != "Running") { state++; }

            if (state >= 5)
            {
                state = 0;
                _sessionOpcUaClient.Close();
                // viene notificato il cambio di stato
                OnClientStatusChanged?.BeginInvoke(this._deviceId, StatoMacchinaDriver.InRiconnessione, null, null);
                lock (_lock_timerTryToConnect)
                    _isConnected = false;
            }

        }


        private Subscription ToSubscribe()
        {
            logger.InfoFormat("{0}| TryToConnect(), ToSubscribe() ...", this._deviceId);
            _subscription = new Subscription(_sessionOpcUaClient.DefaultSubscription) { PublishingInterval = 1000 };

            Browser browser = new Browser(_sessionOpcUaClient);
            NodeId rootId = Objects.ObjectsFolder;
            browser.IncludeSubtypes = true;
            browser.BrowseDirection = BrowseDirection.Forward;

            var resfwplchost = browser.Browse(rootId).FirstOrDefault(o => o.BrowseName.Name == "fwplchost");
            var resIFramework = browser.Browse(resfwplchost.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "IFramework");
            var resPLCModules = browser.Browse(resIFramework.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "PLCModules"); 
            var resiec = browser.Browse(resPLCModules.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "iec");
            var resFunctions = browser.Browse(resiec.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "Functions");
            var resmain = browser.Browse(resFunctions.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "main"); 
            var idLocalVariable = Convert.ToInt32(browser.Browse(resmain.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "LocalVariables").NodeId.Identifier);
           var resLocalVariables = browser.Browse(resmain.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "LocalVariables");
            var idCallUpdate = Convert.ToInt32(browser.Browse(resLocalVariables.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "Update").NodeId.Identifier);
            var list = new List<MonitoredItem>();



            foreach (var item in _items)
            {
                var res2 = browser.Browse(resLocalVariables.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == item.Name);
                if (res2 != null)
                {
                    var x = browser.Browse(res2.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "ValueVariant");
                    item.ValueVariantId = Convert.ToInt32(x.NodeId.Identifier);
                    item.LockedId = Convert.ToInt32(browser.Browse(res2.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "Locked").NodeId.Identifier);

                    var mi = new MonitoredItem()
                    {
                        DisplayName = item.Name,
                        StartNodeId = new NodeId(Convert.ToUInt32(item.ValueVariantId), 2),
                        AttributeId = Attributes.Value,
                        MonitoringMode = MonitoringMode.Reporting,
                        SamplingInterval = 3000,
                        QueueSize = 1,
                    };
                    mi.Notification += OnNotification;
                    _subscription.AddItem(mi);
                }
                else
                {
                    Console.WriteLine("Variable: " + item.Name + " Not Found");
                }
            }//close froeach


            var resFileSystem = browser.Browse(rootId).FirstOrDefault(o => o.BrowseName.Name == "FileSystem");
            var resusersettings = browser.Browse(resFileSystem.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "usersettings");
            var idfile = Convert.ToInt32(browser.Browse(resusersettings.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "QUEUE.XML").NodeId.Identifier);
            var resQUEUE = browser.Browse(resusersettings.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "QUEUE.XML");
            if (resQUEUE != null)
            {
                var idfileopen = Convert.ToInt32(browser.Browse(resQUEUE.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "Open").NodeId.Identifier);
                var idfileClose = Convert.ToInt32(browser.Browse(resQUEUE.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "Close").NodeId.Identifier);
                var idfileWrite = Convert.ToInt32(browser.Browse(resQUEUE.NodeId.ToString()).FirstOrDefault(o => o.BrowseName.Name == "Write").NodeId.Identifier);

                _nodeId_localVariables = new NodeId(Convert.ToUInt32(idLocalVariable), 2);
                _nodeId_update_call = new NodeId(Convert.ToUInt32(idCallUpdate), 2);
                _nodeId_Queue_file = new NodeId(Convert.ToUInt32(idfile), 2);
                _nodeId_Queue_file_open = new NodeId(Convert.ToUInt32(idfileopen), 2);
                _nodeId_Queue_file_write = new NodeId(Convert.ToUInt32(idfileWrite), 2);
                _nodeId_Queue_file_close = new NodeId(Convert.ToUInt32(idfileClose), 2);
            }


            //Step 6 - Add the subscription to the session
            //Logger.Default.Info("Step 6 - Add the subscription to the session.");
            _sessionOpcUaClient.AddSubscription(_subscription);
            try
            {
                _subscription.Create();
            }
               catch(Exception ex)
            {
                //_subscription = null;
                logger.ErrorFormat("{0}| TryToConnect(), ToSubscribe() ... on error, cause:{1}", this._deviceId, ex.Message);
                  //TryToConnect();
            }

            logger.InfoFormat("{0}| TryToConnect(), ToSubscribe() ... completed", this._deviceId);

            return _subscription;
        }//close read

        private void OnNotification(MonitoredItem item, MonitoredItemNotificationEventArgs e)
        {
            MonitoredItem items;
            items = item;
            var messageBag = new List<SystemVariablesDefinitionValue>();

            lock (this)
            {
               
                foreach (var value in item.DequeueValues())
                {
                    object obj = null;
                    if (_cache.TryGetValue(item.DisplayName, out obj))
                    {
                        if (Convert.ToString(obj) != Convert.ToString(value.Value))
                        {
                            logger.DebugFormat("{0}| OnNotification(), {1}={2}", this._deviceId, item.DisplayName, value.Value);

                            _cache[item.DisplayName] = value.Value;

                            //mainTsk$ST_BLD_I_CTRL_PV_Im = corrente assorbita, se <0 non ricevo il messaggio.
                            if ((item.DisplayName == "mainTsk$ST_BLD_I_CTRL_PV_Im") && (Convert.ToString(value?.Value).IndexOf('-')) > -1) return;

                            if (item.DisplayName == "mainTsk$ST_ALARM_DW0")
                            {
                                string allarmDW0a = IntToBinaryString(Convert.ToInt32(value.Value));
                                string fillLeft = new string('0', (32 - allarmDW0a.Length));
                                allarmDW0a = fillLeft + allarmDW0a;
                                var allarmDW0 = allarmDW0a.Reverse().ToList();
                                bool allarme1_PremereReset = (allarmDW0[0] == '1');
                                bool allarme2_TensioneLama = (allarmDW0[1] == '1');
                                bool allarme3_SovraccaricoMotore = (allarmDW0[2] == '1');
                                bool allarme4_Velocitains = (allarmDW0[3] == '1');
                                bool allarme5_PremereReset2 = (allarmDW0[4] == '1');
                                bool allarme6_FungoPremuto = (allarmDW0[5] == '1');
                                bool allarme7_Yblocco1 = (allarmDW0[6] == '1');
                                bool allarme8_Xblocco1 = (allarmDW0[7] == '1');
                                bool allarme9_Yblocco2 = (allarmDW0[8] == '1');
                                bool allarme10_Xblocco2 = (allarmDW0[9] == '1');
                                bool allarme11_LivelloOlioMin = (allarmDW0[10] == '1');
                                bool allarme12_CarterAperto = (allarmDW0[11] == '1');
                                bool allarme13_AlimentatoreBloccato = (allarmDW0[12] == '1');
                                bool allarme14_InverterBloccato = (allarmDW0[13] == '1');
                                bool allarme15_AlimBarra = (allarmDW0[14] == '1');
                                bool allarme16_FCTIFCTA = (allarmDW0[15] == '1');
                                bool allarme17_Parametri = (allarmDW0[16] == '1');
                                bool allarme18_Velocita = (allarmDW0[17] == '1');
                                bool allarme19_NessunProg = (allarmDW0[18] == '1');
                                bool allarme20_Scorta = (allarmDW0[19] == '1');
                                bool allarme21_FineBarra = (allarmDW0[20] == '1');
                                bool allarme22_SWLunghezza = (allarmDW0[21] == '1');
                                bool allarme23_SWAsseX = (allarmDW0[22] == '1');
                                bool allarme24_LamaRotta = (allarmDW0[23] == '1');
                                bool allarme25_ErroreCiclo = (allarmDW0[24] == '1');
                                bool allarme26_DevMax = (allarmDW0[25] == '1');
                                bool allarme27_DevMin = (allarmDW0[26] == '1');
                                bool allarme28_ProtezAperte = (allarmDW0[27] == '1');
                                bool allarme29_PressioneAria = (allarmDW0[28] == '1');
                                bool allarme30_ProtezAperte2 = (allarmDW0[29] == '1');
                                bool allarme31_BloccoTesta = (allarmDW0[30] == '1');
                                bool allarme32_Morse = (allarmDW0[31] == '1');
                                //bool Allarme33_AlimBloccato = (allarmDW1[0] == '1');
                                //bool Allarme34_Homing = (allarmDW1[1] == '1');
                                

                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme1_PremereReset, allarme1_PremereReset));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme2_TensioneLama, allarme2_TensioneLama));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme3_SovraccaricoMotore, allarme3_SovraccaricoMotore));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme4_Velocitains, allarme4_Velocitains));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme5_PremereReset2, allarme5_PremereReset2));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme6_FungoPremuto, allarme6_FungoPremuto));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme7_Yblocco1, allarme7_Yblocco1));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme8_Xblocco1, allarme8_Xblocco1));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme9_Yblocco2, allarme9_Yblocco2));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme10_Xblocco2, allarme10_Xblocco2));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme11_LivelloOlioMin, allarme11_LivelloOlioMin));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme12_CarterAperto, allarme12_CarterAperto));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme13_AlimentatoreBloccato, allarme13_AlimentatoreBloccato));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme14_InverterBloccato, allarme14_InverterBloccato));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme15_AlimBarra, allarme15_AlimBarra));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme16_FCTIFCTA, allarme16_FCTIFCTA));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme17_Parametri, allarme17_Parametri));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme18_Velocita, allarme18_Velocita));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme19_NessunProg, allarme19_NessunProg));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme20_Scorta, allarme20_Scorta));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme21_FineBarra, allarme21_FineBarra));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme22_SWLunghezza, allarme22_SWLunghezza));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme23_SWAsseX, allarme23_SWAsseX));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme24_LamaRotta, allarme24_LamaRotta));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme25_ErroreCiclo, allarme25_ErroreCiclo));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme26_DevMax, allarme26_DevMax));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme27_DevMin, allarme27_DevMin));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme28_ProtezAperte, allarme28_ProtezAperte));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme29_PressioneAria, allarme29_PressioneAria));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme30_ProtezAperte2, allarme30_ProtezAperte2));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme31_BloccoTesta, allarme31_BloccoTesta));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme32_Morse, allarme32_Morse));
                                //messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme33_AlimBloccato, allarme33_AlimBloccato));
                                //messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Allarme34_Homing, allarme34_Homing));
                                
                            }

                            else if (item.DisplayName == "mainTsk$ST_WARNING_DW0") 
                            {
                                string warningDW0 = IntToBinaryString(Convert.ToInt32(value.Value));
                                string fillLeft = new string('0', (32 - warningDW0.Length));
                                warningDW0 = fillLeft + warningDW0;
                                var WarningDW0 = warningDW0.Reverse().ToList();
                                bool warning1_AbilitareMotoreLama = (WarningDW0[0] == '1');
                                bool warning2_StartImpugnatura = (WarningDW0[1] == '1');
                                bool warning3_Scorta = (WarningDW0[2] == '1');
                                bool warning4_LivelloRubrificanteMin = (WarningDW0[3] == '1');
                                bool warning5_FctiFctaIncongruenti = (WarningDW0[4] == '1');
                                bool warning6_Scorta1 = (WarningDW0[5] == '1');
                                bool warning7_Scorta2 = (WarningDW0[6] == '1');
                                bool warning8_ClearPezzi = (WarningDW0[7] == '1');
                                bool warning9_VelocitaTestaAttiva = (WarningDW0[8] == '1');
                                bool warning10_EncoderFa0 = (WarningDW0[9] == '1');
                                bool warning11_TermicheScattate= (WarningDW0[10] == '1');
                                bool warning12_BloccoEvaquatoreTrucioli = (WarningDW0[11] == '1');
                                bool warning13_AsseXnonAzzarata = (WarningDW0[12] == '1');
                                bool warning14_AttesaVelocitaLama = (WarningDW0[13] == '1');
                                bool warning15_FineTagli = (WarningDW0[14] == '1');
                                bool warning16_FineTagli1 = (WarningDW0[15] == '1');
                                bool warning17_DevizioneLama = (WarningDW0[16] == '1');
                                bool warning18_AggiornamentoCoda = (WarningDW0[17] == '1');
                                bool warning19_ComandoInibito = (WarningDW0[18] == '1');
                                bool warning20_StartPedalieraAtt = (WarningDW0[19] == '1');
                                bool warning21_PedalieraPulsanteAtt = (WarningDW0[20] == '1');
                                bool warning22_StopPremereStart = (WarningDW0[21] == '1');
                                bool warning23_SingStopPremereStart = (WarningDW0[22] == '1');
                                bool warning24_PremereStart = (WarningDW0[23] == '1');
                                bool warning25_AsseXBlocco = (WarningDW0[24] == '1');
                                bool warning26_ScritturaFileQueue = (WarningDW0[25] == '1');
                                bool warning27_FileUsoMes = (WarningDW0[26] == '1');
                                bool warning28_RiavvioMacch= (WarningDW0[27] == '1');
                                bool warning29_ErrorCopy = (WarningDW0[28] == '1');
                                bool warning30_AbilitaLama = (WarningDW0[29] == '1');
                                bool warning31_Lunghezza = (WarningDW0[30] == '1');

                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning1_AbilitareMotoreLama, warning1_AbilitareMotoreLama));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning2_StartImpugnatura, warning2_StartImpugnatura));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning3_Scorta, warning3_Scorta));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning4_LivelloRubrificanteMin, warning4_LivelloRubrificanteMin));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning5_FctiFctaIncongruenti, warning5_FctiFctaIncongruenti));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning6_Scorta1, warning6_Scorta1));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning7_Scorta2, warning7_Scorta2));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning8_ClearPezzi, warning8_ClearPezzi));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning9_VelocitaTestaAttiva, warning9_VelocitaTestaAttiva));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning10_EncoderFa0, warning10_EncoderFa0));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning11_TermicheScattate, warning11_TermicheScattate));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning12_BloccoEvaquatoreTrucioli, warning12_BloccoEvaquatoreTrucioli));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning13_AsseXnonAzzarata, warning13_AsseXnonAzzarata));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning14_AttesaVelocitaLama, warning14_AttesaVelocitaLama));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning15_FineTagli, warning15_FineTagli));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning16_FineTagli1, warning16_FineTagli1));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning17_DevizioneLama, warning17_DevizioneLama));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning18_AggiornamentoCoda, warning18_AggiornamentoCoda));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning19_ComandoInibito, warning19_ComandoInibito));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning20_StartPedalieraAtt, warning20_StartPedalieraAtt));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning21_PedalieraPulsanteAtt, warning21_PedalieraPulsanteAtt));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning22_StopPremereStart, warning22_StopPremereStart));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning23_SingStopPremereStart, warning23_SingStopPremereStart));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning24_PremereStart, warning24_PremereStart));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning25_AsseXBlocco, warning25_AsseXBlocco));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning26_ScritturaFileQueue, warning26_ScritturaFileQueue));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning27_FileUsoMes, warning27_FileUsoMes));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning28_RiavvioMacch, warning28_RiavvioMacch));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning29_ErrorCopy, warning29_ErrorCopy));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning30_AbilitaLama, warning30_AbilitaLama));
                                messageBag.Add(new SystemVariablesDefinitionValue(SystemVariables.Warning31_Lunghezza, warning31_Lunghezza));


                            }
                            else
                            {
                                var systemName = SystemVariables.GetMatch(driverName, item.DisplayName);
                                messageBag.Add(new SystemVariablesDefinitionValue(systemName, value.Value));
                            }
                        }
                    }

                }
            }
         
                

            SystemVariablesDataContainer.Instance.Fill(messageBag, _deviceId, this.SerialNumber);

        }//close notification

        public void Write(string name, object value, ILog logger)
        {
            //scrivo sul server
            StatusCodeCollection results = null;
            DiagnosticInfoCollection diagnosticInfos = null;
            var nodesToWrite = new WriteValueCollection();

            lock (_lock_write)
            {
                if (!_isConnected) return;

                var item = this._items.FirstOrDefault(o => o.Name == name);

                //creo il nodo secondario da riempire
                var intWriteVal_locked = new WriteValue { NodeId = new NodeId(Convert.ToUInt32(item.LockedId), 2), AttributeId = Attributes.Value };
                intWriteVal_locked.Value.Value = true;

                //creo il nodo secondario da riempire
                // var intWriteVal = new WriteValue { NodeId = new NodeId(Convert.ToUInt32(item.ValueVariantId), 2), AttributeId = Attributes.Value };
                var intWriteVal = new WriteValue { NodeId = new NodeId(Convert.ToUInt32(item.ValueVariantId), 2), AttributeId = Attributes.Value };

                switch (item.Type)
                {
                    case OpcUaVariantValueType.Int32:
                        intWriteVal.Value.Value = Convert.ToInt32(value);
                        break;
                    case OpcUaVariantValueType.UInt32:
                        intWriteVal.Value.Value = Convert.ToUInt32(value);
                        break;
                    case OpcUaVariantValueType.Double:
                        intWriteVal.Value.Value = Convert.ToDouble(value);
                        break;
                    case OpcUaVariantValueType.Boolean:
                        intWriteVal.Value.Value = Convert.ToBoolean(value);
                        break;

                    default:
                        break;
                }

                logger.InfoFormat("Set (before) {0}.locked = '{1}'", name, true);
                nodesToWrite.Clear();
                nodesToWrite.Add(intWriteVal_locked);
                _sessionOpcUaClient.Write(null, nodesToWrite, out results, out diagnosticInfos); // scrittura del locked
                logger.InfoFormat("Set (after) {0}.locked = '{1}', completed!", name, true);



                logger.InfoFormat("Set (before) {0}.value = '{1}'", name, value);
                nodesToWrite.Clear();
                nodesToWrite.Add(intWriteVal);
                _sessionOpcUaClient.Write(null, nodesToWrite, out results, out diagnosticInfos); // scrittura del valore effettivo
                logger.InfoFormat("Set (after) {0}.value = '{1}', completed!", name, value);



                logger.InfoFormat("Set (before) CALL Update variables", name, value);
                // chiamata del metodo per l'update
                var resWrite = _sessionOpcUaClient.Call(_nodeId_localVariables, _nodeId_update_call, Convert.ToInt16(1)); // chiamata della funzione per aggiornare 
                var resRead = _sessionOpcUaClient.Call(_nodeId_localVariables, _nodeId_update_call, Convert.ToInt16(0)); // chiamata della f
                logger.InfoFormat("Set (after) CALL Update variables, completed!", name, value);

                // cancellazione della lista
                logger.InfoFormat("Set (before) {0}.locked = '{1}'", name, false);
                nodesToWrite.Clear();
                intWriteVal_locked.Value.Value = false;
                nodesToWrite.Add(intWriteVal_locked);
                _sessionOpcUaClient.Write(null, nodesToWrite, out results, out diagnosticInfos);
                logger.InfoFormat("Set (before) {0}.locked = '{1}', completed!", name, false);

            }

        }

        public void WriteFile(string pathfile, ILog logger)
        {
            if (!_isConnected) return;
            lock (_lock_write)
            {
                try
                {
                    StatusCodeCollection results = null;
                    DiagnosticInfoCollection diagnosticInfos = null;

                    // lettura del file da disco
                    var b = new UTF8Encoding().GetBytes(File.ReadAllText(pathfile));

                    var _OPC_QUEUEXML_BUSY = _items.FirstOrDefault(o => o.Name == "mainTsk$OPC_QUEUEXML_BUSY");
                    var _OPC_QUEUEXML_RELOAD = _items.FirstOrDefault(o => o.Name == "mainTsk$OPC_QUEUEXML_RELOAD");

                    // 1.
                    logger.InfoFormat("Set (before) {0}.locked = '{1}'", "OPC_QUEUEXML_BUSY", true);
                    var _OPC_QUEUEXML_BUSY_locked = new WriteValue { NodeId = new NodeId(Convert.ToUInt32(_OPC_QUEUEXML_BUSY.LockedId), 2), AttributeId = Attributes.Value };
                    _OPC_QUEUEXML_BUSY_locked.Value.Value = true;
                    logger.InfoFormat("Set (before) {0}.value = '{1}'", "OPC_QUEUEXML_BUSY", true);
                    var _OPC_QUEUEXML_BUSY_var = new WriteValue { NodeId = new NodeId(Convert.ToUInt32(_OPC_QUEUEXML_BUSY.ValueVariantId), 2), AttributeId = Attributes.Value };
                    _OPC_QUEUEXML_BUSY_var.Value.Value = true;


                    _sessionOpcUaClient.Write(null, new WriteValueCollection { _OPC_QUEUEXML_BUSY_locked, _OPC_QUEUEXML_BUSY_var }, out results, out diagnosticInfos);
                    logger.InfoFormat("Set (after) {0}.value = '{1}', completed!", "OPC_QUEUEXML_BUSY", true);
                    logger.InfoFormat("Set (after) {0}.locked = '{1}', completed!", "OPC_QUEUEXML_BUSY,", true);

          
                   // 2.
                    logger.InfoFormat("Set (before) CALL Update variables");
                    var resToUpdateWrite = _sessionOpcUaClient.Call(_nodeId_localVariables, _nodeId_update_call, Convert.ToInt16(1));
                    var resToUpdateRead = _sessionOpcUaClient.Call(_nodeId_localVariables, _nodeId_update_call, Convert.ToInt16(0));
                    
                    logger.InfoFormat("Set (after) CALL Update variables, completed!");

                    

                    // 3.
                    // apertura del file handle
                    logger.InfoFormat("Set (before) CALL Open file");
                    //IList<object> resOpen = _sessionOpcUaClient.Call(_nodeId_Queue_file, _nodeId_Queue_file_open, Convert.ToByte(120));
                    IList<object> resOpen = _sessionOpcUaClient.Call(_nodeId_Queue_file, _nodeId_Queue_file_open, Convert.ToByte(7));
                    if (!Opc.Ua.StatusCode.IsGood((Opc.Ua.StatusCode)resOpen[1]))
                        return;
                    var fileHandle = resOpen[0];
                    
                    logger.InfoFormat("Set (after) CALL Open file, completed!");

                    //Thread.Sleep(2000);
                    logger.InfoFormat("Set (before) CALL Write file");
                    //Thread.Sleep(200);
                    // invio del file in macchina
                    var resWrite = _sessionOpcUaClient.Call(_nodeId_Queue_file, _nodeId_Queue_file_write, fileHandle, b);
                    //Thread.Sleep(200);
                    if (!Opc.Ua.StatusCode.IsGood((Opc.Ua.StatusCode)resWrite[0]))
                        return;

                    
                    
                    logger.InfoFormat("Set (after) CALL Write file, completed!");


                    logger.InfoFormat("Set (before) CALL Close file");
                    // chiusura del file handle
                    var resClose = _sessionOpcUaClient.Call(_nodeId_Queue_file, _nodeId_Queue_file_close, fileHandle);
                    if (!Opc.Ua.StatusCode.IsGood((Opc.Ua.StatusCode)resClose[0]))
                        return;
                    //Thread.Sleep(500);
                    logger.InfoFormat("Set (after) CALL Close file, completed!");

                   
                    // 4.
                    logger.InfoFormat("Set (before) {0}.locked = '{1}'", "OPC_QUEUEXML_RELOAD", true);
                    var _OPC_QUEUEXML_RELOAD_locked = new WriteValue { NodeId = new NodeId(Convert.ToUInt32(_OPC_QUEUEXML_RELOAD.LockedId), 2), AttributeId = Attributes.Value };
                    _OPC_QUEUEXML_RELOAD_locked.Value.Value = true;
                    logger.InfoFormat("Set (before) {0}.value = '{1}'", "OPC_QUEUEXML_RELOAD", true);
                    var _OPC_QUEUEXML_RELOAD_var = new WriteValue { NodeId = new NodeId(Convert.ToUInt32(_OPC_QUEUEXML_RELOAD.ValueVariantId), 2), AttributeId = Attributes.Value };
                    _OPC_QUEUEXML_RELOAD_var.Value.Value = true;

                    _sessionOpcUaClient.Write(null, new WriteValueCollection { _OPC_QUEUEXML_RELOAD_locked, _OPC_QUEUEXML_RELOAD_var }, out results, out diagnosticInfos);
                    
                    logger.InfoFormat("Set (after) {0}.value = '{1}', completed!", "OPC_QUEUEXML_RELOAD", true);
                    logger.InfoFormat("Set (after) {0}.locked = '{1}', completed!", "OPC_QUEUEXML_RELOAD", true);
                    // 5.
                    var res5 = _sessionOpcUaClient.Call(_nodeId_localVariables, _nodeId_update_call, Convert.ToInt16(1));
                    var res6 = _sessionOpcUaClient.Call(_nodeId_localVariables, _nodeId_update_call, Convert.ToInt16(0));

                }
                catch (Exception exc)
                {
                    string s = exc.Message;
                    logger.Error(exc);
                       
                }

            }
        }

        public void AddVariableIsac()
        {
            logger.InfoFormat("{0}| AddVariableIsac() ...", this._deviceId);
            SystemVariables.AddDriver(driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_ACT_CUT_TIME_M" }, SystemVariables.TempoTaglioSingoloM, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_ACT_CUT_TIME_S" }, SystemVariables.TempoTaglioSingoloS, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_TOTAL_CUT_TIME_H" }, SystemVariables.TempoTotaleTagliH, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_TOTAL_CUT_TIME_M" }, SystemVariables.TempoTotaleTagliM, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_TOTAL_CUT_TIME_S" }, SystemVariables.TempoTotaleTagliS, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_CUT_TIME_LIFE_H" }, SystemVariables.TempoVitaMacchinaH, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_CUT_TIME_LIFE_M" }, SystemVariables.TempoVitaMacchinaM, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_CUT_TIME_LIFE_S" }, SystemVariables.TempoVitaMacchinaS, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPCUA_F_PANEL_ENC_OUT" }, SystemVariables.FeedRateSetPointmm, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPCUA_F_ENCODER_FORCING" }, SystemVariables.Encorder_ForcingF, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_F_PANEL_DISPLAY" }, SystemVariables.FeedRateAttuale, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPCUA_S_PANEL_ENC_OUT" }, SystemVariables.BladeSpeedSetPointm, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPCUA_S_ENCODER_FORCING" }, SystemVariables.Encorder_ForcingS, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_S_PANEL_DISPLAY" }, SystemVariables.BladeSpeedAttuale, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_BLD_I_CTRL_PV_Im" }, SystemVariables.CorrenteAssorbita, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_ALARM_DW0" }, SystemVariables.AllarmiMacchina1, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_ALARM_DW1" }, SystemVariables.AllarmiMacchina2, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_WARNING_DW0" }, SystemVariables.WarningMacchina, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_MKS_UNIT_DISPLAY" }, SystemVariables.UnitaMisuraSelezionata, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$MMI_LENGTH_CUT" }, SystemVariables.TaglioSingoloMisura, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$MMI_NUM_REQ_CUT" }, SystemVariables.TaglioSingoloProgrammati, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_ACT_CUT_JOB_DONE" }, SystemVariables.TaglioSingoloEseguiti, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_ACT_QUEUE_PROG" }, SystemVariables.Programma, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_QUEUE_PART_NUMBER" }, SystemVariables.PartNumber, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_VIS_ACT_LENGTH_LREAL"}, SystemVariables.MisuraProgrammata, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$REQ_NUM_CUT_VALUE" }, SystemVariables.PezziProdurre, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_ACT_CUT_QUEUE_DONE" }, SystemVariables.PezziProdotti, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_X_POSITION" }, SystemVariables.ParametroX, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_Y_POS_DISPLAY" }, SystemVariables.ParametroY, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_BLD_T_CTRL_Q_PV_DISPLAY" }, SystemVariables.TesaturaLama, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$POSITION_ABSOLUTE_ROT" }, SystemVariables.AngoloRotazioneGradi, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_MAC_STATUS_DW" }, SystemVariables.StatoMacchina, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPC_QUEUEXML_RELOAD", Type = OpcUaVariantValueType.Boolean }, SystemVariables.RiletturaQueuexml, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPC_QUEUEXML_BUSY", Type = OpcUaVariantValueType.Boolean }, SystemVariables.PrioritaQueuexmlBusy, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$ST_LOADING_QUEUE", Type = OpcUaVariantValueType.Boolean }, SystemVariables.LetturaCodaQueue, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPC_QUEUE_FILE_ACCEPTED", Type=OpcUaVariantValueType.Boolean }, SystemVariables.LavorazioneAccettata, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$OPC_QUEUE_FILE_REFUSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.LavorazioneRiufiutata, driverName);

            ////digitalInput
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_A_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_A, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_B_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_B, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_C_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_C, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_D_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_D, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F1_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F1, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F2_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F2, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F3_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F3, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F4_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F4, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F5_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F5, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F6_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F6, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F7_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F7, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_F8_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Key_F8, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_BLD_DRV_FAULT", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_Inverter, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_EME_PB", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_PulsanteEmergenza, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_ENABLE_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_PulsanteAbilitazione, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_LS_BAR_PRESENT", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_FCBarraMorsaAlimentatore, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_LS_HOME_X", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_FCAzzeramentoAsseX, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_LS_BLD_COVER_CLSD", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_FCRiparoLamaChiuso, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_LS_MIN_LUB_MIN_OIL", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_LivelloOlioBasso, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_CHIP_CONVAYOR_BLOCKED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_FCEvaquatoreTrucioliBloccato, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_BLADE_MAN_START", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_FCImpugnatura, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_LSPX_BLD_SPEED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_ProxiVelocitaLama, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_BREAKING_BLADE", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_BreakingBlade, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_RESET_KEY_PRESSED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_PulsanteReset, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_XB_JStckMinus", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_JoystickXm, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_XB_JStckPlus", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_JoystickXp, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_Y_JStckMinus", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_JoystickYm, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_Y_JStckPlus", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_JoystickYp, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$I_BARR_OK", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DI_FCRipariPerimetrali, driverName);
            //digitalOutput
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_BLD_DRIVE_EN", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_StartInverter, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_BLD_DW", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_DiscesaTesta, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_BLD_UP", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_SalitaTesta, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_LOAD_CLAMP_CLS", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_MorsaAlimentatoreChiude, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_LOAD_CLAMP_OPN", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_MorsaAlimentatoreApre, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_MIN_LUB", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_LubrificanteMinimale, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_UNLOAD_CLAMP_CLS", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_MorsaTaglioChiude, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_UNLOAD_CLAMP_OPN", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_MorsaTaglioApre, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_EV_Y_DOWNGOING_UNLOCK", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_SbloccoTesta, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_HL_CUTTING_AREA", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_LuceZonaTaglio, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_HYDR_PLATES", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_PlacchetteIdrauliche, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_MAT_HAND_FW", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_RulliereAlimentatoreAvanti, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_MAT_HAND_MAN_BW", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_RulliereAlimentatoreIndietro, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_MAT_HAND_CLEANCUT_BW", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_RulliereTaglioPulito, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_HL_LASER_TRIM", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_TraguardatoreLaser, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_LMP_EMRG", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_Lampeggiaggiante, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_KM_HYDR_PUMP", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_KmAvvioCentralina, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_KM_TRIM_HNDLR_FW", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_KmEvtrAvanti, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_KM_WATER_PUMP", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_KmAvvioPompaAcqua, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_Y_ENABLE", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_YEnable, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_LIKA_ENABLED", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_Lika_enable, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_BUZZER_ON", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_BuzzerOn, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$O_OUT3", Type = OpcUaVariantValueType.Boolean }, SystemVariables.DO_SpazzolaMotorizzata, driverName);

            //////Analog Input
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$AI_BLD_POS" }, SystemVariables.AI_PosizioneTesta, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$AI_BLD_CURRENT" }, SystemVariables.AI_AssorbimentoMotoreLama, driverName);
            //SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$POT_H" }, SystemVariables.AI_PotenzPannelloH, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$AI_BLD_TENS_LOADCELL" }, SystemVariables.AI_CellaTensionamentoLama, driverName);
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$AI_BLD_DEVIATION" }, SystemVariables.AI_DeviazioneLama, driverName);
            ////Analog OutPut
            SystemVariables.Match(new OpaUaNodeEntry { Name = "mainTsk$AO_BLD_DRIVE_VREF" }, SystemVariables.AO_VelocitaLama, driverName);

            _items = new OpcUaNodeEntryCollection();
            foreach (var item in SystemVariables.GetMatches(driverName))
            {
                _cache.TryAdd(item.Key.Name, null);
                _items.Add(item.Key);
            }

            logger.InfoFormat("{0}| AddVariableIsac() ... completed", this._deviceId);

        }

        public void Close()
        {
            this._sessionOpcUaClient.RemoveSubscription(_subscription);
            this._sessionOpcUaClient.Close();
            this._sessionOpcUaClient.Dispose();
        }

        public string IntToBinaryString(int number)
        {
            const int mask = 1;
            var binary = string.Empty;
            while (number > 0)
            {
                // Logical AND the number and prepend it to the result string
                binary = (number & mask) + binary;
                number = number >> 1;
            }

            return binary;
        }
    }




}
