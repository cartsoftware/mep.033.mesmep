﻿using AreaMes.Model.Enums;
using Opc.Ua.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMES.BatchLogic.Mep
{
    public interface IOpcUaClient
    {
        string SerialNumber { get; set; }
        void Close();
        Session Connect();
         Action<string, StatoMacchinaDriver> OnClientStatusChanged { get; set; }
    }
}
