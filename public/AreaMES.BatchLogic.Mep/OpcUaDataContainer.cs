﻿using AreaM2M.RealTime.Dto;
using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Server.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMES.BatchLogic.Mep
{
    public class OpcUaDataContainer : IDataContainer
    {
        public ConcurrentDictionary<string, DeviceItems> Data { get; private set; }
        private object _lock = new object();

        private static OpcUaDataContainer instance;
        public static OpcUaDataContainer Instance { get { return instance; } }

        static OpcUaDataContainer()
        {
            instance = new OpcUaDataContainer();
        }

        public OpcUaDataContainer()
        {
            Data = new ConcurrentDictionary<string, DeviceItems>();
        }

        //public void Create(IEnumerable<Registry> values, string deviceId)
        //{
        //    lock (_lock)
        //    {
        //        var di = new DeviceItems();

        //        if (Data.ContainsKey(deviceId))
        //            Data.TryRemove(deviceId, out di);

        //        di.Items = new Dictionary<string, DataItem>();
        //        di.Device = new Device { Code = deviceId };
        //        foreach (var obj in values)
        //        {
        //            di.Items.Add(obj.Path, new DataItem
        //            {
        //                Registry = obj,
        //                Value = obj.CurrentValue,
        //                OldValue = obj.CurrentValue,

        //                IsChanged = false
        //            });
        //        }


        //        Data.TryAdd(deviceId, di);
        //    }
        //}

        //public void Fill(List<ThingResult> item, string deviceId)
        //{
        //    lock (_lock)
        //    {
        //        DeviceItems deviceItem = null;
        //        if (Data.TryGetValue(deviceId, out deviceItem))
        //        {
        //            var message = new DeviceValueChangedMessage();
        //            message.DeviceId = deviceId;
        //            foreach (var tr in item)
        //            {
        //                DataItem mem = null;

        //                deviceItem.Items.TryGetValue(tr.Path, out mem); //.FirstOrDefault(x => Convert.ToString(x.Registry.Path) == Convert.ToString(tr.Path));
        //                if (mem != null)
        //                {
        //                    mem.OldValue = mem.Value;
        //                    mem.Value = tr.Value;
        //                    if (tr.Info != null)
        //                    {
        //                        mem.ValueDecoded = tr.Info.ValueDecoded;
        //                        mem.OldValueDecoded = tr.Info.OldValueDecoded;
        //                    }
        //                    mem.IsChanged = Convert.ToString(mem.OldValue) != Convert.ToString(mem.Value);
        //                }
        //                if (mem.IsChanged)
        //                {
        //                    File.AppendAllText("C:\\test.txt", String.Format("{0}={1}" + Environment.NewLine, mem.Registry.Path, mem.Value));
        //                    message.Add(mem);
        //                }

        //            }
        //            if (message.Any())
        //                MessageServices.Instance.Publish(DeviceValueChangedMessage.Name, message);
        //        }
        //    }
        //}

        public Dictionary<string, DeviceItems> Get()
        {
            return Data.ToDictionary(o => o.Key, y => y.Value);
        }

        public DeviceItems Get(string pDevice)
        {
            return Data[pDevice];
        }
    }


}
