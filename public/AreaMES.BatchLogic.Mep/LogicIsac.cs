﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Runtime;
using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using static AreaMES.BatchLogic.Mep.Logic;

namespace AreaMES.BatchLogic.Mep
{
    public class LogicIsac : ILogic
    {
        public Macchina Machine { get; set; }
        public string DeviceDriver { get; set; }
        public string DeviceId { get; set; }
        public string DeviceSerialNumber { get; set; }

        public static object _lock = new object();

        public void OnDeviceValueChangedMessage(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, DeviceValueChangedMessage message)
        {

            var MMI_FILE_QUEUE_ACCEPTED = message.FirstOrDefault(o => o.Name == SystemVariables.LavorazioneAccettata.Name);
            var MMI_FILE_QUEUE_REFUSED = message.FirstOrDefault(o => o.Name == SystemVariables.LavorazioneRiufiutata.Name);
            var ST_QUEUE_PART_NUMBER = deviceItems.Items[SystemVariables.PartNumber.Name];


            var ST_ACT_CUT_QUEUE_DONE = message.FirstOrDefault(o => o.Name == SystemVariables.PezziProdotti.Name);

            //var STATO_MACCHINA= message.FirstOrDefault(o => o.Name == SystemVariables.StatoMacchina.Name);

            var STATO_MACCHINA = deviceItems.Items[SystemVariables.StatoMacchina.Name];

            var currentStatus = GetStatus(STATO_MACCHINA);

            // -------------------------------------------------------------------------------------------
            // Gestione Allarmi 

            if(currentStatus!= null)
            {
                if (currentStatus.MacchinaInAllarme == true)
                     runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazioneInEmergenza });
                else if(batch.Stato== AreaMes.Model.Enums.StatoBatch.InLavorazioneInEmergenza)
                    runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
                else if (currentStatus.CicloMacchinaInHold == true)
                    runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazioneInPausa });
                else if ((batch.Stato == AreaMes.Model.Enums.StatoBatch.InLavorazioneInPausa) && currentStatus.MacchinaInRun == true)
                    runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
                else if(currentStatus.CicloMacchinaInCorso==true)
                    runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });
            }

            // -------------------------------------------------------------------------------------------
            // Fase di SETUP, in lavorazione

            if (fMEP_00.Stato == AreaMes.Model.Enums.StatoFasi.InLavorazione)
            {
                

                var OPC_QUEUEXML_RELOAD = message.FirstOrDefault(o => o.Name == SystemVariables.RiletturaQueuexml.Name);

                if ((MMI_FILE_QUEUE_ACCEPTED != null) && (Convert.ToBoolean(MMI_FILE_QUEUE_ACCEPTED.Value) == true))
                {
                    var d = SupervisorBatch.GetOpcUaClient(DeviceId) as OpcUaClientIsac;
                    d.Write("mainTsk$OPC_QUEUEXML_BUSY", false, logger);

                    fMEP_00.Stato = AreaMes.Model.Enums.StatoFasi.Terminato;
                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_00 });
                    //runtimeContext.SetBatch(new SetBatchInfo { idBatch = batch.Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazione });

                    var listBatch = runtimeContext.GetSfo("InLavorazione", "");
                    int i = 0;
                    for (i = 0; i < listBatch.Count(); i++)
                    {
                        if (listBatch[i].Id != batch.Id)
                        {
                            
                            runtimeContext.SetFase(new SetFaseInfo { idBatch = listBatch[i].Id, fase= fMEP_00 });
                            runtimeContext.SetBatch(new SetBatchInfo { idBatch = listBatch[i].Id, idStato = AreaMes.Model.Enums.StatoBatch.InLavorazioneInPausa });
                        }
                       
                    }
                }

                if ((MMI_FILE_QUEUE_REFUSED != null) && (Convert.ToBoolean(MMI_FILE_QUEUE_REFUSED.Value) == true))
                {
                    var d = SupervisorBatch.GetOpcUaClient(DeviceId) as OpcUaClientIsac;
                    d.Write("mainTsk$OPC_QUEUEXML_BUSY", false, logger);

                    fMEP_00.Stato = AreaMes.Model.Enums.StatoFasi.InPausa;
                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_00 });

                }
            }
            // ----------------------------------------------------------------------------------------------
            // Fase di SETUP completata in automatico
            if (fMEP_00.Stato == AreaMes.Model.Enums.StatoFasi.Terminato)
            {

                if ((currentStatus != null) &&
                    (currentStatus.CicloMacchinaInCorso) &&
                    (!(BatchUtils.IsInLavorazione(fMEP_LAV))))
                {
                    // Risulta completata la fase di setup macchina
                    batch.PezziProdotti = 0;

                    fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.InLavorazione;
                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                }
            }


            // ----------------------------------------------------------------------------------------------
            // Fase di LAVORAZIONE
            if (BatchUtils.IsInLavorazione(fMEP_LAV))
            {
                if (ST_QUEUE_PART_NUMBER == null) return;
                if (Convert.ToString(ST_QUEUE_PART_NUMBER.Value) != Convert.ToString(batch.OdPParziale))
                    return;

                //registerTrend(message, "mainTsk$ST_BLD_I_CTRL_PV_Im", _listOfBatchParams["MEP_CORASS"]);


                if (ST_ACT_CUT_QUEUE_DONE != null)
                {
                    batch.PezziProdotti = Convert.ToInt32(ST_ACT_CUT_QUEUE_DONE.Value);

                    //calcPerc();
                }

                if ((currentStatus != null) &&
                    (currentStatus.CicloMacchinaTerminato))
                {
                    fMEP_LAV.Stato = AreaMes.Model.Enums.StatoFasi.Terminato;
                    runtimeContext.SetFase(new SetFaseInfo { idBatch = batch.Id, fase = fMEP_LAV });
                }
            }


            // --------------------------------------------------------------------------------------------
            // GESTIONE DELLA FASE DI STOP LAVORAZIONE
            // --------------------------------------------------------------------------------------------

          






        }

        public void OnFaseChanged(ILog logger, IRuntimeTransportContextController runtimeContext, Batch batch, BatchFase fMEP_00, BatchFase fMEP_LAV, DeviceItems deviceItems, object id, FaseChangedInfo message)
        {
            try
            {
                var d = SupervisorBatch.GetOpcUaClient(DeviceId) as OpcUaClientIsac;

                if (message.Current.CodiceFase == "MEP-00")
                {
                    if (message.Current.Stato == AreaMes.Model.Enums.StatoFasi.InLavorazione)
                    {
                        // l'operatore ha richiesto la fase di setup macchina
                        string tempFileName = Path.Combine(Path.GetTempPath(), PurifyFileName(fMEP_00.Macchine.FirstOrDefault().Macchina.Codice) + ".xml");

                        logger.InfoFormat("Creazione file xml '{0}'", tempFileName);
                        var xmlDoc = CreateXmlDocument(batch);
                        //Thread.Sleep(200);
                        xmlDoc.Save(tempFileName);
                        //d.Write("mainTsk$OPC_QUEUEXML_BUSY", true, Default);
                        d.WriteFile(tempFileName, logger);
                        //Thread.Sleep(500);

                        //d.Write("mainTsk$OPC_QUEUEXML_RELOAD", true, Default);
                        //Thread.Sleep(500);
                    }
                }

            }

            catch (Exception exc)
            {
                logger.Error(exc);
            }
        }

        private MepStatus GetStatus(DataItem valuee)
        {
            //var cacheItem = deviceItems.Items["mainTsk$ST_MAC_STATUS_DW"];
            //var cacheItem = deviceItems.Items["StatoMacchina"];

            //if (cacheItem == null) return null;
            if (valuee == null) return null;

            int value = Convert.ToInt32(valuee.Value);
            //int value = Convert.ToInt32(cacheItem.Value);
            MepStatus s = new MepStatus();
            var binario = Convert.ToInt32(value);
            bool[] convertito = new bool[20];

            var i = 0;
            while (binario > 0)
            {
                convertito[i] = (binario % 2 != 0);
                i++;
                binario = binario / 2;
            }

            s.MacchinaInRun = convertito[0];
            s.MacchinaInAllarme = convertito[1];
            s.MacchinaInWarning = convertito[2];
            s.CicloMacchinaInHold = convertito[3];
            s.CicloMacchinaTerminato = convertito[4];
            s.CicloMacchinaInCorso = convertito[5];

            byte[] by = new byte[1];
            BitArray b = new BitArray(new bool[] { convertito[16], convertito[17], convertito[18], convertito[19] });
            b.CopyTo(by, 0);

            switch (Convert.ToInt16(by[0]))
            {
                case 1:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.Manuale;
                    break;
                case 2:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.SemiAutomaticoDinamico;
                    break;
                case 3:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.SemiAutomatico;
                    break;
                case 4:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.ProgrammaSingolo;
                    break;
                case 5:
                    s.MepAmbienteDiLavoro = MepAmbienteDiLavoro.CodeDiProgrammi;
                    break;
            }//Close Switch

            return s;
        }


        private XmlDocument CreateXmlDocument(Batch batch)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
            doc.AppendChild(docNode);

            XmlNode queueNode = doc.CreateElement("queue");
            doc.AppendChild(queueNode);

            XmlNode listNode = doc.CreateElement("list");
            queueNode.AppendChild(listNode);

            for (int i = 1; i <= 7; i++)
            {
                XmlNode itemNode = doc.CreateElement("item");
                listNode.AppendChild(itemNode);

                var catNode = doc.CreateElement("cat");
                var catPrg = doc.CreateElement("prg");
                var catPn = doc.CreateElement("pn");
                var catLen = doc.CreateElement("len");
                var catNpz = doc.CreateElement("npz");
                var catEnb = doc.CreateElement("enb");

                catNode.InnerText = String.Format("QUEUE{0}", i);
                catPrg.InnerText = String.Format("{0}", i);

                if (i == 1)
                {
                    catPn.InnerText = batch.OdPParziale;
                    catLen.InnerText = Convert.ToInt32(batch.DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_SETUP_LUNGH").ValoreDefault).ToString();
                    catNpz.InnerText = batch.PezziDaProdurre.ToString();
                    catEnb.InnerText = "1";
                }
                else
                {
                    //tPn.InnerText = String.Empty;
                    catPn.InnerText = "0";
                    catLen.InnerText = "0";
                    catNpz.InnerText = "0";
                    catEnb.InnerText = "0";
                }

                itemNode.AppendChild(catNode);
                itemNode.AppendChild(catPrg);
                itemNode.AppendChild(catPn);
                itemNode.AppendChild(catLen);
                itemNode.AppendChild(catNpz);
                itemNode.AppendChild(catEnb);
            }

            return doc;
        }

        private string PurifyFileName(string originalFileName)
        {
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            foreach (char c in invalid)
                originalFileName = originalFileName.Replace(c.ToString(), "");


            return originalFileName;
        }

    }
}
