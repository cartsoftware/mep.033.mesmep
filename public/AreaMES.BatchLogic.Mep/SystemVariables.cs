﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMES.BatchLogic.Mep
{
    public static class SystemVariables
    {
        private static Dictionary<string, Dictionary<OpaUaNodeEntry, SystemVariablesDefinition>> match = new Dictionary<string, Dictionary<OpaUaNodeEntry, SystemVariablesDefinition>>();
        private static object _lockMatch = new object();

        public static SystemVariablesDefinition TempoTaglioSingoloH = new SystemVariablesDefinition { Name = "TempoTaglioSingoloH", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoTaglioSingoloM = new SystemVariablesDefinition { Name = "TempoTaglioSingoloM", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoTaglioSingoloS = new SystemVariablesDefinition { Name = "TempoTaglioSingoloS", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoTotaleTagliH = new SystemVariablesDefinition { Name = "TempoTotaleTagliH", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoTotaleTagliM = new SystemVariablesDefinition { Name = "TempoTotaleTagliM", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoTotaleTagliS = new SystemVariablesDefinition { Name = "TempoTotaleTagliS", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoVitaMacchinaH = new SystemVariablesDefinition { Name = "TempoVitaMacchinaH", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoVitaMacchinaM = new SystemVariablesDefinition { Name = "TempoVitaMacchinaM", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TempoVitaMacchinaS = new SystemVariablesDefinition { Name = "TempoVitaMacchinaS", TypeOf = typeof(int) };
        public static SystemVariablesDefinition FeedRateSetPointmm = new SystemVariablesDefinition { Name = "FeedRateSetPointmm", TypeOf = typeof(int) };
        public static SystemVariablesDefinition Encorder_ForcingF = new SystemVariablesDefinition { Name = "Encorder_ForcingF", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition FeedRateAttuale = new SystemVariablesDefinition { Name = "FeedRateAttuale", TypeOf = typeof(int) };
        public static SystemVariablesDefinition BladeSpeedSetPointm = new SystemVariablesDefinition { Name = "BladeSpeedSetPointm", TypeOf = typeof(int) };
        public static SystemVariablesDefinition Encorder_ForcingS = new SystemVariablesDefinition { Name = "Encorder_ForcingS", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition BladeSpeedAttuale = new SystemVariablesDefinition { Name = "BladeSpeedAttuale", TypeOf = typeof(int) };
        public static SystemVariablesDefinition CorrenteAssorbita = new SystemVariablesDefinition { Name = "CorrenteAssorbita", TypeOf = typeof(float), Delta=0.5d };
        public static SystemVariablesDefinition AllarmiMacchina1 = new SystemVariablesDefinition { Name = "AllarmiMacchina1", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition AllarmiMacchina2 = new SystemVariablesDefinition { Name = "AllarmiMacchina2", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition WarningMacchina = new SystemVariablesDefinition { Name = "WarningMacchina", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition UnitaMisuraSelezionata = new SystemVariablesDefinition { Name = "UnitaMisuraSelezionata", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition TagliProgrammatiSemiautomatico = new SystemVariablesDefinition { Name = "TagliProgrammatiSemiautomatico", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TagliEseguitiSemiautomatico = new SystemVariablesDefinition { Name = "TagliEseguitiSemiautomatico", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TaglioSingoloMisura = new SystemVariablesDefinition { Name = "TaglioSingoloMisura", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TaglioSingoloProgrammati = new SystemVariablesDefinition { Name = "TaglioSingoloProgrammati", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TaglioSingoloEseguiti = new SystemVariablesDefinition { Name = "TaglioSingoloEseguiti", TypeOf = typeof(int) };
        public static SystemVariablesDefinition Programma = new SystemVariablesDefinition { Name = "Programma", TypeOf = typeof(int) };
        public static SystemVariablesDefinition PartNumber = new SystemVariablesDefinition { Name = "PartNumber", TypeOf = typeof(int) };
        public static SystemVariablesDefinition MisuraProgrammata = new SystemVariablesDefinition { Name = "MisuraProgrammata", TypeOf = typeof(float) };
        public static SystemVariablesDefinition PezziProdurre = new SystemVariablesDefinition { Name = "PezziProdurre", TypeOf = typeof(int) };
        public static SystemVariablesDefinition PezziProdotti = new SystemVariablesDefinition { Name = "PezziProdotti", TypeOf = typeof(int) };
        public static SystemVariablesDefinition LoopProgrammati = new SystemVariablesDefinition { Name = "LoopProgrammati", TypeOf = typeof(int) };
        public static SystemVariablesDefinition LoopEseguiti = new SystemVariablesDefinition { Name = "LoopEseguiti", TypeOf = typeof(int) };
        public static SystemVariablesDefinition ParametroX = new SystemVariablesDefinition { Name = "ParametroX", TypeOf = typeof(int) };
        public static SystemVariablesDefinition ParametroY = new SystemVariablesDefinition { Name = "ParametroY", TypeOf = typeof(int) };
        public static SystemVariablesDefinition TesaturaLama = new SystemVariablesDefinition { Name = "TesaturaLama", TypeOf = typeof(int), Delta=2};
        public static SystemVariablesDefinition DeviazioneLama = new SystemVariablesDefinition { Name = "DeviazioneLama", TypeOf = typeof(int) };
        public static SystemVariablesDefinition AsportazioneTotale = new SystemVariablesDefinition { Name = "AsportazioneTotale", TypeOf = typeof(int) };
        public static SystemVariablesDefinition PosizioneMorsaTaglio = new SystemVariablesDefinition { Name = "PosizioneMorsaTaglio", TypeOf = typeof(int) };
        public static SystemVariablesDefinition PosizioneMorsaAlimentatore = new SystemVariablesDefinition { Name = "PosizioneMorsaAlimentatore", TypeOf = typeof(int) };
        public static SystemVariablesDefinition PressBilanciamentoTesta = new SystemVariablesDefinition { Name = "PressBilanciamentoTesta", TypeOf = typeof(int) };
        public static SystemVariablesDefinition AngoloRotazioneGradi = new SystemVariablesDefinition { Name = "AngoloRotazioneGradi", TypeOf = typeof(int) };
        public static SystemVariablesDefinition AngoloRotazionePrimi = new SystemVariablesDefinition { Name = "AngoloRotazionePrimi", TypeOf = typeof(int) };
        public static SystemVariablesDefinition LarghezzaPezzo = new SystemVariablesDefinition { Name = "LarghezzaPezzo", TypeOf = typeof(int) };
        public static SystemVariablesDefinition StatoMacchina = new SystemVariablesDefinition { Name = "StatoMacchina", TypeOf = typeof(int) };
        public static SystemVariablesDefinition VersioneSoftwarePLC = new SystemVariablesDefinition { Name = "VersioneSoftwarePLC", TypeOf = typeof(int) };
        public static SystemVariablesDefinition VersioneFirmware = new SystemVariablesDefinition { Name = "VersioneFirmware", TypeOf = typeof(int) };
        public static SystemVariablesDefinition RiletturaQueuexml = new SystemVariablesDefinition { Name = "RiletturaQueuexml", TypeOf = typeof(int) };
        public static SystemVariablesDefinition PrioritaQueuexmlBusy = new SystemVariablesDefinition { Name = "PrioritaQueuexmlBusy", TypeOf = typeof(int) };
        public static SystemVariablesDefinition LetturaCodaQueue = new SystemVariablesDefinition { Name = "LetturaCodaQueue", TypeOf = typeof(int) };
        public static SystemVariablesDefinition LavorazioneAccettata = new SystemVariablesDefinition { Name = "LavorazioneAccettata", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition LavorazioneRiufiutata = new SystemVariablesDefinition { Name = "LavorazioneRiufiutata", TypeOf = typeof(bool) };
        //ALLARMI ISAC
        public static SystemVariablesDefinition Allarme1_PremereReset = new SystemVariablesDefinition { Name = "Allarme1_PremereReset", TypeOf = typeof(bool), IsAllarm = true };
        public static SystemVariablesDefinition Allarme2_TensioneLama = new SystemVariablesDefinition { Name = "Allarme2_TensioneLama", TypeOf = typeof(bool), IsAllarm = true };
        public static SystemVariablesDefinition Allarme3_SovraccaricoMotore = new SystemVariablesDefinition { Name = "Allarme3_SovraccaricoMotore", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme4_Velocitains = new SystemVariablesDefinition { Name = "Allarme4_Velocitains", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme5_PremereReset2 = new SystemVariablesDefinition { Name = "Allarme5_PremereReset2", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme6_FungoPremuto = new SystemVariablesDefinition { Name = "Allarme6_FungoPremuto", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme7_Yblocco1 = new SystemVariablesDefinition { Name = "Allarme7_Yblocco1", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme8_Xblocco1 = new SystemVariablesDefinition { Name = "Allarme8_Xblocco1", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme9_Yblocco2 = new SystemVariablesDefinition { Name = "Allarme9_Yblocco2", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme10_Xblocco2 = new SystemVariablesDefinition { Name = "Allarme10_Xblocco2", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme11_LivelloOlioMin = new SystemVariablesDefinition { Name = "Allarme11_LivelloOlioMin", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme12_CarterAperto = new SystemVariablesDefinition { Name = "Allarme12_CarterAperto", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme13_AlimentatoreBloccato = new SystemVariablesDefinition { Name = "Allarme13_AlimentatoreBloccato", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme14_InverterBloccato = new SystemVariablesDefinition { Name = "Allarme14_InverterBloccato", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme15_AlimBarra = new SystemVariablesDefinition { Name = "Allarme15_AlimBarra", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme16_FCTIFCTA = new SystemVariablesDefinition { Name = "Allarme16_FCTIFCTA", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme17_Parametri = new SystemVariablesDefinition { Name = "Allarme17_Parametri", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme18_Velocita = new SystemVariablesDefinition { Name = "Allarme18_Velocita", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme19_NessunProg = new SystemVariablesDefinition { Name = "Allarme19_NessunProg", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme20_Scorta = new SystemVariablesDefinition { Name = "Allarme20_Scorta", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme21_FineBarra = new SystemVariablesDefinition { Name = "Allarme21_FineBarra", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme22_SWLunghezza = new SystemVariablesDefinition { Name = "Allarme22_SWLunghezza", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme23_SWAsseX = new SystemVariablesDefinition { Name = "Allarme23_SWAsseX", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme24_LamaRotta = new SystemVariablesDefinition { Name = "Allarme24_LamaRotta", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme25_ErroreCiclo = new SystemVariablesDefinition { Name = "Allarme25_ErroreCiclo", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme26_DevMax = new SystemVariablesDefinition { Name = "Allarme26_DevMax", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme27_DevMin = new SystemVariablesDefinition { Name = "Allarme27_DevMin", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme28_ProtezAperte = new SystemVariablesDefinition { Name = "Allarme28_ProtezAperte", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme29_PressioneAria = new SystemVariablesDefinition { Name = "Allarme29_PressioneAria", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme30_ProtezAperte2 = new SystemVariablesDefinition { Name = "Allarme30_ProtezAperte2", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme31_BloccoTesta = new SystemVariablesDefinition { Name = "Allarme31_BloccoTesta", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme32_Morse = new SystemVariablesDefinition { Name = "Allarme32_Morse", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme33_AlimBloccato = new SystemVariablesDefinition { Name = "Allarme33_AlimBloccato", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Allarme34_Homing = new SystemVariablesDefinition { Name = "Allarme34_Homing", TypeOf = typeof(bool) };
        //Warning ISAC
        public static SystemVariablesDefinition Warning1_AbilitareMotoreLama = new SystemVariablesDefinition { Name = "Warning1_AbilitareMotoreLama ", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning2_StartImpugnatura = new SystemVariablesDefinition { Name = "Warning2_StartImpugnatura", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning3_Scorta = new SystemVariablesDefinition { Name = "Warning3_Scorta", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning4_LivelloRubrificanteMin = new SystemVariablesDefinition { Name = "Warning4_LivelloRubrificanteMin", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning5_FctiFctaIncongruenti = new SystemVariablesDefinition { Name = "Warning_5FctiFctaIncongruenti", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning6_Scorta1 = new SystemVariablesDefinition { Name = "Warning6_Scorta1", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning7_Scorta2 = new SystemVariablesDefinition { Name = "Warning7_Scorta2", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning8_ClearPezzi = new SystemVariablesDefinition { Name = "Warning8_ClearPezzi", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning9_VelocitaTestaAttiva = new SystemVariablesDefinition { Name = "Warning9_VelocitaTestaAttiva", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning10_EncoderFa0 = new SystemVariablesDefinition { Name = "Warning10_EncoderFa0", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning11_TermicheScattate = new SystemVariablesDefinition { Name = "Warning11_TermicheScattate", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning12_BloccoEvaquatoreTrucioli = new SystemVariablesDefinition { Name = "Warning12_BloccoEvaquatoreTrucioli", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning13_AsseXnonAzzarata = new SystemVariablesDefinition { Name = "Warning13_AsseXnonAzzarata", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning14_AttesaVelocitaLama = new SystemVariablesDefinition { Name = "Warning14_AttesaVelocitaLama", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning15_FineTagli = new SystemVariablesDefinition { Name = "Warning15_FineTagli", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning16_FineTagli1 = new SystemVariablesDefinition { Name = "Warning16_FineTagli1", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning17_DevizioneLama = new SystemVariablesDefinition { Name = "Warning17_DevizioneLama", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning18_AggiornamentoCoda = new SystemVariablesDefinition { Name = "Warning18_AggiornamentoCoda", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning19_ComandoInibito = new SystemVariablesDefinition { Name = "Warning19_ComandoInibito", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning20_StartPedalieraAtt = new SystemVariablesDefinition { Name = "Warning20_StartPedalieraAtt", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning21_PedalieraPulsanteAtt = new SystemVariablesDefinition { Name = "Warning21_PedalieraPulsanteAtt", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning22_StopPremereStart = new SystemVariablesDefinition { Name = "Warning22_StopPremereStart", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning23_SingStopPremereStart = new SystemVariablesDefinition { Name = "Warning23_SingStopPremereStart", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning24_PremereStart = new SystemVariablesDefinition { Name = "Warning24_PremereStart", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning25_AsseXBlocco = new SystemVariablesDefinition { Name = "Warning25_AsseXBlocco", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning26_ScritturaFileQueue = new SystemVariablesDefinition { Name = "Warning26_ScritturaFileQueue", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning27_FileUsoMes = new SystemVariablesDefinition { Name = "Warning27_FileUsoMes", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning28_RiavvioMacch = new SystemVariablesDefinition { Name = "Warning28_RiavvioMacch", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning29_ErrorCopy = new SystemVariablesDefinition { Name = "Warning29_ErrorCopy", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning30_AbilitaLama = new SystemVariablesDefinition { Name = "Warning30_AbilitaLama", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition Warning31_Lunghezza = new SystemVariablesDefinition { Name = "Warning31_Lunghezza", TypeOf = typeof(bool) };
        //input isac
        public static SystemVariablesDefinition DI_Key_A = new SystemVariablesDefinition { Name = "DI_Key_A", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_B = new SystemVariablesDefinition { Name = "DI_Key_B", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_C = new SystemVariablesDefinition { Name = "DI_Key_C", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_D = new SystemVariablesDefinition { Name = "DI_Key_D", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F1 = new SystemVariablesDefinition { Name = "DI_Key_F1", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F2 = new SystemVariablesDefinition { Name = "DI_Key_F2", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F3 = new SystemVariablesDefinition { Name = "DI_Key_F3", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F4 = new SystemVariablesDefinition { Name = "DI_Key_F4", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F5 = new SystemVariablesDefinition { Name = "DI_Key_F5", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F6 = new SystemVariablesDefinition { Name = "DI_Key_F6", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F7 = new SystemVariablesDefinition { Name = "DI_Key_F7", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Key_F8 = new SystemVariablesDefinition { Name = "DI_Key_F8", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_Inverter = new SystemVariablesDefinition { Name = "DI_Inverter", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_PulsanteEmergenza = new SystemVariablesDefinition { Name = "DI_PulsanteEmergenza", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_PulsanteAbilitazione = new SystemVariablesDefinition { Name = "DI_PulsanteAbilitazione", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_FCBarraMorsaAlimentatore = new SystemVariablesDefinition { Name = "DI_FCBarraMorsaAlimentatore", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_FCAzzeramentoAsseX = new SystemVariablesDefinition { Name = "DI_FCAzzeramentoAsseX", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_FCRiparoLamaChiuso = new SystemVariablesDefinition { Name = "DI_FCRiparoLamaChiuso", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_LivelloOlioBasso = new SystemVariablesDefinition { Name = "DI_LivelloOlioBasso", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_FCEvaquatoreTrucioliBloccato = new SystemVariablesDefinition { Name = "DI_FCEvaquatoreTrucioliBloccato", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_FCImpugnatura = new SystemVariablesDefinition { Name = "DI_FCImpugnatura", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_ProxiVelocitaLama = new SystemVariablesDefinition { Name = "DI_ProxiVelocitaLama", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_BreakingBlade = new SystemVariablesDefinition { Name = "DI_BreakingBlade", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_PulsanteReset = new SystemVariablesDefinition { Name = "DI_PulsanteReset", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_JoystickXm = new SystemVariablesDefinition { Name = "DI_JoystickXm", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_JoystickXp = new SystemVariablesDefinition { Name = "DI_JoystickXp", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_JoystickYm = new SystemVariablesDefinition { Name = "DI_JoystickYm", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_JoystickYp = new SystemVariablesDefinition { Name = "DI_JoystickYp", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DI_FCRipariPerimetrali = new SystemVariablesDefinition { Name = "DI_FCRipariPerimetrali", TypeOf = typeof(bool) };
        //digitalOutput
        public static SystemVariablesDefinition DO_StartInverter = new SystemVariablesDefinition { Name = "DO_StartInverter", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_DiscesaTesta = new SystemVariablesDefinition { Name = "DO_DiscesaTesta", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_SalitaTesta = new SystemVariablesDefinition { Name = "DO_SalitaTesta", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_MorsaAlimentatoreChiude = new SystemVariablesDefinition { Name = "DO_MorsaAlimentatoreChiude", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_MorsaAlimentatoreApre = new SystemVariablesDefinition { Name = "DO_MorsaAlimentatoreApre", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_LubrificanteMinimale = new SystemVariablesDefinition { Name = "DO_LubrificanteMinimale", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_MorsaTaglioChiude = new SystemVariablesDefinition { Name = "DO_MorsaTaglioChiude", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_MorsaTaglioApre = new SystemVariablesDefinition { Name = "DO_MorsaTaglioApre", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_SbloccoTesta = new SystemVariablesDefinition { Name = "DO_SbloccoTesta", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_LuceZonaTaglio = new SystemVariablesDefinition { Name = "DO_LuceZonaTaglio", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_PlacchetteIdrauliche = new SystemVariablesDefinition { Name = "DO_PlacchetteIdrauliche", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_RulliereAlimentatoreAvanti = new SystemVariablesDefinition { Name = "DO_RulliereAlimentatoreAvanti", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_RulliereAlimentatoreIndietro = new SystemVariablesDefinition { Name = "DO_RulliereAlimentatoreIndietro", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_RulliereTaglioPulito = new SystemVariablesDefinition { Name = "DO_RulliereTaglioPulito", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_TraguardatoreLaser = new SystemVariablesDefinition { Name = "DO_TraguardatoreLaser", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_Lampeggiaggiante= new SystemVariablesDefinition { Name = "DO_Lampeggiaggiante", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_KmAvvioCentralina = new SystemVariablesDefinition { Name = "DO_KmAvvioCentralina", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_KmEvtrAvanti = new SystemVariablesDefinition { Name = "DO_KmEvtrAvanti", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_KmAvvioPompaAcqua = new SystemVariablesDefinition { Name = "DO_KmAvvioPompaAcqua", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_YEnable = new SystemVariablesDefinition { Name = "DO_YEnable", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_Lika_enable = new SystemVariablesDefinition { Name = "DO_Lika_enable", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_BuzzerOn = new SystemVariablesDefinition { Name = "DO_BuzzerOn", TypeOf = typeof(bool) };
        public static SystemVariablesDefinition DO_SpazzolaMotorizzata = new SystemVariablesDefinition { Name = "DO_SpazzolaMotorizzata", TypeOf = typeof(bool) };
        //Analog Input
        public static SystemVariablesDefinition AI_PosizioneTesta = new SystemVariablesDefinition { Name = "AI_PosizioneTesta", TypeOf = typeof(float), Delta = 0.5d };
        public static SystemVariablesDefinition AI_AssorbimentoMotoreLama = new SystemVariablesDefinition { Name = "AI_AssorbimentoMotoreLama", TypeOf = typeof(float), Delta = 0.5d };
        public static SystemVariablesDefinition AI_PotenzPannelloH = new SystemVariablesDefinition { Name = "AI_PotenzPannelloH", TypeOf = typeof(float), Delta = 0.5d };
        public static SystemVariablesDefinition AI_CellaTensionamentoLama = new SystemVariablesDefinition { Name = "AI_CellaTensionamentoLama", TypeOf = typeof(float), Delta = 0.5d };
        public static SystemVariablesDefinition AI_DeviazioneLama= new SystemVariablesDefinition { Name = "AI_DeviazioneLama", TypeOf = typeof(float), Delta = 0.5d };
        //Analog OutPut
        public static SystemVariablesDefinition AO_VelocitaLama = new SystemVariablesDefinition { Name = "AO_VelocitaLama", TypeOf = typeof(float) };

        public static List<SystemVariablesDefinition> Items { get; set; }

        static SystemVariables()
        {
            Items = new List<SystemVariablesDefinition>();
            Items.Add(TempoTaglioSingoloH);
            Items.Add(TempoTaglioSingoloM);
            Items.Add(TempoTaglioSingoloS);
            Items.Add(TempoTotaleTagliH);
            Items.Add(TempoTotaleTagliM);
            Items.Add(TempoTotaleTagliS);
            Items.Add(TempoVitaMacchinaH);
            Items.Add(TempoVitaMacchinaM);
            Items.Add(TempoVitaMacchinaS);
            Items.Add(FeedRateSetPointmm);
            Items.Add(Encorder_ForcingF);
            Items.Add(FeedRateAttuale);
            Items.Add(BladeSpeedSetPointm);
            Items.Add(Encorder_ForcingS);
            Items.Add(BladeSpeedAttuale);
            Items.Add(CorrenteAssorbita);
            Items.Add(AllarmiMacchina1);
            Items.Add(AllarmiMacchina2);
            Items.Add(WarningMacchina);
            Items.Add(UnitaMisuraSelezionata);
            Items.Add(TagliProgrammatiSemiautomatico);
            Items.Add(TagliEseguitiSemiautomatico);
            Items.Add(TaglioSingoloMisura);
            Items.Add(TaglioSingoloProgrammati);
            Items.Add(TaglioSingoloEseguiti);
            Items.Add(Programma);
            Items.Add(PartNumber);
            Items.Add(MisuraProgrammata);
            Items.Add(PezziProdurre);
            Items.Add(PezziProdotti);
            Items.Add(LoopProgrammati);
            Items.Add(LoopEseguiti);
            Items.Add(ParametroX);
            Items.Add(ParametroY);
            Items.Add(TesaturaLama);
            Items.Add(DeviazioneLama);
            Items.Add(AsportazioneTotale);
            Items.Add(PosizioneMorsaTaglio);
            Items.Add(PosizioneMorsaAlimentatore);
            Items.Add(PressBilanciamentoTesta);
            Items.Add(AngoloRotazioneGradi);
            Items.Add(AngoloRotazionePrimi);
            Items.Add(LarghezzaPezzo);
            Items.Add(StatoMacchina);
            Items.Add(VersioneSoftwarePLC);
            Items.Add(VersioneFirmware);
            Items.Add(RiletturaQueuexml);
            Items.Add(PrioritaQueuexmlBusy);
            Items.Add(LetturaCodaQueue);
            Items.Add(LavorazioneAccettata);
            Items.Add(LavorazioneRiufiutata);
            Items.Add(Allarme1_PremereReset);
            Items.Add(Allarme2_TensioneLama);
            Items.Add(Allarme3_SovraccaricoMotore);
            Items.Add(Allarme4_Velocitains);
            Items.Add(Allarme5_PremereReset2);
            Items.Add(Allarme6_FungoPremuto);
            Items.Add(Allarme7_Yblocco1);
            Items.Add(Allarme8_Xblocco1);
            Items.Add(Allarme9_Yblocco2);
            Items.Add(Allarme10_Xblocco2);
            Items.Add(Allarme11_LivelloOlioMin);
            Items.Add(Allarme12_CarterAperto);
            Items.Add(Allarme13_AlimentatoreBloccato);
            Items.Add(Allarme14_InverterBloccato);
            Items.Add(Allarme15_AlimBarra);
            Items.Add(Allarme16_FCTIFCTA);
            Items.Add(Allarme17_Parametri);
            Items.Add(Allarme18_Velocita);
            Items.Add(Allarme19_NessunProg);
            Items.Add(Allarme20_Scorta);
            Items.Add(Allarme21_FineBarra);
            Items.Add(Allarme22_SWLunghezza);
            Items.Add(Allarme23_SWAsseX);
            Items.Add(Allarme24_LamaRotta);
            Items.Add(Allarme25_ErroreCiclo);
            Items.Add(Allarme26_DevMax);
            Items.Add(Allarme27_DevMin);
            Items.Add(Allarme28_ProtezAperte);
            Items.Add(Allarme29_PressioneAria);
            Items.Add(Allarme30_ProtezAperte2);
            Items.Add(Allarme31_BloccoTesta);
            Items.Add(Allarme32_Morse);
            Items.Add(Allarme33_AlimBloccato);
            Items.Add(Allarme34_Homing);
            //Warning
            Items.Add(Warning1_AbilitareMotoreLama);
            Items.Add(Warning2_StartImpugnatura);
            Items.Add(Warning3_Scorta);
            Items.Add(Warning4_LivelloRubrificanteMin);
            Items.Add(Warning5_FctiFctaIncongruenti);
            Items.Add(Warning6_Scorta1);
            Items.Add(Warning7_Scorta2);
            Items.Add(Warning8_ClearPezzi);
            Items.Add(Warning9_VelocitaTestaAttiva);
            Items.Add(Warning10_EncoderFa0);
            Items.Add(Warning11_TermicheScattate);
            Items.Add(Warning12_BloccoEvaquatoreTrucioli);
            Items.Add(Warning13_AsseXnonAzzarata);
            Items.Add(Warning14_AttesaVelocitaLama);
            Items.Add(Warning15_FineTagli);
            Items.Add(Warning16_FineTagli1);
            Items.Add(Warning18_AggiornamentoCoda);
            Items.Add(Warning19_ComandoInibito);
            Items.Add(Warning20_StartPedalieraAtt);
            Items.Add(Warning21_PedalieraPulsanteAtt);
            Items.Add(Warning22_StopPremereStart);
            Items.Add(Warning23_SingStopPremereStart);
            Items.Add(Warning24_PremereStart);
            Items.Add(Warning25_AsseXBlocco);
            Items.Add(Warning26_ScritturaFileQueue);
            Items.Add(Warning27_FileUsoMes);
            Items.Add(Warning28_RiavvioMacch);
            Items.Add(Warning29_ErrorCopy);
            Items.Add(Warning30_AbilitaLama);
            Items.Add(Warning31_Lunghezza);
            // Digital Input
            Items.Add(DI_Key_A);
            Items.Add(DI_Key_B);
            Items.Add(DI_Key_C);
            Items.Add(DI_Key_D);
            Items.Add(DI_Key_F1);
            Items.Add(DI_Key_F2);
            Items.Add(DI_Key_F3);
            Items.Add(DI_Key_F4);
            Items.Add(DI_Key_F5);
            Items.Add(DI_Key_F6);
            Items.Add(DI_Key_F7);
            Items.Add(DI_Key_F8);
            Items.Add(DI_Inverter);
            Items.Add(DI_PulsanteEmergenza);
            Items.Add(DI_PulsanteAbilitazione);
            Items.Add(DI_FCBarraMorsaAlimentatore);
            Items.Add(DI_FCAzzeramentoAsseX);
            Items.Add(DI_FCRiparoLamaChiuso);
            Items.Add(DI_LivelloOlioBasso);
            Items.Add(DI_FCEvaquatoreTrucioliBloccato);
            Items.Add(DI_FCImpugnatura);
            Items.Add(DI_ProxiVelocitaLama);
            Items.Add(DI_BreakingBlade);
            Items.Add(DI_PulsanteReset);
            Items.Add(DI_JoystickXm);
            Items.Add(DI_JoystickXp);
            Items.Add(DI_JoystickYm);
            Items.Add(DI_JoystickYp);
            Items.Add(DI_FCRipariPerimetrali);
            // Digital Output
            Items.Add(DO_StartInverter);
            Items.Add(DO_DiscesaTesta);
            Items.Add(DO_SalitaTesta);
            Items.Add(DO_MorsaAlimentatoreChiude);
            Items.Add(DO_MorsaAlimentatoreApre);
            Items.Add(DO_LubrificanteMinimale);
            Items.Add(DO_MorsaTaglioChiude);
            Items.Add(DO_MorsaTaglioApre);
            Items.Add(DO_SbloccoTesta);
            Items.Add(DO_LuceZonaTaglio);
            Items.Add(DO_PlacchetteIdrauliche);
            Items.Add(DO_RulliereAlimentatoreAvanti);
            Items.Add(DO_RulliereAlimentatoreIndietro);
            Items.Add(DO_RulliereTaglioPulito);
            Items.Add(DO_TraguardatoreLaser);
            Items.Add(DO_Lampeggiaggiante);
            Items.Add(DO_KmAvvioCentralina);
            Items.Add(DO_KmEvtrAvanti);
            Items.Add(DO_KmAvvioPompaAcqua);
            Items.Add(DO_YEnable);
            Items.Add(DO_Lika_enable);
            Items.Add(DO_BuzzerOn);
            Items.Add(DO_SpazzolaMotorizzata);
            //Analog Output
            Items.Add(AI_PosizioneTesta);
            Items.Add(AI_AssorbimentoMotoreLama);
            Items.Add(AI_PotenzPannelloH);
            Items.Add(AI_CellaTensionamentoLama);
            Items.Add(AI_DeviazioneLama);

            Items.Add(AO_VelocitaLama);

        }
        public static void AddDriver(string driverName)
        {
            lock (_lockMatch)
            {
                try
                {
                    if(match.FirstOrDefault(o => o.Key == driverName).Value ==null)
                    match.Add(driverName, new Dictionary<OpaUaNodeEntry, SystemVariablesDefinition>());
                    //return match[driverName].FirstOrDefault(o => o.Key.Name == opcUaName).Value;
                }
                catch{ }
            }     
        }

        public static void Match(OpaUaNodeEntry opcUaVariable, SystemVariablesDefinition systemVariable, string driverName)
        {
            lock (_lockMatch)
                match[driverName].Add(opcUaVariable, systemVariable);
        }


        public static SystemVariablesDefinition GetMatch(string driverName, string opcUaName){

            return match[driverName].FirstOrDefault(o => o.Key.Name == opcUaName).Value;
         }

        public static Dictionary<OpaUaNodeEntry, SystemVariablesDefinition> GetMatches(string driverName)
        {

            return match[driverName] ;
        }
    }

    public class SystemVariablesDefinition
    {
        public string Name { get; set; }

        public Type TypeOf { get; set; }

        bool IsDashboard { get; set; }

        bool IsBatchTrend { get; set; }

        public double Delta { get; set; } = 0;

        public bool IsAllarm { get; set; }
    }
}
