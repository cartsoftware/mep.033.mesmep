﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace AreaMES.BatchLogic.Mep
{
    public class Logic : AreaMes.Model.BatchLogic
    {

        private BatchAvanzamento _lastAvanzamento;
        private DeviceItems _device;
        private ILogic _logic;
        private Macchina _machine;
        private string _deviceDriver;
        private string _deviceId;
        private string _deviceSerialNumber;

        private object _lock_device1 = new object();
        private object _perc_lock = new object();
        private Timer _percClock;

        private BatchFase fMEP_00;
        private BatchFase fMEP_LAV;

        public override async Task Init(IDataContainer data, IRepository repository, IMessageServices messages, IRealTimeManager areaMESClient, IAreaM2MClient areaM2MClient, IRuntimeTransportContextController runtimeContext, Batch batch)
        {
            await base.Init(data, repository, messages, areaMESClient, areaM2MClient, runtimeContext, batch);
            try
            {
                
                // importare id maccgina e passarlo direttamente
                //_machine = batch.Fasi[0].Macchine[0].Macchina;
                _machine = repository.Get<Macchina>(batch.Fasi[0].Macchine[0].Macchina.Id);
                var tipologia = repository.Get<TipiMacchina>(_machine.Tipologia.Id);
                _deviceId = _machine.Id;
                _deviceSerialNumber = _machine.SerialNumber;
                _deviceDriver = tipologia.Driver;

                if (_deviceDriver == DriverSchedaOpc.MEP50_1_ISAC.ToString())
                {
                    _logic = new LogicMEP50_1_ISAC();
                  
                }
                else if (_deviceDriver == DriverSchedaOpc.MEP40_QEM.ToString())
                {
                    _logic = new LogicMEP40_QEM();
                }

                _logic.DeviceDriver = _deviceDriver;
                _logic.DeviceId = _deviceId;
                _logic.DeviceSerialNumber = _deviceSerialNumber;
                _logic.Machine = _machine;

                _device = SystemVariablesDataContainer.Instance.Get(_deviceId);

                //---------------------------------------------------------------------------------
                _percClock = new Timer((a) =>
                {
                    //  calcPerc();
                }, null, 0, 10 * 1000);


            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }


        }

        protected override void OnDeviceValueChangedMessage(object id, DeviceValueChangedMessage message)
        {
            try
            {
                if (message.DeviceId != _deviceId)
                    return;

                fMEP_00 = this._batch.Fasi.FirstOrDefault(o => o.DistintaBaseFase.Codice == "MEP-00");
                fMEP_LAV = this._batch.Fasi.FirstOrDefault(o => o.DistintaBaseFase.Codice == "MEP-LAV");

               

                // logica custom in base al driver
                _logic?.OnDeviceValueChangedMessage(Logger, _runtimeContext, _batch, fMEP_00, fMEP_LAV, _device, id, message); ;

                // Registro variabile di Trend
                if (BatchUtils.IsInLavorazione(fMEP_LAV))
                registerTrend(message, SystemVariables.CorrenteAssorbita.Name, _listOfBatchParams["MEP_CORASS"]);
                
            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }

        }

        protected override void OnBatchChanged(object id, BatchChangedMessage message)
        {

        }

        protected override async void OnFaseChanged(object id, FaseChangedInfo message)
        {
            try
            {
                _lastAvanzamento = message.Current;
                if (message.Current == null) return;
                if (message.Current.Id != this._batchId) return;

                fMEP_00 = this._batch.Fasi.FirstOrDefault(o => o.DistintaBaseFase.Codice == "MEP-00");
                fMEP_LAV = this._batch.Fasi.FirstOrDefault(o => o.DistintaBaseFase.Codice == "MEP-LAV");

                _logic?.OnFaseChanged(Logger, _runtimeContext, _batch, fMEP_00, fMEP_LAV, _device, id, message);

            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }

        }


   

 
        public class MepStatus
        {
            public bool MacchinaInRun { get; set; }
            public bool MacchinaInAllarme { get; set; }
            public bool MacchinaInWarning { get; set; }
            public bool CicloMacchinaInHold { get; set; }
            public bool CicloMacchinaTerminato { get; set; }
            public bool CicloMacchinaInCorso{ get; set; }

            public MepAmbienteDiLavoro MepAmbienteDiLavoro { get; set; }

        }

        public enum MepAmbienteDiLavoro
        {
            Manuale,
            SemiAutomaticoDinamico,
            SemiAutomatico,
            ProgrammaSingolo,
            CodeDiProgrammi
        }

    }
}
