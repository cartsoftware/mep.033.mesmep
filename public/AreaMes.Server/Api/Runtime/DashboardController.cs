﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Runtime
{
    public class DashboardController : ApiController
    {

        private static Dictionary<string, int> _oeePrev = new Dictionary<string, int>();

        public static Dashboard _Dashboard;

        public async Task<Dashboard> Get()
        {

            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);


            var d = new Dashboard { Ordini = new List<BatchSummary>() };

            try
            {

                //----- Cehck licence ------------------------------------------
                if (LicenceManager.Instance.IsValid == false)
                {
                    d.ErrorMessage = translations.FirstOrDefault(o => o.Key == "licenceManager_licenceNotValid")?.Value;
                    return d;
                }



                if (MongoRepository.Instance.IsRun == false)
                    return d;

                //var filterBatch = Builders<Batch>.Filter.Where(o => o.Stato != StatoBatch.Eliminato
                //                                                    && o.Stato != StatoBatch.Terminato
                //                                                    && o.Stato != StatoBatch.TerminatoAbortito);
                //var res = await MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToListAsync();


               
                foreach (var item in BatchManager.Instance.BatchInMemory())
                {
                    var macchina = item.Fasi.SelectMany(x => x.Macchine).FirstOrDefault(x => x.Macchina != null && !String.IsNullOrWhiteSpace( x.Macchina.Id));

                     macchina?.Macchina?.Tipologia?.Resolve();

                    var batchSummary = new BatchSummary
                    {
                        Id = item.Id,
                        Commessa = item.Commessa,
                        PezziDaProdurre = item.PezziDaProdurre,
                        PercCompletamento = item.PercCompletamento,
                        OdP = item.OdP,
                        OdPParziale = item.OdPParziale,
                        OdV = item.OdV,
                        Inizio = item.Inizio,
                        TotaleFermi = item.TotaleFermi,
                        OEE = BatchUtils.CalculateRuntimeEfficiency(item),
                        Stato = item.Stato,
                        Materiale = MongoRepository.Instance.GetAsync<Materiale>(item.Materiale.Id).Result,
                        OdV_DataConsegna = item.OdV_DataConsegna,
                        PezziProdotti = item.PezziProdotti,
                        PezziScartati = item.PezziScartati,
                        Fasi = item.Fasi,
                        MacchinaCorrente = macchina?.Macchina,
                        DistintaBase= item.DistintaBase
                    };

                    d.Ordini.Add(batchSummary);
                }


                lock (this)
                {
                    foreach (var o in d.Ordini)
                    {
                        int oo = 0;
                        if (!_oeePrev.TryGetValue(o.Id, out oo))
                            _oeePrev.Add(o.Id, o.OEE);
                        else
                            _oeePrev[o.Id] = o.OEE;

                        o.OEEPrecedente = oo;

                        if (o.OEEPrecedente < o.OEE)
                            o.OEEAndamento = AndamentoValore.InSalita;
                        else if (o.OEEPrecedente > o.OEE)
                            o.OEEAndamento = AndamentoValore.InDiscesa;
                        else o.OEEAndamento = AndamentoValore.Stabile;

                        //double d0 = o.PezziDaProdurre > 0 ? ((double)o.PezziProdotti / (double)o.PezziDaProdurre) : 0;
                        //o.PercCompletamento = (d0 <= 1) ? Convert.ToInt32(d0 * 100) : 100;

                        var fasiInPausa = o.Fasi.Where(o1 => o1.Stato == StatoFasi.InLavorazioneInPausa || o1.Stato == StatoFasi.InLavorazioneInPausaS);
                        var fasiInAllarme = o.Fasi.Where(o1 => o1.Stato == StatoFasi.InLavorazioneInEmergenza);

                        if (fasiInPausa.Any())
                            o.Stato = StatoBatch.InLavorazioneInPausa;
                    }
                }




                d.StatoOrdini = new Dictionary<StatoBatch, int>();
                d.StatoOrdini.Add(StatoBatch.InPausa, d.Ordini.Count(o => o.Stato == StatoBatch.InPausa || o.Stato == StatoBatch.InLavorazioneInPausa));
                d.StatoOrdini.Add(StatoBatch.InAttesa, d.Ordini.Count(o => o.Stato == StatoBatch.InAttesa));
                d.StatoOrdini.Add(StatoBatch.InLavorazione, d.Ordini.Count(o => o.Stato == StatoBatch.InLavorazioneInEmergenza  || o.Stato == StatoBatch.InLavorazione));
                d.OrdiniInAllarme = d.Ordini.Count(o => o.Stato == StatoBatch.InLavorazioneInEmergenza);
                d.MacchinariAttiviOraPrec = 1;
                d.OperatoriAttivi = SessionManager.Instance.Count();

                d.OEEMedia = Convert.ToInt32(d.Ordini.DefaultIfEmpty(new BatchSummary() { OEE = 0 }).Average(o => o.OEE));
                d.ConsumoEnergiaMezzora = new Dictionary<string, double>();
                

                var rnd = new Random();
                for (int i = 0; i <= 48 * 2; i++)
                    d.ConsumoEnergiaMezzora.Add(i.ToString(), rnd.Next(60, 70));


                _Dashboard = d;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }

        
            return d;

        }


    }
}
