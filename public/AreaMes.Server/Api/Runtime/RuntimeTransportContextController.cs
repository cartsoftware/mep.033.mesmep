﻿
using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.SignalR;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Runtime
{
    [RoutePrefix("api/RuntimeTransportContext")]
    public class RuntimeTransportContextController : ApiController, IRuntimeTransportContextController
    {

        private static RuntimeTransportContextController _instance;
        public static RuntimeTransportContextController Instance { get { return _instance; } }
        static RuntimeTransportContextController() {
            _instance = new RuntimeTransportContextController();
            MessageServices.Instance.Subscribe<SetBatchInfo>("SetBatchInfo", async (o, e) => {
                await _instance.SetBatch(e);
            });

        }



        [Route("GetSfo")]
        [HttpGet]
        public List<Batch> GetSfo(string idStato, string Username)
        {
            if (!MongoRepository.Instance.IsRun)
                return null;

            List<Batch> batchList = AreaMes.Server.BatchManager.Instance.BatchInMemory();

            if (batchList == null)
                return null;


            //nuova modifica Lorenzo da visionare con albert 
            //modifica utile per il resolve della macchina e la visualizzazione dei dati macchina (nome, codice)
            foreach (var item in batchList) {
                item?.DistintaBase?.Fasi[0]?.Macchine[0]?.Macchina.Resolve();
                item?.Fasi[0]?.Macchine[0]?.Macchina?.Tipologia.Resolve();
                if (item?.QueueId != null)
                    item?.QueueId.Resolve();
              
            }



            if (idStato != "-1")
            {
                StatoBatch statusFilter = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                batchList = batchList.Where(o => o.Stato == statusFilter).ToList();

            }
            else
            {
                // filtro i batch per gli stati
                batchList = batchList.Where(o => o.Stato != StatoBatch.Eliminato &&
                                                 o.Stato != StatoBatch.Terminato &&
                                                 o.Stato != StatoBatch.TerminatoAbortito &&
                                                 o.Stato != StatoBatch.InModifica).ToList();
            }

            // ricreazione dell'oggetto semplificato
            batchList = batchList.Select(o =>
                new Batch
                {
                    Id = o.Id,
                    Inizio = o.Inizio,
                    Fine = o.Fine,
                    DistintaBase = o.DistintaBase,
                    Fasi = o.Fasi,
                    PercCompletamento = o.PercCompletamento,
                    OdP = o.OdP,
                    OdPParziale = o.OdPParziale,
                    CreateDate = o.CreateDate,
                    Commessa = o.Commessa,
                    Stato = o.Stato,
                    OEE = o.OEE,
                    OEEAndamento = o.OEEAndamento,
                    OEEPrecedente = o.OEEPrecedente,
                    PezziDaProdurre = o.PezziDaProdurre,
                    PezziProdotti = o.PezziProdotti,
                    PezziScartati = o.PezziScartati,
                    UserLocked = o.UserLocked,
                    UserLockedDate = o.UserLockedDate,
                    QueueId=o.QueueId,
                    
                }).ToList();


            // ----------------------------------------
            // Gestione operatore / login
            var lComp = MongoRepository.Instance.Collection<Operatore>().FindAsync(Builders<Operatore>.Filter.Eq("Username", Username)).Result.FirstOrDefault();
            if (lComp != null)
                lComp.OperatoreCompetenza?.Resolve();

            if (!String.IsNullOrEmpty(Username))
                batchList = batchList.Where(o => o.Fasi.Any(a =>
                   (a.DistintaBaseFase.Manodopera == null) ||
                   (a.DistintaBaseFase.Manodopera.All(b => b.OperatoreCompetenza == null) ||
                   (a.DistintaBaseFase.Manodopera != null && a.DistintaBaseFase.Manodopera.Any(x => x.OperatoreCompetenza?.Id == lComp?.OperatoreCompetenza?.Id))))).ToList();
            // ----------------------------------------

            return batchList;
        }



        [Route("GetSfoFromMachine")]
        [HttpGet]
        public List<Batch> GetSfoFromMachine(string idMacchina, string idStato, string Username)
        {
            if (idStato != "-1")
            {
                StatoBatch MyStatus = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                var filterBatch = Builders<Batch>.Filter.Eq("Stato", MyStatus) &
                                  Builders<Batch>.Filter.Eq("Fasi.Macchine.Macchina._id", idMacchina);
                var batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToList();


                if (DashboardController._Dashboard != null)
                    foreach (var item in batchList)
                    {
                        var firstOrDefault = DashboardController._Dashboard.Ordini.FirstOrDefault(o => o.Id == item.Id);
                        if (firstOrDefault != null)
                            item.PercCompletamento = firstOrDefault.PercCompletamento;
                    }


                return batchList;
            }
            else
            {
                // filtro i batch per gli stati
                var filterBatch = Builders<Batch>.Filter.Where(o => o.Stato != StatoBatch.Eliminato && o.Stato != StatoBatch.Terminato) &
                                  Builders<Batch>.Filter.Eq("Fasi.Macchine.Macchina._id", idMacchina);
                var batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToList();

                if (DashboardController._Dashboard!=null)
                    foreach (var item in batchList)
                    {
                        var firstOrDefault = DashboardController._Dashboard.Ordini.FirstOrDefault(o => o.Id == item.Id);
                        if (firstOrDefault != null)
                            item.PercCompletamento = firstOrDefault.PercCompletamento;
                    }
                return batchList;
            }            
        }



        [Route("GetFasiFromBatch")]
        [HttpGet]
        public List<BatchFase> GetFasiFromBatch(string idBatch, string Username)
        {
            // recupero le eventuali distinte basi per quella macchina
            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);

            if (batch == null)
                return null;

            List<BatchFase> listOfFasi = batch.Fasi.OrderBy(o => o.NumeroFase).ToList();

            // ----------------------------------------
            // Gestione operatore / login
            var lComp = MongoRepository.Instance.Collection<Operatore>().FindAsync(Builders<Operatore>.Filter.Eq("Username", Username)).Result.FirstOrDefault();
            if (lComp != null)
                lComp.OperatoreCompetenza?.Resolve();

            if (!String.IsNullOrEmpty(Username))
                listOfFasi = listOfFasi.Where(o =>
                            (o.DistintaBaseFase.Manodopera == null) ||
                            (o.DistintaBaseFase.Manodopera.All(b => b.OperatoreCompetenza == null) ||
                            o.DistintaBaseFase.Manodopera.Any(y => y.OperatoreCompetenza?.Id == lComp?.OperatoreCompetenza?.Id))).ToList();

            // ----------------------------------------

            return listOfFasi;
        }



        [Route("GetBatch")]
        [HttpGet]
        public Batch GetBatch(string idBatch, string Username)
        {
            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);

            return batch;
        }





        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="faseInfo"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetFase")]
        [HttpPost]
        public async Task<string> SetFase(SetFaseInfo faseInfo)
        {
            string loggedUser = faseInfo.Username; 
            string idBatch = faseInfo.idBatch;
            BatchFase fase = faseInfo.fase;

            var batch = BatchManager.Instance.Get(idBatch);
            var bav = new FaseChangedInfo { Username = faseInfo.Username, NoSetupQueue = faseInfo.NoSetupQueue };
            var batchCorrentiInLavorazione = BatchManager.Instance.BatchInMemory().Where(o => BatchUtils.IsInLavorazione(o) && o.Id != idBatch);

                lock (batch)
                {
                    // se abbiamo avviato lo start su una fase andiamo a settare le compentenze dell'operatore che ha premuto lo start 
                    // devo controllare che lo stato della fase PRIMA dell'aggiornamento a InLavorazione NON sia InLavorazioneInPausa
                    var faseInMemory = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);

                    if (fase.Stato == StatoFasi.InLavorazione)
                    {
                        // viene verificato se è presente un batch in lavorazione che interessa la macchina in uso per la fase ricevuta in avvio
                        var macchinaPerFaseCorrente = faseInMemory.Macchine.FirstOrDefault();
                        if ((macchinaPerFaseCorrente == null) || (String.IsNullOrEmpty(macchinaPerFaseCorrente.Macchina.Codice)))
                        {
                            var batchCorrentInLavorazione = batchCorrentiInLavorazione.FirstOrDefault();
                            //if (batchCorrentInLavorazione != null)
                            //    throw new Exception(String.Format("Non è possibile avviare l'ordine corrente, poichè è attivo l'ordine '{0}' - Odp Parziale:'{1}'", batchCorrentInLavorazione.OdP, batchCorrentInLavorazione.OdPParziale));
                        }
                        else
                        {

                            //var macchineInLavorazione = batchCorrentiInLavorazione.SelectMany(o => o.Fasi)
                            //                                              .Where(o=> BatchUtils.IsInLavorazione(o))
                            //                                              .SelectMany(o => o.Macchine)
                            //                                              .FirstOrDefault(o => o.Macchina.Id == macchinaPerFaseCorrente.Macchina.Id);

                            //if (macchineInLavorazione != null)
                            //    throw new Exception(String.Format("Attenzione non è possibile avviare l'ordine, poichè la macchina '{0}' risulta in uso da un altro ordine!", macchineInLavorazione.Macchina.Codice));
                        }



                        // viene verificato se è presente un lock sulla fase e non è possibile avviarla
                        if (!String.IsNullOrWhiteSpace(faseInMemory.BloccoAvanzamentoCausa))
                            throw new Exception(faseInMemory.BloccoAvanzamentoCausa);
                    }

                    //
                    if (fase.Stato == StatoFasi.InLavorazione && faseInMemory.Stato != StatoFasi.InLavorazioneInPausa)
                    {
                        var filterOp = Builders<Operatore>.Filter.Eq("Username", fase.UserLocked);
                        var op = MongoRepository.Instance.Collection<Operatore>().Find<Operatore>(filterOp).FirstOrDefault();

                        var x = fase.Manodopera.Select(o => { o.Inizio = DateTime.Now; o.Operatore = op; return o; }).ToList();
                        fase.Manodopera = x;
                        // aggiorniamo la data di inizio per le macchine
                        foreach (var item in fase.Macchine)
                            if (!item.Inizio.HasValue)
                                item.Inizio = DateTime.Now;

                        if (fase.Inizio.IsNull())
                            fase.Inizio = DateTime.Now;
                    }

                    if (fase.Stato == StatoFasi.Terminato || fase.Stato == StatoFasi.Abortito)
                        fase.Fine = DateTime.Now;


                    // AVANZAMENTI FASI
                    if (batch.BatchAvanzamenti == null) batch.BatchAvanzamenti = new List<BatchAvanzamento>();



                    var av = batch.BatchAvanzamenti.FirstOrDefault(a => a.NumeroFase == fase.NumeroFase && a.Fine == null);
                    if (av == null)
                    {
                        av = new BatchAvanzamento();
                        av.Id = idBatch;
                        av.Inizio = DateTime.Now;
                        av.NumeroFase = fase.NumeroFase;
                        av.CodiceFase = fase.DistintaBaseFase.Codice;
                        av.BarcodeFase = fase.DistintaBaseFase.Barcode;
                        av.Stato = fase.Stato;
                        av.Nota = fase.Stato.ToString();
                        av.BatchFaseMacchina = new BatchFaseMacchina { Inizio = DateTime.Now };
                        av.BatchFaseManoDopera = new BatchFaseManoDopera { Inizio = DateTime.Now };
                        av.UserLocked = faseInfo.Username;
                        batch.BatchAvanzamenti.Add(av);
                        bav.Current = av;
                    }
                    else
                    {
                        av.Fine = DateTime.Now;
                        var an = new BatchAvanzamento();
                        an.Inizio = DateTime.Now;
                        an.Id = idBatch;
                        av.Id = idBatch;
                        an.NumeroFase = fase.NumeroFase;
                        an.CodiceFase = fase.DistintaBaseFase.Codice;
                        an.BarcodeFase = fase.DistintaBaseFase.Barcode;
                        an.Nota = fase.Stato.ToString();
                        an.Stato = fase.Stato;
                        an.BatchFaseMacchina = new BatchFaseMacchina { Inizio = DateTime.Now };
                        an.BatchFaseManoDopera = new BatchFaseManoDopera { Inizio = DateTime.Now };
                        an.UserLocked = faseInfo.Username;
                        batch.BatchAvanzamenti.Add(an);
                        bav.Current = an;
                    }
                }

         


            lock (batch)
            {
                // update del batch con rispettiva base aggiornata
                if (batch.Fasi.Any(o => o.NumeroFase == fase.NumeroFase))
                {
                    var x = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);
                    batch.Fasi.Remove(x);
                    batch.Fasi.Add(fase);
                }

                // eventuali aggiornamenti del batch
                checkUpdates(batch);
            }

            // notifica inMEMORY
            await MessageServices.Instance.Publish(batch.Id.ToString(), bav);
            AreaMESManager.Instance.NotifyFaseChanged(batch.Id, bav);

            return batch.Id;
        }



        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="utBatch"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetBatch")]
        [HttpPost]
        public async Task<string> SetBatch(SetBatchInfo utBatch)
        {
            var batch = AreaMes.Server.BatchManager.Instance.Get(utBatch.idBatch);
            string loggedUser = utBatch.Username;

            lock (batch)
            {
                var previousState = batch.Stato;
                batch.Stato = utBatch.idStato;

                if (batch.Stato == StatoBatch.InLavorazione)
                    if (previousState == StatoBatch.InLavorazioneInEmergenza)
                    {
                        var bf = batch.Fermi.LastOrDefault();
                        //bf.Fine = DateTime.Now;
                        bf.Fine = utBatch.dataRiavvio;
                        if (utBatch.dataRiavvio.Year == 1)
                            bf.Fine = DateTime.Now;
                        bf.Causale = BatchCausaleFermi.Emergenza;
                        bf.NotaDiRiavvio = utBatch.notaRiavvio;
                    }

                if (batch.Stato == StatoBatch.InLavorazioneInEmergenza)
                {
                    if (batch.Fermi == null)
                        batch.Fermi = new List<BatchFermi>();
                    batch.Fermi.Add(new BatchFermi { Inizio = DateTime.Now, UserLocked = utBatch.Username });
                }

            }

            if (batch.Stato == StatoBatch.Terminato)
            {
                batch.Fine = DateTime.Now;
                await AreaMes.Server.BatchManager.Instance.RemoveAsync(batch.Id);
            }

            AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);

            return utBatch.idBatch;
        }


        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="faseInfo"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>

        [Route("AddPiecesToBatch")]
        [HttpPost]
        public string AddPiecesToBatch(VersamentoBatch data)
        {
            string idBatch = data.idBatch;
            int pezziVersati = int.Parse(data.pezziVersati);
            string numeroFase = data.numeroFase;
            string loggedUser = data.Username;

            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);
            var fase = batch.Fasi.First(f => f.NumeroFase == numeroFase);
            if (loggedUser == fase.UserLocked)
            {
                fase.PezziProdotti += pezziVersati;
                AreaMESManager.Instance.NotifyVersamentoFaseChanged(batch.Id, fase);
            }

            return fase.PezziProdotti.ToString();
        }


        [Route("SendBarcode")]
        [HttpPost]
        public async Task<SendBarcodeResponse> SendBarcode(SendBarcodeRequest data)
        {
            var res = new SendBarcodeResponse();
            try
            {
                var obj = new SendBarcodeMessage {
                    Data = data,
                    Response = res
                };
                await MessageServices.Instance.Publish(SendBarcodeMessage.Name, obj);
            }
            catch (Exception exc)
            {
                res.IsOnError = true;
                res.ErrorMessage = exc.Message;
            }
            return res;
        }



        [Route("ReadBarcode")]
        [HttpPost]
        public async Task<ReadBarcodeResponse> ReadBarcode(ReadBarcodeRequest data)
        {
            var res = new ReadBarcodeResponse();
            try
            {
                var obj = new ReadBarcodeMessage {Data = data, Response = res};
                await MessageServices.Instance.Publish(ReadBarcodeMessage.Name, obj);
            }
            catch (Exception exc)
            {
                res.IsOnError = true;
                res.ErrorMessage = exc.Message;
            }
            return res;
        }



        /// <summary>
        /// Metodo di utility per aggiornare il batch
        /// </summary>
        /// <param name="pBatch"></param>
        /// <returns></returns>
        private Batch checkUpdates(Batch pBatch)
        {
            // se ho lo stato in attesa controllo che almeno una sua fase è partita, aggiorno lo stato e la data di inizio
            if (pBatch.Stato == StatoBatch.InAttesa)
            {
                if (pBatch.Fasi.Any(o => o.Stato == StatoFasi.InLavorazione))
                {
                    pBatch.Stato =  StatoBatch.InLavorazione;
                    pBatch.Inizio = DateTime.Now;
                }
            }

            // inoltre se tutte le fasi sono concluse aggiorno di nuovo il batch
            if (pBatch.Stato != StatoBatch.Terminato)
            {
                if (pBatch.Fasi.All(o => o.Stato == StatoFasi.Terminato))
                {
                    pBatch.Stato = StatoBatch.Terminato;
                    pBatch.Fine = DateTime.Now;
                }
            }

            return pBatch;

        }

        public async Task<string> SetBatchMateriale(string idBatch, List<BatchMateriali> materiali)
        {
            AreaMESManager.Instance.NotifyBatchMaterialeChanged(idBatch, materiali);

            return String.Empty;
        }
    }



}
