﻿
using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.SignalR;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Runtime
{
    [RoutePrefix("api/RuntimeTransportContext")]
    public class RuntimeTransportContextController : ApiController, IRuntimeTransportContextController
    {

        private static RuntimeTransportContextController _instance;
        public static RuntimeTransportContextController Instance { get { return _instance; } }
        static RuntimeTransportContextController() { _instance = new RuntimeTransportContextController(); }



        [Route("GetSfo")]
        [HttpGet]
        public List<Batch> GetSfo(string idStato, string loggedUser)
        {
            List<Batch> batchList = null;
            if (idStato != "-1")
            {
                StatoBatch MyStatus = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(Builders<Batch>.Filter.Eq("Stato", MyStatus)).Result.ToList();
            }
            else
            {
                // filtro i batch per gli stati
                var filterBatch = Builders<Batch>.Filter.Where(o => o.Stato != StatoBatch.Eliminato && 
                                                                    o.Stato != StatoBatch.Terminato && 
                                                                    o.Stato != StatoBatch.InModifica);
                batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToList();
            }

            if (DashboardController._Dashboard != null)
                foreach (var item in batchList)
                {
                    var firstOrDefault = DashboardController._Dashboard.Ordini.FirstOrDefault(o => o.Id == item.Id);
                    if (firstOrDefault != null)
                        item.PercCompletamento = firstOrDefault.PercCompletamento;
                }

            // ----------------------------------------
            // Gestione operatore / login
            var lComp = MongoRepository.Instance.Collection<Operatore>().FindAsync(Builders<Operatore>.Filter.Eq("Username", loggedUser)).Result.FirstOrDefault();
            if (lComp != null)
                lComp.OperatoreCompetenza.Resolve();

            if (!String.IsNullOrEmpty(loggedUser))
                batchList = batchList.Where(o => o.Fasi.Any(a =>
                   (a.DistintaBaseFase.Manodopera == null) ||
                   (a.DistintaBaseFase.Manodopera.All(b=>b.OperatoreCompetenza==null) ||
                   (a.DistintaBaseFase.Manodopera != null && a.DistintaBaseFase.Manodopera.Any(x => x.OperatoreCompetenza?.Id == lComp?.OperatoreCompetenza?.Id))))).ToList();

            // ----------------------------------------

            return batchList;
        }



        [Route("GetSfoFromMachine")]
        [HttpGet]
        public List<Batch> GetSfoFromMachine(string idMacchina, string idStato, string loggedUser)
        {
            if (idStato != "-1")
            {
                StatoBatch MyStatus = (StatoBatch)Enum.Parse(typeof(StatoBatch), idStato, true);
                var filterBatch = Builders<Batch>.Filter.Eq("Stato", MyStatus) &
                                  Builders<Batch>.Filter.Eq("Fasi.Macchine.Macchina._id", idMacchina);
                var batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToList();


                if (DashboardController._Dashboard != null)
                    foreach (var item in batchList)
                    {
                        var firstOrDefault = DashboardController._Dashboard.Ordini.FirstOrDefault(o => o.Id == item.Id);
                        if (firstOrDefault != null)
                            item.PercCompletamento = firstOrDefault.PercCompletamento;
                    }


                return batchList;
            }
            else
            {
                // filtro i batch per gli stati
                var filterBatch = Builders<Batch>.Filter.Where(o => o.Stato != StatoBatch.Eliminato && o.Stato != StatoBatch.Terminato) &
                                  Builders<Batch>.Filter.Eq("Fasi.Macchine.Macchina._id", idMacchina);
                var batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(filterBatch).Result.ToList();

                if (DashboardController._Dashboard!=null)
                    foreach (var item in batchList)
                    {
                        var firstOrDefault = DashboardController._Dashboard.Ordini.FirstOrDefault(o => o.Id == item.Id);
                        if (firstOrDefault != null)
                            item.PercCompletamento = firstOrDefault.PercCompletamento;
                    }
                return batchList;
            }            
        }



        [Route("GetFasiFromBatch")]
        [HttpGet]
        public List<BatchFase> GetFasiFromBatch(string idBatch, string loggedUser)
        {
            List<BatchFase> listOfFasi = null;
            // recupero le eventuali distinte basi per quella macchina
            //var filterDb = Builders<Batch>.Filter.Eq("_id", idBatch);
            //var batch = MongoRepository.Instance.Collection<Batch>().Find<Batch>(filterDb).FirstOrDefault();
            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch); 

            // ciclo provvisorio per nascondere la fase figlia di una fase ciclica
            var x = batch.DistintaBase.Fasi.Where(o=>o.IsCiclica);
            var f = new List<BatchFase>();
            foreach (var db in x)
            {
                string nfbase =(int.Parse( db.NumeroFase) + 1).ToString();
                f.Add(batch.Fasi.FirstOrDefault(o=>o.NumeroFase == nfbase));
            }

            listOfFasi =  batch.Fasi.Where(l=> !f.Contains(l)).OrderBy(o => o.NumeroFase).ToList();


            // ----------------------------------------
            // Gestione operatore / login
            var lComp = MongoRepository.Instance.Collection<Operatore>().FindAsync(Builders<Operatore>.Filter.Eq("Username", loggedUser)).Result.FirstOrDefault();
            if (lComp != null)
                lComp.OperatoreCompetenza.Resolve();

            if (!String.IsNullOrEmpty(loggedUser))
                listOfFasi = listOfFasi.Where(o =>
                            (o.DistintaBaseFase.Manodopera == null) ||
                            (o.DistintaBaseFase.Manodopera.All(b => b.OperatoreCompetenza == null) ||
                            o.DistintaBaseFase.Manodopera.Any(y => y.OperatoreCompetenza?.Id == lComp?.OperatoreCompetenza?.Id))).ToList();
            // ----------------------------------------

            return listOfFasi;
        }



        [Route("GetBatch")]
        [HttpGet]
        public Batch GetBatch(string idBatch, string loggedUser)
        {
            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);

            return batch;
        }





        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="utBatch"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetStateFase")]
        [HttpPost]
        public string SetStateFase(UtilityBatchFase utBatch)
        {
            string loggedUser = utBatch.loggedUser; 
            string idBatch = utBatch.idBatch;
            BatchFase fase = utBatch.fase;

            var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);
            var bav = new BatchEventMessage();

            lock (batch)
            {
                // se abbiamo avviato lo start su una fase andiamo a settare le compentenze dell'operatore che ha premuto lo start 
                // devo controllare che lo stato della fase PRIMA dell'aggiornamento a InLavorazione NON sia InLavorazioneInPausa
                var y = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);

                if (fase.Stato == StatoFasi.InLavorazione && y.Stato != StatoFasi.InLavorazioneInPausa)
                {
                    var filterOp = Builders<Operatore>.Filter.Eq("Username", fase.UserLocked);
                    var op = MongoRepository.Instance.Collection<Operatore>().Find<Operatore>(filterOp).FirstOrDefault();

                    var x = fase.Manodopera.Select(o => { o.Inizio = DateTime.Now; o.Operatore = op; return o; }).ToList();
                    fase.Manodopera = x;
                    // aggiorniamo la data di inizio per le macchine
                    foreach (var item in fase.Macchine)
                        if (!item.Inizio.HasValue)
                            item.Inizio = DateTime.Now;

                    if (fase.Inizio.IsNull())
                        fase.Inizio = DateTime.Now;
                }

                if (fase.Stato == StatoFasi.Terminato || fase.Stato == StatoFasi.Abortito)
                    fase.Fine = DateTime.Now;

                //if (fase.Stato == StatoFasi.InLavorazioneInPausa)
                //{
                //    batch.TotaleFermi++;
                //}



                // AVANZAMENTI FASI
                if (batch.BatchAvanzamenti == null) batch.BatchAvanzamenti = new List<BatchAvanzamento>();

               

                var av = batch.BatchAvanzamenti.FirstOrDefault(a => a.NumeroFase == fase.NumeroFase && a.Fine == null);
                if (av == null)
                {
                    av = new BatchAvanzamento();
                    av.Inizio = DateTime.Now;
                    av.NumeroFase = fase.NumeroFase;
                    av.CodiceFase = fase.DistintaBaseFase.Codice;
                    av.BarcodeFase = fase.DistintaBaseFase.Barcode;
                    av.Stato = fase.Stato;
                    av.Nota = fase.Stato.ToString();
                    av.BatchFaseMacchina = new BatchFaseMacchina { Inizio = DateTime.Now };
                    av.BatchFaseManoDopera = new BatchFaseManoDopera { Inizio = DateTime.Now };
                    batch.BatchAvanzamenti.Add(av);
                    bav.Current = av;
                }
                else
                {
                    av.Fine = DateTime.Now;
                    var an = new BatchAvanzamento();
                    an.Inizio = DateTime.Now;
                    an.NumeroFase = fase.NumeroFase;
                    an.CodiceFase = fase.DistintaBaseFase.Codice;
                    an.BarcodeFase = fase.DistintaBaseFase.Barcode;
                    an.Nota = fase.Stato.ToString();
                    an.Stato = fase.Stato;
                    an.BatchFaseMacchina = new BatchFaseMacchina { Inizio = DateTime.Now };
                    an.BatchFaseManoDopera = new BatchFaseManoDopera { Inizio = DateTime.Now };
                    batch.BatchAvanzamenti.Add(an);
                    bav.Current = an;
                }
            }



            // notifica inMEMORY
            MessageServices.Instance.Publish(batch.Id, bav);
            // notifica ai client
            AreaMESManager.Instance.NotifyFaseChanged(batch.Id, bav);


            lock (batch)
            {
                // update del batch con rispettiva base aggiornata
                if (batch.Fasi.Any(o => o.NumeroFase == fase.NumeroFase))
                {
                    var x = batch.Fasi.FirstOrDefault(o => o.NumeroFase == fase.NumeroFase);
                    batch.Fasi.Remove(x);
                    batch.Fasi.Add(fase);
                }

                // eventuali aggiornamenti del batch
                checkUpdates(batch);
            }

            return batch.Id;
        }



        /// <summary>
        /// Set dello stato per la fase scelta
        /// </summary>
        /// <param name="utBatch"> oggetto contente l'id del batch e la fase</param>
        /// <returns></returns>
        [Route("SetStateBatch")]
        [HttpPost]
        public string SetStateBatch(UtilityBatch utBatch)
        {
            var batch = AreaMes.Server.BatchManager.Instance.Get(utBatch.idBatch);
            string loggedUser = utBatch.loggedUser;

            lock (batch)
            {
                var previousState = batch.Stato;
                batch.Stato = utBatch.idStato;

                if (batch.Stato == StatoBatch.InLavorazione)
                    if (previousState == StatoBatch.InLavorazioneInEmergenza)
                    {
                        var bf = batch.Fermi.LastOrDefault();
                        //bf.Fine = DateTime.Now;
                        bf.Fine = utBatch.dataRiavvio;
                        bf.Causale = BatchCausaleFermi.Emergenza;
                        bf.NotaDiRiavvio = utBatch.notaRiavvio;
                    }

                if (batch.Stato == StatoBatch.InLavorazioneInEmergenza)
                {
                    if (batch.Fermi == null)
                        batch.Fermi = new List<BatchFermi>();
                    batch.Fermi.Add(new BatchFermi { Inizio = DateTime.Now });
                }

            }

            if (batch.Stato == StatoBatch.Terminato)
            {
                batch.Fine = DateTime.Now;
                AreaMes.Server.BatchManager.Instance.Remove(batch.Id);
            }

            AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);

            return utBatch.idBatch;
            //return MongoRepository.Instance.Set(batch).Result;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="batch"></param>
        /// <returns></returns>
        //[Route("AddPiecesToBatch")]
        //[HttpPost]
        //public string AddPiecesToBatch(Batch pBatch)
        //{
        //    //var filterDb = Builders<Batch>.Filter.Eq("_id", pBatch.Id);
        //    //var batch = MongoRepository.Instance.Collection<Batch>().Find<Batch>(filterDb).FirstOrDefault();
        //    var batch = AreaMes.Server.BatchManager.Instance.Get(pBatch.Id);
        //    batch.PezziProdotti++;
        //    return pBatch.Id;

        //    //return MongoRepository.Instance.Set(batch).Result;
        //}
        [Route("AddPiecesToBatch")]
        [HttpPost]
        public string AddPiecesToBatch(VersamentoBatch data)
        {
            string idBatch = data.idBatch;
            int pezziVersati = int.Parse(data.pezziVersati);
            string numeroFase = data.numeroFase;
            string loggedUser = data.loggedUser;

        var batch = AreaMes.Server.BatchManager.Instance.Get(idBatch);
            var fase = batch.Fasi.First(f => f.NumeroFase == numeroFase);
            fase.PezziProdotti += pezziVersati;

            AreaMESManager.Instance.NotifyBatchChanged(batch.Id, batch);

            return fase.PezziProdotti.ToString();

            //return MongoRepository.Instance.Set(batch).Result;
        }



        /// <summary>
        /// Metodo di utility per aggiornare il batch
        /// </summary>
        /// <param name="pBatch"></param>
        /// <returns></returns>
        private Batch checkUpdates(Batch pBatch)
        {
            // se ho lo stato in attesa controllo che almeno una sua fase è partita, aggiorno lo stato e la data di inizio
            if (pBatch.Stato == StatoBatch.InAttesa)
            {
                if (pBatch.Fasi.Any(o => o.Stato == StatoFasi.InLavorazione))
                {
                    pBatch.Stato =  StatoBatch.InLavorazione;
                    pBatch.Inizio = DateTime.Now;
                }
            }

            // inoltre se tutte le fasi sono concluse aggiorno di nuovo il batch
            if (pBatch.Stato != StatoBatch.Terminato)
            {
                if (pBatch.Fasi.All(o => o.Stato == StatoFasi.Terminato))
                {
                    pBatch.Stato = StatoBatch.Terminato;
                    pBatch.Fine = DateTime.Now;
                }
            }

            return pBatch;

        }

    }



}
