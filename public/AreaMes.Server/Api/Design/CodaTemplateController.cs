﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using MongoDB.Bson;
using MongoDB.Driver;
using static AreaMes.Model.BatchUtils;
namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/codatemplate")]
    public class CodaTemplateController : BaseController<CodaTemplate>
    {
       
        [ActionName("GetAll")]
        public async override Task<IEnumerable<CodaTemplate>> GetAll()
        {
            var x = await MongoRepository.Instance.All<CodaTemplate>(new string[] { }, new string[] { }); // campi esclusi
            for (int i = 0; i < x.Count; i++)
            {
                if (x[i].Macchina != null)
                    x[i].Macchina.Resolve();
                if (x[i].Materiale != null)
                    x[i].Materiale.Resolve();
            }

            return x;
        }

        [ActionName("Get")]
        public async override Task<CodaTemplate> Get(string id)
        {
            var a = await MongoRepository.Instance.GetAsync<CodaTemplate>(id);
            if (a == null) return null;

            if (a.Macchina != null)
                a.Macchina.Resolve();
            if (a.Materiale != null)
                a.Materiale.Resolve();

            return a;
        }


        [HttpPost]
        [HttpPut]
        public async override Task<CodaTemplate> Set([FromBody] CodaTemplate item)
        {

            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);

            //----- Cehck licence ------------------------------------------
            if (LicenceManager.Instance.IsValid == false)
                throw new Exception(translations.FirstOrDefault(o => o.Key == "licenceManager_licenceNotValid")?.Value);


            ////----- Data Validation ------------------------------------------

            if (String.IsNullOrWhiteSpace(item.Nome))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_nameQueueNotValid").Value;
                throw new Exception(String.Format(message));
            }

            ////----- Data Validation ------------------------------------------

            if (String.IsNullOrWhiteSpace(item.NomeTemplate))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_nameQueueNotValid").Value;
                throw new Exception(String.Format(message));
            }
            ////----- Data Validation batch ------------------------------------------

            if (item.Batch.Count < 1)
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_queueBatchNotValid").Value;
                throw new Exception(String.Format(message));
            }

            //----- Data Validation -----------------------------------------------
            item.Uniquefy();
            item.LastUpdate = DateTime.Now;

            // viene cercato se esiste un batch per lo stesso ODP
            var batchFoundedByNomeTemplate = MongoRepository.Instance.Collection<CodaTemplate>().FindAsync(Builders<CodaTemplate>.Filter.Eq("NomeTemplate", item.NomeTemplate)).Result.ToList();

            if (batchFoundedByNomeTemplate.Any(o => o.Id != item.Id))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_queueNotValid").Value;
                throw new Exception(String.Format(message + "'{0}'", item.Nome));
            }

            
            //Se lo stato è "inAttesa" e "inModica" allora genero la coda e i batch al suo interno
            if (item.Stato == StatoBatch.InAttesa || item.Stato == StatoBatch.InModifica) {
                // viene cercato se esiste una coda per lo stesso Nome
                var batchFoundedByNome = MongoRepository.Instance.Collection<Coda>().FindAsync(Builders<Coda>.Filter.Eq("Nome", item.Nome)).Result.ToList();
                if (batchFoundedByNome.Any(o => o.Id != item.Id))
                {
                    string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_queueNotValid").Value;
                    throw new Exception(String.Format(message + "'{0}'", item.Nome));
                   
                }
                //strutturo e creo  la coda 
                Coda coda = new Coda();
                if (coda.CreateDate.IsNull())
                    item.CreateDate = DateTime.Now;
                coda.Stato = item.Stato;
                coda.Id = null;
                coda.Uniquefy();
                coda.Nome = item.Nome;
                coda.Commessa = item.Commessa;
                coda.Macchina = item.Macchina;
                coda.Materiale = item.Materiale;
                coda.Batch = new List<ExternalDoc<Batch>>();
            


            //creo i batch
            for (int i = 0; i < item.Batch.Count; i++)
                {
                    //assegno odp e id e tutti i parametri 
                    item.Batch[i].Id = null;
                    item.Batch[i].Uniquefy();
                    item.Batch[i].LastUpdate = DateTime.Now;
                    var odp=await BatchUtils.GetAutoCounterInfo(MongoRepository.Instance, true);
                    item.Batch[i].OdP =Convert.ToString(odp.Global);
                    item.Batch[i].OdPParziale = Convert.ToString(odp.Global);
                    item.Batch[i].Stato = item.Stato;
                    var EditedParams = item.Batch[i].DistintaBase?.DistintaBaseParametri;
                    if (item.Batch[i].Materiale != null)
                        item.Batch[i].Materiale = await MongoRepository.Instance.GetAsync<Materiale>(item.Batch[i].Materiale.Id);

                    if (item.Batch[i].DistintaBase == null)
                        item.Batch[i].DistintaBase = await MongoRepository.Instance.GetAsync<DistintaBase>(item.Batch[i].DistintaBase.Id);

                    if (EditedParams != null && EditedParams.Count == item.Batch[i].DistintaBase.DistintaBaseParametri.Count)
                        item.Batch[i].DistintaBase.DistintaBaseParametri = EditedParams;

                    foreach (var par in item.Batch[i].DistintaBase.DistintaBaseParametri)
                        par.UnitaDiMisura?.Resolve();
                    if (item.CreateDate.IsNull())
                        item.CreateDate = DateTime.Now;

                    //assegno al batch externaldoc della coda
                    item.Batch[i].QueueId = new ExternalDoc<Coda>();
                    item.Batch[i].QueueId.Id = coda.Id;
                    //assegno ai batch externaldoc dei batch
                    var batch = new ExternalDoc<Batch>();
                    batch.Id = item.Batch[i].Id;
                    coda.Batch.Add(batch);
                     
                    //se lo stato è inAttesa allora lancio il batch 
                    if (item.Stato == StatoBatch.InAttesa)
                    {
                        await BatchUtils.PrepareBatchTo_InAttesa(item.Batch[i], MongoRepository.Instance);
                        BatchManager.Instance.StartAsync(item.Batch[i]);
                    }
                   
                    //creo il batch
                    await MongoRepository.Instance.SetAsync(item.Batch[i]);

                }

                //creo la coda
                await MongoRepository.Instance.SetAsync(coda);

            }


            //risetto lo stato del template in creazione cosi nel salvataggio non creo modifiche allo stato 
            item.Stato = StatoBatch.InCreazione;
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);

            //var coda_delete = await MongoRepository.Instance.GetAsync<Coda>(id);
            //foreach (var batch in coda_delete.Batch)
            //{
            //    var batch_update = await MongoRepository.Instance.GetAsync<Batch>(batch.Id);
            //    batch_update.QueueId = null;
            //    await MongoRepository.Instance.SetAsync(batch_update);
            //}


            await AreaMes.Server.BatchManager.Instance.RemoveAsync(id);
            return await MongoRepository.Instance.DeleteAsync<CodaTemplate>(id);
        }

        [HttpPost]
        [ActionName("getBatchAutoCounterInfo")]
        public async Task<BatchAutoCounter> GetBatchAutoCounterInfo(bool getNew)
        {
            return await BatchUtils.GetAutoCounterInfo(MongoRepository.Instance, getNew);
        }

    }
}
