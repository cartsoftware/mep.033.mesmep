﻿using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class OperazioniCicloController : BaseController<OperazioneFase>
    {
        public async override Task<IEnumerable<OperazioneFase>> GetAll()
        {
            var x = await MongoRepository.Instance.All<OperazioneFase>(new string[] {}, new string[] {}); // campi esclusi

            return x;
        }


        public async override Task<OperazioneFase> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<OperazioneFase>(id);
            

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<OperazioneFase> Set([FromBody] OperazioneFase item)
        {
            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }


    }
}
