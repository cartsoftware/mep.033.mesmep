﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class OperatoreCompetenzaController : BaseController<OperatoreCompetenza>
    {
        public async override Task<IEnumerable<OperatoreCompetenza>> GetAll()
        {
            return await MongoRepository.Instance.All<OperatoreCompetenza>(new string[] { }); // campi esclusi
        }


        public async override Task<OperatoreCompetenza> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<OperatoreCompetenza>(id);

            return a;

        }

        

        [HttpPost]
        [HttpPut]
        public async override Task<OperatoreCompetenza> Set([FromBody] OperatoreCompetenza item)
        {
            //controllo se è presente la lingua nell heder escarico la lingua dell'operatore , in caso di errore lascio it come default
            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);

            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_codeNotValid").Value;
                throw new Exception(String.Format(message));
            }

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<OperatoreCompetenza>().AsQueryable().Where(k => k.Codice ==item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_codeNotValid_duplicate").Value;
                throw new Exception(String.Format(message + "'{0}'", item.Codice));

            }
            //----- Data Validation ----------------------


            item.Uniquefy();
             await MongoRepository.Instance.SetAsync(item);
            return item;
        }


    }
}
