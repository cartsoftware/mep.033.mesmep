﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class DistintaBaseFasiController : BaseController<DistintaBaseFasi>
    {
        public async override Task<IEnumerable<DistintaBaseFasi>> GetAll()
        {
            var x = await MongoRepository.Instance.All<DistintaBaseFasi>(new string[] { }, new string[] { }); // campi esclusi

            return x;
        }


        public async override Task<DistintaBaseFasi> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<DistintaBaseFasi>(id);


            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<DistintaBaseFasi> Set([FromBody] DistintaBaseFasi item)
        {
            item.Uniquefy();
             await MongoRepository.Instance.SetAsync(item);
            return item;
        }


    }
}
