﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    public class MaterialeController : BaseController<Materiale>
    {

        public async override Task<IEnumerable<Materiale>> GetAll()
        {
            return await MongoRepository.Instance.All<Materiale>(new string[] { "Descrizione","TipoMateriale", "PesoLordo", "PesoNetto" }); // campi esclusi
        }


        public async override Task<Materiale> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Materiale>(id);
            

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<Materiale> Set([FromBody] Materiale item)
        {
            //controllo se è presente la lingua nell heder escarico la lingua dell'operatore , in caso di errore lascio it come default
            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);

            //----- Cehck licence ------------------------------------------
            if (LicenceManager.Instance.IsValid == false)
                throw new Exception(translations.FirstOrDefault(o => o.Key == "licenceManager_licenceNotValid")?.Value);

            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_codeNotValid").Value;
                throw new Exception(String.Format(message));
            }



            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Materiale>().AsQueryable().Where(k => k.Codice == item.Codice).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_codeNotValid_duplicate").Value;
                throw new Exception(String.Format(message + "'{0}'", item.Codice));

            }
            //----- Data Validation ----------------------

            item.Uniquefy();
             await MongoRepository.Instance.SetAsync(item);
            return item;
        }


        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<Materiale>(id);
        }

    }
}
