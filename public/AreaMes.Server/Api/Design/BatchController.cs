﻿using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using static AreaMes.Model.BatchUtils;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/batch")]
    public class BatchController : BaseController<Batch>
    {
        [ActionName("GetAll")]
        public async override Task<IEnumerable<Batch>> GetAll()
        {
            var x = await MongoRepository.Instance.All<Batch>(new string[] { "BatchParametri", "BatchAvanzamenti", "BatchMateriali", "Campionature", "Fermi", "Fasi" }, new string[] {}); // campi esclusi

            return x;
        }

        [ActionName("Get")]
        public async override Task<Batch> Get(string id)
        {
            var a = await MongoRepository.Instance.GetAsync<Batch>(id);
            if (a == null) return null;

            if (a.DistintaBase.DistintaBaseParametri != null)
                foreach (var item in a.DistintaBase.DistintaBaseParametri)
                    item.UnitaDiMisura?.Resolve();

   

            if (a.Fermi == null)
                a.TempoTotaleFermi = 0;
            else
                a.TempoTotaleFermi = a.Fermi.DefaultIfEmpty(new BatchFermi()).Sum(o => ((o.Fine.HasValue ? o.Fine.Value : DateTime.Now) - (o.Inizio.HasValue ? o.Inizio.Value : DateTime.Now)).TotalSeconds);

            return a;
        }


        [HttpPost]
        [HttpPut]
        public async override Task<Batch> Set([FromBody] Batch item)
        {

            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);

            //----- Cehck licence ------------------------------------------
            if (LicenceManager.Instance.IsValid==false)
                throw new Exception(translations.FirstOrDefault(o=>o.Key == "licenceManager_licenceNotValid")?.Value);


            //----- Data Validation ------------------------------------------
            if (item.Materiale == null)
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_materialNotValid").Value;
                throw new Exception(String.Format(message));
            }

            if (item.DistintaBase == null)
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_listNotValid").Value;
                throw new Exception(String.Format(message));
            }

            if (String.IsNullOrWhiteSpace(item.OdP))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_odpNotValid").Value;
                throw new Exception(String.Format(message));
            }

            if ((Convert.ToInt32(item.DistintaBase.DistintaBaseParametri.FirstOrDefault(o => o.Codice == "MEP_SETUP_LUNGH").ValoreDefault))== 0)
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_lengthNotValid").Value;
                throw new Exception(String.Format(message));
            }

            //----- Data Validation -----------------------------------------------
            item.Uniquefy();
            item.LastUpdate = DateTime.Now;

            // viene cercato se esiste un batch per lo stesso ODP
            var batchFoundedByOdp = MongoRepository.Instance.Collection<Batch>().FindAsync(Builders<Batch>.Filter.Eq("OdP", item.OdP)).Result.ToList();

            if (batchFoundedByOdp.Any(o=> o.Id != item.Id))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_batchNotValid").Value;
                throw new Exception(String.Format(message + "'{0}'", item.OdP));
            }

            if (item.Stato == StatoBatch.InModifica)
            {
                var EditedParams = item.DistintaBase?.DistintaBaseParametri; 
                    
                if (item.Materiale!=null)
                    item.Materiale = await MongoRepository.Instance.GetAsync<Materiale>(item.Materiale.Id);

                if (item.DistintaBase==null)
                    item.DistintaBase = await MongoRepository.Instance.GetAsync<DistintaBase>(item.DistintaBase.Id);

                if (EditedParams != null && EditedParams.Count == item.DistintaBase.DistintaBaseParametri.Count)
                    item.DistintaBase.DistintaBaseParametri = EditedParams;

                foreach (var par in item.DistintaBase.DistintaBaseParametri)
                    par.UnitaDiMisura?.Resolve();
                

                if (item.CreateDate.IsNull())
                    item.CreateDate = DateTime.Now;
            }
            else if (item.Stato == StatoBatch.InAttesa)
            {
                await BatchUtils.PrepareBatchTo_InAttesa(item, MongoRepository.Instance);

                // viene aggiunto l'elemento al sistema
                BatchManager.Instance.StartAsync(item);

            }else if (item.Stato == StatoBatch.TerminatoAbortito)
            {
                item.Fine = DateTime.Now;
                await AreaMes.Server.BatchManager.Instance.RemoveAsync(item.Id);
            }
                
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            //carico le traduzione nel caso dovessi visualizzare dei messaggi
            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);


            //controllo che il batch non appartenga a una coda
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            var batch_delete = await MongoRepository.Instance.GetAsync<Batch>(id);
            if (batch_delete.QueueId != null)
            {
                string message = translations.FirstOrDefault(x => x.Key == "onRemove_validate_batchNotValid").Value;
                throw new Exception(String.Format(message));
            }
            await AreaMes.Server.BatchManager.Instance.RemoveAsync(id);
            return await MongoRepository.Instance.DeleteAsync<Batch>(id);
        }

        [HttpPost]
        [ActionName("getBatchAutoCounterInfo")]
        public async Task<BatchAutoCounter> GetBatchAutoCounterInfo(bool getNew)
        {
            return await BatchUtils.GetAutoCounterInfo(MongoRepository.Instance, getNew);
        }



    }


}
