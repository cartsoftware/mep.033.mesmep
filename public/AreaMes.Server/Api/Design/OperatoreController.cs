﻿using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/operatore")]
    public class OperatoreController : BaseController<Operatore>
    {
        public async override Task<IEnumerable<Operatore>> GetAll()
        {
            return await MongoRepository.Instance.All<Operatore>(new string[] { "Username", "Password" }); // campi esclusi
        }


        public async override Task<Operatore> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Operatore>(id);
            //a.OperatoreCompetenza.Resolve(); // passo le informazioni anche dell'esternal doc (relazioni)

            return a;

        }


        [Route("Login")]
        [HttpGet]
        public async Task<Operatore> Login(string username, string password)
        {
            var f = Builders<Operatore>.Filter.Eq("Username", username) & Builders<Operatore>.Filter.Eq("Password", password);

            var a = MongoRepository.Instance.Collection<Operatore>().Find(f).FirstOrDefault();

            if (a != null)
            {
                SessionManager.Instance.Set(new SessionInfo { Id = a.Username, StartedAt = DateTime.Now, UserName = a.Username });
                await MessageServices.Instance.Publish(Logon.Name, new Logon { Username = a.Username });
            }

            return a;

        }


        [HttpPost]
        [HttpPut]
        public async override Task<Operatore> Set([FromBody] Operatore item)
        {

            //controllo se è presente la lingua nell heder escarico la lingua dell'operatore , in caso di errore lascio it come default
            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);

            //----- Cehck licence ------------------------------------------
            if (LicenceManager.Instance.IsValid == false)
                throw new Exception(translations.FirstOrDefault(o => o.Key == "licenceManager_licenceNotValid")?.Value);

            // verifica del numero massimo di utenti
            var currentMachinesNr = MongoRepository.Instance.Collection<Operatore>().AsQueryable().Count();
            if (currentMachinesNr > LicenceManager.Instance.MaxUsers)
                throw new Exception(translations.FirstOrDefault(o => o.Key == "licenceManager_maxUser")?.Value);

            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Username))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_nameNotValid").Value;
                throw new Exception(String.Format(message));
            }

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Operatore>().AsQueryable().Where(name => name.Username == item.Username).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_nameNotValid_duplicate").Value;
                throw new Exception(String.Format(message + "'{0}'", item.Codice));

            }
            //----- Data Validation ----------------------

            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            return await MongoRepository.Instance.DeleteAsync<Operatore>(id);
        }

    }
}
