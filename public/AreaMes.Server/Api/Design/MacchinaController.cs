﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Model.Dto;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Design
{

    public class MacchinaController : BaseController<Macchina>
    {
       
        public async override Task<IEnumerable<Macchina>> GetAll()
        {
            var x= await MongoRepository.Instance.All<Macchina>(new string[] { "Descrizione" }); // campi esclusi

            for(int i =0; i < x.Count; i++)
            {
                if (x[i].Tipologia != null)
                {
                    x[i].Tipologia.Resolve();
                }

            }

            return x;
        }


        public async override Task<Macchina> Get(string id)
        {
            var mi = MongoRepository.Instance;

            var a = await mi.GetAsync<Macchina>(id);
            if (a.Tipologia != null)
            {
                a.Tipologia.Resolve();
            }
                

            return a;

        }

        [HttpPost]
        [HttpPut]
        public async override Task<Macchina> Set([FromBody] Macchina item)
        {

            //controllo se è presente la lingua nell heder escarico la lingua dell'operatore , in caso di errore lascio it come default
            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations =  Utils.TranslationManager.GetTranslationItems(defaultLanguage);

            //----- Cehck licence ------------------------------------------
            if (LicenceManager.Instance.IsValid == false)
            {
                string message = translations.FirstOrDefault(x => x.Key == "licenceManager_licenceNotValid")?.Value;
                throw new Exception(String.Format(message));
            }
              

            // verifica del numero massimo di macchine
            var currentMachinesNr = MongoRepository.Instance.Collection<Macchina>().AsQueryable().Count();
            if (currentMachinesNr > LicenceManager.Instance.MaxMachines )
            {
                string message = translations.FirstOrDefault(x => x.Key == "licenceManager_maxMachine")?.Value;
                throw new Exception(String.Format(message));
            }
           



            //----- Data Validation ----------------------
            if (String.IsNullOrWhiteSpace(item.Codice))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_codeNotValid").Value;
                throw new Exception(String.Format(message));
            }

            if (String.IsNullOrWhiteSpace(item.SerialNumber)) 
                         {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_serialNumberNotValid").Value;
                throw new Exception(String.Format(message));
            }

            //if (item.SerialNumber != (item.SerialNumber.Replace('/',';')))
            //    throw new Exception(String.Format("Attenzione: non sono ammessi caratteri speciali nel  'SerialNumber'"));
            item.SerialNumber = item.SerialNumber.Replace('/', '_');

            // viene cercato se esiste un record con la stessa chiave
            var batchFoundedByKey = MongoRepository.Instance.Collection<Macchina>().AsQueryable().Where(k => k.Codice.ToLower().Contains(item.Codice.ToLower())).ToList();

            if (batchFoundedByKey.Any(o => o.Id != item.Id))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_codeNotValid_duplicate").Value;
                throw new Exception(String.Format(message + "'{0}'", item.Codice));

            }


            //----- Data Validation ----------------------


            item.Uniquefy();
            await MongoRepository.Instance.SetAsync(item);
            return item;
        }


        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);
            var batchList = MongoRepository.Instance.Collection<Batch>().FindAsync(o => o.DistintaBase.Fasi[0].Macchine[0].Macchina.Id == id).Result.ToList();
            if(batchList.Count()!=0) throw new Exception("Attenzione la Macchina è collegata a dei batch");

            return await MongoRepository.Instance.DeleteAsync<Macchina>(id);
        }

      


    }
}
