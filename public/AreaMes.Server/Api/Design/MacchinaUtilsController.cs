﻿using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Server.SignalR;
using AreaMes.Server.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/macchina/utils")]
    public class MacchinaUtilsController : ApiController
    {
      
        public static ConcurrentDictionary<string, RealTimeMachineInfo> _realtimeClient = new ConcurrentDictionary<string, RealTimeMachineInfo>(); 
        public static ConcurrentDictionary<string, Timer> _realtimeTimerConnection = new ConcurrentDictionary<string, Timer>();
        public static AreaMes.Meta.RegistrationInfo  _subscribeToDeviceValueChanged= null;
        public static object _lockSet = new object();

        [Route("RegisterMachineToM2M")]
        [HttpGet]
        public async Task<RegisterMachineToM2MResult> RegisterMachineToM2M(string id)
        {
            var res = new RegisterMachineToM2MResult();
            res.Success = false;

            try
            {
                Logger.Default.InfoFormat("Call RegisterMachineToM2M({0})", id);
                var mc = new MacchinaController();
                var machine = await mc.Get(id);
                Logger.Default.InfoFormat("Call RegisterMachineToM2M({0}), founded machine with Serial:{1}, Name:{2}", id, machine.SerialNumber, machine.Nome);
                // chiamata al server
                Logger.Default.InfoFormat("Call RegisterMachineToM2M({0}), call server MEP");
                var client = new AreaM2MClient();
                var resFromServer = await client.RegisterDeviceAsync(id, machine.SerialNumber, machine.Nome, "standard");
            
                if (resFromServer.Success)
                {
                    machine.IsRegisteredToM2M = true;
                    machine.RegisteredToM2MAt = DateTime.Now;
                    machine.RegisteredToM2MDeviceId = resFromServer.NewDeviceId;
                    Logger.Default.InfoFormat("Call RegisterMachineToM2M({0}), server MEP response OK message, RegisteredToM2MDeviceId:{1}", machine.RegisteredToM2MDeviceId);
                    // salvataggio informazioni
                    await mc.Set(machine);
                    Logger.Default.InfoFormat("Call RegisterMachineToM2M({0}), completed succesfully");

                    res.IsRegisteredToM2M = true;
                    res.RegisteredToM2MAt = machine.RegisteredToM2MAt;
                    res.RegisteredToM2MDeviceId = machine.RegisteredToM2MDeviceId;
                    res.Success = true;
                }
                else
                {
                    Logger.Default.InfoFormat("Call RegisterMachineToM2M({0}), server MEP response ERROR message:{1}", resFromServer.Exception);
                    res.Error = resFromServer.Exception;
                }

            }
            catch (Exception exc)
            {
                Logger.Default.InfoFormat("Call RegisterMachineToM2M({0}), error:{1}", exc.Message);
                res.Error = exc.Message;
            }


            return res;
        }


        [Route("RealTimeActivate")]
        [HttpGet]
        public async Task<RealTimeActivateResult> RealTimeActivate(string id)
        {
            return await AreaM2MRealtimeManager.RealTimeActivate(id);
        }


        [Route("RealTimeDeactivate")]
        [HttpGet]
        public async Task<RealTimeDeactivateResult> RealTimeDeactivate(string id)
        {
            return await AreaM2MRealtimeManager.RealTimeDeactivate(id);
        }


    }


}
