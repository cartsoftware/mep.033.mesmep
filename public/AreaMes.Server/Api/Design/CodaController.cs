﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using MongoDB.Driver;
using System.Web.Http;
using static AreaMes.Model.BatchUtils;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/coda")]
  public class CodaController : BaseController<Coda>
    {
        [ActionName("GetAll")]
        public async override Task<IEnumerable<Coda>> GetAll()
        {
            var x = await MongoRepository.Instance.All<Coda>(new string[] { }, new string[] { }); // campi esclusi
            for(int i = 0; i < x.Count; i++)
            {
                if (x[i].Macchina != null)
                    x[i].Macchina.Resolve();
            }
          
            return x;
        }

        [ActionName("Get")]
        public async override Task<Coda> Get(string id)
        {
            var a = await MongoRepository.Instance.GetAsync<Coda>(id);
            if (a == null) return null;

            if (a.Batch != null)
                foreach (var item in a.Batch)
                      item.Resolve();

            if (a.Macchina != null)
                    a.Macchina.Resolve();

            if (a.Materiale != null)
                a.Materiale.Resolve();

            return a;
        }


        [HttpPost]
        [HttpPut]
        public async override Task<Coda> Set([FromBody] Coda item)
        {

            string defaultLanguage = "it";
            if (Request.Headers.Contains("Language"))
                defaultLanguage = Request.Headers.GetValues("Language").First();
            var translations = Utils.TranslationManager.GetTranslationItems(defaultLanguage);

            //----- Cehck licence ------------------------------------------
            if (LicenceManager.Instance.IsValid == false)
                throw new Exception(translations.FirstOrDefault(o => o.Key == "licenceManager_licenceNotValid")?.Value);


            ////----- Data Validation ------------------------------------------

            if (String.IsNullOrWhiteSpace(item.Nome))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_nameQueueNotValid").Value;
                throw new Exception(String.Format(message));
            }


            ////----- Data Validation batch ------------------------------------------

            if (item.Batch.Count<1)
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_queueBatchNotValid").Value;
                throw new Exception(String.Format(message));
            }

            //----- Data Validation -----------------------------------------------
            item.Uniquefy();
            item.LastUpdate = DateTime.Now;

            // viene cercato se esiste un batch per lo stesso ODP
            var batchFoundedByNome = MongoRepository.Instance.Collection<Coda>().FindAsync(Builders<Coda>.Filter.Eq("Nome", item.Nome)).Result.ToList();

            if (batchFoundedByNome.Any(o => o.Id != item.Id))
            {
                string message = translations.FirstOrDefault(x => x.Key == "onSave_validate_queueNotValid").Value;
                throw new Exception(String.Format(message + "'{0}'", item.Nome));
            }

            // stato in modifica cioè creazione
            if (item.Stato == StatoBatch.InModifica)
            {

                if (item.CreateDate.IsNull())
                    item.CreateDate = DateTime.Now;

                //scarico i batch possibili di questa macchina e li setto il QueueId a null
                var arrayBatchAvaiable = MongoRepository.Instance.Collection<Batch>().FindAsync(Builders<Batch>.Filter.Eq("Stato", 0)).Result.ToList();
                if (arrayBatchAvaiable.Count != 0)
                {
                    var arrayBatchMachine = arrayBatchAvaiable.FindAll(x => x.DistintaBase.Fasi[0].Macchine[0].Macchina.Id == item.Macchina.Id);
                    foreach (var it in arrayBatchMachine)
                    {
                        if (it.QueueId?.Id == item.Id)
                        {
                            it.QueueId = null;
                            await MongoRepository.Instance.SetAsync(it);
                        }
                        
                    }
                }

                //setto i batch selezionati dall'utente e setto il  QueueId uguale all'id coda
                foreach (var batch in item.Batch)
                {
                    var batch_update = arrayBatchAvaiable.First(x => x.Id == batch.Id);
                    ExternalDoc<Coda> queueId = new ExternalDoc<Coda>();
                    queueId.Id = item.Id;
                     batch_update.QueueId = queueId;
                    batch_update.QueueId.Id = item.Id;
                    await MongoRepository.Instance.SetAsync(batch_update);

                   
                }

                //scarico i batch possibili di questa macchina e li setto il QueueId a null
                var arrayBatchDelete = MongoRepository.Instance.Collection<Batch>().FindAsync(Builders<Batch>.Filter.Eq("Stato", 0)).Result.ToList();
                if (arrayBatchAvaiable.Count != 0)
                {
                    var arrayBatchMachine = arrayBatchAvaiable.FindAll(x => x.QueueId==null);
                    foreach (var it in arrayBatchMachine)
                    {
                        await AreaMes.Server.BatchManager.Instance.RemoveAsync(it.Id);
                        await MongoRepository.Instance.DeleteAsync<Batch>(it.Id);
                    }
                }

            }

            // stato in attesa,  lanciato
            else if (item.Stato == StatoBatch.InAttesa)
            {
                // ciclo tutti i batch e li setto come lanciati
                foreach (var batch in item.Batch)
                {
                    var batch_update =  await MongoRepository.Instance.GetAsync<Batch>(batch.Id);
                    batch_update.Stato = StatoBatch.InAttesa;
                    await BatchUtils.PrepareBatchTo_InAttesa(batch_update, MongoRepository.Instance);
                    BatchManager.Instance.StartAsync(batch_update);
                }

            }
            else if (item.Stato == StatoBatch.TerminatoAbortito)
            {
                item.Fine = DateTime.Now;
                await AreaMes.Server.BatchManager.Instance.RemoveAsync(item.Id);
            }

            await MongoRepository.Instance.SetAsync(item);


            return item;
        }

        [HttpDelete]
        public override async Task<bool> Delete(string id)
        {
            if (String.IsNullOrEmpty(id)) return await Task.FromResult(false);

            var coda_delete = await MongoRepository.Instance.GetAsync<Coda>(id);
            foreach (var batch in coda_delete.Batch)
            {
                var batch_update = await MongoRepository.Instance.GetAsync<Batch>(batch.Id);
                batch_update.QueueId = null;
                await MongoRepository.Instance.SetAsync(batch_update);
            }
                

                await AreaMes.Server.BatchManager.Instance.RemoveAsync(id);
            return await MongoRepository.Instance.DeleteAsync<Coda>(id);
        }

        [HttpPost]
        [ActionName("getBatchAutoCounterInfo")]
        public async Task<BatchAutoCounter> GetBatchAutoCounterInfo(bool getNew)
        {
            return await BatchUtils.GetAutoCounterInfo(MongoRepository.Instance, getNew);
        }



    }
}
