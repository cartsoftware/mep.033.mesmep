﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;

namespace AreaMes.Server.Api.Design
{
    public class TipoMacchinaController : ApiController
    {
        [HttpGet]
        public virtual IEnumerable<TipoMacchina> GetAll()
        {
            return new List<TipoMacchina> {TipoMacchina.Tipo1, TipoMacchina.Tipo2};
        }


    }
}
