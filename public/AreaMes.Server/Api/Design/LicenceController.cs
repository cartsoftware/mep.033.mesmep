﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using AreaMes.Model;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using MongoDB.Driver;

namespace AreaMes.Server.Api.Design
{
    [RoutePrefix("api/licence")]
    public class LicenceController : ApiController
    {
        [Route("GetLicence")]
        public  Licence GetLicence()
        {


            var licence = LicenceManager.Instance;
            Model.Licence licenze = new Model.Licence();
            licenze.ActivatedAt = licence.ActivatedAt;
            licenze.IsValid = licence.IsValid;
            licenze.LastValidationAt = licence.LastValidationAt;
            licenze.LicencedAt_Key = licence.LicencedAt_Key;
            licenze.LicencedAt_Name = licence.LicencedAt_Name;
            licenze.LicencedAt_Type = licence.LicencedAt_Type;
            licenze.LicencedAt_VatCode = licence.LicencedAt_VatCode;
            licenze.MaxMachines = licence.MaxMachines;

            //licenze.ActivatedAt = DateTime.Now;
            //licenze.IsValid = false;
            //licenze.LastValidationAt = DateTime.Now;
            //licenze.LicencedAt_Key = "KIAVE:IRIRR";
            //licenze.LicencedAt_Name = "Lorenzospa";
            //licenze.LicencedAt_Type = 5;
            //licenze.LicencedAt_VatCode = "0123456789";
            //licenze.MaxMachines = 5;

            return licenze;

        }

        [HttpPost]
        [HttpPut]
        public async  Task<Model.Licence> Set([FromBody] Model.Licence item)
        {
            var a = LicenceManager.Instance;
            Model.Licence c = new Model.Licence();
            return c;

        }












        }
}
