﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using MongoDB.Driver;
using AreaMes.Meta;

namespace AreaMes.Server.Api
{
    public class BaseController<T> : ApiController
        where T : IDoc
    {

        public async virtual Task<IEnumerable<T>> GetAll()
        {
            return await Task.Run(() => { return new List<T>(); });
        }


        public async virtual Task<T> Get(string id)
        {
            return await Task.Run(() => { return default(T); }); 
        }

        [HttpPost]
        public async virtual Task<T> Login(string field1, string field2)
        {
            return await Task.Run(() => { return default(T); });
        }

        [HttpPost]
        [HttpPut]
        public async virtual Task<T> Set(T item)
        {
            return await Task.Run(() => { return default(T); });
        }

        [HttpDelete]
        public async virtual Task<bool> Delete(string id) 
        {
            return await Task.FromResult<bool>(false);
        } 

    }
}
