﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Server.Api.Design;
using AreaMes.Server.Utils;
using AreaProfessional.Utility;
using FluentFTP;
using Microsoft.Azure.DataLake.Store;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using Microsoft.Rest.Azure.Authentication;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Threading.Timer;

namespace AreaMes.Server.SignalR
{
    public class AreaM2MOfflineManager
    {
        private static AreaM2MOfflineManager _instance;
        public static AreaM2MOfflineManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AreaM2MOfflineManager();
                return _instance;
            }

        }

        #region Public 
        public bool IsRunning { get; private set; }
        public DateTime LastUploadAt { get; private set; } 
        public string LastError { get; private set; }

        #endregion

        #region Private
        private static string applicationId = "f981b17d-2449-4c2c-9467-09edb0257c2a";     // Also called client id
        private static string clientSecret = "HhiIotZAyUF[PhW0b/76xBGu2.Vxfhx_";
        private static string tenantId = "2c858bfd-8471-4242-8e89-e7cb4a7dfa90";
        private static string adlsAccountFQDN = "esempio.azuredatalakestore.net";   // full account FQDN, not just the account name like example.azure.datalakestore.net



        private List<Model.Macchina> _listOfMachinesRegistered;
        private AreaMes.Meta.RegistrationInfo _subscribeToDeviceValueChanged = null;
        private DispatcherManager<DeviceValueChangedMessage> _dispatcherManager;
        private string _runningFolder;
        private string _uploadFolder;
        private Timer _timerTick;
        #endregion

        public async Task Start()
        {
            try
            {

                //AreaMes.Server.Utils.Mail.SendMail("MESSAGGIO DA MANDARE");
                IsRunning = false;
                Logger.AreaM2MOffline.InfoFormat("Start the process...");

                // creazioe del manager di scrittura su file
                if (_dispatcherManager==null)
                    _dispatcherManager = new DispatcherManager<DeviceValueChangedMessage>(EvaluateIncomingMessage, 5*1000);
                _dispatcherManager.Start();

                // impostazione delle cartelle
                _runningFolder = Path.Combine(Application.StartupPath, "m2m-running");
                if (!Directory.Exists(_runningFolder))
                    Directory.CreateDirectory(_runningFolder);

                _uploadFolder = Path.Combine(Application.StartupPath, "m2m-upload");
                if (!Directory.Exists(_uploadFolder))
                    Directory.CreateDirectory(_uploadFolder);

                //// prelievo delle macchine
                //var mc = new MacchinaController();
                //var listOfMachines = await mc.GetAll();

                //_listOfMachinesRegistered = listOfMachines.Where(o => !String.IsNullOrWhiteSpace(o.SerialNumber)).ToList();

                //// avvio del processo, se necessario
                //if (_listOfMachinesRegistered.Count > 0)
                //{
                    // sottoscrizione ai cambiamenti di valore
                    _subscribeToDeviceValueChanged = MessageServices.Instance.Subscribe<DeviceValueChangedMessage>(DeviceValueChangedMessage.Name, (o, message) =>
                        {
                            var th = new Thread(() => { _dispatcherManager.Enqueue(message); });
                            th.Start();
                        });

                    Logger.AreaM2MOffline.InfoFormat("Running folder: {0}", _runningFolder);
                    Logger.AreaM2MOffline.InfoFormat("Upload folder: {0}", _uploadFolder);
                    Logger.AreaM2MOffline.InfoFormat("Start the process...completed");

                    IsRunning = true;

                    // avvio del processo di upload
                    _timerTick = new Timer((a) => { CheckTick(); }, null, 10 * 1000, 5 * 60 * 1000);
                //}
                //else
                //    Logger.AreaM2MOffline.InfoFormat("Start the process...completed with info: no machines registerd to M2M founded. The process cannot start");
   
                    
            }
            catch (Exception exc)
            {
                Logger.AreaM2MOffline.Error(exc);
            }
          
        }

        private void Stop()
        {
            try
            {
                Logger.AreaM2MOffline.InfoFormat("Closing the process...");
                if (_subscribeToDeviceValueChanged != null)
                    MessageServices.Instance.UnSubscribe(_subscribeToDeviceValueChanged);

                if (_dispatcherManager != null)
                {
                    _dispatcherManager.Stop();
                    _dispatcherManager = null;
                }

                Logger.AreaM2MOffline.InfoFormat("Closing the process...completed");
            }
            catch (Exception exc)
            {
                Logger.AreaM2MOffline.Error(exc);
            }
        }

        private void EvaluateIncomingMessage(DeviceValueChangedMessage message)
        {
            try
            {
                lock (this)
                {
                    string dn = Meta.Utility.PurifyDeviceSerialNumberForM2M(message.DeviceSerialNumber);
;
                    // DEVICE_NAME=0;   VARIABLE_NAME=1;    VALUE=2;    DATA_READ=3;   
                    string deviceRunningPath = Path.Combine(_runningFolder, String.Format("{0}_{1}_{2}.m2m", dn, DateTime.Now.ToString("yyyyMMdd_HH", CultureInfo.InvariantCulture), "1"));

                    foreach (var item in message)
                    {
                        string s = String.Format("{0};{1};{2};{3}" , message.DeviceSerialNumber, item.Name, item.Value, item.ChangeAt.ToString("yyyy-MM-dd HH:mm:ss \"GMT\"zzz"));
                        Logger.AreaM2MOffline.DebugFormat("{0} -> {1}", message.DeviceSerialNumber, s);
                        File.AppendAllText(deviceRunningPath, s + Environment.NewLine);

                    }
                  
                }
            }
            catch (Exception exc)
            {
                Logger.AreaM2MOffline.Error(exc);
            }
        }

        private void CheckTick()
        {
            CheckMoveRunningToUpload();
            CheckForUpload();
        }

        private void CheckMoveRunningToUpload()
        {
            try
            {
                lock (this)
                {
                    var listOfFiles = Directory.GetFiles(_runningFolder).ToList();

                    foreach (var item in listOfFiles)
                    {
                        string fileName = item.Replace(_runningFolder, String.Empty);

                        var splitted = fileName.Split('_');
                        var date = splitted[1]; // yyyyMMdd
                        var hour = splitted[2]; // HH

                        DateTime dt = DateTime.ParseExact(date+ hour, "yyyyMMddHH", CultureInfo.InvariantCulture, DateTimeStyles.None);

                        if (dt < DateTime.Now.AddMinutes(-15))
                        {
                            string fileA = item;
                            string fileB = _uploadFolder + item.Replace(_runningFolder, String.Empty);
                            Logger.AreaM2MOffline.InfoFormat("Moving file from '{0}' to '{1}'", fileA, fileB);
                            File.Move(fileA, fileB);
                        }

 
                    }

                }
            }
            catch (Exception exc)
            {
                Logger.AreaM2MOffline.Error(exc);
            }
        }


        private void CheckForUpload()
        {
            Logger.AreaM2MOffline.InfoFormat("Check file upload to server MEP AZURE...");
            AdlsClient client = null;
            try
            {
                lock (this)
                {
                   var listOfFiles = Directory.GetFiles(_uploadFolder);
                    if (listOfFiles.Count() == 0)
                    {
                        Logger.AreaM2MOffline.InfoFormat(String.Format("No file founded on upload folder: the process cannot continue."));
                        return;
                    }

                    Logger.AreaM2MOffline.InfoFormat(String.Format("File founded on upload folder:{0}", listOfFiles.Count()));


                    // creazione del canale verso AZURE
                    Logger.AreaM2MOffline.InfoFormat(String.Format("Creating AZURE client..."));
                    var creds = new ClientCredential(applicationId, clientSecret);
                    var clientCreds = ApplicationTokenProvider.LoginSilentAsync(tenantId, creds).GetAwaiter().GetResult();
                    // Create ADLS client object
                    client = AdlsClient.CreateClient(adlsAccountFQDN, clientCreds);
                    Logger.AreaM2MOffline.InfoFormat(String.Format("Creating AZURE client...completed"));


                    // connessione verso AZURE
                    Logger.AreaM2MOffline.InfoFormat(String.Format("Connecting to the server '{0}' with user '{1}'...", Const.AREAM2M_SERVER_IP, Const.AREAM2M_FTP_USER));
                    
                    Logger.AreaM2MOffline.InfoFormat(String.Format("Connected to the server"));

                
                    // vengono ciclati tutti i files
                    foreach (var item in listOfFiles)
                    {
                        var progression = new Progression { FileName = item };
                        var fi = new FileInfo(item);
                        var machineSerialNumber = fi.Name.Split('_')[0];
                        if (!String.IsNullOrWhiteSpace(machineSerialNumber))
                        {
                            Logger.AreaM2MOffline.InfoFormat(String.Format("Uploading file '{0}' ...", item));
                            string fileName = String.Format(@"/{0}/{1}", machineSerialNumber, fi.Name);

                            // Create a file - automatically creates any parent directories that don't exist
                            using (var stream = client.CreateFile(fileName, IfExists.Overwrite))
                            {
                                byte[] textByteArray = File.ReadAllBytes(item);
                                stream.Write(textByteArray, 0, textByteArray.Length);
                            }

                            Logger.AreaM2MOffline.InfoFormat(String.Format("Upload completed to {0}", fileName));
                            File.Delete(item);
                            Logger.AreaM2MOffline.InfoFormat(String.Format("File '{0}' deleted", item));
                        }

                    }

                    // disconnessione
                    Logger.AreaM2MOffline.InfoFormat(String.Format("Disconnecting from the server..."));
                    // ...
                 
                    Logger.AreaM2MOffline.InfoFormat(String.Format("Disconnected"));
                }


            }
            catch (AdlsException e)
            {
                PrintAdlsException(e);
            }
            catch (Exception exc)
            {
                Logger.AreaM2MOffline.Error(exc);
            }finally
            { int a;
                if (client != null)
                     a= 0;
              //      client.Disconnect();
            }
        }

        private static void PrintAdlsException(AdlsException exp)
        {
            Logger.AreaM2MOffline.ErrorFormat("ADLException");
            Logger.AreaM2MOffline.ErrorFormat($"   Http Status: {exp.HttpStatus}");
            Logger.AreaM2MOffline.ErrorFormat($"   Http Message: {exp.HttpMessage}");
            Logger.AreaM2MOffline.ErrorFormat($"   Remote Exception Name: {exp.RemoteExceptionName}");
            Logger.AreaM2MOffline.ErrorFormat($"   Server Trace Id: {exp.TraceId}"); 
            Logger.AreaM2MOffline.ErrorFormat($"   Exception Message: {exp.Message}");
            Logger.AreaM2MOffline.ErrorFormat($"   Exception Stack Trace: {exp.StackTrace}");
        }


    }
    public class Progression : IProgress<FtpProgress>
    {
        public string FileName { get; set; }
        private double lastProgression;
        public void Report(FtpProgress value)
        {
            if ((value.Progress - lastProgression) >= 20)
            { 
                Logger.AreaM2MOffline.InfoFormat(String.Format("Uploading file '{0}' ...{1}%", FileName, Convert.ToInt32( value.Progress) ));
                lastProgression = value.Progress;
            }
            
        }
    }
}
