﻿using AreaM2M.RealTime.Dto;
using AreaMes.Meta;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server.SignalR
{
    public class AreaM2MClient : IAreaM2MClient
    {
        /// <summary>
        /// Riferimento all'oggetto di comunicazione con il server
        /// </summary>
        private static AreaM2MClient _Instance;
        public static AreaM2MClient Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new AreaM2MClient();

                return _Instance;
            }
        }
        private HubConnection HubConnection { get; set; }
        private IHubProxy HubProxy { get; set; }

        public static string AreaM2M_ServerIp { get; set; }
        public static int AreaM2M_ServerPort { get; set; }
        public static string AreaM2M_UserName { get; set; }
        public static string AreaM2M_Password { get; set; }
        public static string AreaM2M_ProjectId { get; set; }

        public string AuthTokenId;
        public string SecurityCode = "5arE3dx!";

        public async Task Start()
        {
            try
            {
                Logger.AreaM2M.Info("AreaM2M Client starting..., on " + Const.AREAM2M_SERVER_IP + ":" + Const.AREAM2M_SERVER_PORT);

                HubConnection = new HubConnection(String.Format("http://{0}:{1}", Const.AREAM2M_SERVER_IP, Const.AREAM2M_SERVER_PORT));
                HubConnection.TransportConnectTimeout = new TimeSpan(0, 5, 0);
                HubProxy = HubConnection.CreateHubProxy("SignalRManager");

                HubConnection.Closed += async () =>
                {
                    try
                    {   // riconnessione al server
                        await Init();
                    }
                    catch (Exception )
                    {
                    }
                };
                HubConnection.Error += exception => { };
                HubConnection.Reconnecting += () => { };
                HubConnection.StateChanged += (e) => { };


                await Init();
            }
            catch (Exception exc)
            {
                Logger.AreaM2M.Error("AreaM2M Client starting..., on " + Const.AREAM2M_SERVER_IP + ":" + Const.AREAM2M_SERVER_PORT, exc);

                throw exc;
            }


        }

        private async Task Init()
        {
           
                await HubConnection.Start();

                Logger.AreaM2M.Info("AreaM2M Client init...");

                Logger.AreaM2M.InfoFormat("AreaM2M Client login, user:'{0}', password:'{1}' ", AreaM2M_UserName, AreaM2M_Password);
                var res = await HubProxy.Invoke<ActionResult>("LoginAsync", AreaM2M_UserName, AreaM2M_Password);

                if (res.Success)
                {
                    AuthTokenId = (string)res.Data;

                    Logger.AreaM2M.InfoFormat("AreaM2M Client login successfully, tokenId:'{0}' ", AuthTokenId);
                }
                else
                    Logger.AreaM2M.InfoFormat("AreaM2M Client login failed, cause:{0} ", res.Exception);
  

           
        }

        public async Task Close()
        {
            if (HubConnection.State == ConnectionState.Connected)
                HubConnection.Stop();


        }

        public async Task<bool> SetValueAsync(string deviceId, string path, object value)
        {
            Logger.AreaM2M.InfoFormat("SetValueAsync(), deviceId:'{0}', path:'{1}', value:{2}...", deviceId, path, value);
            var res = await HubProxy.Invoke<ActionResult>("PublishThingsAsync", deviceId, SecurityCode, new string[] { path }, new object[] { value }, AuthTokenId);
            Logger.AreaM2M.InfoFormat("SetValueAsync(), deviceId:'{0}', path:'{1}', value:{2}, completed with result = {3}", deviceId, path, value, res.Success);
            return res.Success;
        }

        public async Task<bool> SetValueAsync(string deviceId, string[] path, object[] value)
        {
            Logger.AreaM2M.InfoFormat("SetValueAsync(), deviceId:'{0}', multibag", deviceId);
            for (int i = 0; i < path.Length-1; i++)
                Logger.AreaM2M.InfoFormat("SetValueAsync(), deviceId:'{0}', path:'{1}', value:{2}...", deviceId, path[i], value[i]);

            var res = await HubProxy.Invoke<ActionResult>("PublishThingsAsync", deviceId, SecurityCode, path ,  value , AuthTokenId);
            Logger.AreaM2M.InfoFormat("SetValueAsync(), deviceId:'{0}', multibag completed with result = {1}", deviceId, res.Success);
            return res.Success;
        }

        public async Task<RegisterDeviceAsyncResponse> RegisterDeviceAsync(string deviceId, string serialNumber, string machineName, string machineTemplateCode)
        {
            RegisterDeviceAsyncResponse res = null;
            HubConnection hubConn = null;
            IHubProxy hubProxy = null;

            try
            {
                Logger.AreaM2M.InfoFormat("DeviceCreateAsync(), deviceId:'{0}', serialNumber:'{1}', value:{2}...", deviceId, serialNumber);

                // creazione degli oggetti necessari
                hubConn = new HubConnection(String.Format("http://{0}:{1}", Const.AREAM2M_SERVER_IP, Const.AREAM2M_SERVER_PORT));
                hubConn.TransportConnectTimeout = new TimeSpan(0, 5, 0);
                hubProxy = hubConn.CreateHubProxy("SignalRManager");

                // connessione al server
                await hubConn.Start();

                // chiamata di Login
                Logger.AreaM2M.InfoFormat("AreaM2M Client login, user:'{0}', password:'{1}' ", AreaM2M_UserName, AreaM2M_Password);
                var resLogin = await hubProxy.Invoke<ActionResult>("LoginAsync", AreaM2M_UserName, AreaM2M_Password);

                if (resLogin.Success)
                {
                    AuthTokenId = (string)resLogin.Data;

                    Logger.AreaM2M.InfoFormat("AreaM2M Client login successfully, tokenId:'{0}' ", AuthTokenId);

                    res = await hubProxy.Invoke<RegisterDeviceAsyncResponse>("RegisterDeviceAsync", SecurityCode, AuthTokenId, new RegisterDeviceAsyncRequest {
                        DeviceCode = Utility.PurifyDeviceSerialNumberForM2M( serialNumber),
                        Name = machineName,
                        DeviceTemplateCode = machineTemplateCode,
                        ProjectId = AreaM2MClient.AreaM2M_ProjectId });

                    Logger.AreaM2M.InfoFormat("DeviceCreateAsync(), deviceId:'{0}', serialNumber:'{1}', value:{2}, completed with result = {3}", deviceId, serialNumber, res.Success);
                }

                return res;
            }
            catch (Exception exc)
            {
                throw exc;

            } finally
            {
                if (hubConn.State == ConnectionState.Connected)
                    hubConn.Stop();
            }
           
        }
    }
}
