﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Model.Runtime;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using AreaMES.BatchLogic.Mep;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server.SignalR
{
    [HubName("AreaMESManager")]
    public class AreaMESManager : Hub, IRealTimeManager
    {
        private static IDisposable webApp;
        private static Timer _timerClock;
        private static string _lastDate;

        private static AreaMESManager instance;
        public static AreaMESManager Instance { get { return instance; } }

        static AreaMESManager()
        {
            instance = new AreaMESManager();

            // sottoscrizione del messaggio per il cambio di valore di una variabile legata ad un device
            MessageServices.Instance.Subscribe<DeviceValueChangedMessage>(DeviceValueChangedMessage.Name, async (s,e)=> {
              await InMemoryContext.Clients.Group(e.DeviceId).OnDeviceValueChange(e.DeviceId, e);
            });


            // sottoscrizione del messaggio per il cambio di stato di un driver legato ad una macchina
            MessageServices.Instance.Subscribe<StatoMacchinaChangedMessage>(StatoMacchinaChangedMessage.Name, async (s, e) => {
             await InMemoryContext.Clients.Group(e.MacchinaId).OnStatoMacchinaChangedMessage(e.MacchinaId, e);
            });
        }


        private static IHubContext InMemoryContext { get { return GlobalHost.ConnectionManager.GetHubContext<AreaMESManager>(); } }

        public  void Start()
        {
            try
            {
                webApp = WebApp.Start(String.Format("http://*:{0}", Const.AREAMES_REALTIMESERVER_PORT));
                _timerClock = new Timer((a) =>
                {
                    var s = DateTime.Now.ToString("yyyyMMdd HHmmss");
                    if (_lastDate != s)
                    {
                        _lastDate = s;
                        InMemoryContext.Clients.All.GetDateAsync(s);
                    }
                }, null, 2 * 1000, 5 * 1000);

                Logger.Default.Info("Service RUNTIME running, on " + String.Format("http://*:{0}", Const.AREAMES_REALTIMESERVER_PORT));
            }
            catch (Exception e) {
                Console.WriteLine(e.StackTrace);
            }

        }

        public static void Stop()
        {
            webApp.Dispose();
        }


        [HubMethodName("SetAction")]
        public async Task<bool> SetAction(Macchina s)
        {
            return await Task.Run(() => { return default(bool); });
        }


        [HubMethodName("SubscribeToBatch")]
        public async Task<BatchHeader> SubscribeToBatch(string batchId)
        {
            if (String.IsNullOrWhiteSpace(batchId)) return null;
            var batch = AreaMes.Server.BatchManager.Instance.Get(batchId);
            if (batch == null) return null;

            await InMemoryContext.Groups.Add(Context.ConnectionId, batchId);
            return ToBatchHeader(batch);
        }

        [HubMethodName("SubscribeToMachine")]
        public async Task<TrendMacchina> SubscribeToMachine(string machineId)
        {
            if (String.IsNullOrWhiteSpace(machineId)) return null;
            await InMemoryContext.Groups.Add(Context.ConnectionId, machineId);
            return ToBatchMachine(machineId);
        }





        [HubMethodName("UnSubscribeToBatch")]
        public async Task<bool> UnSubscribeToBatch(string batchId)
        {
            await InMemoryContext.Groups.Remove(Context.ConnectionId, batchId);
            return true;
        }



    

        public void NotifyFaseChanged(string batchId, FaseChangedInfo fase)
        {
            InMemoryContext.Clients.Group(batchId).OnFaseChanged(batchId, fase);
        }


        public void NotifyBatchMaterialeChanged(string batchId, List<BatchMateriali> batchMateriali)
        {
            InMemoryContext.Clients.Group(batchId).OnBatchMaterialeChanged(batchId, batchMateriali);
        }

        public void NotifyVersamentoFaseChanged(string batchId, BatchFase batchFase)
        {
            InMemoryContext.Clients.Group(batchId).OnVersamentoFaseChanged(batchId, batchFase);
        }


        public void NotifyBatchChanged(string batchId,Batch batch)
        {
            InMemoryContext.Clients.Group(batchId).OnBatchChanged(batchId, ToBatchHeader(batch));
        }
        
        private BatchHeader ToBatchHeader(Batch batch)
        {
            return new AreaMes.Model.Dto.BatchHeader
            {
                Id = batch.Id,
                OdP = batch.OdP
            };

        }

        private TrendMacchina ToBatchMachine(String machineId)
            {
            //variabili
            
            List<TrendMacchinaVariable> variabili=new List<TrendMacchinaVariable>();
            List<TrendMacchinaVariabileValoreGiorno> listadati = new List<TrendMacchinaVariabileValoreGiorno>();
            List<TrendMacchinaVariabileValoreOra> valoriora = new List<TrendMacchinaVariabileValoreOra>();
            var macchina = MongoRepository.Instance.GetAsync<Macchina>(machineId).Result;
            macchina?.Tipologia?.Resolve();
            var data = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
          
            //for(int i = 0; i < 15; i++)
            //{
            //    valoriora.Add(new TrendMacchinaVariabileValoreOra { Valore = i });
    
           
            //}
            listadati.Add(new TrendMacchinaVariabileValoreGiorno { Data = data, Valori = valoriora });
            
            try
            {
               var  _device1 = SystemVariablesDataContainer.Instance.Get(machineId);
           

                if (_device1.Items.Count != 0)
            {
               var c=  _device1.Items[SystemVariables.PartNumber.Name];
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.ParametroX.Name, Nome = "ParametroX", UltimoValore = _device1.Items[SystemVariables.ParametroX.Name].Value });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.ParametroY.Name, Nome = "ParametroY", UltimoValore = _device1.Items[SystemVariables.ParametroY.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.StatoMacchina.Name, Nome = "StatoMacchina", UltimoValore = _device1.Items[SystemVariables.StatoMacchina.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoTaglioSingoloH.Name, Nome = "TempoTaglioSingoloH", UltimoValore = _device1.Items[SystemVariables.TempoTaglioSingoloH.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoTaglioSingoloM.Name, Nome = "TempoTaglioSingoloM", UltimoValore = _device1.Items[SystemVariables.TempoTaglioSingoloM.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoTaglioSingoloS.Name, Nome = "TempoTaglioSingoloS", UltimoValore = _device1.Items[SystemVariables.TempoTaglioSingoloS.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoTotaleTagliH.Name, Nome = "TempoTotaleTagliH", UltimoValore = _device1.Items[SystemVariables.TempoTotaleTagliH.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoTotaleTagliM.Name, Nome = "TempoTotaleTagliM", UltimoValore = _device1.Items[SystemVariables.TempoTotaleTagliM.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoTotaleTagliS.Name, Nome = "TempoTotaleTagliS", UltimoValore = _device1.Items[SystemVariables.TempoTotaleTagliS.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoVitaMacchinaH.Name, Nome = "TempoVitaMacchinaH", UltimoValore = _device1.Items[SystemVariables.TempoVitaMacchinaH.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoVitaMacchinaM.Name, Nome = "TempoVitaMacchinaM", UltimoValore = _device1.Items[SystemVariables.TempoVitaMacchinaM.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TempoVitaMacchinaS.Name, Nome = "TempoVitaMacchinaS", UltimoValore = _device1.Items[SystemVariables.TempoVitaMacchinaS.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.FeedRateAttuale.Name, Nome = "FeedRateAttuale", UltimoValore = _device1.Items[SystemVariables.FeedRateAttuale.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.BladeSpeedAttuale.Name, Nome = "BladeSpeedAttuale", UltimoValore = _device1.Items[SystemVariables.BladeSpeedAttuale.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Programma.Name, Nome = "Programma", UltimoValore = _device1.Items[SystemVariables.Programma.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.TesaturaLama.Name, Nome = "TesaturaLama", UltimoValore = _device1.Items[SystemVariables.TesaturaLama.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.PezziProdurre.Name, Nome = "PezziProdurre", UltimoValore = _device1.Items[SystemVariables.PezziProdurre.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.PezziProdotti.Name, Nome = "PezziProdotti", UltimoValore = _device1.Items[SystemVariables.PezziProdotti.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.PartNumber.Name, Nome = "PartNumber", UltimoValore = _device1.Items[SystemVariables.PartNumber.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.MisuraProgrammata.Name, Nome = "MisuraProgrammata", UltimoValore = _device1.Items[SystemVariables.MisuraProgrammata.Name].Value });
                variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.CorrenteAssorbita.Name, Nome = "CorrenteAssorbita", UltimoValore = _device1.Items[SystemVariables.CorrenteAssorbita.Name].Value, });
                    //aggiungo gli allarmi
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme1_PremereReset.Name, Nome = "Allarme1_PremereReset", UltimoValore = _device1.Items[SystemVariables.Allarme1_PremereReset.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme2_TensioneLama.Name, Nome = "Allarme2_TensioneLama", UltimoValore = _device1.Items[SystemVariables.Allarme2_TensioneLama.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme3_SovraccaricoMotore.Name, Nome = "Allarme3_SovraccaricoMotore", UltimoValore = _device1.Items[SystemVariables.Allarme3_SovraccaricoMotore.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme4_Velocitains.Name, Nome = "Allarme4_Velocitains", UltimoValore = _device1.Items[SystemVariables.Allarme4_Velocitains.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme5_PremereReset2.Name, Nome = "Allarme5_PremereReset2", UltimoValore = _device1.Items[SystemVariables.Allarme5_PremereReset2.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme6_FungoPremuto.Name, Nome = "Allarme6_FungoPremuto", UltimoValore = _device1.Items[SystemVariables.Allarme6_FungoPremuto.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme7_Yblocco1.Name, Nome = "Allarme7_Yblocco1", UltimoValore = _device1.Items[SystemVariables.Allarme7_Yblocco1.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme8_Xblocco1.Name, Nome = "Allarme8_Xblocco1", UltimoValore = _device1.Items[SystemVariables.Allarme8_Xblocco1.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme9_Yblocco2.Name, Nome = "Allarme9_Yblocco2", UltimoValore = _device1.Items[SystemVariables.Allarme9_Yblocco2.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme10_Xblocco2.Name, Nome = "Allarme10_Xblocco2", UltimoValore = _device1.Items[SystemVariables.Allarme10_Xblocco2.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme11_LivelloOlioMin.Name, Nome = "Allarme11_LivelloOlioMin", UltimoValore = _device1.Items[SystemVariables.Allarme11_LivelloOlioMin.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme12_CarterAperto.Name, Nome = "Allarme12_CarterAperto", UltimoValore = _device1.Items[SystemVariables.Allarme12_CarterAperto.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme13_AlimentatoreBloccato.Name, Nome = "Allarme13_AlimentatoreBloccato", UltimoValore = _device1.Items[SystemVariables.Allarme13_AlimentatoreBloccato.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme14_InverterBloccato.Name, Nome = "Allarme14_InverterBloccato", UltimoValore = _device1.Items[SystemVariables.Allarme14_InverterBloccato.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme15_AlimBarra.Name, Nome = "Allarme15_AlimBarra", UltimoValore = _device1.Items[SystemVariables.Allarme15_AlimBarra.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme16_FCTIFCTA.Name, Nome = "Allarme16_FCTIFCTA", UltimoValore = _device1.Items[SystemVariables.Allarme16_FCTIFCTA.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme17_Parametri.Name, Nome = "Allarme17_Parametri", UltimoValore = _device1.Items[SystemVariables.Allarme17_Parametri.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme18_Velocita.Name, Nome = "Allarme18_Velocita", UltimoValore = _device1.Items[SystemVariables.Allarme18_Velocita.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme19_NessunProg.Name, Nome = "Allarme19_NessunProg", UltimoValore = _device1.Items[SystemVariables.Allarme19_NessunProg.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme20_Scorta.Name, Nome = "Allarme20_Scorta", UltimoValore = _device1.Items[SystemVariables.Allarme20_Scorta.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme21_FineBarra.Name, Nome = "Allarme21_FineBarra", UltimoValore = _device1.Items[SystemVariables.Allarme21_FineBarra.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme22_SWLunghezza.Name, Nome = "Allarme22_SWLunghezza", UltimoValore = _device1.Items[SystemVariables.Allarme22_SWLunghezza.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme23_SWAsseX.Name, Nome = "Allarme23_SWAsseX", UltimoValore = _device1.Items[SystemVariables.Allarme23_SWAsseX.Name].Value, });
                    variabili.Add(new TrendMacchinaVariable { Codice = SystemVariables.Allarme24_LamaRotta.Name, Nome = "Allarme24_LamaRotta", UltimoValore = _device1.Items[SystemVariables.Allarme24_LamaRotta.Name].Value, });


                }
            }
            catch (Exception exc)
            { }
            return new AreaMes.Model.Runtime.TrendMacchina
            {
             Macchina=macchina,
              Variabili=variabili           
                };
            }

        
    }
}
