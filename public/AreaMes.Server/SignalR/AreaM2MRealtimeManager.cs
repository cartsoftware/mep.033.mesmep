﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Model.Dto;
using AreaMes.Server.Api.Design;
using AreaMes.Server.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server.SignalR
{
    public static class AreaM2MRealtimeManager
    {
        public static ConcurrentDictionary<string, RealTimeMachineInfo> _realtimeClient = new ConcurrentDictionary<string, RealTimeMachineInfo>();
        public static ConcurrentDictionary<string, Timer> _realtimeTimerConnection = new ConcurrentDictionary<string, Timer>();
        public static AreaMes.Meta.RegistrationInfo _subscribeToDeviceValueChanged = null;
        public static object _lockSet = new object();


        public static async Task<RealTimeActivateResult> RealTimeActivate(string id)
        {
            var res = new RealTimeActivateResult();
            res.Success = false;

            try
            {
                Logger.Default.InfoFormat("Call RealTimeActivate({0})", id);
                var mc = new MacchinaController();
                var machine = await mc.Get(id);
                Logger.Default.InfoFormat("Call RealTimeActivate({0}), founded machine with Serial:{1}, Name:{2}", id, machine.SerialNumber, machine.Nome);

                if (_realtimeClient.Count >= 1)
                    throw new Exception("Impossibile proseguire poichè è già attiva una connessione in realtime per la macchina.");

                if (!machine.IsRegisteredToM2M)
                    throw new Exception("Impossibile proseguire poichè la macchina non risulta registrata al server.");

                // ---------------------------------------------------------------------------------------------------------------
                RealTimeMachineInfo client;
                if (_realtimeClient.TryGetValue(id, out client))
                {
                    Logger.Default.InfoFormat("Call RealTimeActivate({0}), the client already exist, using this one.");
                }
                else
                {
                    // chiamata al server
                    Logger.Default.InfoFormat("Call RealTimeActivate({0}), create the new client");
                    client = new RealTimeMachineInfo();
                    client.Client = new AreaM2MClient();
                    await client.Client.Start();
                    client.StartedAt = DateTime.Now;
                    client.MachineId = id;
                    client.MachineName = machine.Nome;
                    client.MachineSerialNumber = machine.SerialNumber;
                    client.MachineSerialNumberPurify = Meta.Utility.PurifyDeviceSerialNumberForM2M(client.MachineSerialNumber);
                    _realtimeClient.TryAdd(id, client);

                    // attivazione della sottoscrizione
                    _subscribeToDeviceValueChanged = MessageServices.Instance.Subscribe<DeviceValueChangedMessage>(DeviceValueChangedMessage.Name, (o, message) => {

                        Thread th = new Thread(() => {
                            lock (_lockSet)
                                try
                                {

                                    if (message.DeviceId == client.MachineId)
                                    {
                                        if (message.Count() == 1)
                                            client.Client.SetValueAsync(client.MachineSerialNumberPurify, message[0].Name, message[0].Value);
                                        else
                                        {
                                            string[] paths = new string[message.Count];
                                            object[] values = new object[message.Count];

                                            for (int i = 0; i < message.Count; i++)
                                            {
                                                paths[i] = message[i].Name;
                                                values[i] = message[i].Value;
                                            }

                                            client.Client.SetValueAsync(client.MachineSerialNumberPurify, paths, values);
                                        }
                                    }
                                }
                                catch (Exception exc)
                                {
                                    Logger.AreaM2M.Error(exc);
                                }

                        });
                        th.Start();

                    });

                    // impostazione macchina
                    machine.isActiveRealtime = true;
                    await mc.Set(machine);

                    // risultato del messaggio
                    res.Success = true;
                }

            }
            catch (Exception exc)
            {
                Logger.Default.InfoFormat("Call RealTimeActivate({0}), error:{1}", exc.Message);
                res.Error = exc.Message;
            }


            return res;
        }


        public static async Task<RealTimeDeactivateResult> RealTimeDeactivate(string id)
        {
            var res = new RealTimeDeactivateResult();
            res.Success = false;

            try
            {
                Logger.Default.InfoFormat("Call RealTimeDeactivate({0})", id);

                // impostazione macchina
                var mc = new MacchinaController();
                var machine = await mc.Get(id);
                machine.isActiveRealtime = false;
                await mc.Set(machine);

                // rimozione della sottoscrizione
                if (_subscribeToDeviceValueChanged != null)
                {
                    MessageServices.Instance.UnSubscribe(_subscribeToDeviceValueChanged);
                    _subscribeToDeviceValueChanged = null;
                }


                RealTimeMachineInfo client = null;
                if (_realtimeClient.TryGetValue(id, out client))
                {
                    _realtimeClient.TryRemove(id, out client);

                    // chiusura del canale
                    await client.Client.Close();
                }


                Logger.Default.InfoFormat("Call RealTimeDeactivate({0})...completed", id);

                res.Success = true;
            }
            catch (Exception exc)
            {
                Logger.Default.InfoFormat("Call RealTimeDeactivate({0}), error:{1}", exc.Message);
                res.Error = exc.Message;
            }


            return res;
        }
    }


    public class RealTimeMachineInfo
    {
        public AreaM2MClient Client { get; set; }
        public string MachineId { get; set; }
        public string MachineSerialNumber { get; set; }
        public DateTime StartedAt { get; set; }
        public string MachineName { get; set; }
        public string MachineSerialNumberPurify { get; internal set; }
    }
}
