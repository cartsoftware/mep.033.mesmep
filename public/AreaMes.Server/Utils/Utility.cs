﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server.Utils
{
    public static class Utility
    {
        /// <summary>
        /// Restituisce la cartella di esecuzione dell'applicativo
        /// </summary>
        public static string GetApplicationFolder
        {
            get
            {
                return  System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            }
        }
    }
}
