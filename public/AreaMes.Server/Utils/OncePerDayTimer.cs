﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AreaProfessional.Utility
{
    public class OncePerDayTimer : IDisposable
    {
        private DateTime _lastRunDate; 
        private TimeSpan _time; 
        private Timer _timer; 
        private Action _callback;
        private Action _callbackAfterDelay;
        private double _delaySecondCallback;
        private DateTime _createTime;
        public OncePerDayTimer(TimeSpan time, Action callback, Action callbackAfterDelay = null, double delaySecondCallback = 0)
        {
            _time = time; 
            _timer = new Timer(CheckTime, null, TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30)); 
            _callback = callback;
            _callbackAfterDelay = callbackAfterDelay;
            _delaySecondCallback = delaySecondCallback;
            _createTime = DateTime.Now;
        }

        private void CheckTime(object state)
        {
            // 2016-08-02 Modifica se _lastRunDate a null perchè altrimenti mi carica la sequenza
            if (_lastRunDate == DateTime.MinValue)
                if ((_createTime.TimeOfDay > _time) && (_createTime.Date == DateTime.Now.Date))
                    return;
            if (_lastRunDate == DateTime.Today) 
                return; 
            if (DateTime.Now.TimeOfDay < _time) 
                return; 
            _lastRunDate = DateTime.Today;
            _callback();
            if (_callbackAfterDelay !=null)
                Task.Delay( Convert.ToInt32(_delaySecondCallback*1000)).ContinueWith((_) => { _callbackAfterDelay(); });
        }

        public void Dispose()
        {
            if (_timer == null) 
                return; 
            _timer.Dispose(); 
            _timer = null;
        }

    }
}
