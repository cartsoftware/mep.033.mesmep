﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server
{
    public static class Logger
    {
        public static readonly log4net.ILog Default = log4net.LogManager.GetLogger("MES");

        public static readonly log4net.ILog AreaM2M = log4net.LogManager.GetLogger("M2M");

        public static readonly log4net.ILog AreaM2MOffline = log4net.LogManager.GetLogger("SRV");
    }
}
