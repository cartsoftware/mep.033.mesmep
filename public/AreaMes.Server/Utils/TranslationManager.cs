﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server.Utils
{
    public class TranslationManager
    {
        private static Dictionary<string, TranslationCacheInfo> _cachedTranslation = new Dictionary<string, TranslationCacheInfo>();

        public static List<TranslationItem> GetTranslationItems(string languageId)
        {
            var res = new List<TranslationItem>();

            if (String.IsNullOrWhiteSpace(languageId))
                languageId = "it";

            lock (_cachedTranslation)
            {
                try
                {

                    bool requestUpdate = false;
                    TranslationCacheInfo temp = null;
                    if (!_cachedTranslation.TryGetValue(languageId, out temp))
                    {
                        var di = new DirectoryInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        string fileName = Path.Combine(di.Parent.FullName, "resources", "translation." + languageId + ".xml");

                        temp = new TranslationCacheInfo();
                        temp.FileName = fileName;

                        _cachedTranslation.Add(languageId, temp);
                    }
 

                    requestUpdate = (temp.LastUpdateAt != new FileInfo(temp.FileName).LastWriteTime);
                 

                    if (requestUpdate)
                    {
                        temp.Items = new List<TranslationItem>();

                        var ds = new DataSet();
                        ds.ReadXml(temp.FileName);

                        var dtTable = ds.Tables[0];

                        // inizio il ciclo
                        foreach (DataRow dr in dtTable.Rows)
                            temp.Items.Add(new TranslationItem { Key = dr["key"].ToString(), Value = dr["value"].ToString() });

                        temp.LastUpdateAt = new FileInfo(temp.FileName).LastWriteTime;
                    }

                    res = temp.Items;


                }
                catch (Exception ex)
                {
                    throw new Exception("Cannot read Xml translation file", ex);
                }

            }

            return res;
        }

        internal class TranslationCacheInfo
        {
            public string FileName { get; set; }
            public DateTime LastUpdateAt { get; set; }
            public List<TranslationItem> Items { get; set; }
        }

    }


    public class TranslationItem
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }

    public class Translation
    {

        public string LanguageId { get; set; }

        public DateTime LastUpdate { get; set; }

        public List<TranslationItem> Items { get; set; }
    }


}
