﻿using AreaM2M.RealTime.Dto;
using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Server.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server
{
    public  class DataContainer : IDataContainer
    {
        public  ConcurrentDictionary<string, DeviceItems> Data { get; private set; }
        private object _lock = new object();

        private static DataContainer instance;
        public static DataContainer Instance {get { return instance; } }

         static DataContainer()
        {
            instance = new Server.DataContainer();
        }

        public  DataContainer()
        {
            Data = new ConcurrentDictionary<string, DeviceItems>();
        }

        public  void Create(IEnumerable<Registry> values, string deviceId)
        {
            lock (_lock)
            {
                var di = new DeviceItems();
                di.Items = new Dictionary<string, DataItem>();
                di.Device = new Device { Code = deviceId };
                foreach (var obj in values)
                {
                    di.Items.Add(obj.Path, new DataItem
                    {
                        Name = obj.Code,
                        Value = obj.CurrentValue,
                        OldValue = obj.CurrentValue,
 
                        IsChanged = false
                    });
                }
                if (Data.ContainsKey(deviceId))
                    Data.TryRemove(deviceId, out di);

                Data.TryAdd(deviceId, di);
            }
        }

        public  void Fill(List<ThingResult> item, string deviceId)
        {
            lock (_lock)
            {
                DeviceItems deviceItem = null ;
                if (Data.TryGetValue(deviceId,  out deviceItem)){
                    var message = new DeviceValueChangedMessage();
                    message.DeviceId = deviceId;
                    foreach (var tr in item)
                    {
                        DataItem mem = null;
   
                        deviceItem.Items.TryGetValue(tr.Path, out mem); //.FirstOrDefault(x => Convert.ToString(x.Registry.Path) == Convert.ToString(tr.Path));
                        if (mem != null)
                        {
                            mem.OldValue = mem.Value;
                            mem.Value = tr.Value;
                            if (tr.Info != null)
                            {
                                mem.ValueDecoded = tr.Info.ValueDecoded;
                                mem.OldValueDecoded = tr.Info.OldValueDecoded;
                            }
                            mem.IsChanged = Convert.ToString(mem.OldValue) != Convert.ToString(mem.Value);
                        }
                        message.Add(mem);
                    }
                    MessageServices.Instance.Publish(DeviceValueChangedMessage.Name, message);
                }
            }
        }

        public Dictionary<string, DeviceItems> Get()
        {
            return Data.ToDictionary(o => o.Key, y => y.Value);
        }

        public DeviceItems Get(string pDevice)
        {
            var ds = new DeviceItems();
            Data.TryGetValue(pDevice, out ds);

            return ds;
        }
    }


}
