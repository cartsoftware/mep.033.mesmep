﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.SelfHost;
using AreaMes.Server.SignalR;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using AreaMes.Server.MongoDb;
using AreaMes.Model;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using AreaMes.Meta;
using System.ServiceProcess;
using AreaMes.Server.Utils;
using System.Threading;
using AreaMes.Server.Api.Design;

namespace AreaMes.Server
{

    class Program
    {
        #region UI
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
        [DllImport("user32.dll")]
        static extern IntPtr GetShellWindow();
        private const int MF_BYCOMMAND = 0x00000000;
        public const int SC_CLOSE = 0xF060;
        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);
        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern IntPtr GetDesktopWindow();
        static NotifyIcon notifyIcon;
        static IntPtr processHandle;
        static IntPtr WinShell;
        static IntPtr WinDesktop;
        static MenuItem HideMenu;
        static MenuItem RestoreMenu;
        static bool isMaximize;



        #endregion

        #region Nested classes to support running as service
        public const string ServiceName = "MEP (MES Light Server)";

        public class Service : ServiceBase
        {
            public static ManualResetEvent StopRequest = new ManualResetEvent(false);
            public Service()
            {
                ServiceName = Program.ServiceName;
            }

            protected override void OnStart(string[] args)
            {
                Program.RunInternal();
            }

            protected override void OnStop()
            {
                Environment.Exit(1);
            }
        }
        #endregion

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            if (Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Length > 1)
            {
                var pro = Process.GetCurrentProcess();
                Logger.Default.InfoFormat("Another application is already running. Kill the last one with ID:{0} ", pro.Id);
                pro.Kill();
            }


            if (!Environment.UserInteractive)
            {
                Logger.Default.InfoFormat("RUNNING AS A SERVICE");

                Service.StopRequest.Reset();

                Task.Run(() => {
                    var service = new Service();
                    ServiceBase.Run(service);
                });

                Service.StopRequest.WaitOne();

            }
            else
            {
                Logger.Default.InfoFormat("RUNNING AS A CONSOLE APPLICATION");
                notifyIcon = new NotifyIcon { Icon = new Icon("gear_icon_J1E_icon.ico"), Text = "AreaMES Light Server", Visible = true };
                notifyIcon.DoubleClick += NotifyIcon_DoubleClick;
                ContextMenu menu = new ContextMenu();
                HideMenu = new MenuItem("Hide", Minimize_Click);
                RestoreMenu = new MenuItem("Restore", Maximize_Click);
                menu.MenuItems.Add(RestoreMenu);
                menu.MenuItems.Add(HideMenu);
                menu.MenuItems.Add(new MenuItem("Open Web Designer", (s, e) => { System.Diagnostics.Process.Start(String.Format("http://{0}:{1}/login.html", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_DESIGN_PORT)); }));
                menu.MenuItems.Add(new MenuItem("Open Web Runtime", (s, e) => { System.Diagnostics.Process.Start(String.Format("http://{0}:{1}/login.html", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_TABLET_PORT)); }));
                menu.MenuItems.Add(new MenuItem("Exit", CleanExit));
                notifyIcon.ContextMenu = menu;

                Logger.Default.InfoFormat("Creating menù");

                try
                {
                    Task.Run(() =>
                    {
                        RunInternal();
                    });
                }
                catch (Exception exc)
                {
                    Logger.Default.Error(exc);
                }
                Logger.Default.InfoFormat("Process.GetCurrentProcess():" + Process.GetCurrentProcess().MainWindowHandle);

                processHandle = Process.GetCurrentProcess().MainWindowHandle;
                WinShell = GetShellWindow();
                WinDesktop = GetDesktopWindow();
                DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
                ResizeWindow(false);

                ///This is required for triggering WinForms activity in Console app
                Application.Run();
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception e1 = (Exception)e.ExceptionObject;
            Console.WriteLine("MyHandler caught : " + e1.Message);
            Console.WriteLine("Runtime terminating: {0}", e.IsTerminating);
            Logger.Default.Error(e1);
        }

        public static void RunInternal()
        {

            try
            {
                Logger.Default.Info("Start application");

                Logger.Default.Info("-----------------------------------------------------------");
                Logger.Default.Info("Checking for installation file...");
                LicenceManager.Instance.CheckAndImportInstallationFile();
                LicenceManager.Instance.CheckAndImportDatabase();
                Logger.Default.Info("Checking for installation file... done!");


                Logger.Default.Info("-----------------------------------------------------------");
                Logger.Default.Info("Configuration settings:");
                System.Configuration.ConfigurationManager.RefreshSection("appSettings");
                foreach (var item in System.Configuration.ConfigurationManager.AppSettings.Keys)
                    Logger.Default.InfoFormat("{0}={1}", item, System.Configuration.ConfigurationManager.AppSettings[item.ToString()]);
                Logger.Default.Info("-----------------------------------------------------------");

                Const.AREAMES_MONGODB = System.Configuration.ConfigurationManager.AppSettings["AREAMES_MONGODB"];
                Const.AREAMES_MONGODB_NAME = System.Configuration.ConfigurationManager.AppSettings["AREAMES_MONGODB_NAME"];

                Const.AREAMES_APISERVER_IP = System.Configuration.ConfigurationManager.AppSettings["AREAMES_APISERVER_IP"];
                Const.AREAMES_APISERVER_PORT = System.Configuration.ConfigurationManager.AppSettings["AREAMES_APISERVER_PORT"];
                Const.AREAMES_WEBSERVER_IP = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEBSERVER_IP"];
                Const.AREAMES_WEBSERVER_TABLET_PORT = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEBSERVER_TABLET_PORT"];
                Const.AREAMES_WEBSERVER_DESIGN_PORT = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEBSERVER_DESIGN_PORT"];
                Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEBSERVER_MULTIMEDIA_PORT"];

                Const.AREAMES_WEBSERVER_TABLET_PATH = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEBSERVER_TABLET_PATH"];
                Const.AREAMES_WEBSERVER_DESIGN_PATH = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEBSERVER_DESIGN_PATH"];
                Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEBSERVER_MULTIMEDIA_PATH"];

                Const.AREAM2M_SERVER_IP = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_SERVER_IP"];
                Const.AREAM2M_SERVER_PORT = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_SERVER_PORT"];
                Const.AREAM2M_USERNAME = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_USERNAME"];
                Const.AREAM2M_PASSWORD = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_PASSWORD"];
                Const.AREAM2M_PROJECT = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_PROJECT"];
                Const.AREAM2M_SUBSCRIBE_TO_DEVICES = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_SUBSCRIBE_TO_DEVICES"];
                Const.AREAM2M_SUBSCRIBE_TO_DEVICES_SECURITY_CODE = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_SUBSCRIBE_TO_DEVICES_SECURITY_CODE"];
                Const.AREAMES_REALTIMESERVER_PORT = System.Configuration.ConfigurationManager.AppSettings["AREAMES_REALTIMESERVER_PORT"];
                Const.AREAM2M_BATCH_LOGIC_DLL = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_BATCH_LOGIC_DLL"];

                Const.AREAM2M_FTP_USER = System.Configuration.ConfigurationManager.AppSettings["AREAM2M_FTP_USER"];
                Const.AREAM2M_FTP_PASS= System.Configuration.ConfigurationManager.AppSettings["AREAM2M_FTP_PASS"];
                Const.AREAM2M_FTP_PORT = Convert.ToInt32( System.Configuration.ConfigurationManager.AppSettings["AREAM2M_FTP_PORT"]);

                Const.AREAMES_SMTP_SERVER = System.Configuration.ConfigurationManager.AppSettings["AREAMES_SMTP_SERVER"];
                Const.AREAMES_SMTP_SERVER_PORT = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AREAMES_SMTP_SERVER_PORT"]);
                Const.AREAMES_SMTP_SERVER_FROM = System.Configuration.ConfigurationManager.AppSettings["AREAMES_SMTP_SERVER_FROM"];

                Const.AREAMES_WEB_SERVICE_ADDRESS = System.Configuration.ConfigurationManager.AppSettings["AREAMES_WEB_SERVICE_ADDRESS"];


            }
            catch (Exception exc)
            {
                Logger.Default.Error(exc);
            }


            // ---------------------------------------------------------------------------------------
            // CARICAMENTO DELLE TRADUZIONI
            // ---------------------------------------------------------------------------------------

            //// lettura del file delle traduzioni
            //var list = TranslationManager.GetTranslationItems("it");


            try
            {
                Logger.Default.Info("Creating service API  on " + string.Format("http://{0}:{1}", Const.AREAMES_APISERVER_IP, Const.AREAMES_APISERVER_PORT));
                // -----------------------------------------------------------------------
                // Avvio del servizio webapi
                var config = new HttpSelfHostConfiguration(string.Format("http://{0}:{1}", Const.AREAMES_APISERVER_IP, Const.AREAMES_APISERVER_PORT));
                config.MapHttpAttributeRoutes();
                //config.Routes.MapHttpRoute(name: "DefaultApiWithAction", routeTemplate: "api/{controller}/{action}/{id}", defaults: new { id = RouteParameter.Optional });
                config.Routes.MapHttpRoute(name: "serverApi", routeTemplate: "api/{controller}/{id}", defaults: new { id = RouteParameter.Optional });


                config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = true });
                config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
                config.MaxBufferSize = int.MaxValue;
                config.MaxReceivedMessageSize = int.MaxValue;
                config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
                var server = new HttpSelfHostServer(config);
                server.OpenAsync().Wait();
                Logger.Default.Info("Service API running, on " + string.Format("http://{0}:{1}", Const.AREAMES_APISERVER_IP, Const.AREAMES_APISERVER_PORT));
            }
            catch (Exception exc)
            {
                Logger.Default.Error(exc);

                throw;
            }



            // -----------------------------------------------------------------------
            // Avvio del servizio signalr
            AreaMESManager.Instance.Start();


            // -----------------------------------------------------------------------
            // Avvio del servizio DB
            MongoRepository.Instance.Connect();
            Logger.Default.InfoFormat("Service DB running, on '{0}', {1}", Const.AREAMES_MONGODB, Const.AREAMES_MONGODB_NAME);

            Logger.Default.Info("-----------------------------------------------------------");
            Logger.Default.Info("Checking for installation key...");
            var tInstKey = new TaskCompletionSource<bool>();
            Task.Run(async () => { await LicenceManager.Instance.CheckAndImportInstallationKey(); tInstKey.SetResult(true); });
            var tInstKeyResult = tInstKey.Task.Result;
            Logger.Default.Info("Checking for installation key... done!");
            Logger.Default.Info("-----------------------------------------------------------");

            //// -----------------------------------------------------------------------
            //// Avvio del servizio signalr verso AREAM2M
            //if (!String.IsNullOrEmpty(Const.AREAM2M_SERVER_IP))
            //{
            if ((!String.IsNullOrWhiteSpace(Const.AREAM2M_SERVER_PORT)) && (!String.IsNullOrWhiteSpace(Const.AREAM2M_PROJECT)))
            {
                AreaM2MClient.AreaM2M_Password = Const.AREAM2M_PASSWORD;
                AreaM2MClient.AreaM2M_UserName = Const.AREAM2M_USERNAME;
                AreaM2MClient.AreaM2M_ServerIp = Const.AREAM2M_SERVER_IP;
                AreaM2MClient.AreaM2M_ServerPort = Convert.ToInt32(Const.AREAM2M_SERVER_PORT);
                AreaM2MClient.AreaM2M_ProjectId = Const.AREAM2M_PROJECT;
            }

            //    var t = new TaskCompletionSource<bool>();
            //    Task.Run(async () => { await AreaM2MClient.Instance.Start(); t.SetResult(true); });
            //    var t1 = t.Task.Result;
            //}


            // -----------------------------------------------------------------------
            // Avvio del servizio web SERVER
            if (!String.IsNullOrEmpty(Const.AREAMES_WEBSERVER_IP))
                WebServerManager.Instance.Run();


            // -----------------------------------------------------------------------
            // Caricamento DLL

            Task.Run(async () =>
            {
                try
                {
                    await BatchManager.Instance.Init();
                }
                catch (Exception exc)
                {
                    Logger.Default.ErrorFormat("Logic '{0}' loading, error. Cause: {1}", Const.AREAM2M_BATCH_LOGIC_DLL, exc.Message);
                }
            });


            // -----------------------------------------------------------------------
            // Reset di tutti i realtime per le macchine
            var mc = new MacchinaController();
            foreach (var item in mc.GetAll().Result)
            {
                item.isActiveRealtime = false;
                mc.Set(item);
            }

            // -----------------------------------------------------------------------
            // Attivazione del profilo di avvio dati offline
            //
            var t = new TaskCompletionSource<bool>();
            Task.Run(async () => { await AreaM2MOfflineManager.Instance.Start(); t.SetResult(true); });
            var t1 = t.Task.Result;

            // -----------------------------------------------------------------------
            // Avvio del gestore degli allarmi
            AlarmManager.Instance.Start();



            Logger.Default.Info("-----------------------------------------------------------");
            Logger.Default.Info("Checking for validation licence...");
            var tCheckLicence = new TaskCompletionSource<bool>();
            Task.Run(async () => { await LicenceManager.Instance.CheckLicence(); tCheckLicence.SetResult(true); });
            var ttCheckLicenceResult = t.Task.Result;
            Logger.Default.Info("Checking for validation licence... done!");
            Logger.Default.Info("-----------------------------------------------------------");


            if (Environment.UserInteractive)
            {
                // -----------------------------------------------------------------------
                notifyIcon.BalloonTipText = "Sistema avviato correttamente";
                notifyIcon.BalloonTipTitle = notifyIcon.Text;
                notifyIcon.ShowBalloonTip(3000);
            }


            Console.ReadLine();
        }


        private static void ApplyValidationAfterInstallation()
        {

        }

        private static void NotifyIcon_DoubleClick(object sender, EventArgs e)
        {
            ResizeWindow(!isMaximize);
        }


        private static void CleanExit(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "AreaMES Light Server", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            notifyIcon.Visible = false;
            Application.Exit();
            Environment.Exit(1);
        }

        static void Minimize_Click(object sender, EventArgs e)
        {
            ResizeWindow(false);
        }

        static void Maximize_Click(object sender, EventArgs e)
        {
            ResizeWindow();
        }

        static void ResizeWindow(bool Restore = true)
        {
            if (Restore)
            {
                RestoreMenu.Enabled = false;
                HideMenu.Enabled = true;
                SetParent(processHandle, WinDesktop);
                isMaximize = true;
            }
            else
            {
                RestoreMenu.Enabled = true;
                HideMenu.Enabled = false;
                SetParent(processHandle, WinShell);
                isMaximize = false;
            }
        }
    }
}
