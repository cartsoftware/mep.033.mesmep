﻿using AreaMes.Model;
using AreaMes.Server.MongoDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Server
{ 
    public class SettingsManager : ISettings
    {
        private Dictionary<string, object> _values;

        public Dictionary<string, object> Values
        {
             get
            {
            return _values;
            }
        }


        public SettingsManager Load()
        {
            _values = new Dictionary<string, object>();

            var items = MongoRepository.Instance.All<Parametri>().Result.ToList();

            foreach (var item in items)
                _values.Add(item.Codice, item.ValoreDefault);
          
            return this;
        }
    }
}
