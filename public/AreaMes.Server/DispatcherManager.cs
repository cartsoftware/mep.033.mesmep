﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server
{
    public class DispatcherManager<T>
    {
        private Queue<T> _incomingQueue = new Queue<T>();
        private bool _incomingMessageMustShutDown;
        private ManualResetEvent _incomingMessageInQueue = new ManualResetEvent(false);
        private Action<T> _execute;
        public int WaitThread { get; set; }

        public DispatcherManager(Action<T> execute, int waitThread)
        {
            _execute = execute;
            WaitThread = waitThread;
        }

        private void Dispatcher()
        {
            while (!_incomingMessageMustShutDown)
            {
                _incomingMessageInQueue.WaitOne(WaitThread);

                var itemCnt = 0;
                lock (_incomingQueue)
                    itemCnt = _incomingQueue.Count;

                if (itemCnt > 0)
                {
                    T message = default(T);

                    try
                    {
                        lock (_incomingQueue)
                            message = _incomingQueue.Dequeue();


                        Task.Run(() => { _execute(message); });
                    }
                    catch (Exception exc)
                    {

                    }
                }
                else
                    _incomingMessageInQueue.Reset();
            }
        }

        public void Stop()
        {
            _incomingMessageMustShutDown = true;
        }

        public void Enqueue(T message)
        {
            lock (_incomingQueue)
                _incomingQueue.Enqueue(message);


            _incomingMessageInQueue.Set();
        }

        internal void Start()
        {
            _incomingMessageMustShutDown = false;
            var incomingMessageBagsThread = new Thread(Dispatcher) { IsBackground = false };
            incomingMessageBagsThread.Start();
        }
    }
}
