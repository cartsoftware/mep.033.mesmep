﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using AreaMes.Server.MongoDb;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server
{
   public class LicenceManager
    {
        public static  LicenceManager Instance { get; }
        static LicenceManager()
        {
            Instance = new LicenceManager();
        }

        #region Public 
        public bool IsValid { get; private set; }
        public int MaxMachines { get; private set; }
        public int MaxUsers { get; private set; }
        public string LicencedAt_VatCode { get; private set; }
        public string LicencedAt_Name { get; private set; }
        public string LicencedAt_Key { get; private set; }
        public int LicencedAt_Type { get; private set; }
        public DateTime? LastValidationAt { get; private set; }
        public DateTime? ActivatedAt { get; private set; }
        #endregion


        #region Private

        private string fileName = "mep.dll";
        private string import_SERVER_IP_value;
        private string import_SERVER_DESIGN_PORT_value;
        private string import_SERVER_RUNTIME_PORT_value;

        internal Task SetAsync(object item)
        {
            throw new NotImplementedException();
        }

        private string import_WEBSERVER_RUNTIME_PORT_value;
        private string import_WEBSERVER_DESIGN_PORT_value;
        private string import_AREAMES_WEBSERVER_TABLET_PATH_value;
        private string import_AREAMES_WEBSERVER_DESIGN_PATH_value;
        private string import_KEY_value;
        private string import_LICENCE_TYPE_value;
        private string import_MAX_MACHINE_value;
        private string import_VAT_CODE_value;
        private string import_Company_Name;
        private string import_License_Type;
        private string import_Max_Machines;
        private string import_Max_Users;
        private DateTime import_Activation_Date;
        private bool import_isEnabled;
        private string licenceId;
        private int reCheckLicenceAfter = 30; // dopo 30 giorni viene ri-verificata la licenza
        private string activationServerPath = "";
        private string installerGUID = "9F8C3AF7-2706-49EE-9EA4-F67A97A226F9";
        #endregion



        public  void CheckAndImportInstallationFile()
        {
            licenceId = "B5513569-0B2C-4BE5-BADD-15152E35F9F3";

            string fullMepDllFile = Path.Combine(Path.GetTempPath(), fileName);
            string appFile = Path.Combine(Utils.Utility.GetApplicationFolder, "AreaMes.Server.exe.config");

            Logger.Default.InfoFormat("Checking file {0}...", fullMepDllFile);

            // -------------------------------------------------------------------------------------------
            // 1. Verifica se il file è presente
            if (!File.Exists(fullMepDllFile))
            {
                Logger.Default.InfoFormat("Checking file {0}... file not exist, cannot continue!", fullMepDllFile);
                return;
            }


            // -------------------------------------------------------------------------------------------
            // 2. Lrettura del file ed improtazione dati
            Logger.Default.InfoFormat("Checking file {0}... done!", fullMepDllFile);
            Logger.Default.InfoFormat("Reading file...", fullMepDllFile);

            string[] items = File.ReadAllLines(fullMepDllFile);
            foreach (var item in items)
            {
                var keyValue = item.Split('=');
                string key = keyValue[0];
                string value = keyValue[1];
                Logger.Default.InfoFormat("    {0}={1}", key, value);

                if (key== "SERVER_IP")
                    import_SERVER_IP_value = value.Trim();
                else if (key == "SERVER_DESIGN_PORT")
                    import_SERVER_DESIGN_PORT_value = value.Trim();
                else if (key == "SERVER_RUNTIME_PORT")
                    import_SERVER_RUNTIME_PORT_value = value.Trim();
                else if (key == "WEBSERVER_RUNTIME_PORT")
                    import_WEBSERVER_RUNTIME_PORT_value = value.Trim();
                else if (key == "WEBSERVER_DESIGN_PORT")
                    import_WEBSERVER_DESIGN_PORT_value = value.Trim();
                else if (key == "AREAMES_WEBSERVER_TABLET_PATH")
                    import_AREAMES_WEBSERVER_TABLET_PATH_value = value.Trim();
                else if (key == "AREAMES_WEBSERVER_DESIGN_PATH")
                    import_AREAMES_WEBSERVER_DESIGN_PATH_value = value.Trim();
                else if (key == "KEY")
                    import_KEY_value = value.Trim();
                else if (key == "LICENCE_TYPE")
                    import_LICENCE_TYPE_value = value.Trim();
                else if (key == "MAX_MACHINE")
                    import_MAX_MACHINE_value = value.Trim();
                else if (key == "COMPANY_VAT_CODE")
                    import_VAT_CODE_value = value.Trim();
                else if (key == "COMPANY_NAME")
                    import_Company_Name = value.Trim();
                else if (key == "MAX_USERS")
                    import_Max_Users = value.Trim();
            }
            Logger.Default.InfoFormat("Reading file... done!", fullMepDllFile);


            // -------------------------------------------------------------------------------------------
            // 4. Aggiornamento del file di configurazione
            Logger.Default.InfoFormat("Change configuration file AreaMes.Server.exe.config...", fullMepDllFile);
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["AREAMES_APISERVER_IP"].Value = import_SERVER_IP_value;
            config.AppSettings.Settings["AREAMES_APISERVER_PORT"].Value = import_SERVER_DESIGN_PORT_value;
            config.AppSettings.Settings["AREAMES_REALTIMESERVER_PORT"].Value = import_SERVER_RUNTIME_PORT_value;
            config.AppSettings.Settings["AREAMES_WEBSERVER_IP"].Value = import_SERVER_IP_value;
            config.AppSettings.Settings["AREAMES_WEBSERVER_TABLET_PORT"].Value = import_WEBSERVER_RUNTIME_PORT_value;
            config.AppSettings.Settings["AREAMES_WEBSERVER_DESIGN_PORT"].Value = import_WEBSERVER_DESIGN_PORT_value;
            config.AppSettings.Settings["AREAMES_WEBSERVER_TABLET_PATH"].Value = import_AREAMES_WEBSERVER_TABLET_PATH_value;
            config.AppSettings.Settings["AREAMES_WEBSERVER_DESIGN_PATH"].Value = import_AREAMES_WEBSERVER_DESIGN_PATH_value;
            config.Save(ConfigurationSaveMode.Modified);
            Logger.Default.InfoFormat("Change configuration file AreaMes.Server.exe.config... done!", fullMepDllFile);

            // 4. Cancellazione file
            Logger.Default.InfoFormat("Deleting file {0}...", fullMepDllFile);
            File.Delete(fullMepDllFile);
            Logger.Default.InfoFormat("Deleting file {0}... done!", fullMepDllFile);
        }

        public void CheckAndImportDatabase()
        {
            try
            {
                string fullInstallerFolder = Path.Combine(Path.GetTempPath(), "mep_temp");
                string fullCMD_InitMongodb = Path.Combine(fullInstallerFolder, "InitMongodb.bat");
                string fullCMD_RestoreBatFile = Path.Combine(fullInstallerFolder, "RestoreDB.bat");
                

                Logger.Default.InfoFormat("Checking database restore folder '{0}'...", fullInstallerFolder);

                // -------------------------------------------------------------------------------------------
                // 1. Verifica se il file è presente
                if (!Directory.Exists(fullInstallerFolder))
                {
                    Logger.Default.InfoFormat("Checking database restore folder '{0}'... folder not exist, cannot continue!", fullInstallerFolder);
                    return;
                }

                Logger.Default.InfoFormat("Checking database restore folder '{0}'... done!", fullInstallerFolder);

                // ---------------------------------------------------------------------------------------------
                // Avvio del servizio
                // ---------------------------------------------------------------------------------------------
                var resStartMongoDbService = RunBatFile(fullCMD_InitMongodb);
                if (resStartMongoDbService == 0)
                {
                    // ---------------------------------------------------------------------------------------------
                    // Restore del datbase
                    // ---------------------------------------------------------------------------------------------
                    var resRestoreBatFile = RunRestoreCommand(); // RunBatFile(fullCMD_RestoreBatFile);

                    if (resRestoreBatFile == 0)
                    {
                        Logger.Default.InfoFormat("Deleting database restore folder {0}...", fullInstallerFolder);
                        Directory.Delete(fullInstallerFolder, true);
                        Logger.Default.InfoFormat("Deleting database restore folder {0}... done!", fullInstallerFolder);
                    }
                }

            }
            catch (Exception exc)
            {
                Logger.Default.Error(exc.Message, exc);
            }
        }


        private int RunRestoreCommand()
        {
            string fullpathRestoreDb = Path.Combine(Path.GetTempPath(), "mep_temp");

            if (Directory.Exists(fullpathRestoreDb))
            {

                string s = String.Format(@"--db MEP --verbose ""{0}\{1}""", Path.GetTempPath(), "mep_temp");

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = @"C:\Program Files\MongoDB\Server\3.2\bin\mongorestore.exe";
                startInfo.Arguments = s;
                //startInfo.RedirectStandardOutput = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                string result = "";

                Logger.Default.InfoFormat("     Running script command  {0} {1}...", startInfo.FileName, s);


                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit(20 * 1000);
                    //result = process.StandardOutput.ReadToEnd();
                }
                Thread.Sleep(2000);

                Logger.Default.InfoFormat("     Running script command  {0} {1}... done!", startInfo.FileName, s);

                // Display the command output.
                //Console.WriteLine(result);

                return 0;
            }
            return 1;
        }

        private int RunBatFile(string filename)
        {
            Logger.Default.InfoFormat("Running script '{0}'... ", filename);

            Process process;
            var startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = filename;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = false;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            process = Process.Start(startInfo);

            process.WaitForExit(20000);
            //reads output and error of command prompt to string.
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            Logger.Default.InfoFormat("     Output >>");
            Logger.Default.InfoFormat(output);
            Logger.Default.InfoFormat("     Error >>");
            Logger.Default.InfoFormat(error);
            var exitCode = process.ExitCode;
            Logger.Default.InfoFormat("     Exit code: {0}", exitCode);

            process.Close();
            Logger.Default.InfoFormat("Running script '{0}'... done!", filename);

            return exitCode;
        }

        public async Task CheckAndImportInstallationKey()
        {
            Logger.Default.InfoFormat("Checking for installation key... ");
            if (String.IsNullOrEmpty(import_KEY_value))
            {
                Logger.Default.InfoFormat("Checking for installation key... key not exist, cannot continue!");
                return;
            }

            Logger.Default.InfoFormat("Upgrade KEY='{0}', VAT='{1}', LICENCE TPYE={2}...", import_KEY_value, import_VAT_CODE_value, import_LICENCE_TYPE_value);

            // verifica se esiste l'elemento
            var licenceDocs = await MongoRepository.Instance.All<Licenza>();

            Licenza licenceDoc;
            if (licenceDocs.Any())
            {
                Logger.Default.InfoFormat("     Licence exist, upgrading...");
                licenceDoc = licenceDocs.FirstOrDefault();
            }  
            else
            {
                Logger.Default.InfoFormat("     Licence doesn't exist, creating...");
                licenceDoc = new Licenza();
                licenceDoc.Id = licenceId;
            }

            licenceDoc.Vat = import_VAT_CODE_value;
            licenceDoc.Key = import_KEY_value;
            licenceDoc.IsEnabled = false;  // viene invalidata la licenza
            licenceDoc.LicenceType = Convert.ToInt32( import_LICENCE_TYPE_value);
            

            Logger.Default.InfoFormat("     Licence upgraded, now will be validated");
            await MongoRepository.Instance.SetAsync(licenceDoc);

        }


        /// <summary>
        ///  Viene verifica la licenza attuale
        /// </summary>
        public async Task CheckLicence() {

            // verifica se esiste l'elemento
            var licenceDocs = await MongoRepository.Instance.All<Licenza>();
            var licence = licenceDocs.FirstOrDefault();

            if (licence == null) return;

            this.IsValid = licence.IsEnabled;
            this.LicencedAt_Key = licence.Key;
            this.LicencedAt_VatCode = licence.Vat;
            this.LicencedAt_Type = Convert.ToInt32(licence.LicenceType);
            this.LastValidationAt = licence.LastServerValidationAt;
            this.LicencedAt_Name = licence.Name;
            this.MaxMachines = Convert.ToInt32(licence.MaxMachines);
            this.ActivatedAt = licence.ActivateAt;
            this.MaxUsers = Convert.ToInt32(licence.MaxUsers);

            bool mustBeNecessaryCheckLicence = false;
            if (licence.IsEnabled == false)
            {
                // viene avviato il processo di verifica della licenza, per la prima installazione
                mustBeNecessaryCheckLicence = true;
                Logger.Default.InfoFormat("     First activation, will be necessary check the server");
            }
            else
            {
                Logger.Default.InfoFormat("     Not First activation, last validation at {0}", LastValidationAt.Value);
                if (DateTime.Now.Subtract(LastValidationAt.Value).TotalDays > reCheckLicenceAfter)
                {
                    Logger.Default.InfoFormat("     Not First activation, last validation at {0}... after {1} expired, will be necessary check the server", LastValidationAt.Value, reCheckLicenceAfter);
                    mustBeNecessaryCheckLicence = true;
                }
            }


            if (mustBeNecessaryCheckLicence)
            {
                Logger.Default.InfoFormat("     Calling the activation server...");

                var client = new HttpClient();
                string[] resultObj;
                string rawResult;

                var callResult = await client.GetStringAsync("http://" + Const.AREAMES_WEB_SERVICE_ADDRESS + "/api/activation/activateToString?serialCode=" + LicencedAt_Key + "&companyVAT=" + LicencedAt_VatCode);
                Logger.Default.Info("http://" + Const.AREAMES_WEB_SERVICE_ADDRESS + "/api/activation/activateToString?serialCode=" + LicencedAt_Key + "&companyVAT=" + LicencedAt_VatCode);

                rawResult = callResult;
                resultObj = callResult.Split('|');

                bool result = resultObj[0].Equals("\"OK");
                bool result2 = resultObj[1].Equals(LicencedAt_VatCode);
                bool result3 = resultObj[2].Equals("True");
                string dateActivation = resultObj[3];
                DateTime import_Activation_Date;
                CultureInfo enUS = new CultureInfo("en-US");

                Logger.Default.InfoFormat("WebService call result:  {0}, {1}, {2}", Convert.ToString(result), Convert.ToString(result2), Convert.ToString(result3));

                if ((result == true) && (result2 == true) && (result3 == true))
                {
                    // valori di ritorno dal server
                    import_isEnabled = Convert.ToBoolean(resultObj[2]);
                    import_Company_Name = resultObj[4];
                    import_License_Type = resultObj[5];
                    import_Max_Machines = resultObj[6];
                    import_Max_Users = resultObj[7];

                    if (DateTime.TryParseExact(dateActivation, "M/dd/yyyy h:mm:ss tt", enUS, DateTimeStyles.None, out import_Activation_Date))
                        Console.WriteLine("Converted '{0}' to {1} ({2}).", dateActivation, import_Activation_Date, import_Activation_Date.Kind);
                    else
                        Console.WriteLine("'{0}' is not in an acceptable format.", dateActivation);


                    Logger.Default.InfoFormat("The server sent the value '{0}' for the activation date, '{1}' for the company name, " +
                                              "'{2}' for the license type, '{3}' for the max machine and '{4}' for the max user",
                                              import_Activation_Date, import_Company_Name, import_License_Type, import_Max_Machines, import_Max_Users);

                    Licenza licenceDoc;
                    if (licenceDocs.Any())
                    {
                        Logger.Default.InfoFormat("     Licence exist, upgrading after web service call...");
                        licenceDoc = licenceDocs.FirstOrDefault();
                    }
                    else
                    {
                        Logger.Default.InfoFormat("     Licence doesn't exist, creating after web service call...");
                        licenceDoc = new Licenza();
                        licenceDoc.Id = licenceId;
                    }

                    licenceDoc.Name = import_Company_Name;
                    licenceDoc.ActivateAt = import_Activation_Date;
                    licenceDoc.IsEnabled = import_isEnabled; //Validazione licenza
                    licenceDoc.LicenceType = Convert.ToInt32(import_LICENCE_TYPE_value);
                    licenceDoc.MaxMachines = Convert.ToInt32(import_Max_Machines);
                    licenceDoc.MaxUsers = Convert.ToInt32(import_Max_Users);

                    await MongoRepository.Instance.SetAsync(licenceDoc);

                    this.IsValid = import_isEnabled;
                    this.LicencedAt_Key = licence.Key;
                    this.LicencedAt_VatCode = licence.Vat;
                    this.LicencedAt_Type = Convert.ToInt32(licence.LicenceType);
                    this.LastValidationAt = licence.LastServerValidationAt;
                    this.LicencedAt_Name = licence.Name;
                    this.MaxMachines = Convert.ToInt32(licence.MaxMachines);
                    this.ActivatedAt = licence.ActivateAt;

                    Logger.Default.InfoFormat("Value written on database: IsValid = {0}, LicenceKey = {1}, VatCode = {2}, CompanyName = {4}, LicenseType = {3}, MaxMachine{5}, ActivatedAt = {6}",
                            this.IsValid, this.LicencedAt_Key, this.LicencedAt_VatCode, this.LicencedAt_Type, this.LicencedAt_Name, this.MaxMachines, this.ActivatedAt);

                    Logger.Default.Info("License correctly updated!");
                }
                else
                {
                    this.IsValid = false;
                    this.LicencedAt_Key = licence.Key;
                    this.LicencedAt_VatCode = licence.Vat;
                    this.LicencedAt_Type = Convert.ToInt32(licence.LicenceType);
                    this.LastValidationAt = licence.LastServerValidationAt;

                    Logger.Default.Info("ERROR: server  returned ----> " + rawResult);
                    Logger.Default.Info("THE LICENSE WASN'T UPDATED CORRECTLY!");
                    
                }
             
                Logger.Default.InfoFormat("     Calling the activation server and database upgrading... done!");


            }


        }

        Timer t = new Timer(TimerCallback, null, 2*60*1000, 30*60*1000);

        private async static void TimerCallback(Object o)
        {
            var licenceDocs = await MongoRepository.Instance.All<Licenza>();
            var licence = licenceDocs.FirstOrDefault();
            var client = new HttpClient();
            string[] resultObj;


            try
            {
                if (licence.Key == null)
                {
                    Logger.Default.Info("Something get wrong, license key = 'null'");
                    return;
                }
                else
                {
                    Licenza licenceDoc;
                    licenceDoc = licenceDocs.FirstOrDefault();
                    var callResult = await client.GetStringAsync("http://" + Const.AREAMES_WEB_SERVICE_ADDRESS + "/api/activation/checkToString?serialCode=" + licence.Key + "&companyVAT=" + licence.Vat);
                    Logger.Default.Info("Checking activation state for license: " + licence.Key + " and Vat numbert: " + licence.Vat);
                    resultObj = callResult.Split('|');
                    bool checkIfTrue = resultObj[2].Equals("True");

                    if (!checkIfTrue)
                    {
                        var license = LicenceManager.Instance;
                        
                        licenceDoc.IsEnabled = false;
                        await MongoRepository.Instance.SetAsync(licenceDoc);

                        license.IsValid = false;
                    }                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in TimerCallback(): {0}", ex.ToString());
            }
            
        }

    }
}
