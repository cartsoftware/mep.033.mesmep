﻿using AreaMes.Meta;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.SelfHost;
using Unosquare.Labs.EmbedIO;

namespace AreaMes.Server
{
    internal class WebServerManager
    {
        private static WebServerManager instance;
        public static WebServerManager Instance { get { return instance; } }
        static WebServerManager()
        {
            instance = new WebServerManager();
        }



        private IWebServer _serverDesign;
        private IWebServer _serverTablet;
        private IWebServer _serverMultimedia;

        
        public  void Run()
        {

            // creazione del servizio web DESIGNER
            if (Directory.Exists(Const.AREAMES_WEBSERVER_DESIGN_PATH))
            {

                _serverDesign = WebServer
                .Create(String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_DESIGN_PORT))
                .EnableCors()
                .WithLocalSession()
                .WithStaticFolderAt(Const.AREAMES_WEBSERVER_DESIGN_PATH);



                _serverDesign.RunAsync();
                Logger.Default.Info("Service WEB-SERVER Design running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_DESIGN_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_DESIGN_PATH));
            }


            if (Directory.Exists(Const.AREAMES_WEBSERVER_TABLET_PATH))
            {
                // creazione del servizio web TABLET
                _serverTablet = WebServer
                    .Create(String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_TABLET_PORT))
                    .EnableCors()
                    .WithLocalSession()
                    .WithStaticFolderAt(Const.AREAMES_WEBSERVER_TABLET_PATH);

                _serverTablet.RunAsync();
                Logger.Default.Info("Service WEB-SERVER Tablet running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_TABLET_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_TABLET_PATH));
            }


            if (Directory.Exists(Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH))
            {
                // creazione del servizio web TABLET
                _serverMultimedia = WebServer
                    .Create(String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT))
                    .EnableCors()
                    .WithLocalSession()
                    .WithStaticFolderAt(Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH);

                _serverMultimedia.RunAsync();
                Logger.Default.Info("Service WEB-SERVER Multimedia running, on " + String.Format("http://{0}:{1}", Const.AREAMES_WEBSERVER_IP, Const.AREAMES_WEBSERVER_MULTIMEDIA_PORT + ", local folder:" + Const.AREAMES_WEBSERVER_MULTIMEDIA_PATH));
            }
        }
    }
}
