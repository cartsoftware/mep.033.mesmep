﻿using AreaMes.Meta;
using AreaMes.Model;
using AreaMes.Server.MongoDb;
using AreaMes.Server.Utils;
using AreaMES.BatchLogic.Mep;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Net;

namespace AreaMes.Server
{
    public  class AlarmManager
    {
        private static AlarmManager instance;
        public static AlarmManager Instance { get { return instance; } }
        static AlarmManager()
        {
            instance = new AlarmManager();
        }

        public AlarmManager()
        {

        }

        public void Start()
        {
            // sottoscrizione del messaggio per il cambio di valore di una variabile legata ad un device
            MessageServices.Instance.Subscribe<DeviceValueChangedMessageAlarm>(DeviceValueChangedMessageAlarm.Name, async (s, e) => {

                var th = new Thread(() => { SendMail(e); });
                th.Start();

            });
        }


        public void SendMail(DeviceValueChangedMessageAlarm message)
        {
            try
            {
                //chiamo mongodb per il name macchina
                var macchina = MongoRepository.Instance.GetAsync<Macchina>(message?.DeviceId).Result;
                //scarico tutti gli indirizzi email
                var Operatori = MongoRepository.Instance.All<Operatore>().Result.FindAll(o => o.IsAbilityNotice == true);

                //foreach per ogni operatore mando la mail
                foreach (var Oper in Operatori)
                {
                    //controllo se la casella mail è populata
                    if (String.IsNullOrWhiteSpace(Oper.Mail)) return;

                    //Genero la prima parte dell'allarme
                    var arraytranslate = TranslationManager.GetTranslationItems(Oper.Lingua);
                    string sText = "";
                    sText += arraytranslate.FirstOrDefault(x => Convert.ToString(x.Key) == Convert.ToString("firstpartmail")).Value + macchina.Nome +" Serial Number: "+ macchina.SerialNumber+ " \n <br />";
                    sText += arraytranslate.FirstOrDefault(x => Convert.ToString(x.Key) == Convert.ToString("secondpartmail")).Value + DateTime.Now.ToString();
                    sText += arraytranslate.FirstOrDefault(x => Convert.ToString(x.Key) == Convert.ToString("terxpartmail")).Value + " \n <br />";

                    //inserisco gli allarmi scattati nella lingua operatore
                    foreach (var item in message)
                    {
                        var squaredNumbers = arraytranslate.FirstOrDefault(x => Convert.ToString(x.Key) == Convert.ToString(item.Name.ToLower()));
                        sText += squaredNumbers?.Value + " \n  <br />";
                    }

                    //preparo la mail e spedisco 
                    try
                    {

                        MailMessage mail = new MailMessage();
                        mail.From = new System.Net.Mail.MailAddress(Const.AREAMES_SMTP_SERVER_FROM.ToString(), "MEP SAWS");//(Const.AREAMES_SMTP_SERVER_FROM.ToString() 

                        // The important part -- configuring the SMTP client
                        SmtpClient smtp = new SmtpClient();
                        smtp.Port =Convert.ToInt32(Const.AREAMES_SMTP_SERVER_PORT);   // [1] You can try with 465 also, I always used 587 and got success
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                        smtp.UseDefaultCredentials = false; // [3] Changed this
                        smtp.Credentials = new NetworkCredential(mail.From.Address, "inizio2019");  // [4] Added this. Note, first parameter is NOT string.
                        smtp.Host = Const.AREAMES_SMTP_SERVER.ToString();

                        //recipient address
                        mail.To.Add(new MailAddress(Oper.Mail.ToString()));
                        mail.Subject = "Mep Service Alarm: " + macchina.Nome.ToString();
                        mail.Body = sText;
                        //Formatted mail body
                        mail.IsBodyHtml = true;

                        try
                        {
                            smtp.Send(mail);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                                        ex.ToString());
                        }

                        //MailMessage mail = new MailMessage(Const.AREAMES_SMTP_SERVER_FROM.ToString(), Oper.Mail.ToString());
                        //mail.To.Add(new MailAddress(Oper.Mail.ToString()));
                        //SmtpClient client = new SmtpClient();
                        //client.Port = Convert.ToInt32(Const.AREAMES_SMTP_SERVER_PORT);
                        //client.DeliveryMethod = SmtpDeliveryMethod.Network;

                        //client.Credentials = new NetworkCredential("controllomacchinemep@mepsaws.it","inizio2019");
                        //client.EnableSsl = true;

                        ////client.UseDefaultCredentials = true;
                        //client.Host = Const.AREAMES_SMTP_SERVER.ToString();  // "smtp.google.com";
                        //mail.Subject = "Mep Service Alarm: " + macchina.Nome.ToString();
                        //mail.Body = sText;
                        //try
                        //{
                        //    client.Send(mail);
                        //}
                        //catch (Exception ex)
                        //{
                        //    Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                        //                ex.ToString());
                        //}//aggiunto questa parte in un try catch cosi se una email fallisce continuo con le altre
                    }
                    catch (Exception exc) {}

                }

            }
            catch (Exception exc) { }

        }//close send mail
    }
}
