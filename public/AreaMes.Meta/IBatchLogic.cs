﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Meta
{
    public interface IBatchLogic
    {
       
        Task Init(IDataContainer data, IRepository repository, IMessageServices messages, IRealTimeManager realtime, string batchId);

        void Start();

        void Destroy();
    }
}
