﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Meta
{
    public interface IMessageServices
    {
        RegistrationInfo Subscribe(string messagename, Action<object, object> action);

        RegistrationInfo Subscribe<T>(string messagename, Action<object, T> action);

        void UnSubscribe(string messagename, long registrationid, Type type);

        void UnSubscribe(params RegistrationInfo[] infos);

         Task Publish<T>(string messagename, T item, object sender = null);
    }


    public class RegistrationInfo
    {
        public string MessageName { get; private set; }
        public long RegistrationId { get; private set; }
        public Type ArgumentType { get; private set; }

        public RegistrationInfo(string name, long id, Type argumenttype)
        {
            MessageName = name;
            RegistrationId = id;
            ArgumentType = argumenttype;
        }
    }
}
