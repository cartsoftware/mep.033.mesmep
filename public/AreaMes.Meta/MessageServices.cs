﻿using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Server.Utils
{
    public class MessageServices : IMessageServices
    {
        private static Lazy<MessageServices> _service = new Lazy<MessageServices>(() => new MessageServices(), System.Threading.LazyThreadSafetyMode.ExecutionAndPublication);

        public static IMessageServices Instance { get { return _service.Value; } }

        private static long lastId = 0;
        private static Dictionary<string, Dictionary<Type, Dictionary<long, Delegate>>> _subscriptors = new Dictionary<string, Dictionary<Type, Dictionary<long, Delegate>>>();

        private MessageServices() { }

        public RegistrationInfo Subscribe(string messagename, Action<object, object> action)
        {
            return Subscribe<object>(messagename, action);
        }

        public RegistrationInfo Subscribe<T>(string messagename, Action<object, T> action)
        {
            lock (_subscriptors)
            {
                if (!_subscriptors.ContainsKey(messagename))
                    _subscriptors.Add(messagename, new Dictionary<Type, Dictionary<long, Delegate>>());

                var t = typeof(T);

                if (!_subscriptors[messagename].ContainsKey(t))
                    _subscriptors[messagename].Add(t, new Dictionary<long, Delegate>());

                var v = Interlocked.Increment(ref lastId);
                _subscriptors[messagename][t].Add(v, action);

                return new RegistrationInfo(messagename, v, typeof(T));
            }
        }

        public void UnSubscribe(string messagename, long registrationid, Type type)
        {
            lock (_subscriptors)
            {
                Dictionary<Type, Dictionary<long, Delegate>> m;
                _subscriptors.TryGetValue(messagename, out m);


                Dictionary<long, Delegate> t;
                if (m != null && m.TryGetValue(type, out t))
                {
                    if (t.ContainsKey(registrationid))
                        t.Remove(registrationid);

                    if (t.Count == 0)
                        m.Remove(type);
                }
            }
        }
        public void UnSubscribe(params RegistrationInfo[] infos)
        {
            Interlocked.MemoryBarrier();

            foreach (var info in infos)
                UnSubscribe(info.MessageName, info.RegistrationId, info.ArgumentType);
        }

        public Task Publish(string messagename, object item = null, object sender = null)
        {
            return Publish<object>(messagename, item, sender);
        }

        public Task Publish<T>(string messagename, T item, object sender = null)
        {
            Dictionary<Type, Dictionary<long, Delegate>> m;
            _subscriptors.TryGetValue(messagename, out m);

            var type = typeof(T);

            Dictionary<long, Delegate> t;
            if (m != null && m.TryGetValue(type, out t))
            {
                var subscriptors = t.Values.ToList();
                return Task.Run(() =>
                {
                    foreach (var s in subscriptors)
                    {
                        s.DynamicInvoke(sender, item);
                    }
                });
            }

            return Task.FromResult<object>(null);
        }
    }


}
