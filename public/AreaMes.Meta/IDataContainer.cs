﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Meta
{
    public interface IDataContainer
    {
        Dictionary<string, DeviceItems> Get();

        DeviceItems Get(string pDevice);
    }
}
