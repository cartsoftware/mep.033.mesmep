﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Meta
{
    public class DeviceItems
    {
        public AreaM2M.RealTime.Dto.Device Device { get; set; }

        public Dictionary<string, DataItem> Items { get; set; }
    }

    public class DataItem
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public object OldValue { get; set; }

        public string ValueDecoded { get; set; }
        public string OldValueDecoded { get; set; }

        public DateTime ChangeAt { get; set; }

        public bool IsAllarm { get; set; }
        

        public bool IsChanged
        {
            get;set;
        }
    }


}
