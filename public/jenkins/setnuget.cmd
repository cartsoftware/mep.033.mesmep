:: ************************************
:: Andrea 09/02/2021
:: carica i prerequisiti della compilazione dei progetti 
:: ************************************

echo %cd%
md ..\packages
cd ..\packages
nuget install ..\AreaMes.Web.Designer\packages.config
nuget install ..\AreaMes.Model\packages.config
nuget install ..\AreaMES.BatchLogic.Mep\packages.config
nuget install ..\AreaMes.Tablet\packages.config
nuget install ..\AreaMes.Server\packages.config
nuget install ..\AreaMes.Meta\packages.config

