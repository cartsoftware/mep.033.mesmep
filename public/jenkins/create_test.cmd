:: ************************************
:: Andrea 12/02/2021
:: crea delle classi di test di alcune classi del progetto
:: ************************************

echo %cd%

cd public

dotnet new mstest -n AreaMes.Tablet.Test
dotnet sln add AreaMes.Tablet.Test/AreaMes.Tablet.Test.csproj
dotnet add AreaMes.Tablet.Test/AreaMes.Tablet.Test.csproj reference AreaMes.Tablet/AreaMes.Tablet.csproj

dotnet new mstest -n AreaMes.Model.Test
dotnet sln add AreaMes.Model.Test/AreaMes.Model.Test.csproj
dotnet add AreaMes.Model.Test/AreaMes.Model.Test.csproj reference AreaMes.Model/AreaMes.Model.csproj

dotnet new mstest -n AreaMes.Meta.Test
dotnet sln add AreaMes.Meta.Test/AreaMes.Meta.Test.csproj
dotnet add AreaMes.Meta.Test/AreaMes.Meta.Test.csproj reference AreaMes.Meta/AreaMes.Meta.csproj

dotnet new mstest -n AreaMes.Server.Test
dotnet sln add AreaMes.Server.Test/AreaMes.Server.Test.csproj
dotnet add AreaMes.Server.Test/AreaMes.Server.Test.csproj reference AreaMes.Server/AreaMes.Server.csproj

dotnet new mstest -n AreaMes.Web.Designer.Test
dotnet sln add AreaMes.Web.Designer.Test/AreaMes.Web.Designer.Test.csproj
dotnet add AreaMes.Web.Designer.Test/AreaMes.Web.Designer.Test.csproj reference AreaMes.Web.Designer/AreaMes.Web.Designer.csproj

dotnet new mstest -n AreaMES.BatchLogic.Mep.Test
dotnet sln add AreaMES.BatchLogic.Mep.Test/AreaMES.BatchLogic.Mep.Test.csproj
dotnet add AreaMES.BatchLogic.Mep.Test/AreaMES.BatchLogic.Mep.Test.csproj reference AreaMES.BatchLogic.Mep/AreaMes.BatchLogic.Mep.csproj