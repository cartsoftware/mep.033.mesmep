﻿//...
app.factory('DataFactory', ['$http', '$rootScope', '$timeout', '$cookies', '$window', function ($http, $rootScope, $timeout, $cookies, $window) {
    var datafactory = {};
    var connection;
    var hub;
    var _id, _username, _authdata, _nome, _cognome, _lingua;



    // metodo generale di ritorno
    datafactory.getAll = function (baseUrl) {
        
        return $http.get(baseUrl);
    }

    // metodo per ritornare tutti i tipi di macchine
    datafactory.getTipiMacchine = function () {

        return $http.get(urlApi + "/" + subUrlApi_TabelleTipoMacchina);
    }

    // metodo per ritornare le macchine
    datafactory.getMacchine = function () {

        return $http.get(urlApi + "/" + subUrlApi_Macchina);
    }

    // metodo per ritornare tutte le competenze degli operatori
    datafactory.getCompetenzeOperatori = function () {

        return $http.get(urlApi + "/" + subUrlApi_OperatoreCompetenza);
    }

    // metodo per ritornare tutte le competenze delle macchine
    datafactory.getCompetenzeMacchina = function () {

        return $http.get(urlApi + "/" + subUrlApi_MacchinaCompetenza);
    }

    // metodo per ritornare tutti materiali
    datafactory.getMateriali = function () {

        return $http.get(urlApi + "/" + subUrlApi_Materiali);
    }

    // metodo per ritornare le unità di misura
    datafactory.getUnitaDiMisura = function () {

        return $http.get(urlApi + "/" + subUrlApi_UnitaDiMisura);
    }

    // metodo per ritornare i parametri
    datafactory.getParametri = function () {

        return $http.get(urlApi + "/" + subUrlApi_Parametri);
    }

    // metodo per ritornare le CausaliRiavvio
    datafactory.getCausaliRiavvio = function () {

        return $http.get(urlApi + "/" + subUrlApi_CausaliRiavvio);
    }

    // metodo per ritornare le operazioni ciclo
    datafactory.GetOperazioniCiclo = function () {
        return $http.get(urlApi + "/" + subUrlOperazioniCiclo);
    }


    // metodo per ritornare le Code
    datafactory.GetLine = function () {
        return $http.get(urlApi + "/" + subUrlLine);
    }

    
    // metodo per ritornare le distinte basi
    datafactory.GetDistinteBasi = function () {
        return $http.get(urlApi + "/" + subUrlDistintaBase);
    }

    // metodo per ritornare le distinte basi dal materiale
    datafactory.GetDistinteBasiFromMateriale = function (idMateriale) {
        return $http.get(urlApi + "/" + subUrlDistintaBase + "/" + subUrlActionGetDistinteBasiFromMateriale + "?idMateriale=" + idMateriale);
    }

    // metodo per ritornare le distinte basi dal materiale
    datafactory.GetDistinteBasiFromMacchina = function (idMacchina) {
        return $http.get(urlApi + "/" + subUrlDistintaBase + "/" + subUrlActionGetDistinteBasiFromMacchina + "?idMacchina=" + idMacchina);
    }




    
    /*******************1****************************************/
    /*****Enums*************************************************/
    /***********************************************************/
    // otteniamo i livelli di accesso per gli utenti
    datafactory.GetLivelliAccesso = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetLivelliAccesso);
    }

    // otteniamo i tipi di dato
    datafactory.GetTipiDato = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTipiDato);
    }

    // otteniamo le unità di misura (as enum)
    datafactory.GetUmList = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetUmList);
    }


    // otteniamo i tipi di materiale
    datafactory.GetTipiMateriale = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTipiMateriale);
    }

    // otteniamo i tipi di causale
    datafactory.GetCausaliFase = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetCausaliFase);
    }

    // otteniamo gli stati della distinta fase
    datafactory.GetStatiDistintaBase = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetStatiDistintaBase);
    }

    // otteniamo gli stati del batch
    datafactory.GetStatiBatch = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetStatiBatch);
    }

    // metodo per ritornare i driver delle schede 
    datafactory.GetTipoDriver = function () {
        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTipoDriver);
    }

    // metodo per ritornare le lingue Disponibili
    datafactory.GetTipoLingua = function () {
        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTipoLingua); 
    }


    // metodo per ritornare le licenze
    datafactory.getLicence = function () {

        return $http.get(urlApi + "/" + subUrlApi_Licenza +"/GetLicence");
    }

    datafactory.GetBatch = function () {
        return $http.get(urlApi + '/' + subUrlBatch);
    };

    // otteniamo le traduzioni 
    datafactory.GetTranslations = function (languageId) {

        var languageInfo = localStorage.getItem(languageId);

        if (languageInfo != null)
            return JSON.parse(languageInfo).items; 
    }

    // metodo per logout
    datafactory.ClearCredentials = function () {
        $rootScope.globals = {};
        $cookies.remove('MesAuth');
        $http.defaults.headers.common.Authorization = 'Basic ';
        $window.location.href = 'login.html';
    }

    // metodo di autenticazione
    datafactory.Login = function (username, password) {
        
        return $http.get(urlApi + "/" + subUrlApi_Operatore + "/" + 'login?username=' + username + '&password=' + password);
    }

    // metodo per settare le credenziali
    datafactory.SetCredentials = function (id, username, password, nome, cognome,lingua) {
        var authdata = username + ':' + password; 

        $rootScope.globals = {
            currentUser: {
                idUser: id,
                username: username,
                authdata: authdata,
                nome: nome,
                cognome: cognome,
                lingua: lingua
            }
        };
     
        $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
        $cookies.put('MesAuth', JSON.stringify($rootScope.globals));
        $window.location.href = 'dashboard.html';

    }

  

    // Dashboard

    datafactory.GetDashboard = function () {
        return $http.get(urlApi + "/" + subUrlApi_Dashboard + "/get" );
    }

    datafactory.GetBatchAutoCounterInfo = function () {
        return $http.post(urlApi + "/" + subUrlBatch + "/getBatchAutoCounterInfo?getNew=true");
    };


    // metodo per ritornare le macchine
    datafactory.RegisterMachineToM2M = function (id) {
        return $http.get(urlApi + "/macchina/utils/RegisterMachineToM2M?id=" + id);
    };

    datafactory.RealTimeActivate = function (id) {
        return $http.get(urlApi + "/macchina/utils/RealTimeActivate?id=" + id);
    };

    datafactory.RealTimeDeactivate = function (id) {
        return $http.get(urlApi + "/macchina/utils/RealTimeDeactivate?id=" + id);
    };



    return datafactory;
}]);



app.config(['$translateProvider', function ($translateProvider) {
    // add translation table
    urlApi = $.cookie("WebApiC");
    $translateProvider.useUrlLoader(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTranslations);
    $translateProvider.preferredLanguage('it');
}]);
