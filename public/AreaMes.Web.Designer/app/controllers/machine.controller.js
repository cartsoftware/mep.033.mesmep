﻿var app = angular.module("AppMacchine", ['datatables']);

app.constant('Enums', {
    ViewState: {
        R: 'R',
        C: 'C',
        U: 'U',
        D: 'D',
        RO: 'RO'
    }
});


// --------------------------------------------------------------------------------------------------------------------------------
// API Service
// --------------------------------------------------------------------------------------------------------------------------------
app.factory('ServiceMacchine', function ($http) {

    var items = [];
    return {
        load: function () {
            $http.get(urlApi + '/macchina')
                 .success(function (data) {
                     items.push.apply(items, data);
                 });
        },

        loadSingle: function (id) {
            return $http.get(urlApi + '/macchina/' + id);
        }
    };
});


// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('ListOfMachinesCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', 'ServiceMacchine',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, ServiceMacchine) {

        $scope.items = [];
        $scope.currentItem = null;
        $scope.viewState = Enums.ViewState['R'];
        $scope.titolo = "Dettaglio";
        $scope.dtInstance = {};

        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(5)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);



        $scope.showItemDetail = function (item) { basicCrud_doShowItemDetail($scope, $http, subUrlApi_Macchina, Enums, item); };

        $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlApi_Macchina); };

        $scope.insert = function () {
            $scope.viewState = Enums.ViewState['C'];
            $scope.currentItem = null;
        };

        $scope.update = function () {  $scope.viewState = Enums.ViewState['U'];  };

        $scope.cancel = function () {  $scope.viewState = Enums.ViewState['R'];  };

        $scope.confirm = function () { basicCrud_doConfirm($scope, $http, subUrlApi_Macchina, Enums); };

        $scope.delete = function () { $scope.viewState = Enums.ViewState['D'];  };

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };

        basicCrud_doInitWatch($scope);
        $scope.reload();

    }]);

