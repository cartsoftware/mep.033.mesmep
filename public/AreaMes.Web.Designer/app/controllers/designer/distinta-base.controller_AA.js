﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('DistintaBaseCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory) {

    $scope.IsFullScreenOnEdit = true;
    $scope.items = [];

    $scope.idFase = {};

    // flag per abilitare la seconda tab dei tempi
    //$scope.isEnabledTabTimers = false;

    // flag per visualizzare i controlli sulle fasi
    $scope.isEnabledDettagliFase = false;
    

    $scope.currentItem = null;
    $scope.viewState = Enums.ViewState['R'];
    $scope.titolo = "Dettaglio";
    $scope.dtInstance = {};

    $scope.ListaMateriali = null;
    $scope.ListaUnitaDiMisura = null;
    $scope.ListaStatiDistintaBase = null;

    $scope.IsVisibleFaseCiclica = true;

    DataFactory.getMateriali().then(function (response) {
        $scope.ListaMateriali = response.data;
    });

    DataFactory.getUnitaDiMisura().then(function (response) {
        $scope.ListaUnitaDiMisura = response.data;
    });

    DataFactory.GetStatiDistintaBase().then(function (response) {
        $scope.ListaStatiDistintaBase = response.data;
    });

   

    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(5)
        .withOption('stateSave', true)
        .withOption("retrieve", true)
        .withOption('LengthChange', false)
        .withLanguageSource(urlLanguageDataTable)
        .withPaginationType("full_numbers")
        .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

    $scope.showItemDetail = function (item) {

        setTotaleTempi($scope, item);

        $scope.CausaliFase = null;
        $scope.CompetenzeOperatori = null;
        $scope.Macchine = null;
        $scope.CompetenzeMacchine = null;
        $scope.CausaliFase = null;
        $scope.OperazioniCiclo = null;
       

        basicCrud_doShowItemDetail($scope, $http, subUrlDistintaBase, Enums, item);

        // popolo le varie dropdown
        DataFactory.getCompetenzeOperatori().then(function (response) {
            $scope.CompetenzeOperatori = response.data;
        });

        DataFactory.getMacchine().then(function (response) {
            $scope.Macchine = response.data;
        });

        DataFactory.getCompetenzeMacchina().then(function (response) {
            $scope.CompetenzeMacchine = response.data;
        });

        DataFactory.GetCausaliFase().then(function (response) {
            $scope.CausaliFase = response.data;
            // creazione dell'albero
            createOrRefreshTreeView();
        });

        DataFactory.GetOperazioniCiclo().then(function (response) {
            $scope.OperazioniCiclo = response.data;
        });
       
    };

    $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlDistintaBase); };

    $scope.insert = function () {
        $scope.viewState = Enums.ViewState['C'];
        $scope.currentItem = new Object();
    };

    $scope.update = function () {
        $scope.viewState = Enums.ViewState['U'];
        //$scope.pitems = [];
        //$scope.pitems.push.apply($scope.pitems, $scope.currentItem.distintaBaseParametri);          //ZZZZZZZ
    };

    $scope.cancel = function () { $scope.viewState = Enums.ViewState['R']; resetAllFields(); createOrRefreshTreeView(); closeModal('#modalConfirmUndo'); };

    $scope.confirm = function () {
        resetAllFields();
        $scope.selectedNode = null;
        createOrRefreshTreeView();
        basicCrud_doConfirmWithOutReload($scope, $http, subUrlDistintaBase, Enums);
        setTotaleTempi($scope, $scope.currentItem)
    };

    $scope.confirmAndClose = function () {
        resetAllFields();
        createOrRefreshTreeView();
        basicCrud_doConfirm($scope, $http, subUrlDistintaBase, Enums);
    };

    $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlDistintaBase, Enums, item) };

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };

    basicCrud_doInitWatch($scope);

    $scope.reload();

    // funzioni che rendono mutuamente esclusive le drop down
    $scope.selectionMacchine = function (id) {
            $scope.isEnabledcompentenzaMacchineSelection[id] = ($scope.modelsSelMacchine[id] == -1);
    }

    $scope.selectionCompetenzaMacchine = function (id) {
            $scope.isEnabledMacchinaSelection[id] = ($scope.modelsSelCompetenzaMacchine[id] == -1);
    }

    // opzioni per l'albero
    $scope.treeOptions = {
        nodeChildren: "children",
        dirSelectable: true,
        injectClasses: {
            ul: "a1",
            li: "a2",
            liSelected: "a7",
            iExpanded: "a3",
            iCollapsed: "a4",
            iLeaf: "a5",
            label: "a6",
            labelSelected: "a8"
        }
    }

    // nodo base per il treeview
    $scope.dataForTheTree =
    [
        { "numeroFase": "", "nome": "Elenco fasi", "id": "0", "type":"-1", "children": [] }
    ];

    // menù con il click destro del mouse
    $scope.myContextDiv = "<ul id='contextmenu-node' class='nav nav-pills nav-stacked nav-bracket'>" +
                                "<li class='contextmenu-item' ng-click='clickedFaseSemplice()'><i class='fa fa-tag'></i>Fase semplice</li>" +
                                "<li class='contextmenu-item' ng-click='clickedFaseCiclica()'><i class='fa fa-gears'></i>Fase ciclica</li>" +
                                "<li class='contextmenu-item' ng-click='deleteFase()'><i class='fa fa-eraser'></i>Elimina fase</li>" +
                          "</ul>";

    // creazione fase semplice, andiamo a popolare solo i nodi del tree view
    $scope.clickedFaseSemplice = function () {
        
        // se non ho selezionato un nodo di fase ciclica devo aggiungere il ramo alla fina dell'albero
        // se ho selezionato un nodo di fase ciclica devo aggiungerlo dentro quest'ultima
        // creo l'id dinamicamente
        var idLeave = 0;
        var parentNode = 0;
        var tmpFaseForTree = {
            "numeroFase": idLeave,
            "nome": "Nuova Fase",
            "id": idLeave,
            "descrizione": "",
            "type": "0",
            "parentNode": parentNode,
            "isAndon": false,
            "isAutomatica" : false,
            "children": []
        }

        if ($scope.dataForTheTree[0].children != null)
            if ($scope.dataForTheTree[0].children[$scope.dataForTheTree[0].children.length - 1] != null)
                idLeave = $scope.dataForTheTree[0].children[$scope.dataForTheTree[0].children.length - 1].id;
        idLeave++;
       
        var bflag = false;
        if ($scope.selectedNode != null) {
            if ($scope.type == "1") {
                tmpFaseForTree.id = idLeave;
                tmpFaseForTree.numeroFase = idLeave;
                tmpFaseForTree.parentNode = $scope.dataForTheTree[0].children[$scope.dataForTheTree[0].children.length - 1].id;
                $scope.selectedNode.children.push(tmpFaseForTree);
            }
            else {
                bflag = true;
            }
        }
        else {
            bflag = true;
        }
        if (bflag) {
            
            tmpFaseForTree.id = idLeave;
            tmpFaseForTree.numeroFase = idLeave;
            $scope.dataForTheTree[0].children.push(tmpFaseForTree);
        }
            
       
    };

    // creazione fase ciclica, andiamo a popolare solo i nodi del tree view
    $scope.clickedFaseCiclica = function () {

        if ($scope.IsVisibleFaseCiclica) {
            // creo l'id dinamicamente
            var idLeave = 0;
            var parentNode = 0;
            //if ($scope.dataForTheTree[0].children != null)
            //    if ($scope.dataForTheTree[0].children[$scope.dataForTheTree[0].children.length - 1] != null)
            //        idLeave = $scope.dataForTheTree[0].children[$scope.dataForTheTree[0].children.length - 1].id;
            var arrayOrdered = orderById($scope.currentItem.fasi, "numeroFase", false);

            idLeave = arrayOrdered[arrayOrdered.length - 1].id;
            idLeave++;

            var tmpFaseForTree = {
                "numeroFase": idLeave,
                "nome": "Nuova Fase Ciclica",
                "id": idLeave,
                "descrizione": "",
                "type": "1",
                "parentNode": parentNode,
                "isAndon": false,
                "isAutomatica": false,
                "children": []
            }
            $scope.dataForTheTree[0].children.push(tmpFaseForTree);
        }
        else {
            alert("Già inserita una fase ciclica per questo nodo");
        }
        
    };

    $scope.selectedNode = null;

    // quando ho selezionato un ramo
    $scope.showSelected = function (sel) {

        // inizializzazione dei models
        resetAllFields();
    

        if (sel.id != 0) {
            $scope.selectedNode = sel;
            $scope.numeroFase = sel.numeroFase;
            $scope.nomeFase = sel.nome;
            $scope.descrizioneFase = sel.descrizione;
            $scope.type = sel.type;
            $scope.isAndon = sel.isAndon;
            $scope.isAutomatica = sel.isAutomatica;
            
            $scope.IsVisibleFaseCiclica = (sel.type == "0");
            
            // array per abilitazione macchine e competenze macchine
            $scope.isEnabledMacchinaSelection = {};
            $scope.isEnabledcompentenzaMacchineSelection = {};

            // abilitazione pannello dettagli fase
            $scope.isEnabledDettagliFase = true;

            // abilitazione checkbox di collegamento all'operazione ciclo
            $scope.isEnabledLinkedOption = false;

            angular.forEach($scope.CausaliFase, function (obj, key) {
                // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
                //if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                    $scope.isEnabledMacchinaSelection[obj.id] = true;
            });

            // trovo la fase dal suo numero
            angular.forEach($scope.currentItem.fasi, function (fase, key) {
                if (fase.numeroFase == sel.numeroFase) {
                    fillItemsInView(fase);
                }
            });
        }
    };


    // importazione da operazioni ciclo
    $scope.selectionOperazioneCiclo = function (id) {
        $scope.isEnabledLinkedOption = false;
        angular.forEach($scope.OperazioniCiclo, function (fase, key) {
            if (fase.id == id) {
                fillItemsInView(fase);
                $scope.isEnabledLinkedOption = true;
            }
        });
       
    }


    // inserimento/modifica fase
    $scope.confirmFase = function () {

        var shiftAllOtherPosition = false;
        //ordino l'array per il numero fasi
        var arrayOrdered = orderById($scope.currentItem.fasi, "numeroFase", false);

        if ($scope.selectedNode.numeroFase != $scope.numeroFase)
        {
            angular.forEach(arrayOrdered, function (fase, key) {
                if (fase.numeroFase == $scope.selectedNode.numeroFase)
                    arrayOrdered.splice(key, 1);
            });
        }
        // rimuovo l'eventuale oggetto e lo re-inserisco nel json
        angular.forEach(arrayOrdered, function (fase, key) {

            if (fase.numeroFase == $scope.numeroFase)
            {
                // controllo se ho cambiato il numero fase allora ho un shift di posizione
                if ($scope.selectedNode.numeroFase != $scope.numeroFase) {
                    shiftAllOtherPosition = true;
                }
                else // in questo caso non ho cambiato il numero di fase ma devo fare un aggiornamento della fase
                {
                    arrayOrdered.splice(key, 1); //elimino la fase per poi inserirla dopo
                }
            }

            if (shiftAllOtherPosition) {
                fase.numeroFase++;
            }
        });

        $scope.currentItem.fasi = arrayOrdered;
        var isCiclica = false;
        if ($scope.selectedNode.type == "1") isCiclica = true;
        // creazione dell'oggetto fase
        var tmpDistintaBaseFase = {
            numeroFase: $scope.numeroFase, nome: $scope.nomeFase, descrizione: $scope.descrizioneFase,
            barcode : $scope.currentItem.barcode, isDisabled : "false", macchine : [], 
            totaleTempiMacchina : [], manodopera : [], totaleTempiManodopera : [], 
            isAutomatica: $scope.isAutomatica, isAndon: $scope.isAndon, taktMacchina: 0, taktManodopera: 0, taktTotale: 0, isCiclica: isCiclica, id: $scope.numeroFase
        };

        var macchine = [];
        var manodopera = [];

        var totaleTempiMacchina = [];

        var totaleTempiManodopera = [];

        var tmpTempoTotaleManodopera = null;
        var tmpTempoTotaleMacchina = null;
        
        angular.forEach($scope.CausaliFase, function (obj, key) {
            
            // recupero il tempo uomo ed il tempo macchina trasformandoli in double
            var tmpTempoUomo = ($scope.modelsOreUomo[obj.id]*3600) + ($scope.modelsMinutiUomo[obj.id]*60) + $scope.modelsSecondiUomo[obj.id];
            var tmpTempoMacchina = ($scope.modelsOreMacchina[obj.id]*3600) + ($scope.modelsMinutiMacchina[obj.id]*60) + $scope.modelsSecondiMacchina[obj.id];

            // associo la competenza all'oggetto da passare
            var tmpCompetenzaOperatore = null;
            angular.forEach($scope.CompetenzeOperatori, function (singleCompetenzeOperatori, key) {
                if (singleCompetenzeOperatori.id == $scope.modelsSelCompetenzaOperatore[obj.id]) {
                    tmpCompetenzaOperatore = singleCompetenzeOperatori;
                }
            })

            var tmpMacchine = null;
            angular.forEach($scope.Macchine, function (singleMacchina, key) {
                if (singleMacchina.id == $scope.modelsSelMacchine[obj.id]) {
                    tmpMacchine = singleMacchina;
                }
            })

            var tmpCompetenzaMacchine = null;
            angular.forEach($scope.CompetenzeMacchine, function (singleCompetenzeMacchine, key) {
                if (singleCompetenzeMacchine.id == $scope.modelsSelCompetenzaMacchine[obj.id]) {
                    tmpCompetenzaMacchine = singleCompetenzeMacchine;
                }
            })

            // creazione degli oggetti
            var tmpFaseManodopera = {
                operatoreCompetenza: tmpCompetenzaOperatore,
                causaleFase: obj.id,
                tempo: tmpTempoUomo
            }
            var tmpFaseMacchina = {
                macchina: tmpMacchine,
                macchinaCompetenza: tmpCompetenzaMacchine,
                causaleFase: obj.id,
                tempo: tmpTempoMacchina
            }

            tmpTempoTotaleManodopera = {
                causaleFase: obj.id,
                totale: tmpTempoUomo
            }

            tmpTempoTotaleMacchina = {
                causaleFase: obj.id,
                totale: tmpTempoMacchina
            }
            
            // add sull'array
            manodopera.push(tmpFaseManodopera);
            macchine.push(tmpFaseMacchina);
            
        });

        // chiedere ad Alberto per questa cosa dei tempi totali
        totaleTempiManodopera = tmpTempoTotaleManodopera;
        totaleTempiMacchina = tmpTempoTotaleMacchina;

        tmpDistintaBaseFase.macchine = macchine;
        tmpDistintaBaseFase.totaleTempiMacchina = totaleTempiMacchina;
        tmpDistintaBaseFase.manodopera = manodopera;
        tmpDistintaBaseFase.totaleTempiManodopera = totaleTempiManodopera;

        if ($scope.currentItem.fasi == null)
            $scope.currentItem.fasi = [];
        $scope.currentItem.fasi.push(tmpDistintaBaseFase);

        //aggiornamento dell'albero
        //createOrRefreshTreeView();

        //basicCrud_doConfirmWithOutReload($scope, $http, subUrlDistintaBase, Enums);

        //$scope.isEnabledDettagliFase = false;
    }

    // funzione per cancellare un ramo dell'albero
    $scope.deleteFase = function ()
    {
       
        var arrayOrdered = orderById($scope.currentItem.fasi, "numeroFase", false);
        angular.forEach(arrayOrdered, function (fase, key) {
            if ($scope.selectedNode.numeroFase == fase.numeroFase) {
                arrayOrdered.splice(key, 1);
            }
        });
        $scope.currentItem.fasi = arrayOrdered;
        createOrRefreshTreeView();
    }

    //funzione per popolare gli elementi della finestra di dettaglio quando seleziono un ramo dal treeview (tempi) o seleziono la voce dalla ddl delle operazioni ciclo
    function fillItemsInView(item) {
        angular.forEach($scope.CausaliFase, function (obj, key) {

            $scope.isEnabledMacchinaSelection[obj.id] = true;
            $scope.isEnabledcompentenzaMacchineSelection[obj.id] = true;

            // valore selezionato per le competenze operatore
            if (item.manodopera[obj.id].operatoreCompetenza != null) {
                $scope.modelsSelCompetenzaOperatore[obj.id] = item.manodopera[obj.id].operatoreCompetenza.id;
            }

            // recupero il tempo dell'operatore per ogni causale e popolo i vari input text
            var tmpDoubleOperatore = item.manodopera[obj.id].tempo;
            $scope.modelsOreUomo[obj.id] = parseInt(tmpDoubleOperatore / 3600);
            $scope.modelsMinutiUomo[obj.id] = parseInt((tmpDoubleOperatore % 3600) / 60);
            $scope.modelsSecondiUomo[obj.id] = parseInt(tmpDoubleOperatore % 60);

            // popolo le drop down list con il valore selezionato
            if (item.macchine[obj.id].macchina == null) {
                $scope.modelsSelMacchine[obj.id] = -1;
                $scope.isEnabledMacchinaSelection[obj.id] = false;
            }
            else
                $scope.modelsSelMacchine[obj.id] = item.macchine[obj.id].macchina.id;


            if (item.macchine[obj.id].macchinaCompetenza == null) {
                $scope.modelsSelCompetenzaMacchine[obj.id] = -1;
                $scope.isEnabledcompentenzaMacchineSelection[obj.id] = false;
            }
            else
                $scope.modelsSelCompetenzaMacchine[obj.id] = item.macchine[obj.id].macchinaCompetenza.id;

            // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
            if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                $scope.isEnabledMacchinaSelection[obj.id] = true;

            // recupero il tempo della macchina per ogni causale e popolo i vari input text
            var tmpDoubleMacchina = item.macchine[obj.id].tempo;
            $scope.modelsOreMacchina[obj.id] = parseInt(tmpDoubleMacchina / 3600);
            $scope.modelsMinutiMacchina[obj.id] = parseInt((tmpDoubleMacchina % 3600) / 60);
            $scope.modelsSecondiMacchina[obj.id] = parseInt(tmpDoubleMacchina % 60);
        });

        
    }

    // aggiornamento dell'albero
    function createOrRefreshTreeView()
    {
        $scope.defaultExpanded = [];
        $scope.dataForTheTree[0].children = [];
        if ($scope.currentItem.fasi != null) {
            var arrayOrdered = orderById($scope.currentItem.fasi, "numeroFase", false);
            var parentNode = 0;
            angular.forEach(arrayOrdered, function (fase, key) {
                var type = "0";
                if (fase.isCiclica) {
                    type = "1";
                }

                
                var tmpFase = {
                    "numeroFase": fase.numeroFase,
                    "nome": fase.nome,
                    "id": fase.id,
                    "descrizione": fase.descrizione,
                    type: type,
                    isAndon: fase.isAndon,
                    isAutomatica: fase.isAutomatica,
                    "children": []
                }
                if (parentNode == 0) {
                    $scope.dataForTheTree[0].children.push(tmpFase);
                }
                else {
                    // provvisorio per inserire un nodo figlio dentro ad un nodo ciclico
                    angular.forEach($scope.dataForTheTree[0].children, function (leaf, key) {
                        if (leaf.id == parentNode) {
                            leaf.children.push(tmpFase);
                        }
                    });
                    parentNode = 0;
                }

                
                if (fase.isCiclica) {
                    parentNode = fase.id;
                }

                
            });
        }
        $scope.defaultExpanded = [$scope.dataForTheTree[0].children[0], $scope.dataForTheTree[0].children[1]];
    }

    // ordinamento di json per campo richiesto
    function orderById(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };

    // funzione provvisoria per gestire solo un pulsante di salvataggio
    $scope.globalChange = function () {
        $scope.confirmFase();
    }

    // funzione per resettare tutti i campi
    function resetAllFields()
    {
        $scope.modelsOreUomo = {};
        $scope.modelsMinutiUomo = {};
        $scope.modelsSecondiUomo = {};
        $scope.modelsSelCompetenzaOperatore = {};
        $scope.modelsOreMacchina = {};
        $scope.modelsMinutiMacchina = {};
        $scope.modelsSecondiMacchina = {};
        $scope.modelsSelCompetenzaMacchine = {};
        $scope.modelsSelMacchine = {};

        $scope.modelOperazioniCiclo = {};
        $scope.numeroFase = null;
        $scope.nomeFase = null;
        $scope.descrizioneFase = null;

        $scope.isEnabledDettagliFase = null;

        $scope.pcurrentItem = null; // Clear "distinta-base-parametri" (Dettaglio)
    }





    //------------------------------------------------------------------------------------------------------------------------------
    // distinta-base-parametri
    //------------------------------------------------------------------------------------------------------------------------------
    $scope.pdtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(5)
    .withOption('stateSave', true)
    .withOption("retrieve", true)
    .withOption('LengthChange', false)
    .withLanguageSource(urlLanguageDataTable)
    .withPaginationType("full_numbers")
    .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


    ////$scope.cbUnitaDiMisure = null;
    ////DataFactory.getUnitaDiMisura().then(function (response) {
    ////    $scope.cbUnitaDiMisure = response.data;
    ////});


    //// Combo box Unità di Misura
    ////$scope.cbUnitaDiMisure = null;
    ////DataFactory.GetUmList().then(function (response) {
    ////    var xs = response.data;
    ////    angular.forEach(xs, function (x) {
    ////        x.value = x.value.toLowerCase();
    ////    })
    ////    $scope.cbUnitaDiMisure = xs;
    ////});

    // Combo box Tipi Dato
    $scope.cbTipiDato = null;
    DataFactory.GetTipiDato().then(function (response) {
        var xs = response.data;
        angular.forEach(xs, function (x) {
            x.value = x.value.toLowerCase();
        })
        $scope.cbTipiDato = xs;                   
    });

    //----------------------


    $scope.pIsFullScreenOnEdit = false;
    //$scope.pitems = [];
    $scope.pcurrentItem = null;
    $scope.pviewState = Enums.ViewState['R'];
    $scope.ptitolo = "Dettaglio";
    $scope.pdtInstance = {};
    $scope.originalPCurrentItem = [];

    $scope.ptitle = function () { pdoTitle($scope, Enums); };
    function pdoTitle($scope, Enums) {
        switch ($scope.pviewState) {
            case Enums.ViewState['R']:
                $scope.ptitolo = "Dettaglio ";// + $scope.currentItem.codice;
                break;
            case Enums.ViewState['U']:
                $scope.ptitolo = "Modifica ";// + $scope.currentItem.codice;
                break;
            case Enums.ViewState['C']:
                $scope.ptitolo = "Inserimento ";
                break;
        }
    };


    $scope.pinsert = function () {
        $scope.pviewState = Enums.ViewState['C'];
        $scope.pcurrentItem = new Object();
        $scope.pcurrentItem.tipologia = new Object();
        $scope.originalPCurrentItem = [];
    };


    $scope.pupdate = function () {
        $scope.pviewState = Enums.ViewState['U'];
        $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));
    };

    $scope.pcancel = function () {
        $scope.pviewState = Enums.ViewState['R'];
        var itemToUndo = null;
        var allParams = $scope.currentItem.distintaBaseParametri;

        for (var orig = 0; orig < $scope.originalPCurrentItem.length; orig++){
            itemToUndo = $scope.originalPCurrentItem[orig].codice;
            for (var i = 0; i < allParams.length; i++) {
                if (allParams[i].codice == itemToUndo) {
                    allParams[i] = $scope.originalPCurrentItem[orig];
                    i = allParams.length;
                }
            }
            if ($scope.pcurrentItem.codice == itemToUndo)
                $scope.pcurrentItem = $scope.originalPCurrentItem[orig]; //restore detail
        }
        $scope.originalPCurrentItem = [];
    };


    $scope.pdelete = function (item) {
        $scope.pviewState = Enums.ViewState['D'];
        var itemToRemove = $scope.pcurrentItem.codice;
        var allParams = $scope.currentItem.distintaBaseParametri;
        for (var i = 0; i < allParams.length; i++) {
            if (allParams[i].codice == itemToRemove) {
                allParams.splice(i, 1); 
                i = allParams.length;
            }
        }
        $scope.pviewState = Enums.ViewState['R'];
        $scope.originalPCurrentItem = [];
        angular.element(document.querySelector('#pmodalConfirmDelete')).modal('hide');
    };


    $scope.pconfirm = function () {
        if ($scope.pviewState === Enums.ViewState['C']) {
            if ($scope.currentItem.distintaBaseParametri == null) 
                $scope.currentItem.distintaBaseParametri = [];
            
            $scope.currentItem.distintaBaseParametri.push($scope.pcurrentItem)
        }
        else if ($scope.pviewState === Enums.ViewState['U']) {
     
            var um = $.grep(  $scope.ListaUnitaDiMisura, function (e) { return e.id == $scope.pcurrentItem.unitaDiMisura.id; });
            $scope.pcurrentItem.unitaDiMisura.item = um[0];
        }
        $scope.originalPCurrentItem = [];
        $scope.pviewState = Enums.ViewState['R'];
    };
    

    $scope.pshowItemDetail = function (item) {
        $scope.pcurrentItem = item;
        if ($scope.pviewState == Enums.ViewState['U'] && !itemExists($scope.originalPCurrentItem, item.codice))
            $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));
    };
    function itemExists(arr, value) {
        var r = false
        angular.forEach(arr, function (item) {
            if (item.codice === value)
                r = true;
        });
        return r;
    }



    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
        var x = $scope;
    });




    $scope.$watch('pviewState', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.ptitle();
    });

    //------------------------------------------------------------------------------------------------------------------------------
    // distinta-base-parametri
    //------------------------------------------------------------------------------------------------------------------------------


    function setTotaleTempi(_scope, item) {
        if (_scope.tothh == null) _scope.tothh = 0;
        if (_scope.totmm == null) _scope.totmm = 0;
        if (_scope.totss == null) _scope.totss = 0;

        var totseconds = 0;
        var minuteseconds = 0;
        var seconds = 0;
        var fasi = item.fasi;
        angular.forEach(fasi, function (fase) {
            if (fase.manodopera != null) {
                var mans = fase.manodopera;
                angular.forEach(mans, function (man) {
                    totseconds += man.tempo;
                })
            }
            if (fase.macchine != null) {
                var macs = fase.macchine;
                angular.forEach(macs, function (mac) {
                    totseconds += mac.tempo;
                })
            }
        });

        _scope.tothh = Math.floor(totseconds / 3600);
        _scope.totmm = Math.floor((totseconds - (_scope.tothh * 3600)) / 60);
        _scope.totss = Math.floor((totseconds - (_scope.tothh * 3600) - (_scope.totmm * 60)));
    }


}]);





app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});


// direttiva per il menu con il tasto destro del mouse
app.directive("contextMenu", function ($compile) {
    contextMenu = {};
    contextMenu.restrict = "AE";
    contextMenu.link = function (lScope, lElem, lAttr) {
        lElem.on("contextmenu", function (e) {
            e.preventDefault(); // default context menu is disabled
            //  The customized context menu is defined in the main controller. To function the ng-click functions the, contextmenu HTML should be compiled.
            lElem.append($compile(lScope[lAttr.contextMenu])(lScope));
            // The location of the context menu is defined on the click position and the click position is catched by the right click event.
            //$("#contextmenu-node").css("left", e.clientX);
            //$("#contextmenu-node").css("top", e.clientY);
        });
        lElem.on("mouseleave", function (e) {
            // on mouse leave, the context menu is removed.
            if ($("#contextmenu-node"))
                $("#contextmenu-node").remove();
        });
    };
    return contextMenu;



});

