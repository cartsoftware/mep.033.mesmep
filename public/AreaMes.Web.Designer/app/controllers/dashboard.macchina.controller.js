﻿

// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('DashboardMacchinaCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$translate', '$rootScope', '$location',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $translate, $rootScope, $location) {

        var connection;
        var hub;
        $scope.time = 0;
        $scope.timeW = 0;
        //parametri
        $scope.StatoMacchina = false;
        $scope.AllarmiMacchina = 0;
        $scope.AllarmiMacchinaSelezionato = "";
        $scope.ParametroX = 0;
        $scope.ParametroY = 0;
        $scope.NomeMacchina;
        $scope.IndirizzoIp;
        $scope.isActiveRealtime = false;
        $scope.Programma = "0";
        $scope.PartNumber = 0;
        $scope.PezziProdurre = 0;
        $scope.PezziProdotti = 0;
        $scope.FeedRateAttuale = 0;
        $scope.BladeSpeedAttuale = 0;
        $scope.CorrenteAssorbita = 0;
        $scope.datiGrafico;
        $scope.MisuraProgrammata = 0;
        $scope.TestaturaLama = 0;
        //tempo taglio singolo
        $scope.TempoTaglioSingoloH = 0;
        $scope.TempoTaglioSingoloM = 0;
        $scope.TempoTaglioSingoloS = 0;
        //tempo taglio totale 
        $scope.TempoTotaleTagliH = 0;
        $scope.TempoTotaleTagliM = 0;
        $scope.TempoTotaleTagliS = 0;
        //tempo vita macchina
        $scope.TempoVitaMacchinaH = 0;
        $scope.TempoVitaMacchinaM = 0;
        $scope.TempoVitaMacchinaS = 0;

        $scope.isRealTime = false;
        $scope.realTimeErrorMessage = '';
        $scope.ListaAllarmi = [];
        $scope.ListaWarning = [];
        $scope.isActiveAlarm = false;
        $scope.WarningMacchinaSelezionato = "";
        $scope.NumeroWarning = 0;

        var modalTestAreaM2MLoading = angular.element(document.querySelector('#modalTestAreaM2MLoading'));
        var modalTestAreaM2MError = angular.element(document.querySelector('#modalTestAreaM2MError'));
        var modalRealTimeActivate = angular.element(document.querySelector('#modalRealTimeActivate'));
        var modalRealTimeDeactivate = angular.element(document.querySelector('#modalRealTimeDeactivate'));
  

        $scope.Machine_Time = "Tempo";
        $scope.Machine_Value = "Valore";
        $scope.CorrenteAssorbitaString = "Corrente";
        //prendo url e parametri 
        stringUrl = $location.$$absUrl.substring(1);
        var c = stringUrl.split('=');
        $scope.IdMacchina = c[1];
        

        //traduzioni 
        //$translate.use($scope.$parent.globals.currentUser.lingua);
        $translate.use($scope.$parent.globals.currentUser.lingua).then(function (translation) {
            $scope.Machine_Value = $translate.instant('dashboard_Machine_Value');
            $scope.Machine_Time = $translate.instant('dashboard_Machine_Time');
            $scope.CorrenteAssorbitaString = $translate.instant('dashboard_Machine_CorrenteAssorbita');
            
            $translate.use($scope.$parent.globals.currentUser.lingua);

            //opzioni Grafico
            $scope.data_multiBarChart = [{ key: $scope.CorrenteAssorbitaString, values: [] }];

            //opzioni grafico
            $scope.options_multiBarChart = {
                chart: {
                    type: 'multiBarChart',
                    height: 450,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 45,
                        left: 45
                    },
                    clipEdge: false,
                    duration: 500,
                    stacked: false,
                    xAxis: {

                        // axisLabel: 'Tempo (Secondi)',
                        axisLabel: $scope.Machine_Time,
                        showMaxMin: false,
                        tickFormat: function (d) {
                            return d;
                            //return d3.format(',f')(d);
                        }
                    },
                    yAxis: {
                        //axisLabel: 'Valore Corrente',
                        axisLabel: $scope.Machine_Value,
                        axisLabelDistance: -20,
                        tickFormat: function (d) {
                            return d3.format(',.1f')(d);
                        }
                    }
                }

            };
        });

        //inizio la connessione a signal
        $rootScope.signalRconnection = $.cookie("SigalRc");
        connection = $.hubConnection($rootScope.signalRconnection);
        connection.logging = false;
        hub = connection.createHubProxy('AreaMESManager');
        hub.logging = false;

        connection.start();


        // Viene intercettato l'evento di cambio stato in SignalR
        $(connection).bind("onStateChanged", function (e, data) {
            if (data.newState === $.signalR.connectionState.connected) {
                doSubscribe($scope.IdMacchina);
            }
            else if (data.newState === $.signalR.connectionState.disconnected) {
                setTimeout(function () { connection.start(); }, 5 * 1000); // Restart connection after 5 seconds.
            }
        });


        // Viene intercettato l'evento di cambio di variabile in SignalR
        hub.on('OnDeviceValueChange', function (batchId, message) {
            $scope.$apply(function () {
                //ignora messaggi per altri batch
                angular.forEach(message, function (e, i) {
                    $scope.MatchVariable(e.Name, e.Value, e.ChangeAt);
                });

            });
        });


        connection.start();


        //gestione degli allarmi e warning  in rotazione, ogni 5 secondi cambio dettaglio allarme 
        setInterval(function () {
            try {
                //se azzerano l'array degli allarmi la variabile visualizzata (AllarmiMacchinaSelezionato) va a ""
                $scope.$apply(function () {

                if ($scope.time >= $scope.ListaAllarmi.length) $scope.time = 0;
                    if ($scope.ListaAllarmi.length != 0) {
                        //invoco il translate e invoco il toLowerCase per settare tutto in minuscolo altriemnti non funziona il translate
                        var translation = $translate.instant($scope.ListaAllarmi[$scope.time].toLowerCase());
                        $scope.AllarmiMacchinaSelezionato = translation;
                        $scope.time = $scope.time + 1;
                    }//
                    else  AllarmiMacchinaSelezionato = "";
                        
                if ($scope.timeW >= $scope.ListaWarning.length) $scope.timeW = 0;
                if ($scope.ListaWarning.length == 0) {
                    $scope.WarningMacchinaSelezionato = "";
                        return;
                    }//

                  //invoco il translate e invoco il toLowerCase per settare tutto in minuscolo altriemnti non funziona il translate
                var translation = $translate.instant($scope.ListaWarning[$scope.timeW].toLowerCase());
                   $scope.WarningMacchinaSelezionato = translation;
                   $scope.timeW = $scope.timeW + 1;   

                });
            }
            catch{}
        }, 8000);





        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(5)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


        // **********************************************************************
        function doSubscribe(batchId) {
            $scope.StatoMacchinaTest = null;

            hub.invoke('SubscribeToMachine', batchId).done(function (res) {
                // caricamento di ulteriori informazioni
                $scope.$apply(function () {

                    var x = res;

                    angular.forEach(x.Variabili, function (i, e) {
                        if (i.Codice == "StatoMacchina")
                            $scope.StatoMacchinaTest = i.UltimoValore;
                    });

                    //se l'array non è vuoto importo tutte le varibaili 
                    if (x.Variabili.length != 0 && $scope.StatoMacchinaTest != null) {
                        //prima le impostazioni della macchina
                        $scope.NomeMacchina = res.Macchina.Tipologia.Item.Nome,
                        $scope.IndirizzoIp = res.Macchina.IndirizzoIp,
                        $scope.NomeMacchinaUtente = res.Macchina.Nome;
                        $scope.isActiveRealtime = res.Macchina.isActiveRealtime;
                        //checko a true o a folse il bottone del real protocol
                        $("#switchValue").prop('checked', $scope.isActiveRealtime);
                        $scope.StatoMacchina = true;

                        //importo le varibili storiche della corrente  con un ciclo foreach
                        angular.forEach(x.Variabili, function (i, e) {
                            if (i.Codice == "CorrenteAssorbita") {
                                angular.forEach(i.Valori, function (i, e) {
                                    angular.forEach(i.Valori, function (i, e) {
                                        //converto l'ora in un formato comprensibile e lo aggiungo al database
                                        //$scope.ora= $scope.convert(i.Ora);
                                        //$scope.data_multiBarChart[0].values.push({ x: $scope.ora, y: i.Valore });
                                    });
                                });
                            }
                        });

                        //poi importo le altre variabili utilizzando il matchvariable
                        angular.forEach(x.Variabili, function (i, e) {
                            if (i.UltimoValore != null)
                            $scope.MatchVariable(i.Nome, i.UltimoValore, i.ChangeAt);
                        });

                    }//fine if

                    else {
                        //se l'array è vuoto cioè macchina spenta importo solo le impostazioni macchina salvate sul database 
                        $scope.NomeMacchina = res.Macchina.Tipologia.Item.Nome;
                        $scope.IndirizzoIp = res.Macchina.IndirizzoIp;
                        $scope.NomeMacchinaUtente = res.Macchina.Nome;
                        $("#switchValue").prop('checked', res.Macchina.isActiveRealtime);
                      
                    }

                });//fine apply
            }).fail(function (error) {
                // errore nella sottoscrizione
            });
        }


        //funzione che utilizzo per converire l'ora data time in un ora comprensibile 
        $scope.convert = function (d) {

            return d3.time.format('%d/%m/%y %I %p:%I:%M:%S')(new Date(d))
              
            chart.xScale(d3.time.scale());
        };


       

        // ------------------------------------------------------------------------------------
        //richiesta di attivazione e disabilitazione del realtime
        $scope.ToRealTime = function () {

            //true voglio attivare il real time 
            if ($scope.isRealTime) {
                modalRealTimeActivate.modal('toggle');      // visualizzazione finestra di domanda per Abilitazione
                $scope.isRealTime = false;
            }
            else 
                modalRealTimeDeactivate.modal('toggle');    // visualizzazione finestra di domanda per Disabilitazione
            
               
        };

        // ------------------------------------------------------------------------------------
        //richiesta di attivazione del realtime 
        $scope.ToRealTimeActivate = function () {

            modalRealTimeActivate.modal('toggle');      // chiusura finestra di domanda per Abilitazione
            modalTestAreaM2MLoading.modal('toggle');    // visualizzazione finestra di loading

            // chiamata al server locale
            DataFactory.RealTimeActivate($scope.IdMacchina).then(function (response) {

                
                $scope.realTimeErrorMessage = response.data.error;

                modalTestAreaM2MLoading.modal('toggle');         // chiusura della finestra di loading

                if (response.data.success) 
                    $scope.isRealTime =true;
                else
                    modalTestAreaM2MError.modal('toggle');      // visualizzazione finestra di errore
            });
        };


        // ------------------------------------------------------------------------------------
        //richiesta di disabilitazione del realtime
        $scope.ToRealTimeDeactivate = function () {
            modalRealTimeDeactivate.modal('toggle');    // chiusura finestra di domanda per Disabilitazione
            modalTestAreaM2MLoading.modal('toggle');    // visualizzazione finestra di loading

            // chiamata al server locale
            DataFactory.RealTimeDeactivate($scope.IdMacchina).then(function (response) {
                $scope.realTimeErrorMessage = response.data.error;
               
                modalTestAreaM2MLoading.modal('toggle');  // chiusura della finestra di loading

                if (response.data.success) 
                    $scope.isRealTime = false;
                else
                    modalTestAreaM2MError.modal('toggle');  // visualizzazione finestra di errore
            });
        };

        // ------------------------------------------------------------------------------------
        //funzione utilizzata per  matchare le nostre variabili 
        $scope.MatchVariable = function (code, value, changeAt) {

            $scope.StatoMacchina = true;

            if (code === "ParametroX") {
                value = parseFloat(value).toFixed(2);
                $scope.ParametroX = value;
            }
             
            if (code === "ParametroY")
                $scope.ParametroY = value;
            if (code === "TempoTaglioSingoloH")
                $scope.TempoTaglioSingoloH = value;
            if (code === "TempoTaglioSingoloM")
                $scope.TempoTaglioSingoloM = value;
            if (code === "TempoTaglioSingoloS")
                $scope.TempoTaglioSingoloS = value;
            if (code === "TempoTotaleTagliH")
                $scope.TempoTotaleTagliH = value;
            if (code === "TempoTotaleTagliM")
                $scope.TempoTotaleTagliM = value;
            if (code === "TempoTotaleTagliS")
                $scope.TempoTotaleTagliS = value;
            if (code === "TempoVitaMacchinaH")
                $scope.TempoVitaMacchinaH = value;
            if (code === "TempoVitaMacchinaM")
                $scope.TempoVitaMacchinaM = value;
            if (code === "TempoVitaMacchinaS")
                $scope.TempoVitaMacchinaS = value;
            if (code === "FeedRateAttuale")
                $scope.FeedRateAttuale = value;
            if (code === "BladeSpeedAttuale")
                $scope.BladeSpeedAttuale = value;
            if (code === "Programma")
                $scope.Programma = value;
            if (code === "TesaturaLama")
                $scope.TestaturaLama = value;
            if (code === "PezziProdurre")
                $scope.PezziProdurre = value;
            if (code === "PezziProdotti")
                $scope.PezziProdotti = value;
            if (code === "PartNumber")
                $scope.PartNumber = value;
            if (code === "MisuraProgrammata")
                $scope.MisuraProgrammata = value;
            if (code === "CorrenteAssorbita") {
                $scope.CorrenteAssorbita = value;
                if (value > 1) {
                   
                    if ($scope.data_multiBarChart[0].values.length >=500) {
                        $scope.data_multiBarChart[0].values.shift();
                        $scope.ora = $scope.convert(changeAt);
                        $scope.data_multiBarChart[0].values.push({ x: $scope.ora, y: value });
                    }
                    else {
                        $scope.ora = $scope.convert(changeAt);
                        $scope.data_multiBarChart[0].values.push({ x: $scope.ora, y: value });
                    }
                }//>1   
            }

            if (code.indexOf("Allarme") != (-1))
                $scope.allarmi(code, value)
            if (code.indexOf("Warning") != (-1))
                $scope.warning(code, value)
        };


        $scope.allarmi = function (code, value) {
            if (value == true) {
                if ($scope.ListaAllarmi.indexOf(code) == (-1))
                $scope.ListaAllarmi.push(code)
            }
            else {
                var pos = $scope.ListaAllarmi.indexOf(code);
                if (pos != (-1))
                    $scope.ListaAllarmi.splice(pos, 1);
            }

        
            $scope.AllarmiMacchina = $scope.ListaAllarmi.length;
            if ($scope.ListaAllarmi.length != 0) $scope.isActiveAlarm = true;
            else $scope.isActiveAlarm = false;
        };


        $scope.warning = function (code, value) {
            if (value == true) {
                if ($scope.ListaWarning.indexOf(code) == (-1))
                    $scope.ListaWarning.push(code)
            }
            else {
                var pos = $scope.ListaWarning.indexOf(code);
                if (pos != (-1))
                    $scope.ListaWarning.splice(pos, 1);
            }

            $scope.NumeroWarning = $scope.ListaWarning.length;
        };
}]);

