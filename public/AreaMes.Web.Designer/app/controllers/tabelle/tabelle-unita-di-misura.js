﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('UnitaDiMisuraCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$translate',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $translate) {

    $scope.IsFullScreenOnEdit = false;
    $scope.items = [];
    $scope.currentItem = null;
    $scope.viewState = Enums.ViewState['R'];
    $scope.titolo = "Dettaglio";
    $scope.dtInstance = {};

    $translate.use($scope.$parent.globals.currentUser.lingua);

    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(5)
        .withOption('stateSave', true)
        .withOption("retrieve", true)
        .withOption('LengthChange', false)
        .withLanguageSource(urlLanguageDataTable)
        .withPaginationType("full_numbers")
        .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

    $scope.showItemDetail = function (item) {

        basicCrud_doShowItemDetail($scope, $http, subUrlApi_UnitaDiMisura, Enums, item);
    };

    $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlApi_UnitaDiMisura); };

    $scope.insert = function () {
        $scope.viewState = Enums.ViewState['C'];
        $scope.currentItem = new Object();
        $scope.currentItem.tipologia = new Object();
    };

    $scope.update = function () { $scope.viewState = Enums.ViewState['U']; };

    $scope.cancel = function () { $scope.viewState = Enums.ViewState['R']; };

    $scope.confirm = function () {

        basicCrud_doConfirm($scope, $http, subUrlApi_UnitaDiMisura, Enums);
    };


    $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlApi_UnitaDiMisura, Enums, item) };

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };

    basicCrud_doInitWatch($scope);

    $scope.reload();

    $scope.crudException = function (err) {
        $scope.exceptionMessageText = err.exceptionMessage;
        angular.element(document.querySelector('#modalWarning1')).modal('toggle');
    };

}]);