﻿//var app = angular.module("AppMacchine", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl', 'nvd3', 'pascalprecht.translate'])
//.run(function ($rootScope, $location, $cookies, $http, $window) {
//    // keep user logged in after page refresh
//    $rootScope.globals = $cookies.getObject('MesAuth');
//    if ($rootScope.globals!= null) {
//        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        
//    }
//    var x = $window.location.href;
//    $rootScope.$on('$locationChangeStart', function (event, next, current) {
//        // redirect to login page if not logged in
//        if (x.indexOf('/login') == -1 && !$rootScope.globals) {
//            $window.location.href = 'login.html';
//        }
//    });
//});

// ------------------------------------------------------------------------
// definizione generale
// ------------------------------------------------------------------------
app.controller('menuCtrl', [
    '$scope', '$http', '$compile',  '$window',
    function ($scope, $http, $compile, $window) {
        $scope.appName = "MEP";
        $scope.pagina = $window.location.href;
    }]);



app.constant('Enums', {
    ViewState: {
        R: 'R',
        C: 'C',
        U: 'U',
        D: 'D',
        RO: 'RO'
    }
});



