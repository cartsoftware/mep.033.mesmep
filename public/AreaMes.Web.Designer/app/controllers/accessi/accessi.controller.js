﻿
// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------

app.controller('AccessiCtrl', ['$scope', '$http', 'DataFactory', '$rootScope', '$location', '$translate', 
function ($scope, $http, DataFactory, $rootScope, $location, $translate) {

    urlApi = $.cookie("WebApiC");
    $scope.IsFullScreenOnEdit = false;
    $scope.items = [];
    $scope.currentItem = null;
   
    var respo;
    $scope.dtInstance = {};

    $scope.logOut = function () { DataFactory.ClearCredentials(); }
    
    if ($rootScope.globals != null)
        $scope.LoggedUser = $rootScope.globals.currentUser.nome + " " + $rootScope.globals.currentUser.cognome;
    
    $scope.login = function () {
        $scope.dataLoading = true;
        DataFactory.Login($scope.username, $scope.password).then(function (response) {
            if (response.data != null) {

                // vengono prelevate le traduzioni
                // DataFactory.GetTranslations(response.data.lingua);

                DataFactory.SetCredentials(response.data.id, $scope.username, $scope.password, response.data.nome, response.data.cognome, response.data.lingua);

                $translate.use(response.data.lingua);
             
            } else {
                $scope.error = "Username o password errate";
                $scope.dataLoading = false;
            }
        });
    };
}]);


