﻿//-------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('DashboardCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$translate',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $translate) {
  
    var respo;
    var datafactory = {};
    var _id, _username, _authdata, _nome, _cognome, _lingua;

    $translate.use($scope.$parent.globals.currentUser.lingua);

    $scope.OperatoriAttivi = 0;
    $scope.OperatoriAttiviOraPrec = 0;;
    $scope.OrdiniInAllarme = 0;
    $scope.OrdiniInAllarmeOraPrec = 0;
    $scope.MacchinariAttivi = 0;
    $scope.MacchinariAttiviOraPrec = 0;
    $scope.OEEMedia = 0;
    $scope.OEEMediaOraPrec = 0;
 


  

    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(5)
        .withOption('stateSave', true)
        .withOption("retrieve", true)
        .withOption('LengthChange', false)
        .withLanguageSource(urlLanguageDataTable)
        .withPaginationType("full_numbers")
        .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

    $scope.optionsGraphOrderStatus = {
        chart: {
            type: 'pieChart',
            height: 400,
            x: function (d) { return d.key + ' (' + d.y + ')'; },
            y: function (d) { return d.y; },
            showLabels: true,
            duration: 500,
            labelThreshold: 0.01,
            labelSunbeamLayout: true,
            color: ['#ff7f0e', 'gray', 'green'],
            legend: {
                margin: {
                    top: 5,
                    right: 35,
                    bottom: 0,
                    left: 0
                }
            }
        }
    };

    $scope.options = {
        chart: {
            type: 'lineChart',
            height: 300,
            margin: {
                top: 20,
                right: 20,
                bottom: 40,
                left: 55
            },
            x: function (d) { return d.x; },
            y: function (d) { return d.y; },
            useInteractiveGuideline: true,
            xAxis: {
                axisLabel: 'Tempo',

            },
            yAxis: {
                axisLabel: 'Kwh',
                tickFormat: function (d) {
                    return d3.format('.02f')(d);
                },
                axisLabelDistance: -10
            }
        }
    };


   

    $scope.options_multiBarChart = {
        chart: {
            type: 'multiBarChart',
            height: 450,
            margin: {
                top: 20,
                right: 20,
                bottom: 45,
                left: 45
            },
            clipEdge: true,
            duration: 500,
            stacked: true,
            xAxis: {
                axisLabel: 'Tempo (giorni)',
                showMaxMin: false,
                tickFormat: function (d) {
                    return d3.format(',f')(d);
                }
            },
            yAxis: {
                axisLabel: 'Valore',
                axisLabelDistance: -20,
                tickFormat: function (d) {
                    return d3.format(',.1f')(d);
                }
            }
        }
    };


    $scope.reload = function (e) {
        DataFactory.GetDashboard().then(function (response) {

       
            $scope.OperatoriAttivi = response.data.operatoriAttivi;
            $scope.OperatoriAttiviOraPrec = response.data.operatoriAttiviOraPrec;
            $scope.OrdiniInAllarme = response.data.ordiniInAllarme;
            $scope.OrdiniInAllarmeOraPrec = response.data.ordiniInAllarmeOraPrec;
            $scope.MacchinariAttivi = response.data.macchinariAttivi;
            $scope.MacchinariAttiviOraPrec = response.data.macchinariAttiviOraPrec;
            $scope.OEEMedia = response.data.oeeMedia;
            $scope.OEEMediaOraPrec = response.data.oeeMediaOraPrec;

            $scope.Ordini = response.data.ordini;

            angular.forEach($scope.Ordini, function (db, key) {
                if ($scope.Ordini[key] != null) {
                    $scope.Ordini[key].lunghezza = $scope.Ordini[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                }

            });


            $scope.dataGraphOrderStatus = [];
            angular.forEach(response.data.statoOrdini, function (i, e) {
                $scope.dataGraphOrderStatus.push({ key: e, y: i });
            });

            var sin2 = [];
            angular.forEach(response.data.consumoEnergiaMezzora, function (i, e) {
                sin2.push({ x: e, y: i });
            });

            ////Data is represented as an array of {x,y} pairs.
            //for (var i = 0; i < 100; i++) {
            //    sin2.push({ x: i, y: i % 10 == 5 ? null : Math.sin(i / 10) * 0.25 + 0.5 });
            //}
            $scope.data = [
                {
                    values: sin2,
                    key: 'Consumo energetico ',
                    color: '#7777ff',
                    area: true //area - set to true if you want this line to turn into a filled area chart.
                }
            ];
        }); 
    }

    //$scope.generateData = function () {
    //    var values = [];
    //    var values0 = [];
    //    var values1 = [];
    //    //change number of bars here by editing '90'//
    //    for (var h = 0; h < 90; h++) {
    //        //replace the y values with your own values//
    //        values.push({ x: h, y: Math.random() + 1 });
    //        values0.push({ x: h, y: Math.sqrt(h) / 2 });
    //        values1.push({ x: h, y: Math.abs(h - 18) })
    //    }

    //    return [{
    //        key: 'Sent',
    //        color: '#bcbd22',
    //        values: values
    //    },
    //      {
    //          key: 'Received',
    //          color: '#1f77b4',
    //          values: values0
    //      },
    //      {
    //          key: 'Spam',
    //          color: 'black',
    //          values: values1
    //      }
    //    ];
    //}

    
    $scope.reload();
    //$scope.data_multiBarChart = generateData();

    var c1 = JSON.stringify($scope.data_multiBarChart);
    var c2 = $scope.options_multiBarChart;



    /* Random Data Generator (took from nvd3.org) */
    function generateData() {
        return stream_layers(3, 50 + Math.random() * 50, .1).map(function (data, i) {
            return {
                key: 'Potenza F' + i,
                values: data
            };
        });
    }
    /* Inspired by Lee Byron's test data generator. */
    function stream_layers(n, m, o) {
        if (arguments.length < 3) o = 0;
        function bump(a) {
            var x = 1 / (.1 + Math.random()),
                y = 2 * Math.random() - .5,
                z = 10 / (.1 + Math.random());
            for (var i = 0; i < m; i++) {
                var w = (i / m - y) * z;
                a[i] += x * Math.exp(-w * w);
            }
        }
        return d3.range(n).map(function () {
            var a = [], i;
            for (i = 0; i < m; i++) a[i] = o + o * Math.random();
            for (i = 0; i < 5; i++) bump(a);
            return a.map(stream_index);
        });
    }

    /* Another layer generator using gamma distributions. */
    function stream_waves(n, m) {
        return d3.range(n).map(function (i) {
            return d3.range(m).map(function (j) {
                var x = 20 * j / m - i / 3;
                return 2 * x * Math.exp(-.5 * x);
            }).map(stream_index);
        });
    }

    function stream_index(d, i) {
        return { x: i, y: Math.max(0, d) };
    }

    setInterval(function () { $scope.reload(); }, 3000);
}]);

