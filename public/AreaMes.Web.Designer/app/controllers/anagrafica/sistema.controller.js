﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('SistemaCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$translate',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $translate) {

        $scope.IsFullScreenOnEdit = false;
        $scope.items = [];
        $scope.viewState = Enums.ViewState['R'];
        $scope.titolo = "Dettaglio";
        $scope.dtInstance = {};
        $scope.licence = null;
        $translate.use($scope.$parent.globals.currentUser.lingua);
        $scope.currentItem = null;
        $scope.prova = "ddddddddd";

        DataFactory.getLicence().then(function (response) {
            $scope.currentItem = response.data;
        });

        //$scope.reload = function () { basicCrud_doReload($scope, $http, subUrlApi_Licenza +"/GetLicence"); };

        //$scope.showItemDetail = function () { basicCrud_doReload($scope, $http, subUrlApi_Licenza+"/GetLicence"); };
        

        $scope.update = function () {
            $scope.viewState = Enums.ViewState['U'];
        };

        $scope.confirm = function () {

            
            basicCrud_doConfirm($scope, $http, subUrlApi_Licenza, Enums);

            $scope.viewState = Enums.ViewState['R'];
        };

        $scope.cancel = function () { $scope.viewState = Enums.ViewState['R']; };

        //$scope.title = function () { basicCrud_doTitle($scope, Enums); };

        //basicCrud_doInitWatch($scope);

        //$scope.reload();

        



    }]);