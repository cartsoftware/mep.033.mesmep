﻿
// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('AnagraficaMacchineCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$translate',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $translate) {

        $scope.IsFullScreenOnEdit = false;
        $scope.items = [];
        $scope.currentItem = null;
        $scope.viewState = Enums.ViewState['R'];
        $scope.titolo = "Dettaglio";
        $scope.dtInstance = {};
        $scope.registerMachineToM2MResult = null;
        $scope.registerMachineToM2MResultError = '';
        $translate.use($scope.$parent.globals.currentUser.lingua);
        $scope.typeMachines = null;
        var modalTestAreaM2MLoading = angular.element(document.querySelector('#modalTestAreaM2MLoading'));
        var modalTestAreaM2MError = angular.element(document.querySelector('#modalTestAreaM2MError'));
        var modalTestAreaM2MOk = angular.element(document.querySelector('#modalTestAreaM2MOk'));

        // funzione per popolare il combo box dei tipiMacchina
         DataFactory.getTipiMacchine().then(function (response) {
         $scope.typeMachines = response.data;
         });

        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(5)
            .withOption('stateSave', true)
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.showItemDetail = function (item) {
           
            basicCrud_doShowItemDetail($scope, $http, subUrlApi_Macchina, Enums, item);


            // funzione per popolare il combo box dei tipiMacchina
            $scope.typeMachines = null;
            DataFactory.getTipiMacchine().then(function (response) {
                $scope.typeMachines = response.data;
            });

            // funzione per popolare il combo box per le competenze delle macchine
            $scope.competenzeMacchine = null;
            DataFactory.getCompetenzeMacchina().then(function (response) {
                $scope.competenzeMacchine = response.data;
            });
        };



    $scope.toCheckMachine = function (item) {

        //$scope.c = true;
        //if ($scope.c)
        //    angular.element(document.querySelector('#modalOk')).modal('toggle');

        //else
        //angular.element(document.querySelector('#modalError')).modal('toggle');
    };


    // Chiamata per la registrazione al server della macchina
    $scope.toCheckServer = function (item) {

        // reset dei valori
        $scope.currentItem.isRegisteredToM2M = false;
        $scope.currentItem.registeredToM2MAt = null;
        $scope.currentItem.registeredToM2MDeviceId = 0;

        modalTestAreaM2MLoading.modal('toggle');
        // chiamata al server locale
        DataFactory.RegisterMachineToM2M(item.id).then(function (response) {
            // risposta dal server
            $scope.registerMachineToM2MResult = response.data;
            $scope.registerMachineToM2MResultError = response.data.error;
            // chiusura della finestra di loading
            modalTestAreaM2MLoading.modal('toggle');

            if ($scope.registerMachineToM2MResult.success) {
                // esito positivo,
                $scope.currentItem.isRegisteredToM2M = response.data.isRegisteredToM2M;
                $scope.currentItem.registeredToM2MAt = response.data.registeredToM2MAt;
                $scope.currentItem.registeredToM2MDeviceId = response.data.registeredToM2MDeviceId;

                // visualizzazione finestra di esito positivo
                modalTestAreaM2MOk.modal('toggle');
            }
            else
                modalTestAreaM2MError.modal('toggle');
        });

    };

    $scope.toDashboard = function (item) {
        this.router.navigateByUrl('/user');
    };

    $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlApi_Macchina); };

    $scope.insert = function () {
        $scope.viewState = Enums.ViewState['C'];
        $scope.currentItem = new Object();
        $scope.currentItem.tipologia = new Object();
    };

    $scope.update = function () {  $scope.viewState = Enums.ViewState['U']; };

    $scope.cancel = function () { $scope.viewState = Enums.ViewState['R']; };

    $scope.confirm = function () { basicCrud_doConfirm($scope, $http, subUrlApi_Macchina, Enums); };

    $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlApi_Macchina, Enums, item) };

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };

    basicCrud_doInitWatch($scope);

    $scope.reload();


    $scope.crudException = function (err) {
        $scope.exceptionMessageText = err.exceptionMessage;
        angular.element(document.querySelector('#modalWarning1')).modal('toggle');
    };

    }]);

