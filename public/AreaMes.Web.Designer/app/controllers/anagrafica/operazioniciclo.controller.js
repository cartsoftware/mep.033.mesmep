﻿
// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('AnagraficaOperazioniCicloCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory) {

    $scope.IsFullScreenOnEdit = true;
    $scope.items = [];

    $scope.modelsOreUomo = {};
    $scope.modelsMinutiUomo = {};
    $scope.modelsSecondiUomo = {};
    $scope.modelsSelCompetenzaOperatore = {};
    $scope.modelsOreMacchina = {};
    $scope.modelsMinutiMacchina = {};
    $scope.modelsSecondiMacchina = {};
    $scope.modelsSelCompetenzaMacchine = {};
    $scope.modelsSelMacchine = {};

    // flag per abilitare la seconda tab dei tempi
    $scope.isEnabledTabTimers = false;

    // array per abilitazione macchine e competenze macchine
    $scope.isEnabledMacchinaSelection = {};
    $scope.isEnabledcompentenzaMacchineSelection = {};

    $scope.currentItem = null;
    $scope.viewState = Enums.ViewState['R'];
    $scope.titolo = "Dettaglio";
    $scope.dtInstance = {};



    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(5)
        .withOption('stateSave', true)
        .withOption("retrieve", true)
        .withOption('LengthChange', false)
        .withLanguageSource(urlLanguageDataTable)
        .withPaginationType("full_numbers")
        .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

    $scope.showItemDetail = function (item) {

        $scope.isEnabledTabTimers = true;

        basicCrud_doShowItemDetail($scope, $http, subUrlOperazioniCiclo, Enums, item);
        
        $scope.CompetenzeOperatori = null;
        $scope.Macchine = null;
        $scope.CompetenzeMacchine = null;
        $scope.CausaliFase = null;

        DataFactory.getCompetenzeOperatori().then(function (response) {
            $scope.CompetenzeOperatori = response.data;
        });

        DataFactory.getMacchine().then(function (response) {
            $scope.Macchine = response.data;
        });

        DataFactory.getCompetenzeMacchina().then(function (response) {
            $scope.CompetenzeMacchine = response.data;
        });

        DataFactory.GetCausaliFase().then(function (response) {
            $scope.CausaliFase = response.data;
            fillItemsInView(item);
        });
    };

    $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlOperazioniCiclo); };

    $scope.insert = function () {
        $scope.isEnabledTabTimers = false;
        $scope.viewState = Enums.ViewState['C'];
        $scope.currentItem = new Object();
    };

    $scope.update = function () { $scope.viewState = Enums.ViewState['U']; };

    $scope.cancel = function () { $scope.viewState = Enums.ViewState['R']; closeModal('#modalConfirmUndo'); };

    $scope.confirm = function () {

        basicCrud_doConfirmWithOutReload($scope, $http, subUrlOperazioniCiclo, Enums);
    };

    $scope.confirmAndClose = function () {

        basicCrud_doConfirm($scope, $http, subUrlOperazioniCiclo, Enums);
    };

    $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlOperazioniCiclo, Enums, item) };

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };

    basicCrud_doInitWatch($scope);

    $scope.reload();

    // funzioni che rendono mutuamente esclusive le drop down
    $scope.selectionMacchine = function (id)
    {
        $scope.isEnabledcompentenzaMacchineSelection[id] = ($scope.modelsSelMacchine[id] == -1);
    }

    $scope.selectionCompetenzaMacchine = function (id) {
        $scope.isEnabledMacchinaSelection[id] = ($scope.modelsSelCompetenzaMacchine[id] == -1);
    }

    // inserimento/modifica operazione ciclo
    $scope.confirmFase = function () {
        //inizializzazione degli array
        $scope.currentItem.macchine = [];
        $scope.currentItem.manodopera = [];
        $scope.currentItem.totaleTempiMacchina = [];
        $scope.currentItem.totaleTempiManodopera = [];

        var tmpTempoTotaleManodopera = null;
        var tmpTempoTotaleMacchina = null;
        
        angular.forEach($scope.CausaliFase, function (obj, key) {
            
            // recupero il tempo uomo ed il tempo macchina trasformandoli in double
            var tmpTempoUomo = ($scope.modelsOreUomo[obj.id]*3600) + ($scope.modelsMinutiUomo[obj.id]*60) + $scope.modelsSecondiUomo[obj.id];
            var tmpTempoMacchina = ($scope.modelsOreMacchina[obj.id]*3600) + ($scope.modelsMinutiMacchina[obj.id]*60) + $scope.modelsSecondiMacchina[obj.id];

            // associo la competenza all'oggetto da passare
            var tmpCompetenzaOperatore = null;
            angular.forEach($scope.CompetenzeOperatori, function (singleCompetenzeOperatori, key) {
                if (singleCompetenzeOperatori.id == $scope.modelsSelCompetenzaOperatore[obj.id]) {
                    tmpCompetenzaOperatore = singleCompetenzeOperatori;
                }
            })

            var tmpMacchine = null;
            angular.forEach($scope.Macchine, function (singleMacchina, key) {
                if (singleMacchina.id == $scope.modelsSelMacchine[obj.id]) {
                    tmpMacchine = singleMacchina;
                }
            })

            var tmpCompetenzaMacchine = null;
            angular.forEach($scope.CompetenzeMacchine, function (singleCompetenzeMacchine, key) {
                if (singleCompetenzeMacchine.id == $scope.modelsSelCompetenzaMacchine[obj.id]) {
                    tmpCompetenzaMacchine = singleCompetenzeMacchine;
                }
            })

            // creazione degli oggetti
            var tmpFaseManodopera = {
                OperatoreCompetenza: tmpCompetenzaOperatore,
                CausaleFase: obj.id,
                Tempo: tmpTempoUomo
            }
            var tmpFaseMacchina = {
                Macchina: tmpMacchine,
                MacchinaCompetenza: tmpCompetenzaMacchine,
                CausaleFase: obj.id,
                Tempo: tmpTempoMacchina
            }

            tmpTempoTotaleManodopera = {
                CausaleFase: obj.id,
                Totale: tmpTempoUomo
            }

            tmpTempoTotaleMacchina = {
                CausaleFase: obj.id,
                Totale: tmpTempoMacchina
            }
            
            // add sull'array
            $scope.currentItem.manodopera.push(tmpFaseManodopera);
            $scope.currentItem.macchine.push(tmpFaseMacchina);
            
            
        });

        // chiedere ad Alberto per questa cosa dei tempi totali
        $scope.currentItem.totaleTempiManodopera = tmpTempoTotaleManodopera;
        $scope.currentItem.totaleTempiMacchina = tmpTempoTotaleMacchina;
        
        //basicCrud_doConfirm($scope, $http, subUrlOperazioniCiclo, Enums);
    }

    //funzione per popolare gli elementi della finestra di dettaglio (tempi)
    function fillItemsInView(item)
    {
        angular.forEach($scope.CausaliFase, function (obj, key) {
            
            $scope.isEnabledMacchinaSelection[obj.id] = true;
            $scope.isEnabledcompentenzaMacchineSelection[obj.id] = true;

            // valore selezionato per le competenze operatore
            if (item.manodopera[obj.id].operatoreCompetenza != null) {
                $scope.modelsSelCompetenzaOperatore[obj.id] = item.manodopera[obj.id].operatoreCompetenza.id;
            }

            // recupero il tempo dell'operatore per ogni causale e popolo i vari input text
            var tmpDoubleOperatore = item.manodopera[obj.id].tempo;
            $scope.modelsOreUomo[obj.id] = parseInt(tmpDoubleOperatore / 3600);
            $scope.modelsMinutiUomo[obj.id] = parseInt((tmpDoubleOperatore % 3600) / 60);
            $scope.modelsSecondiUomo[obj.id] = parseInt(tmpDoubleOperatore % 60);

            // popolo le drop down list con il valore selezionato
            if (item.macchine[obj.id].macchina == null) {
                $scope.modelsSelMacchine[obj.id] = -1;
                $scope.isEnabledMacchinaSelection[obj.id] = false;
            }
            else
                $scope.modelsSelMacchine[obj.id] = item.macchine[obj.id].macchina.id;

           
            if (item.macchine[obj.id].macchinaCompetenza == null) {
                $scope.modelsSelCompetenzaMacchine[obj.id] = -1;
                $scope.isEnabledcompentenzaMacchineSelection[obj.id] = false;
            }
            else
                $scope.modelsSelCompetenzaMacchine[obj.id] = item.macchine[obj.id].macchinaCompetenza.id;

            // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
            if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                $scope.isEnabledMacchinaSelection[obj.id] = true;

            
            // recupero il tempo della macchina per ogni causale e popolo i vari input text
            var tmpDoubleMacchina = item.macchine[obj.id].tempo;
            $scope.modelsOreMacchina[obj.id] = parseInt(tmpDoubleMacchina / 3600);
            $scope.modelsMinutiMacchina[obj.id] = parseInt((tmpDoubleMacchina % 3600) / 60);
            $scope.modelsSecondiMacchina[obj.id] = parseInt(tmpDoubleMacchina % 60);
        });
    }

    // funzione provvisoria per gestire solo un pulsante di salvataggio
    $scope.globalChange = function () {
        $scope.confirmFase();
    }
}]);