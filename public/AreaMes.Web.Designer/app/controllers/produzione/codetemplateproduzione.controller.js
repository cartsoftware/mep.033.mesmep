﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('CodeTemplateProduzioneCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$rootScope', '$translate', '$window', '$location',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $rootScope, $translate, $timeout, $window, $location) {


        // DataTables configurable options
        $scope.dtOptions1 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        // DataTables configurable options
        $scope.dtOptionsTableBatch2 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.dtOptionsTableBatch3 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


        $scope.IsFullScreenOnEdit = true;
        $scope.items = [];
        $scope.idFase = {};

        $scope.isEnableWrite = false;
        $scope.currentItem = null;

        $scope.viewState = Enums.ViewState['R'];
        $scope.currentStatus = null;
        $scope.dtInstance1 = {};
        $scope.dtInstance2 = {};
        $scope.dtInstance3 = {};

        $scope.InitFilter == 'inModifica';
        $scope.AddBatch = false;
        $scope.batchSelect = [];
        $scope.batchAvable = [];
        $scope.formNameLine = "";
        $scope.modalViewBotton;
        $translate.use($scope.$parent.globals.currentUser.lingua);
        $scope.ListaMateriali = [];
        //funzione 
        DataFactory.getMacchine().then(function (response) {
        $scope.Macchine = jQuery.grep(response.data, function (a) { return a.tipologia.item.isEnableLine == true; });
        });


        // DataTables configurable options
        $scope.dtOptions1 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        // DataTables configurable options
        $scope.dtOptionsTableBatch2 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        $scope.dtOptionsTableBatch3 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);





        //funzione per la visualizzazione del bottone lancia e carica 
        DataFactory.getParametri().then(function (response) {
            $scope.modalViewBotton = ((response.data.find(x => x.codice == "CNT_MODAL_VIEW_BOTTON").valoreDefault).toLowerCase() === 'true');
        });

        //vengono scaricati tutti i materiali 
        DataFactory.getMateriali().then(function (response) {
            $scope.ListaMateriali = response.data;

        });
        //chiamata per gestire il dettaglio del template
        $scope.showItemDetail = function (item) {

            if (item.stato != null) {
                $scope.currentStatus = item.stato;
            }
            else {
                $scope.currentStatus = ListaStatiBatch[0].value;
            }

            basicCrud_doShowItemDetail($scope, $http, subUrlLineTemplate, Enums, item);

            $scope.isEnableWrite = true;

            
            $scope.nodesTableArr = [];
        };
        //reload
        $scope.reload = function () {
            basicCrud_doReload($scope, $http, subUrlLineTemplate);
            if ($scope.items != null) {
                angular.forEach($scope.items.macchina, function (db, key) {
                    //cliclo tutti i batch e machine essendo un external Doc i dati sono contenenti nella cartella items
                    var machine = $scope.items[key].macchina.item;
                    $scope.items[key].macchina = machine;
                });
            }
        };
        //
        $scope.createQueid = function () {
            if ($scope.currentItem != null) {
                angular.forEach($scope.currentItem.batch, function (db, key) {
                    if ($scope.currentItem.batch[key] != null) {
                        $scope.currentItem.batch[key].lunghezza = $scope.currentItem.batch[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                    }

                });
            }

            $scope.viewState = Enums.ViewState['C'];
        };

        $scope.create = function () {
            angular.forEach($scope.currentItem.batch, function (db, key) {
                $scope.currentItem.batch[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault = $scope.currentItem.batch[key].lunghezza;
            });

            $scope.currentItem.stato = "inModifica";
            basicCrud_doConfirm($scope, $http, subUrlLineTemplate, Enums);
        }

        $scope.createandLaunch = function () {

            angular.forEach($scope.currentItem.batch, function (db, key) {
                $scope.currentItem.batch[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault = $scope.currentItem.batch[key].lunghezza;
            });

            $scope.currentItem.stato = "InAttesa";
            basicCrud_doConfirm($scope, $http, subUrlLineTemplate, Enums);

        }



        
        $scope.dtInstanceCallback = function () {
            //
        }

        $scope.deleteItems = function (batch) {
            var index = $scope.currentItem.batch.indexOf(batch);
            $scope.currentItem.batch.splice(index, 1);
            $scope.batchAvable.push(batch);

        };

        $scope.update = function (event, item) {

            if (event.srcElement.id != 'launch') {
                $scope.showItemDetail(item);

                if ($scope.currentItem != null) {
                    angular.forEach($scope.currentItem.batch, function (db, key) {
                        if ($scope.currentItem.batch[key] != null) {
                            $scope.currentItem.batch[key].lunghezza = $scope.currentItem.batch[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                        }

                    });
                    $scope.viewState = Enums.ViewState['U'];
                }

            }
        };

        $scope.cancel = function () {
            $scope.viewState = Enums.ViewState['R']; ResetAllFields(); closeModal('#modalConfirmUndo2');
            $scope.viewState = Enums.ViewState['R']; ResetAllFields(); closeModal('#modalConfirmUndo');
        };

        $scope.confirm = function () {
            basicCrud_doConfirmWithOutReload($scope, $http, subUrlLineTemplate, Enums);
        };

        $scope.refresh = function () {
            var objTemp = { id: $scope.currentItem.id };
            $scope.currentItem = { id: "reloading", Codice: "reloading" };
            basicCrud_doShowItemDetail($scope, $http, subUrlLineTemplate, Enums, objTemp, function (item) {
                $scope.showItemDetail($scope.currentItem);
                $scope.viewState = Enums.ViewState['U'];

                angular.forEach($scope.trcurrentItem, function (it) {
                    $scope.trshowItemDetail(it);
                })

            });

        }

        $scope.confirmAndClose = function () {
            $scope.currentItem.revisione = $rootScope.globals.currentUser.username;
            basicCrud_doConfirm($scope, $http, subUrlLineTemplate, Enums);

        };

        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

        $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlLineTemplate, Enums) };

        $scope.title = function () { basicCrud_doTitle($scope, Enums); };

        basicCrud_doInitWatch($scope);

        $scope.reload();

        // ordinamento di json per campo richiesto
        function orderById(items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) filtered.reverse();
            return filtered;
        };

        // funzione per resettare tutti i campi
        function ResetAllFields() {
        }

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        // Definizione del filtro di visualizzazione in griglia
        function filter() {
            if ($scope.InitFilter) {
                $scope.items = $.grep($scope.items, function (e) {
                    return e.stato.startsWith($scope.InitFilter);
                });
            }
        }

        function generateData() {
            var yCount = $scope.trcurrentItem.length;
            var res = [];
            for (n = 0; n < $scope.trcurrentItem.length; n++) {
                var paramItem = $scope.trcurrentItem[n];
                res.push({ key: paramItem.nome, values: paramItem.data });
            }

            if (res.length > 0)
                return res;
            else
                return [{ key: '', values: [{ x: 0, y: 0 }] }];
        }

        $scope.toggleStatus = false;

        $scope.toggleDropdown = function (node, scopeNodes) {
            node.toggleStatus = node.toggleStatus == true ? false : true;
            $scope.toggleStatus = node.toggleStatus;
            $scope.toggleDropdownHelper(node.id, $scope.toggleStatus, scopeNodes);
        };

        $scope.toggleDropdownHelper = function (parentNodeId, toggleStatus, scopeNodes) {
            for (var i = 0; i < scopeNodes.length; i++) {
                node = scopeNodes[i];
                if (node.parentId == parentNodeId) {
                    if (toggleStatus == false)
                        $scope.toggleDropdownHelper(node.id, toggleStatus, scopeNodes);
                    scopeNodes[i].isShow = toggleStatus;
                }
            }
        };

    }]);


app.directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'yyyy-MM-dd';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})