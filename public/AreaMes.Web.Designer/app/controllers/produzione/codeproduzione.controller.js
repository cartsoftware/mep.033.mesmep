﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('CodeproduzioneCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$rootScope', '$translate', '$window', '$location',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $rootScope, $translate, $timeout, $window, $location) {

        $scope.IsFullScreenOnEdit = true;
        $scope.items = [];
        $scope.idFase = {};
        $scope.isEnableWrite = false;
        $scope.currentItem = null;
        $scope.currentItemBatch = null;
        $scope.viewState = Enums.ViewState['R'];
        $scope.currentStatus = null;
        $scope.dtInstance0 = {};
        $scope.dtInstance2 = {};
        $scope.InitFilter = 'inModifica';
        $scope.ViewAsInModifica = ($scope.InitFilter == 'inModifica');
        $scope.AddBatch = false;
        $scope.batchSelect = [];
        $scope.batchAvailable = [];
        $scope.formNameLine = "";
        $scope.modalViewBotton;



        //$translate.use($scope.$parent.globals.currentUser.lingua);
        $translate.use($scope.$parent.globals.currentUser.lingua).then(function (translation) {
            $scope.ListPhase = $translate.instant('general_label_listphase');
            $scope.dataForTheTree =
                [
                    { "numeroFase": "", "nome": $scope.ListPhase, "id": "0", "type": "-1", "fasi": [] }
                ];
            $translate.use($scope.$parent.globals.currentUser.lingua);
        });

        //vengono scaricati gli stati
        DataFactory.GetStatiBatch().then(function (response) {
            $scope.ListaStatiBatch = response.data;
        });

        //vengono scaricate tutte le possibili macchine
        DataFactory.getMacchine().then(function (response) {
            $scope.Macchine = jQuery.grep(response.data, function (a) { return a.tipologia.item.isEnableLine == true; });
        });

        //vengono scaricati tutti i batch in modifica e queueId ==null quindi utilizzabili
        DataFactory.GetBatch().then(function (response) {
            $scope.AllBatch = jQuery.grep(response.data, function (e) { return (e.stato.startsWith("inModifica") && e.queueId == null); });
        });

        //vengono scaricati tutti i materiali 
        DataFactory.getMateriali().then(function (response) {
            $scope.ListaMateriali = response.data;

        });

        DataFactory.getParametri().then(function (response) {
            $scope.modalViewBotton = ((response.data.find(x => x.codice == "CNT_MODAL_VIEW_BOTTON").valoreDefault).toLowerCase() === 'true');
        });

        //Configurazioni 3 Table 
        // DataTables configurable options
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


        // DataTables configurable options
        $scope.dtOptionsTableBatch1 = DTOptionsBuilder.newOptions()
            .withDisplayLength(20)
            .withOption('stateSave', true)
            .withOption('order', [[1, 'desc']])
            .withOption("retrieve", true)
            .withOption('LengthChange', false)
            .withLanguageSource(urlLanguageDataTable)
            .withPaginationType("full_numbers")
            .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);

        //funzione per scaricare il dettaglio fi un items
        $scope.showItemDetail = function (item, onSuccessCallback) {
            if (item.stato != null) {
                $scope.currentStatus = item.stato;
            }
            else {
                $scope.currentStatus = ListaStatiBatch[0].value;
            }

            basicCrud_doShowItemDetail($scope, $http, subUrlLine, Enums, item, function () {
                if ($scope.currentItem.batch)
                    angular.forEach($scope.currentItem.batch, function (db, key) {
                        if ($scope.currentItem.batch[key].item != null) {

                            var batch = $scope.currentItem.batch[key].item;
                            $scope.currentItem.batch[key] = batch;
                            $scope.currentItem.batch[key].lunghezzaPezzo = $scope.currentItem.batch[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                        }
                    });

                $scope.isEnableWrite = (item.stato === 'inModifica') || (item.stato === 'inAttesa');

                if (onSuccessCallback != null)
                    onSuccessCallback($scope.currentItem);
            });

            //if ($scope.currentItem.batch) 
            //    angular.forEach($scope.currentItem.batch, function (db, key) {
            //        if ($scope.currentItem.batch[key].item != null) {

            //            var batch = $scope.currentItem.batch[key].item;
            //            $scope.currentItem.batch[key] = batch;
            //            $scope.currentItem.batch[key].lunghezzaPezzo = $scope.currentItem.batch[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
            //        }
            //    });

            //$scope.isEnableWrite = (item.stato === 'inModifica') || (item.stato === 'inAttesa');

            //$scope.nodesTableArr = [];

            //$scope.nodesTableArr = [];
        };

        $scope.reload = function () {
            basicCrud_doReload($scope, $http, subUrlLine, filter);
            if ($scope.items != null) {

                angular.forEach($scope.items.macchina, function (db, key) {
                    var machine = $scope.items[key].macchina.item;
                    $scope.items[key].macchina = machine;
                });

            }

        };

        //funzione per aggiungere un items
        $scope.insert = function () {
            //scarico tutti i possibili batch
            DataFactory.GetBatch().then(function (response) {
                $scope.AllBatch = jQuery.grep(response.data, function (e) { return (e.stato.startsWith("inModifica") && e.queueId == null); });
            });
            //azzero l'arrey dei batch disponibili della macchina 
            $scope.batchAvailable = [];
            //aggiorno lo stato e quindi la visualizzazione 
            $scope.viewState = Enums.ViewState['C'];
            //creo un nuovo currentItems stato in modifica e inizializzo l'arrey dei batch
            $scope.currentItem = new Object();
            $scope.isEnableWrite = true;
            $scope.currentStatus = "inModifica";
            $scope.currentItem.batch = [];
            ResetAllFields();
        };

        //change machine viene richiamata ogni volta che viene selezionata la macchina 
        $scope.changeMachine = function (macchina) {
            //si occupa di filtrate tutti i batch disponibili solo per la macchina selezionata 
            $scope.batchAvailable = [];

            angular.forEach($scope.AllBatch, function (item) {
                if (item.distintaBase.fasi[0].macchine[0].macchina.id == macchina) {
                    //seleziono la lunghezza pezzo dalla distinta base e creo una nuova variabile 
                    item.lunghezzaPezzo = item.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                    $scope.batchAvailable.push(item);
                }
            });

        };

        $scope.dtInstanceCallback = function () {
            //
        }

        //al click sul + chiamo la funzione che toglie dall'array dei disponibili e l'aggiunge all'array dei bacth del currentitems
        $scope.addBatch = function (batch) {
            var index = $scope.batchAvailable.indexOf(batch);
            $scope.batchAvailable.splice(index, 1);
            $scope.currentItem.batch.push(batch);

        };




        $scope.select = function (event, item) {
            $scope.currentItem = item;

        };

        //funzione per scaricare il dettaglio di un batch
        $scope.showItemDetailBatch = function (item) {
            //basicCrud_doShowItemDetailBatch($scope, $http, subUrlLine, Enums, item);

            //setTotaleTempi($scope, item);
            //setRiepilogo($scope, item);

            $scope.CausaliFase = null;
            $scope.CompetenzeOperatori = null;
            $scope.Macchine = null;
            $scope.CompetenzeMacchine = null;
            $scope.CausaliFase = null;
            $scope.OperazioniCiclo = null;


            if (item.stato != null) {
                $scope.currentStatus = item.stato;
            }
            else {
                $scope.currentStatus = ListaStatiBatch[0].value;
            }

            basicCrud_doShowItemDetailBatch($scope, $http, subUrlBatch, Enums, item);

            // popolo le varie dropdown
            // DataFactory.getCompetenzeOperatori().then(function (response) { $scope.CompetenzeOperatori = response.data; });

            DataFactory.getMacchine().then(function (response) { $scope.Macchine = response.data; });

            DataFactory.getCompetenzeMacchina().then(function (response) { $scope.CompetenzeMacchine = response.data; });

            DataFactory.GetCausaliFase().then(function (response) { $scope.CausaliFase = response.data; });

            DataFactory.GetOperazioniCiclo().then(function (response) { $scope.OperazioniCiclo = response.data; });


            // carico le distinte basi
            DataFactory.GetDistinteBasiFromMateriale(item.materiale.id).then(function (response) {
                $scope.ListaDistintaBase = response.data;

                if (item.distintaBase == null) return;
                // carico le fasi dalla distinta base 
                angular.forEach($scope.ListaDistintaBase, function (db, key) {
                    if (db.id === item.distintaBase.id) {
                        $scope.selectedDistintaBase = db;
                    }
                });
                createOrRefreshTreeView();
            });

            $scope.isEnableWrite = (item.stato === 'inModifica') || (item.stato === 'inAttesa');

            $scope.nodesTableArr = [];
            $scope.currentItemBatch = [];

            //if ($scope.currentItemBatch != null)
            //    $scope.initializeNodeTreeTable($scope.currentItemBatch.distintaBase.fasi, $scope.nodesTableArr);

            // ---------------------------------------------------------------------------------------
        };
        //update Coda
        $scope.update = function (event, item) {

            //if (event.srcElement.id != 'launch') {
                $scope.showItemDetail(item);

                if ($scope.currentItem != null) {
                    angular.forEach($scope.currentItem.batch, function (db, key) {
                        if ($scope.currentItem.batch[key] != null) {
                            $scope.currentItem.batch[key].lunghezza = $scope.currentItem.batch[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                        }

                    });
                    $scope.changeMachine($scope.currentItem.macchina.id);
                    $scope.viewState = Enums.ViewState['U'];
                }

           // }


            ////scarico i possibili batch aggiungibili 
            //$scope.batchAvailable = [];

            //$scope.showItemDetail(item, function () {


            //    $scope.changeMachine($scope.currentItem.macchina.id);
                
            //    $scope.viewState = Enums.ViewState['U'];

            //});

        
        };

        //creare un nuovo batch da aggiungere alla coda
        $scope.createnewbatch = function () {
            $('#modalAddBatch').modal('show');
            $scope.viewState = Enums.ViewState['C'];
            $scope.isBatchAutoCounterEnabled = false;
            $scope.currentItemBatch = new Object();
            $scope.isEnableWrite = true;
            $scope.currentStatus = "inModifica";
            $scope.currentItemBatch.distintaBase = null;
            // autocaricamento del materiale se ne è presente uno solo
            if ($scope.ListaMateriali.length == 1) {
                $scope.currentItemBatch.materiale = $scope.ListaMateriali[0];
                DataFactory.GetDistinteBasiFromMateriale($scope.currentItemBatch.materiale.id).then(function (response) {
                    $scope.ListaDistintaBase = response.data;
                    if ($scope.ListaDistintaBase.length == 1) {
                        $scope.currentItemBatch.distintaBase = $scope.ListaDistintaBase[0];
                    }
                });
            }
            //aggiunta lunghezza pezzo 07/08/2019
            $scope.currentItemBatch.lunghezza;

            // verifica dell'auto-contatore
            DataFactory.GetBatchAutoCounterInfo().then(function (response) {
                $scope.isBatchAutoCounterEnabled = response.data.isEnabled;
                if (response.data.isEnabled) {
                    $scope.currentItemBatch.odP = response.data.global;
                    $scope.currentItemBatch.odPParziale = response.data.partial;
                }
            });
            //caricoladistintabase dalla macchina 
            // carico le distinte basi a seconda del materiale in uscita

            $scope.ListaDistintaBase = null;
            DataFactory.GetDistinteBasiFromMacchina($scope.currentItem.macchina.id).then(function (response) {
                $scope.ListaDistintaBase = response.data;
            });





        };

        //salvare un nuovo batch
        $scope.savenewbatch = function () {
            // $('#modalAddBatch').modal('show');
            if ($scope.currentItemBatch.distintaBase != null)
                $scope.currentItemBatch.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault = $scope.currentItemBatch.lunghezza;

            $scope.currentItemBatch.materiale = $scope.currentItem.materiale;
            $scope.currentItemBatch.revisione = $rootScope.globals.currentUser.username;
            basicCrud_doConfirmWithOutReloadBatch($scope, $http, subUrlBatch, Enums);

        };

        //aggiorna un batch
        $scope.updatebatch = function (item) {
            // $('#modalAddBatch').modal('show');
            $scope.showItemDetailBatch(item);

        };

        //rimuovi il batch dalla coda
        $scope.removeBatch = function (item) {
            var index = $scope.currentItem.batch.indexOf(item);
            $scope.currentItem.batch.splice(index, 1);
            $scope.batchAvailable.push(item);
        };

        $scope.changeDistintaBase = function (id) {

            //ResetAllFields();

            if (id == -1) {
                $scope.selectedDistintaBase = null;
                $scope.currentItemBatch.distintaBase = null;
            }
            else
                angular.forEach($scope.ListaDistintaBase, function (db, key) {
                    if (db.id === id) {
                        $scope.selectedDistintaBase = db;
                        $scope.currentItemBatch.distintaBase = db;
                    }
                });

            createOrRefreshTreeView();
        }
        // aggiornamento dell'albero
        function createOrRefreshTreeView() {
            $scope.defaultExpanded = [];
            $scope.dataForTheTree[0].fasi = [];
            if (($scope.selectedDistintaBase != null) &&
                ($scope.selectedDistintaBase.fasi != null)) {
                var arrayOrdered = orderById($scope.selectedDistintaBase.fasi, "numeroFase", false);
                $scope.dataForTheTree[0].fasi = arrayOrdered;
            }
            $scope.defaultExpanded = [$scope.dataForTheTree[0], $scope.dataForTheTree[0].fasi[0]];
        }

        $scope.cancel = function () {
            $scope.viewState = Enums.ViewState['R']; ResetAllFields(); closeModal('#modalConfirmUndo');
            $scope.batchAvailable = [];
            $scope.currentItem.bacth = [];

        };

        $scope.confirm = function () {
            basicCrud_doConfirmWithOutReload($scope, $http, subUrlLine, Enums);
        };

        $scope.refresh = function () {
            var objTemp = { id: $scope.currentItem.id };
            $scope.currentItem = { id: "reloading", Codice: "reloading" };
            basicCrud_doShowItemDetail($scope, $http, subUrlLine, Enums, objTemp, function (item) {
                $scope.showItemDetail($scope.currentItem);
                $scope.viewState = Enums.ViewState['U'];

                angular.forEach($scope.trcurrentItem, function (it) {
                    $scope.trshowItemDetail(it);
                })

            });

        }

        //conferma e chiudi
        $scope.confirmAndClose = function () {
            $scope.currentItem.revisione = $rootScope.globals.currentUser.username;
            basicCrud_doConfirm($scope, $http, subUrlLine, Enums);
            $scope.batchAbatchAvailable = [];
            $scope.AllBatch = [];
            $scope.currentItem.bacth = [];
        };

        //alert di eccezzione dal server
        $scope.crudException = function (err) {
            $scope.exceptionMessageText = err.exceptionMessage;
            angular.element(document.querySelector('#modalWarning1')).modal('toggle');
        };

        //per eliminare il batch
        $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlLine, Enums) };

        //$scope.title = function () { basicCrud_doTitle($scope, Enums); };

        basicCrud_doInitWatch($scope);

        $scope.reload();

        // funzione per lanciare la produzione
        $scope.launch = function () {
            $scope.viewState = Enums.ViewState['C'];
            //item.stato = "1";
            $scope.currentItem.stato = "1";
            //$scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

            basicCrud_doConfirmWithForcedReload($scope, $http, subUrlLine, Enums);

            angular.element(document.querySelector('#modalConfirmChangeState')).modal('hide');
        }

        // funzione per lanciare la produzione
        $scope.launchAndLink = function () {
            $scope.viewState = Enums.ViewState['C'];
            //item.stato = "1";
            $scope.currentItem.stato = "1";
            //$scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

            basicCrud_doConfirmWithForcedReload($scope, $http, subUrlLine, Enums);

            angular.element(document.querySelector('#modalConfirmChangeState2')).modal('hide');

            var path = "http://" + location.hostname + ":8082/sfofasi.html#?idBatch=" + $scope.currentItem.batch[0].id;
            location.href = path;

        }

        function filter() {
            if ($scope.InitFilter) {
                $scope.items = $.grep($scope.items, function (e) { return e.stato.startsWith($scope.InitFilter); });
            }
        }

        // ordinamento di json per campo richiesto
        function orderById(items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) filtered.reverse();
            return filtered;
        };

        // funzione per resettare tutti i campi
        function ResetAllFields() { }

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        // Definizione del filtro di visualizzazione in griglia
        function filter() {
            if ($scope.InitFilter) {
                $scope.items = $.grep($scope.items, function (e) {
                    return e.stato.startsWith($scope.InitFilter);
                });
            }
        }

        //funzione per aggiungere la coda al batch
        $scope.addLineTemplate = function () {
            angular.element(document.querySelector('#modalConfirmAddTemplate')).modal('hide');

            $scope.currentItem.stato = "InCreazione";
            $scope.currentItem.id = null;
            angular.forEach($scope.currentItem.batch, function (db, key) {
                $scope.currentItem.batch[key].stato = "InModifica";
                $scope.currentItem.batch[key].createDate = null;
                $scope.currentItem.batch[key].odP = null;
                $scope.currentItem.batch[key].odPParziale = null;
                $scope.currentItem.batch[key].queueId = null;
            });


            $scope.currentItem.NomeTemplate = $scope.formNameLine;
            basicCrud_doConfirm($scope, $http, subUrlLineTemplate, Enums);

            $scope.formNameLine = "";
            $scope.refresh();
            //$scope.currentItem.stato = "InModifica";
        }

        function generateData() {
            var yCount = $scope.trcurrentItem.length;
            var res = [];
            for (n = 0; n < $scope.trcurrentItem.length; n++) {
                var paramItem = $scope.trcurrentItem[n];
                res.push({ key: paramItem.nome, values: paramItem.data });
            }

            if (res.length > 0)
                return res;
            else
                return [{ key: '', values: [{ x: 0, y: 0 }] }];
        }

        $scope.toggleStatus = false;

        $scope.toggleDropdown = function (node, scopeNodes) {
            node.toggleStatus = node.toggleStatus == true ? false : true;
            $scope.toggleStatus = node.toggleStatus;
            $scope.toggleDropdownHelper(node.id, $scope.toggleStatus, scopeNodes);
        };

        $scope.toggleDropdownHelper = function (parentNodeId, toggleStatus, scopeNodes) {
            for (var i = 0; i < scopeNodes.length; i++) {
                node = scopeNodes[i];
                if (node.parentId == parentNodeId) {
                    if (toggleStatus == false)
                        $scope.toggleDropdownHelper(node.id, toggleStatus, scopeNodes);
                    scopeNodes[i].isShow = toggleStatus;
                }
            }
        };

    }]);


app.directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'yyyy-MM-dd';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})