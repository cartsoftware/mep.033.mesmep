﻿

// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('ProduzioneCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$rootScope', '$translate', '$window', '$location',
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $rootScope, $translate, $timeout, $window, $location) {

    $scope.IsFullScreenOnEdit = true;
    $scope.items = [];

    $scope.idFase = {};

    // flag per abilitare la seconda tab dei tempi
    //$scope.isEnabledTabTimers = false;

    $scope.isEnableWrite = false;

    // flag per visualizzare i controlli sulle fasi
    $scope.isEnabledDettagliFase = false;
   
    $scope.currentItem = null;
    $scope.viewState = Enums.ViewState['R'];
    $scope.titolo = "Dettaglio";
    $scope.dtInstance = {};
    $scope.ListaMateriali = null;
    $scope.ListaUnitaDiMisura = null;
    $scope.ListaStatiBatch = null;
    $scope.ListaDistintaBase = null;
    $scope.currentStatus = null;
    $scope.InitFilter = getParameterByName("i");
    $scope.ViewAsInModifica = ($scope.InitFilter == 'inModifica');
    $scope.ViewAsInAttesa = ($scope.InitFilter == 'inAttesa');
    $scope.ViewAsInLavorazione = ($scope.InitFilter == 'inLavorazione');
    $scope.ViewAsTerminato = ($scope.InitFilter == 'terminato');
    $scope.ListPhase = "Elenco fasi";
        //$translate.use($scope.$parent.globals.currentUser.lingua);

        $translate.use($scope.$parent.globals.currentUser.lingua).then(function (translation) {
            $scope.ListPhase = $translate.instant('general_label_listphase');
            $scope.Machine_Value = $translate.instant('dashboard_Machine_Value');
            $scope.Machine_Time = $translate.instant('dashboard_Machine_Time');
            

            $scope.dataForTheTree =
                [
                    { "numeroFase": "", "nome": $scope.ListPhase, "id": "0", "type": "-1", "fasi": [] }
                ];
            $translate.use($scope.$parent.globals.currentUser.lingua);
        });


    DataFactory.getMateriali().then(function (response) {
        $scope.ListaMateriali = response.data;
    });

    DataFactory.getUnitaDiMisura().then(function (response) {
        $scope.ListaUnitaDiMisura = response.data;
    });

    DataFactory.GetStatiBatch().then(function (response) {
        $scope.ListaStatiBatch = response.data;
    });


    // DataTables configurable options
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(20)
        .withOption('stateSave', true)
        .withOption('order', [[1, 'desc']])
        .withOption("retrieve", true)
        .withOption('LengthChange', false)
        .withLanguageSource(urlLanguageDataTable)
        .withPaginationType("full_numbers")
        .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


    $scope.showItemDetail = function (item) {

        setTotaleTempi($scope, item);
        setRiepilogo($scope, item);
        $scope.CausaliFase = null;
        $scope.CompetenzeOperatori = null;
        $scope.Macchine = null;
        $scope.CompetenzeMacchine = null;
        $scope.CausaliFase = null;
        $scope.OperazioniCiclo = null;


        if (item.stato != null) {
            $scope.currentStatus = item.stato;
        }
        else {
            $scope.currentStatus = ListaStatiBatch[0].value;
        }

        basicCrud_doShowItemDetail($scope, $http, subUrlBatch, Enums, item);

        // popolo le varie dropdown
        DataFactory.getCompetenzeOperatori().then(function (response) {  $scope.CompetenzeOperatori = response.data; });

        DataFactory.getMacchine().then(function (response) {  $scope.Macchine = response.data;  });

        DataFactory.getCompetenzeMacchina().then(function (response) {  $scope.CompetenzeMacchine = response.data; });

        DataFactory.GetCausaliFase().then(function (response) {    $scope.CausaliFase = response.data; });

        DataFactory.GetOperazioniCiclo().then(function (response) { $scope.OperazioniCiclo = response.data; });

        
        // carico le distinte basi
        DataFactory.GetDistinteBasiFromMateriale(item.materiale.id).then(function (response) {
            $scope.ListaDistintaBase = response.data;
           
            if (item.distintaBase == null) return;
            // carico le fasi dalla distinta base 
            angular.forEach($scope.ListaDistintaBase, function (db, key) {
                if (db.id === item.distintaBase.id) {
                    $scope.selectedDistintaBase = db;
                }
            });
            createOrRefreshTreeView();
        });

        $scope.isEnableWrite = (item.stato === 'inModifica') || (item.stato === 'inAttesa');

        $scope.nodesTableArr = [];
        if ($scope.currentItem!=null)
            $scope.initializeNodeTreeTable($scope.currentItem.distintaBase.fasi, $scope.nodesTableArr);


        // ---------------------------------------------------------------------------------------
        // Preparazione esportazione dati per gli eventi
        if (item.distintaBase) {
            $scope.exportData = [];
            $scope.fileName = "Report stato Fasi";
            $scope.exportData.push(["NUMERO FASE", "NOTA", "STATO", "INIZIO", "FINE"]);
            angular.forEach(item.batchAvanzamenti, function (value, key) {
                var singleItem = jQuery.grep(item.distintaBase.fasi, function (a) { return a.numeroFase == value.numeroFase; });
                $scope.exportData.push([singleItem[0].nome + '(' + value.numeroFase + ')', value.nota, value.stato, $scope.dFormat(value.inizio, value.nota), $scope.dFormat(value.fine, value.nota)]);
            });
        }

        // ---------------------------------------------------------------------------------------
        // Preparazione dei parametri di trend per i grafici
        $scope.exportDataTrend = [];
        $scope.fileNameTrend = "Report parametri";
        $scope.exportDataTrend.push(["PARAMETRO", "DATA/ORA", "VALORE"]);

        $scope.parametersData = [];
        angular.forEach(item.batchParametri, function (db, key) {
            if (db.parametro.isTrend) {
                var tTemp = {
                    "id": key,
                    "nome": db.parametro.nome,
                    "vavg": db.avg,
                    "vmax": db.max,
                    "vmin": db.min,
                    "data": []
                };
                $scope.parametersData.push(tTemp);
                angular.forEach(db.trend, function (db1, key1) {
                    tTemp.data.push({ x: db1.dataOra, y: db1.valore });
                    $scope.exportDataTrend.push([db.parametro.nome, db1.dataOra, db1.valore]);
                });
            }
        });
        // ---------------------------------------------------------------------------------------

    };



    $scope.reload = function () {
        basicCrud_doReload($scope, $http, subUrlBatch, filter);
        //if ($scope.items != null) {
        //    angular.forEach($scope.items, function (db, key) {
        //        $scope.items[key].lunghezza = $scope.items[key].distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
        //    })
        //}
       
    
    };

    $scope.insert = function () {
        $scope.viewState = Enums.ViewState['C'];
        $scope.isBatchAutoCounterEnabled = false;
        $scope.currentItem = new Object();
        $scope.isEnableWrite = true;
        $scope.currentStatus = "inModifica";

        // autocaricamento del materiale se ne è presente uno solo
        if ($scope.ListaMateriali.length == 1) {
            $scope.currentItem.materiale = $scope.ListaMateriali[0];  
            DataFactory.GetDistinteBasiFromMateriale($scope.currentItem.materiale.id).then(function (response) {
                $scope.ListaDistintaBase = response.data;
                if ($scope.ListaDistintaBase.length == 1) {
                    $scope.currentItem.distintaBase = $scope.ListaDistintaBase[0];
                }
            });
        }
        //aggiunta lunghezza pezzo 07/08/2019
        $scope.currentItem.lunghezza;


        // verifica dell'auto-contatore
        DataFactory.GetBatchAutoCounterInfo().then(function (response) {
            $scope.isBatchAutoCounterEnabled = response.data.isEnabled;
            if (response.data.isEnabled) {
                $scope.currentItem.odP = response.data.global;
                $scope.currentItem.odPParziale = response.data.partial;
            }

        });
        ResetAllFields();
    };

    $scope.isOdpSelected = function (item) {
        return ($scope.currentItem != null && $scope.currentItem.odP == item.odP)
    }

    $scope.update = function (event, item) {
        if (event.srcElement.id != 'launch') {
            $scope.showItemDetail(item);
            if ($scope.currentItem != null && $scope.currentItem.odP == item.odP) {
             $scope.currentItem.lunghezza = $scope.currentItem.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
             $scope.viewState = Enums.ViewState['U'];
            }
               
        }
    };

    $scope.cancel = function () {
        $scope.viewState = Enums.ViewState['R']; ResetAllFields(); closeModal('#modalConfirmUndo');
    };

        $scope.confirm = function () {
            $scope.currentItem.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault = $scope.currentItem.lunghezza;
            $scope.currentItem.revisione = $rootScope.globals.currentUser.username;
            basicCrud_doConfirmWithOutReload($scope, $http, subUrlBatch, Enums);
    };

    $scope.refresh = function () {
        var objTemp = { id: $scope.currentItem.id };
        $scope.currentItem = { id: "reloading", Codice:"reloading" };
        basicCrud_doShowItemDetail($scope, $http, subUrlBatch, Enums, objTemp, function (item) {
            $scope.showItemDetail($scope.currentItem);
            $scope.viewState = Enums.ViewState['U'];

            angular.forEach($scope.trcurrentItem, function (it) {
                $scope.trshowItemDetail(it);
            })
         
        });
        
    }

        $scope.confirmAndClose = function () {

     //aggiunta lunghezza pezzo 07/08/2019
            if ($scope.currentItem.distintaBase!=null)
                $scope.currentItem.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault = $scope.currentItem.lunghezza;

            $scope.currentItem.revisione = $rootScope.globals.currentUser.username;
        basicCrud_doConfirm($scope, $http, subUrlBatch, Enums);
    };

    $scope.crudException = function (err) {
        $scope.exceptionMessageText = err.exceptionMessage;
        angular.element(document.querySelector('#modalWarning1')).modal('toggle'); 
    };

    $scope.delete = function (item) { $scope.viewState = Enums.ViewState['D']; basicCrud_doDelete($scope, $http, subUrlBatch, Enums) };

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };

    basicCrud_doInitWatch($scope);

    $scope.reload();

    // funzioni che rendono mutuamente esclusive le drop down
    $scope.selectionMacchine = function (id) {
        $scope.isEnabledcompentenzaMacchineSelection[id] = ($scope.modelsSelMacchine[id] == -1);
    }

    $scope.selectionCompetenzaMacchine = function (id) {
        $scope.isEnabledMacchinaSelection[id] = ($scope.modelsSelCompetenzaMacchine[id] == -1);
    }

    // carico le distinte basi a seconda del materiale in uscita
    $scope.loadDistintaBaseFromMateriale = function (id) {
        $scope.ListaDistintaBase = null;
        DataFactory.GetDistinteBasiFromMateriale(id).then(function (response) {
            $scope.ListaDistintaBase = response.data;
        });
    }

    // funzione per cambiare le fasi di una distinta base
    $scope.changeDistintaBase = function (id) {

        ResetAllFields();

        if (id == -1) {
            $scope.selectedDistintaBase = null;
            $scope.currentItem.distintaBase = null;
        }
        else
            angular.forEach($scope.ListaDistintaBase, function (db, key) {
                if (db.id === id) {
                    $scope.selectedDistintaBase = db;
                    $scope.currentItem.distintaBase = db;
                }
            });

        createOrRefreshTreeView();
    }

    // funzione per lanciare la produzione
    $scope.launch = function () {
        $scope.viewState = Enums.ViewState['C'];
        //item.stato = "1";
        $scope.currentItem.stato = "1";
        //$scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

        basicCrud_doConfirmWithForcedReload($scope, $http, subUrlBatch, Enums);

        angular.element(document.querySelector('#modalConfirmChangeState')).modal('hide');
    }

    // funzione per lanciare la produzione
    $scope.launchAndLink = function () {
        $scope.viewState = Enums.ViewState['C'];
        //item.stato = "1";
        $scope.currentItem.stato = "1";
        //$scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

        basicCrud_doConfirmWithForcedReload($scope, $http, subUrlBatch, Enums);

        angular.element(document.querySelector('#modalConfirmChangeState')).modal('hide');

        var path = "http://"+location.hostname+":8082/sfofasi.html#?idBatch=" + $scope.currentItem.id;
        location.href = path;

    }

    // opzioni per l'albero
    $scope.treeOptions = {
        nodeChildren: "fasi",
        dirSelectable: true,
        injectClasses: {
            ul: "a1",
            li: "a2",
            liSelected: "a7",
            iExpanded: "a3",
            iCollapsed: "a4",
            iLeaf: "a5",
            label: "a6",
            labelSelected: "a8"
        }
    }

    // nodo base per il treeview
    $scope.dataForTheTree =
    [
        { "numeroFase": "", "nome": $scope.ListPhase, "id": "0", "type": "-1", "fasi": [] }
    ];

    $scope.selectedNode = null;

    // quando ho selezionato un ramo
    $scope.showSelected = function (sel) {

        // inizializzazione dei models
        ResetDistintaCicli();

        if (sel.id != 0) {
            $scope.selectedNode = sel;
            $scope.numeroFase = sel.numeroFase;
            $scope.nomeFase = sel.nome;
            $scope.descrizioneFase = sel.descrizione;
            $scope.type = sel.type;

            $scope.IsVisibleFaseCiclica = (sel.type == "0");

            // array per abilitazione macchine e competenze macchine
            $scope.isEnabledMacchinaSelection = {};
            $scope.isEnabledcompentenzaMacchineSelection = {};

            // abilitazione pannello dettagli fase
            $scope.isEnabledDettagliFase = true;

            // abilitazione checkbox di collegamento all'operazione ciclo
            $scope.isEnabledLinkedOption = false;

            fillPhaseItems($scope.selectedDistintaBase.fasi, sel.id)
        }
    };

    function fillPhaseItems(objRoot, phaseId) {  // $scope.currentItem.fasi, tmpDistintaBaseFase
        var arrFasi = objRoot;
        var objRoot = objRoot;
        angular.forEach(arrFasi, function (fase, key) {
            if (fase.id == phaseId) {
                fillItemsInView(objRoot[key])
                return;
            }
            if (objRoot[key].fasi != null)
                fillPhaseItems(objRoot[key].fasi, phaseId);
        })
    }

    // aggiornamento dell'albero
    function createOrRefreshTreeView() {
        $scope.defaultExpanded = [];
        $scope.dataForTheTree[0].fasi = [];
        if (($scope.selectedDistintaBase != null) &&
            ($scope.selectedDistintaBase.fasi != null)) {
            var arrayOrdered = orderById($scope.selectedDistintaBase.fasi, "numeroFase", false);
            $scope.dataForTheTree[0].fasi = arrayOrdered;
        }
        $scope.defaultExpanded = [$scope.dataForTheTree[0], $scope.dataForTheTree[0].fasi[0]];
    }


    //funzione per popolare gli elementi della finestra di dettaglio quando seleziono un ramo dal treeview (tempi) o seleziono la voce dalla ddl delle operazioni ciclo
    function fillItemsInView(item) {
        angular.forEach($scope.CausaliFase, function (obj, key) {

            $scope.isEnabledMacchinaSelection[obj.id] = true;
            $scope.isEnabledcompentenzaMacchineSelection[obj.id] = true;

            // valore selezionato per le competenze operatore
            if (item.manodopera[obj.id].operatoreCompetenza != null) {
                $scope.modelsSelCompetenzaOperatore[obj.id] = item.manodopera[obj.id].operatoreCompetenza.id;
            }

            // recupero il tempo dell'operatore per ogni causale e popolo i vari input text
            var tmpDoubleOperatore = item.manodopera[obj.id].tempo;
            $scope.modelsOreUomo[obj.id] = parseInt(tmpDoubleOperatore / 3600);
            $scope.modelsMinutiUomo[obj.id] = parseInt((tmpDoubleOperatore % 3600) / 60);
            $scope.modelsSecondiUomo[obj.id] = parseInt(tmpDoubleOperatore % 60);

            // popolo le drop down list con il valore selezionato
            if (item.macchine[obj.id].macchina == null) {
                $scope.modelsSelMacchine[obj.id] = -1;
                $scope.isEnabledMacchinaSelection[obj.id] = false;
            }
            else
                $scope.modelsSelMacchine[obj.id] = item.macchine[obj.id].macchina.id;


            if (item.macchine[obj.id].macchinaCompetenza == null) {
                $scope.modelsSelCompetenzaMacchine[obj.id] = -1;
                $scope.isEnabledcompentenzaMacchineSelection[obj.id] = false;
            }
            else
                $scope.modelsSelCompetenzaMacchine[obj.id] = item.macchine[obj.id].macchinaCompetenza.id;

            // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
            if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                $scope.isEnabledMacchinaSelection[obj.id] = true;

            // recupero il tempo della macchina per ogni causale e popolo i vari input text
            var tmpDoubleMacchina = item.macchine[obj.id].tempo;
            $scope.modelsOreMacchina[obj.id] = parseInt(tmpDoubleMacchina / 3600);
            $scope.modelsMinutiMacchina[obj.id] = parseInt((tmpDoubleMacchina % 3600) / 60);
            $scope.modelsSecondiMacchina[obj.id] = parseInt(tmpDoubleMacchina % 60);
        });
    }

    // ordinamento di json per campo richiesto
    function orderById(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if (reverse) filtered.reverse();
        return filtered;
    };


    // funzione per resettare tutti i campi
    function ResetAllFields() {

        ResetDistintaCicli();


        $scope.pcurrentItem = null;  
        $scope.parametersData = null;
        $scope.nodesTableArr = null;
        $scope.AnalisiData = null;
        $scope.trcurrentItem = [];
        $scope.data_multiBarChart = [{ key: '', values: [{ x: 0, y: 0 }] }];
    }

    function ResetDistintaCicli() {
        $scope.modelsOreUomo = {};
        $scope.modelsMinutiUomo = {};
        $scope.modelsSecondiUomo = {};
        $scope.modelsSelCompetenzaOperatore = {};
        $scope.modelsOreMacchina = {};
        $scope.modelsMinutiMacchina = {};
        $scope.modelsSecondiMacchina = {};
        $scope.modelsSelCompetenzaMacchine = {};
        $scope.modelsSelMacchine = {};

        $scope.modelOperazioniCiclo = {};
        $scope.numeroFase = null;
        $scope.nomeFase = null;
        $scope.descrizioneFase = null;

        $scope.isEnabledDettagliFase = null;

    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    // Definizione del filtro di visualizzazione in griglia
    function filter() {
        if ($scope.InitFilter) {
            $scope.items = $.grep($scope.items, function (e) {
                return e.stato.startsWith($scope.InitFilter);
            });
        }
    }

    // Viene verificato lo stato della produzione corrente
    $scope.IsOnLavorazione = function () {

        if ($scope.currentItem == null) return false;
        return (($scope.currentItem.stato === 'inLavorazioneInEmergenza')
            || ($scope.currentItem.stato === 'inLavorazione')
            || ($scope.currentItem.stato === 'inLavorazioneInPausa'))
    }

    // Viene verificato lo stato della produzione corrente
    $scope.IsOnTerminato = function () {

        if ($scope.currentItem == null) return false;
        return (($scope.currentItem.stato === 'terminato')
            || ($scope.currentItem.stato === 'terminatoAbortito'))
    }



    // Metodo per richiamare il server e abortire l'ordine corrente
    $scope.abort = function () {
        angular.element(document.querySelector('#modalConfirmAbort')).modal('hide');
        if ($scope.currentItem == null) return ;
        
        $scope.currentItem.stato = 'terminatoAbortito';
        this.confirmAndClose();
       
    }




    //------------------------------------------------------------------------------------------------------------------------------
    // produzione-parametri
    //------------------------------------------------------------------------------------------------------------------------------
    $scope.pdtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(10)
    .withOption('stateSave', true)
    .withOption("retrieve", true)
    .withOption('LengthChange', false)
    .withLanguageSource(urlLanguageDataTable)
    .withPaginationType("full_numbers")
    .withOption('lengthMenu', [[1, 5, 10, 25, 50, -1], [1, 5, 10, 25, 50, "Tutti"]]);


    // Combo box Tipi Dato
    $scope.cbTipiDato = null;
    DataFactory.GetTipiDato().then(function (response) {
        var xs = response.data;
        angular.forEach(xs, function (x) {
            //x.value = x.value.toLowerCase();
            x.value = x.value.charAt(0).toLowerCase() + x.value.slice(1);
        })
        $scope.cbTipiDato = xs;
    });

    //----------------------


    $scope.pIsFullScreenOnEdit = false;
    //$scope.pitems = [];
    $scope.pcurrentItem = null;
    $scope.pviewState = Enums.ViewState['R'];
    $scope.ptitolo = "Dettaglio";
    $scope.pdtInstance = {};
    $scope.originalPCurrentItem = [];

    $scope.ptitle = function () { pdoTitle($scope, Enums); };
    function pdoTitle($scope, Enums) {
        switch ($scope.pviewState) {
            case Enums.ViewState['R']:
                $scope.ptitolo = "Dettaglio ";// + $scope.currentItem.codice;
                break;
            case Enums.ViewState['U']:
                $scope.ptitolo = "Modifica ";// + $scope.currentItem.codice;
                break;
            case Enums.ViewState['C']:
                $scope.ptitolo = "Inserimento ";
                break;
        }
    };


    $scope.pinsert = function () {
        $scope.PlistaValori = [];
        $scope.pviewState = Enums.ViewState['C'];
        $scope.pcurrentItem = new Object();
        $scope.pcurrentItem.tipologia = new Object();
        $scope.originalPCurrentItem = [];
    };

    $scope.pupdate = function () {
        if (!$scope.pcurrentItem.visibilita || $scope.currentStatus != "inModifica") return;

        // 1. cercare nella distinta base il parametro che ha lo stesso codice del parametro selezionato (= $scope.pcurrentItem).
        // 2. dopo aver trovato il parametro, prelevare il valore di default
        // 3. effettuare lo split del valore di default usando come carattere di split ","
        // 4. popolare l'array di $scope.listaValori con gli elementi splittati
        $scope.pviewState = Enums.ViewState['U'];

        $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));

        if ($scope.pcurrentItem.tipo == "list") {
            $scope.PlistaValori = [];

            var stringa = $.grep($scope.currentItem.distintaBase.distintaBaseParametri,
                function (o) { return o.codice == $scope.pcurrentItem.codice });

            if (stringa[0].valoreDefault.length <= 1) return;

            $scope.PlistaValori = ["---x---"];
            angular.forEach(stringa[0].valoreDefault.split("\n"), function (item) { $scope.PlistaValori.push(item); });
        }//fine if
     
    };

    $scope.pcancel = function () {
        $scope.pviewState = Enums.ViewState['R'];
        var itemToUndo = null;
        var allParams = $scope.currentItem.distintaBase.distintaBaseParametri;

        for (var orig = 0; orig < $scope.originalPCurrentItem.length; orig++) {
            itemToUndo = $scope.originalPCurrentItem[orig].codice;
            for (var i = 0; i < allParams.length; i++) {
                if (allParams[i].codice == itemToUndo) {
                    allParams[i] = $scope.originalPCurrentItem[orig];
                    i = allParams.length;
                }
            }
            if ($scope.pcurrentItem.codice == itemToUndo)
                $scope.pcurrentItem = $scope.originalPCurrentItem[orig]; //restore detail
        }
       
        $scope.originalPCurrentItem = [];
    };

    $scope.pdelete = function (item) {
        $scope.pviewState = Enums.ViewState['D'];
        var itemToRemove = $scope.pcurrentItem.codice;
        var allParams = $scope.currentItem.distintaBase.distintaBaseParametri;
        for (var i = 0; i < allParams.length; i++) {
            if (allParams[i].codice == itemToRemove) {
                allParams.splice(i, 1);
                i = allParams.length;
            }
        }
        $scope.pviewState = Enums.ViewState['R'];
        $scope.originalPCurrentItem = [];
        angular.element(document.querySelector('#pmodalConfirmDelete')).modal('hide');
    };

    $scope.pconfirm = function () {
        if ($scope.pviewState === Enums.ViewState['C']) {
            if ($scope.currentItem.distintaBase.distintaBaseParametri == null)
                $scope.currentItem.distintaBase.distintaBaseParametri = [];

            $scope.currentItem.distintaBase.distintaBaseParametri.push($scope.pcurrentItem)
        }
        else if ($scope.pviewState === Enums.ViewState['U']) {

            if ($scope.pcurrentItem.unitaDiMisura != null) {
                var um = $.grep($scope.ListaUnitaDiMisura, function (e) { return e.id == $scope.pcurrentItem.unitaDiMisura.id; });
                $scope.pcurrentItem.unitaDiMisura.item = um[0];
            }

        }
        $scope.originalPCurrentItem = [];
        $scope.pviewState = Enums.ViewState['R'];
    };

    $scope.pshowItemDetail = function (item) {
        if ($scope.pviewState != Enums.ViewState['R'])
            return;
        $scope.pcurrentItem = item;
        if ($scope.pviewState == Enums.ViewState['U'] && !itemExists($scope.originalPCurrentItem, item.codice))
            $scope.originalPCurrentItem.push(angular.copy($scope.pcurrentItem));

    };
    function itemExists(arr, value) {
        var r = false
        angular.forEach(arr, function (item) {
            if (item.codice === value)
                r = true;
        });
        return r;
    }

    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
        var x = $scope;
    });


    $scope.$watch('pviewState', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.ptitle();
    });

    //------------------------------------------------------------------------------------------------------------------------------
    // produzione-parametri
    //------------------------------------------------------------------------------------------------------------------------------

    function setTotaleTempi(_scope, item) {
        if (_scope.tothh == null) _scope.tothh = 0;
        if (_scope.totmm == null) _scope.totmm = 0;
        if (_scope.totss == null) _scope.totss = 0;
        var totseconds = 0;
        var minuteseconds = 0;
        var seconds = 0;
        if (item.distintaBase == null) return;
        var fasi = item.distintaBase.fasi;
        angular.forEach(fasi, function (fase) {
            if (fase.manodopera != null) {
                var mans = fase.manodopera;
                angular.forEach(mans, function (man) {
                    totseconds += man.tempo;
                })
            }
            if (fase.macchine != null) {
                var macs = fase.macchine;
                angular.forEach(macs, function (mac) {
                    totseconds += mac.tempo;
                })
            }
        });
        _scope.tothh = Math.floor(totseconds / 3600);
        _scope.totmm = Math.floor((totseconds - (_scope.tothh * 3600)) / 60);
        _scope.totss = Math.floor((totseconds - (_scope.tothh * 3600) - (_scope.totmm * 60)));
    }


    function setRiepilogo(_scope, item) {

        if (_scope.oreUomo == null) _scope.oreUomo = 0;         // teorici
        if (_scope.minutiUomo == null) _scope.minutiUomo = 0;
        if (_scope.secondiUomo == null) _scope.secondiUomo = 0;
        if (_scope.oreMacchina == null) _scope.oreMacchina = 0;
        if (_scope.minutiMacchina == null) _scope.minutiMacchina = 0;
        if (_scope.secondiMacchina == null) _scope.secondiMacchina = 0;

        if (_scope.oreUomoR == null) _scope.oreUomo = 0;        // realtime
        if (_scope.minutiUomoR == null) _scope.minutiUomoR = 0;
        if (_scope.secondiUomoR == null) _scope.secondiUomoR = 0;
        if (_scope.oreMacchinaR == null) _scope.oreMacchinaR = 0;
        if (_scope.minutiMacchinaR == null) _scope.minutiMacchinaR = 0;
        if (_scope.secondiMacchinaR == null) _scope.secondiMacchinaR = 0;


        var totsecondsU = 0;
        var totsecondsM = 0;
        var totsecondsUR = 0;
        var totsecondsMR = 0;
        if (item.distintaBase == null) return;
        var fasi = item.distintaBase.fasi;
        angular.forEach(fasi, function (fase) {
            if (fase.manodopera != null) {
                var mans = fase.manodopera;
                angular.forEach(mans, function (man) {
                    totsecondsU += man.tempo;
                })
            }
            if (fase.macchine != null) {
                var macs = fase.macchine;
                angular.forEach(macs, function (mac) {
                    totsecondsM += mac.tempo;
                })
            }
        });

        var fasiR = item.fasi;
        angular.forEach(fasiR, function (fase) {
            if (fase.manodopera != null) {
                var mans = fase.manodopera;
                angular.forEach(mans, function (man) {
                    if (man.inizio != null && man.fine != null)
                        totsecondsUR += moment(man.fine).diff(moment(man.inizio), 'seconds');
                })
            }
            if (fase.macchine != null) {
                var macs = fase.macchine;
                angular.forEach(macs, function (mac) {
                    if (mac.inizio != null && mac.fine != null)
                        totsecondsMR += moment(mac.fine).diff(moment(mac.inizio), 'seconds');
                })
            }
        });


        _scope.oreUomo = Math.floor(totsecondsU / 3600);
        _scope.minutiUomo = Math.floor((totsecondsU - (_scope.oreUomo * 3600)) / 60);
        _scope.secondiUomo = Math.floor((totsecondsU - (_scope.oreUomo * 3600) - (_scope.minutiUomo * 60)));
        _scope.oreMacchina = Math.floor(totsecondsM / 3600);
        _scope.minutiMacchina = Math.floor((totsecondsM - (_scope.oreMacchina * 3600)) / 60);
        _scope.secondiMacchina = Math.floor((totsecondsM - (_scope.oreMacchina * 3600) - (_scope.minutiMacchina * 60)));

        _scope.oreUomoR = Math.floor(totsecondsUR / 3600);
        _scope.minutiUomoR = Math.floor((totsecondsUR - (_scope.oreUomoR * 3600)) / 60);
        _scope.secondiUomoR = Math.floor((totsecondsUR - (_scope.oreUomoR * 3600) - (_scope.minutiUomoR * 60)));
        _scope.oreMacchinaR = Math.floor(totsecondsMR / 3600);
        _scope.minutiMacchinaR = Math.floor((totsecondsMR - (_scope.oreMacchinaR * 3600)) / 60);
        _scope.secondiMacchinaR = Math.floor((totsecondsMR - (_scope.oreMacchinaR * 3600) - (_scope.minutiMacchinaR * 60)));
    }






    //------------------------------------------------------------------------------------------------------------------------------
    // produzione-analisi
    //------------------------------------------------------------------------------------------------------------------------------
    $scope.evshowItemDetail = function (item) {
        //$scope.evcurrentItem = item;          // Grid Row Selection -> Disabled //
    };


    $scope.addChildNode = function (node) {
        alert('addChildNode')
    }
    $scope.deleteNode = function (node) {
        alert('deleteNode')
    }
    $scope.editNode = function (node) {
        alert('editNode')
    }

    $scope.AnalisiPhaseRowSelected = function (node) {
        $scope.AnalisiPhaseSelected = node;
        var batchAvanzamenti = $scope.currentItem.batchAvanzamenti;

        if (node.level ==0)
            $scope.AnalisiData = batchAvanzamenti; // nel caso di selezione della root, vengono prelevati tutti gli elementi
        else {
            if (batchAvanzamenti != null) {
                $scope.AnalisiData = $.grep(batchAvanzamenti, function (e) {
                    return e.numeroFase == node.numeroFase;
                });

                $scope.AnalisiDataFase = $.grep($scope.currentItem.fasi, function (e) {
                    return e.numeroFase === node.numeroFase;
                });  
            }
                
        }

    }




    $scope.dFormat = function (d, nota) {
        if (!d) return (nota == "Terminato" ? "" : "in corso");
        return d.substr(8, 2) + "/" + d.substr(5, 2) + "/" + d.substr(0, 4) + " " + d.substr(11, 8);
    }





    //------------------------------------------------------------------    
    $scope.initializeNodeTreeTable = function (pChildArr0, scopeNodes) { 
        var pChildArr = [{ id: '0', nome: 'Elenco fasi',  fasi: pChildArr0 }];
        var length = pChildArr.length || 0;
        for (var i = 0; i < length; i++) {

            var theElement = pChildArr[i];
            var level = 0;
            var childCount = 0;
            if (theElement.fasi && theElement.fasi.length)
                childCount = theElement.fasi.length;
            scopeNodes.push({
                name: theElement.nome,   
                id: theElement.id,
                numeroFase: theElement.numeroFase,
                parent: "root",
                toggleStatus: false,
                parentId: -1,
                isShow: true,
                level: 0,
                childCount: childCount,
                isSaveBtn: false,
                isUpdateBtn: false
            });
            if (theElement.fasi != undefined)
                $scope.initializeNodeTreeTableChild(theElement.fasi, theElement.nome, theElement.id, level, scopeNodes);

        }
    };

    $scope.initializeNodeTreeTableChild = function (pChildArr, pParentName, pPparentId, pLevel, scopeNodes) {
        var isShowNode = false;
        pLevel = pLevel + 1;
        for (var i = 0; i < pChildArr.length; i++) {
            var node = pChildArr[i];
            var childCount = node.fasi != undefined ? node.fasi.length : 0
            scopeNodes.push({
                name: node.nome,
                id: node.id,
                numeroFase: node.numeroFase,
                parent: pParentName,
                toggleStatus: false,
                parentId: pPparentId,
                isShow: isShowNode,
                level: pLevel,
                childCount: childCount,
                isSaveBtn: false,
                isUpdateBtn: false
            });
            if (node.fasi != undefined)
                $scope.initializeNodeTreeTableChild(node.fasi, node.nome, node.id, pLevel, scopeNodes)
        }
    };

    $scope.toggleStatus = false;

    $scope.toggleDropdown = function (node, scopeNodes) {
        node.toggleStatus = node.toggleStatus == true ? false : true;
        $scope.toggleStatus = node.toggleStatus;
        $scope.toggleDropdownHelper(node.id, $scope.toggleStatus, scopeNodes);
    };

    $scope.toggleDropdownHelper = function (parentNodeId, toggleStatus, scopeNodes) {
        for (var i = 0; i < scopeNodes.length; i++) {
            node = scopeNodes[i];
            if (node.parentId == parentNodeId) {
                if (toggleStatus == false)
                    $scope.toggleDropdownHelper(node.id, toggleStatus, scopeNodes);
                scopeNodes[i].isShow = toggleStatus;
            }
        }
    };
    //------------------------------------------------------------------    

    // viewer related functions.
    $scope.selectIndentationClass = function (node) {
        return 'level' + node.level;
    };

    $scope.hasDropdown = function (node) {
        if (node.childCount > 0)
            return "hasDropdown";
        else
            return "noDropdown";
    };

    ////http://nvd3.org/examples/discreteBar.html
    ////http://krispo.github.io/angular-nvd3/#/lineChart
    ////http://nvd3-community.github.io/nvd3/examples/documentation.html#lineChart
    ////https://nvd3-community.github.io/nvd3/examples/documentation.html#discreteBarChart
    //------------------------------------------------------------------------------------------------------------------------------
    // produzione-analisi
    //------------------------------------------------------------------------------------------------------------------------------



    //------------------------------------------------------------------------------------------------------------------------------
    // produzione-trend
    //------------------------------------------------------------------------------------------------------------------------------
    $scope.trcurrentItem = [];
    $scope.trshowItemDetail = function (item) {
        var found = false;
        for (var i = 0; i < $scope.trcurrentItem.length; i++) {
            if ($scope.trcurrentItem[i].id == item.id) {
                $scope.trcurrentItem.splice(i, 1);
                i = $scope.trcurrentItem.length;
                found = true;
            }
        }
        if (!found)
            $scope.trcurrentItem.push(item);

        delete $scope.data_multiBarChart;
        $scope.data_multiBarChart = generateData();


    };


    $scope.checkIfSelected = function (item) {
        var found = false;
        angular.forEach($scope.trcurrentItem, function (it) {
            if (item.id == it.id) { found = true; return; }
        })
        return found;
    }

    $scope.options_multiBarChart = {
        chart: {
            type: 'multiBarChart',           // 'lineWithFocusChart', //'multiBarChart', 'historicalBarChart'
            height: 450,
            margin: {
                top: 20,
                right: 30,
                bottom: 45,
                left: 65
            },
            //clipEdge: true,
            //duration: 500,
            //stacked: true,
            //useInteractiveGuideline: true,



            xAxis: {
                //axisLabel: 'Tempo (hh:mm)',
                axisLabel: $scope.Machine_Time ,
                showMaxMin: false,
                //tickFormat: function (d) { return d3.time.format('%d/%m %H:%M:%S')(new Date(d)); }
                tickFormat: function (x) { return d3.time.format('%d-%m %H:%M:%S')(new Date(x)); }
            },

            x2Axis: {
                tickFormat: function (y) { return d3.time.format('%d-%m %H:%M:%S')(new Date(y)); }
            },

            yAxis: {
                //axisLabel: 'Valore',
                axisLabel: $scope.Machine_Value,
                axisLabelDistance: 0,
                tickFormat: function (d) { return d3.format(',.1f')(d); }
                
            },

            //zoom: {
            //    enabled: true,
            //    scaleExtent: [1, 10],
            //    useFixedDomain: false,
            //    useNiceScale: false,
            //    horizontalOff: false,
            //    verticalOff: true//,
            //    //unzoomEventType: "dblclick.zoom"
            //}


            //y1Axis: {
            //    tickFormat: function (d) { return d3.format(',.1f')(d); }
            //},
            //y2Axis: {
            //    tickFormat: function (d) { return d3.format(',.1f')(d); }
            //},
            //y3Axis: {
            //    tickFormat: function (d) { return d3.format(',.1f')(d); }
            //},
            //y4Axis: {
            //    tickFormat: function (d) { return d3.format(',.1f')(d); }
            //}

        }
    };

     



    function generateData() {
        var yCount = $scope.trcurrentItem.length;
        var res = [];
        for (n = 0; n < $scope.trcurrentItem.length; n++)
        {
            var paramItem = $scope.trcurrentItem[n];
            res.push({ key: paramItem.nome, values: paramItem.data });
        }

        if (res.length > 0)
            return res;
        else
            return [{ key: '', values: [{ x: 0, y: 0 }] }];
    }

}]);



//=============================================================================================================
app.directive('excelExport',
    function () {
        return {
            restrict: 'A',
            scope: {
                fileName: "@",
                data: "&exportData"
            },
            replace: true,
            template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()">Export to Excel <i class="fa fa-download"></i></button>',
            link: function (scope, element) {
                scope.download = function () {

                    function datenum(v, date1904) {
                        if (date1904) v += 1462;
                        var epoch = Date.parse(v);
                        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                    };

                    function getSheet(data, opts) {
                        var ws = {};
                        var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
                        for (var R = 0; R != data.length; ++R) {
                            for (var C = 0; C != data[R].length; ++C) {
                                if (range.s.r > R) range.s.r = R;
                                if (range.s.c > C) range.s.c = C;
                                if (range.e.r < R) range.e.r = R;
                                if (range.e.c < C) range.e.c = C;
                                var cell = { v: data[R][C] };
                                if (cell.v == null) continue;
                                var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                                if (typeof cell.v === 'number') cell.t = 'n';
                                else if (typeof cell.v === 'boolean') cell.t = 'b';
                                else if (cell.v instanceof Date) {
                                    cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                    cell.v = datenum(cell.v);
                                }
                                else cell.t = 's';

                                ws[cell_ref] = cell;
                            }
                        }
                        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                        return ws;
                    };

                    function Workbook() {
                        if (!(this instanceof Workbook)) return new Workbook();
                        this.SheetNames = [];
                        this.Sheets = {};
                    }

                    var wb = new Workbook(), ws = getSheet(scope.data());
                    /* add worksheet to workbook */
                    wb.SheetNames.push(scope.fileName);
                    wb.Sheets[scope.fileName] = ws;
                
                    var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

                    function s2ab(s) {
                        var buf = new ArrayBuffer(s.length);
                        var view = new Uint8Array(buf);
                        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                        return buf;
                    }

                    saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), scope.fileName + '.xlsx');

                };
            }
        };
    }
 );
//=============================================================================================================



app.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});


// direttiva per il menu con il tasto destro del mouse
app.directive("contextMenu", function ($compile) {
    contextMenu = {};
    contextMenu.restrict = "AE";
    contextMenu.link = function (lScope, lElem, lAttr) {
        lElem.on("contextmenu", function (e) {
            e.preventDefault(); // default context menu is disabled
            //  The customized context menu is defined in the main controller. To function the ng-click functions the, contextmenu HTML should be compiled.
            lElem.append($compile(lScope[lAttr.contextMenu])(lScope));
            // The location of the context menu is defined on the click position and the click position is catched by the right click event.
            //$("#contextmenu-node").css("left", e.clientX);
            //$("#contextmenu-node").css("top", e.clientY);
        });
        lElem.on("mouseleave", function (e) {
            // on mouse leave, the context menu is removed.
            if ($("#contextmenu-node"))
                $("#contextmenu-node").remove();
        });
    };
    return contextMenu;
});


app.directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'yyyy-MM-dd';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})