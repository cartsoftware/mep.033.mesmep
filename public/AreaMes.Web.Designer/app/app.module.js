﻿var urlApi = "http://localhost:9000/api";
var urlLanguageDataTable = './app/localization/datatable/It.txt';
var subUrlApi_Macchina = "macchina";
var subUrlApi_TabelleTipoMacchina = "TipiMacchina";
var subUrlApi_MacchinaCompetenza = "macchinacompetenza";
var subUrlApi_Operatore = "operatore";
var subUrlApi_OperatoreCompetenza = "operatorecompetenza";
var subUrlApi_UnitaDiMisura = "unitadimisura";
var subUrlApi_Parametri = "parametri";
var subUrlApi_CausaliRiavvio = "causaliriavvio";
var subUrlApi_Materiali = "materiale";
var subUrlOperazioniCiclo = "operazioniciclo";
var subUrlDistintaBase = "distintabase";
var subUrlBatch = "batch";
var subUrlLine = "coda";
var subUrlLineTemplate = "codatemplate";
var subUrlApi_Enums = "enum";
var subUrlApi_Dashboard = "dashboard";
var subUrlApi_Licenza = "licence";

// action specificche per caricare le enum
var subUrlAction_GetTipiDato = "GetTipiDato";
var subUrlAction_GetUmList = "GetUmList";
var subUrlAction_GetLivelliAccesso = "GetLivelliAccesso";
var subUrlAction_GetTipiMateriale = "GetTipiMateriale";
var subUrlAction_GetCausaliFase = "GetCausaliFase";
var subUrlAction_GetStatiDistintaBase = "GetStatiDistintaBase";
var subUrlActionGetDistinteBasiFromMateriale = "GetDistinteBasiFromMateriale";
var subUrlActionGetDistinteBasiFromMacchina = "GetDistinteBasiFromMacchina";
var subUrlAction_GetStatiBatch = "GetStatiBatch";
var subUrlAction_GetTranslations = "GetTranslations";
var subUrlAction_GetTipoDriver = "GetTipoDriver";
var subUrlAction_GetTipoLingua = "GetTipoLingua";

//var suburlAction_GetDriver = "GetUmList";
var customerName = "AreaMes";

var subUrlAction_Login = "Login";




//var app = angular.module("AppMacchine", ['$rootScope', '$location', '$cookies', '$http', '$window', 'pascalprecht.translate'])
var app = angular.module("AppMacchine", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl', 'nvd3', 'pascalprecht.translate', 'ngJustGage'])
.run(function ($rootScope, $location, $cookies, $http, $window) {

    /*
    * Function to 'Angular-fy' dynamically loaded content
    * by JQuery. This compiles the new html code and injects it
    * into the DOM so Angular 'knows' about the new code.
    */


    try{
        var Ling= JSON.parse($cookies.get("MesAuth"));
        var lingua = Ling.currentUser.lingua;
        if (lingua == null) {
            lingua = "it";
        }
        urlLanguageDataTable = './app/localization/datatable/' +lingua+'.txt';
    }
    catch (e) { }

    $window.angularfy = function (target, newHtml) {
        // must use JQuery to query for element by id.
        // must wrap as angular element otherwise 'scope' function is not defined.
        var targetScope = angular.element($(target)).scope();

        //        elem.replaceWith( $compile( newHtml )(elemScope)); Displays html wrong.
        //        elem.html( $compile( newHtml )(elemScope)); Does not work.
        $(target)['html']($compile(newHtml)(targetScope)); // Does work!
        targetScope.$apply();
    }

        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('MesAuth');
        if ($rootScope.globals!= null) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        
        }
        var x = $window.location.href;
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if (x.indexOf('/login') == -1 && !$rootScope.globals) {
                $window.location.href = 'login.html';
            }
        });
});


app.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.cache = false;
});

//var translations = {
//    HEADLINE: 'What an awesome moduerewrewrewrewrewrewrewrewrewle!',
//    PARAGRAPH: 'Srsly!'
//};

//app.config(['$translateProvider', function ($translateProvider) {
//    // add translation table
//    $translateProvider.translations('en', translations);
//    $translateProvider.preferredLanguage('en');
//}]);



function basicCrud_doTitle($scope, Enums, itemName) {

    switch ($scope.viewState) {
        case Enums.ViewState['R']:
            $scope.titolo = "Dettaglio " + ((itemName == null) ? $scope.currentItem.codice : itemName);
            break;
        case Enums.ViewState['U']:
            $scope.titolo = "Modifica " + ((itemName == null) ? $scope.currentItem.codice : itemName);
            break;
        case Enums.ViewState['C']:
            $scope.titolo = "Inserimento ";
            break;
    }
};

function basicCrud_doReload($scope, $http, subUrlApi, filterCallback) {
    $http.get(urlApi + '/' + subUrlApi)
               .success(function (data) {
                   $scope.items = [];
                   $scope.items.push.apply($scope.items, data);

                   if (filterCallback)
                        filterCallback($scope.items);

               }).error(function (e) {
                   console.log(e);
               });
}

function basicCrud_doConfirm($scope, $http, subUrlApi, Enums) {

    var config = {
        headers: {
            'Language': $scope.$parent.globals.currentUser.lingua
        }
    }

    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem, config )
               .success(function (data) {
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem = data;
                       $scope.reload();
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   $scope.viewState = Enums.ViewState['R'];
                   $scope.currentItem = null;
               })
               .error(function (e) {
                   console.log(e);
                   $scope.crudException(e)
               });
}

function basicCrud_doConfirmWithOutReload($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
               .success(function (data) {
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem = data;
                       //$scope.reload();
                       $scope.reload();
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   //$scope.viewState = Enums.ViewState['R'];
               })
               .error(function (e) {
                   console.log(e);
                   $scope.crudException(e)
               });
}

/////////////////////////////////////////////////// code
function basicCrud_doConfirmWithOutReloadBatch($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItemBatch)
        .success(function (data) {
            var newBatch = null;
            var newBatch = $scope.currentItem.batch.find(x => x.id == data.id);
            if (newBatch == null) {
                $scope.currentItemBatch = data;
                data.lunghezzaPezzo = $scope.currentItemBatch.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                $scope.currentItem.batch.push(data);
                $scope.reload();

            }
            else  {
                var elementPos = $scope.currentItem.batch.map(function (x) { return x.id; }).indexOf($scope.currentItemBatch.id);
                $scope.currentItemBatch.lunghezzaPezzo = $scope.currentItemBatch.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                $scope.currentItem.batch[elementPos] = $scope.currentItemBatch;
            }
            //if ($scope.viewState === Enums.ViewState['C']) {
            //    $scope.currentItemBatch = data;
            //    data.lunghezzaPezzo = $scope.currentItemBatch.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
            //    $scope.currentItem.batch.push(data);
            //    $scope.reload();
                
            //}
            //else if ($scope.viewState === Enums.ViewState['U']) {
            //    var elementPos = $scope.currentItem.batch.map(function (x) { return x.id; }).indexOf($scope.currentItemBatch.id);
            //    $scope.currentItemBatch.lunghezzaPezzo = $scope.currentItemBatch.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
            //    $scope.currentItem.batch[elementPos] = $scope.currentItemBatch;
            //}

            //$scope.viewState = Enums.ViewState['R'];
        })
        .error(function (e) {
            console.log(e);
            $scope.crudException(e)
        });
}
////////////////////////////////////////////////////


function basicCrud_doConfirmWithForcedReload($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
               .success(function (data) {
                   closeModal('#modalConfirmChangeState');
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem = data;
                       
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   $scope.viewState = Enums.ViewState['R'];
                   $scope.reload();
               })
               .error(function (e) {
                   console.log(e);
                   $scope.crudException(e)
               });
}


function basicCrud_doConfirmWithBatchTemplate(batch, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, batch)
        .success(function (data) {
            //closeModal('#modalConfirmChangeState');
            //if ($scope.viewState === Enums.ViewState['C']) {
            //    $scope.currentItem = data;

            //}
            //else if ($scope.viewState === Enums.ViewState['U']) {
            //    var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
            //    $scope.items[elementPos] = $scope.currentItem;
            //}

            $scope.viewState = Enums.ViewState['R'];
            $scope.reload();
        })
        .error(function (e) {
            console.log(e);
            $scope.crudException(e)
        });
}


function basicCrud_doShowItemDetail($scope, $http, subUrlApi, Enums, item, onSuccessCallback) {
         if(($scope.currentItem == null) || (item.id !== $scope.currentItem.id)) {
        $http.get(urlApi + '/' + subUrlApi + '/' + item.id)
            .success(function (data) {
                $scope.currentItem = data;
                $scope.viewState = Enums.ViewState['R'];
                if (onSuccessCallback != null)
                    onSuccessCallback($scope.currentItem);
            })
        .error(function(e) {
            console.log(e);
            $scope.crudException(e)
            });
    }
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function basicCrud_doShowItemDetailBatch($scope, $http, subUrlApi, Enums, item, onSuccessCallback) {
    if (($scope.currentItemBatch == null) || (item.id !== $scope.currentItemBatch.id)) {
        $http.get(urlApi + '/' + subUrlApi + '/' + item.id)
            .success(function (data) {
                $scope.currentItemBatch = data;
                    //$scope.viewState = Enums.ViewState['R'];
                    //if (onSuccessCallback != null)
                    //    onSuccessCallback($scope.currentItemBatch);
                if ($scope.currentItemBatch != null && $scope.currentItemBatch.odP == item.odP) {
                    $scope.currentItemBatch.lunghezza = $scope.currentItemBatch.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                    //$scope.viewState = Enums.ViewState['U'];
                    //$('#modalAddBatch').modal('toggle');
                    $('#modalAddBatch').modal({
                        show: 'true'
                    });
                }
               
            })
            .error(function (e) {
                console.log(e);
                $scope.crudException(e)
            });
    }
}
////////////////////////////////////////////////////////////////////////
function basicCrud_doInitWatch($scope) {
    $scope.$watch('viewState', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        //$scope.title();
    });
    $scope.$watch('currentItem', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        //$scope.title();
    });
}

function basicCrud_doDelete($scope, $http, subUrlApi, Enums) {
    var config = {
        headers: {
            'Language': $scope.$parent.globals.currentUser.lingua
        }
    }

    $http.delete(urlApi + '/' + subUrlApi + '/' + $scope.currentItem.id, config)
               .success(function (data) {
                   $scope.reload();
                   $scope.viewState = Enums.ViewState['R'];
                   angular.element(document.querySelector('#modalConfirmDelete')).modal('hide');
               })
               .error(function (e) {
                   console.log(e);
                   $scope.crudException(e)
               });
}

///////////////////////////////////////////////////////////////
function basicCrud_doDeleteBatch($scope, $http, subUrlApi, Enums, item) {
    var config = {
        headers: {
            'Language': $scope.$parent.globals.currentUser.lingua
        }
    }

    $http.delete(urlApi + '/' + subUrlApi + '/' + item.id, config)
        .success(function (data) {
            $scope.reload();
            $scope.viewState = Enums.ViewState['R'];
            angular.element(document.querySelector('#modalConfirmDelete')).modal('hide');
        })
        .error(function (e) {
            console.log(e);
            $scope.crudException(e)
        });
}
///////////////////////////////////////////////////////

function closeModal(id) {
    angular.element(document.querySelector(id)).modal('hide');
}

