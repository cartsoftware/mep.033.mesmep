﻿
var app = angular.module("appCfg", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl']);


app.controller('cfgCtrl', ['$scope', '$location', '$rootScope', '$cookies' , function ($scope,  $location, $rootScope, $cookies) {


    $scope.serverIP = "localhost";
    $scope.webApi = "9000";


    // --------------------------------------------------------------------------------------------------------------
    // In fase di avvio, se è presente la chiave come query string allora viene subito memorizzata nel localstorage
    var serverIp = getUrlParameter('serverIp');
    var webApi = getUrlParameter('webApiPort');
    if (serverIp) {
        $scope.serverIP = serverIp;
        $scope.webApi = webApi;

        $.cookie("WebApiC", "http://" + $scope.serverIP + ":" + $scope.webApi + "/api", { expires: 70000 });
        window.location.href = "login.html";
    }
    // --------------------------------------------------------------------------------------------------------------


    $scope.confirm = function () {
        $.cookie("WebApiC", "http://" + $scope.serverIP + ":" + $scope.webApi + "/api", { expires: 70000 });
        window.location.href = "login.html";
    };
}]);


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

