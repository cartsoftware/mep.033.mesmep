
jQuery(window).load(function() {
   
   // Page Preloader --> set delay a 350 ms
   //jQuery('#status').fadeOut(); //Simone B. 09/05/2014 Delete
   jQuery('#preloader').delay(350).fadeOut(function(){
      jQuery('body').delay(350).css({'overflow':'visible'});
   });
});

jQuery(document).ready(function () {   

    
   // Toggle Left Menu
   jQuery('.leftpanel .nav-parent > a').live('click', function() {
      
      var parent = jQuery(this).parent();
      var sub = parent.find('> ul');
      
      // Dropdown works only when leftpanel is not collapsed
      if(!jQuery('body').hasClass('leftpanel-collapsed')) {
         if(sub.is(':visible')) {
            sub.slideUp(200, function(){
               parent.removeClass('nav-active');
               jQuery('.mainpanel').css({height: ''});
               adjustmainpanelheight();
            });
         } else {
            closeVisibleSubMenu();
            parent.addClass('nav-active');
            sub.slideDown(200, function(){
               adjustmainpanelheight();
            });
         }
      }
      return false;
   });
   
   function closeVisibleSubMenu() {
      jQuery('.leftpanel .nav-parent').each(function() {
         var t = jQuery(this);
         if(t.hasClass('nav-active')) {
            t.find('> ul').slideUp(200, function(){
               t.removeClass('nav-active');
            });
         }
      });
   }
   
   function adjustmainpanelheight() {
      // Adjust mainpanel height
      var docHeight = jQuery(document).height();
      if(docHeight > jQuery('.mainpanel').height())
         jQuery('.mainpanel').height(docHeight);
   }
   adjustmainpanelheight();
   
   
   // Tooltip
   jQuery('.tooltips').tooltip({ container: 'body'});
   
   // Popover
   jQuery('.popovers').popover();
   
   // Close Button in Panels
   jQuery('.panel .panel-close').click(function(){
      jQuery(this).closest('.panel').fadeOut(200);
      return false;
   });

    //Simone B. 22.05.2014 : Toogle sul heading di un panello
    //Simone B. 28.05.2014 : Toogle corretto al primo click
   jQuery('.panel-heading-toggle').click(function () {
       
       var t = jQuery(this);
       var p = t.closest('.panel');
       if (!jQuery(this).hasClass('maximize')) {
           
           p.find('.panel-body, .panel-footer').slideDown(200);
           t.addClass('maximize');
          
       } else {
         
           p.find('.panel-body, .panel-footer').slideUp(200);
           t.removeClass('maximize');
          
       }
       return false;
   });

   
   
       

    //Simone B. 22.05.2014 : Al load tutti i pannelli vengono chiusi
    //Simone B. 28.05.2014 : Rimodificata funzione
    //Simone B. 29.09.2014 : Aggiunta funzione dentro la toggle perch� senza di essa non faceva vedere il body del panello (es. in modifica operatore e nuovo operatore)
   //jQuery('.panel-body').toggle(function () {
   //    if (jQuery('.panel-body')[0].style.display == 'none') {
   //        jQuery('.panel-body').show();
   //     }
   //    else {          
   //        jQuery('.panel-body').hide();
   //    }

       
   // });// Simone B. 29/09/2014 Da problemi in IE e Firefox (non si vede il corpo del panello in modifica operatore/nuovo operatore)     
   
   
   jQuery('.open-body-anagrafica').toggle(function () {

       if (jQuery('.open-body-anagrafica')[0].style.display == 'none') {
           jQuery('.open-body-anagrafica').slideDown(200);
           jQuery('.open-body-anagrafica').removeClass('maximize');
       }
       else {        
           jQuery('.open-body-anagrafica').slideUp(200);
           jQuery('.open-body-anagrafica').addClass('maximize');
       }
   });

   jQuery('.collapse-group-intern-anagrafica').click(function () {
     
       if (jQuery('.open-body-anagrafica')[0].style.display == 'none') {
           jQuery('.open-body-anagrafica').slideDown(200);
           jQuery('.open-body-anagrafica').removeClass('maximize');
       }
       else {
           jQuery('.open-body-anagrafica').slideUp(200);
           jQuery('.open-body-anagrafica').addClass('maximize');
       }
   });


    /*
     * Funzioni per aprire/chiudere i panelli in modifica apparato
     */


   jQuery('.open-body-status-False').toggle(function () {         
       if (jQuery('.open-body-status-False')[0].style.display == 'none') {
           jQuery('.open-body-status-False').slideDown(200);
           jQuery('.open-body-status-False').removeClass('maximize');
       }
       else {         
           jQuery('.open-body-status-False').slideUp(200);
           jQuery('.open-body-status-False').addClass('maximize');
       }
   });


   jQuery('.collapse-group-intern-status-False').click(function () {

       if (jQuery('.open-body-status-False')[0].style.display == 'none') {
           jQuery('.open-body-status-False').slideDown(200);
           jQuery('.open-body-status-False').removeClass('maximize');
       }
       else {
           jQuery('.open-body-status-False').slideUp(200);
           jQuery('.open-body-status-False').addClass('maximize');
       }
   });

   jQuery('.open-body-status-True').toggle(function () {      
       
       // se abbiamo un gruppo di panelli dispari 
       if (jQuery('.open-body-status-True').find('.panel-body').length % 2 == 1) {
           
           jQuery('.open-body-status-True').slideDown(0);
           jQuery('.open-body-status-True').removeClass('maximize');         
           jQuery('.open-body-status-True .panel-body').slideUp(0);
           jQuery('.open-body-status-True .panel-body').addClass('maximize');
          
       }
       else 
       {         
           jQuery('.open-body-status-True .panel-body').slideUp(0);
           jQuery('.open-body-status-True .panel-body').addClass('maximize');
           if (jQuery('.open-body-status-True')[0].style.display == 'none') {
               jQuery('.open-body-status-True').slideDown(200);
               jQuery('.open-body-status-True').removeClass('maximize');
           }
           else {
               jQuery('.open-body-status-True').slideUp(200);
               jQuery('.open-body-status-True').addClass('maximize');
           }
       }
   });

   jQuery('.collapse-group-intern-status-True').click(function () {

       if (jQuery('.open-body-status-True')[0].style.display == 'none') {
           jQuery('.open-body-status-True').slideDown(200);
           jQuery('.open-body-status-True').removeClass('maximize');
       }
       else {
           jQuery('.open-body-status-True').slideUp(200);
           jQuery('.open-body-status-True').addClass('maximize');
       }
   });



   

   // Minimize Button in Panels
   jQuery('.minimize').click(function(){
      var t = jQuery(this);
      var p = t.closest('.panel');
      if(!jQuery(this).hasClass('maximize')) {
          p.find('.panel-body, .panel-footer').slideUp(200);
         t.addClass('maximize');
         t.html('&plus;');
      } else {
          p.find('.panel-body, .panel-footer').slideDown(200);
         t.removeClass('maximize');
         t.html('&minus;');
      }
      return false;
   });
   
   
   // Add class everytime a mouse pointer hover over it
   jQuery('.nav-bracket > li').hover(function(){
      jQuery(this).addClass('nav-hover');
   }, function(){
      jQuery(this).removeClass('nav-hover');
   });
   
   
   // Menu Toggle
   jQuery('.menutoggle').click(function(){
      
      var body = jQuery('body');
      var bodypos = body.css('position');
      
      if(bodypos != 'relative') {
         
         if(!body.hasClass('leftpanel-collapsed')) {
            body.addClass('leftpanel-collapsed');
            jQuery('.nav-bracket ul').attr('style','');
            
            jQuery(this).addClass('menu-collapsed');
            
         } else {
            body.removeClass('leftpanel-collapsed chat-view');
            jQuery('.nav-bracket li.active ul').css({display: 'block'});
            
            jQuery(this).removeClass('menu-collapsed');
            
         }
      } else {
         
         if(body.hasClass('leftpanel-show'))
            body.removeClass('leftpanel-show');
         else
            body.addClass('leftpanel-show');
         
         adjustmainpanelheight();         
      }

   });
   
   // Chat View
   jQuery('#chatview').click(function(){
      
      var body = jQuery('body');
      var bodypos = body.css('position');
      
      if(bodypos != 'relative') {
         
         if(!body.hasClass('chat-view')) {
            body.addClass('leftpanel-collapsed chat-view');
            jQuery('.nav-bracket ul').attr('style','');
            
         } else {
            
            body.removeClass('chat-view');
            
            if(!jQuery('.menutoggle').hasClass('menu-collapsed')) {
               jQuery('body').removeClass('leftpanel-collapsed');
               jQuery('.nav-bracket li.active ul').css({display: 'block'});
            } else {
               
            }
         }
         
      } else {
         
         if(!body.hasClass('chat-relative-view')) {
            
            body.addClass('chat-relative-view');
            body.css({left: ''});
         
         } else {
            body.removeClass('chat-relative-view');   
         }
      }
      
   });
   
   reposition_topnav();
   reposition_searchform();
   
   jQuery(window).resize(function(){
      
      if(jQuery('body').css('position') == 'relative') {

         jQuery('body').removeClass('leftpanel-collapsed chat-view');
         
      } else {
         
         jQuery('body').removeClass('chat-relative-view');         
         jQuery('body').css({left: '', marginRight: ''});
      }
      
      reposition_searchform();
      reposition_topnav();
      
       

   });
   
   
   
   /* This function will reposition search form to the left panel when viewed
    * in screens smaller than 767px and will return to top when viewed higher
    * than 767px
    */ 
   function reposition_searchform() {
      if(jQuery('.searchform').css('position') == 'relative') {
         jQuery('.searchform').insertBefore('.leftpanelinner .userlogged');
      } else {
         jQuery('.searchform').insertBefore('.header-right');
      }
   }
   
   

   /* This function allows top navigation menu to move to left navigation menu
    * when viewed in screens lower than 1024px and will move it back when viewed
    * higher than 1024px
    */
   function reposition_topnav() {
      if(jQuery('.nav-horizontal').length > 0) {
         
         // top navigation move to left nav
         // .nav-horizontal will set position to relative when viewed in screen below 1024
         if(jQuery('.nav-horizontal').css('position') == 'relative') {
                                  
            if(jQuery('.leftpanel .nav-bracket').length == 2) {
                jQuery('.nav-horizontal').insertAfter('.nav-bracket:eq(1)');
            } else {
               // only add to bottom if .nav-horizontal is not yet in the left panel
               if(jQuery('.leftpanel .nav-horizontal').length == 0){
                   jQuery('.nav-horizontal').appendTo('.leftpanelinner');//.leftpanelinner           
               }
            }
            
            jQuery('.nav-horizontal').css({display: 'block'})
                                  .addClass('nav-pills nav-stacked nav-bracket');
            
            jQuery('.nav-horizontal .children').removeClass('dropdown-menu');
            jQuery('.nav-horizontal > li').each(function() { 
               
               jQuery(this).removeClass('open');
               jQuery(this).find('a').removeAttr('class');
               jQuery(this).find('a').removeAttr('data-toggle');
               
            });
            
             //Simone B. 23.05.2014 : Commentate queste righe per il motivo che i children di una voce di menu non si aprivano.

            //if(jQuery('.nav-horizontal li:last-child').has('form')) {
            //   jQuery('.nav-horizontal li:last-child form').addClass('searchform').appendTo('.topnav');
            //   jQuery('.nav-horizontal li:last-child').hide();
            //}
         
         } else {
            // move nav only when .nav-horizontal is currently from leftpanel
            // that is viewed from screen size above 1024
            if(jQuery('.leftpanel .nav-horizontal').length > 0) {
               
               jQuery('.nav-horizontal').removeClass('nav-pills nav-stacked nav-bracket')
                                        .appendTo('.topnav');
               jQuery('.nav-horizontal .children').addClass('dropdown-menu').removeAttr('style');
               jQuery('.nav-horizontal li:last-child').show();
               jQuery('.searchform').removeClass('searchform').appendTo('.nav-horizontal li:last-child .dropdown-menu');
               jQuery('.nav-horizontal > li > a').each(function() {
                  
                  jQuery(this).parent().removeClass('nav-active');
                  
                  if(jQuery(this).parent().find('.dropdown-menu').length > 0) {
                     jQuery(this).attr('class','dropdown-toggle');
                     jQuery(this).attr('data-toggle','dropdown');  
                  }
                  
               });              
            }
            
         }
         
      }
   }
   
   // Check if leftpanel is collapsed
   if(jQuery('body').hasClass('leftpanel-collapsed'))
      jQuery('.nav-bracket .children').css({display: ''});
      
     
   // Handles form inside of dropdown 
   jQuery('.dropdown-menu').find('form').click(function (e) {
      e.stopPropagation();
    });
      

    // Simone B. 28.05.2014 
    // Questa funzione permette di rendere cliccabile un intera riga
    // di una tabella
   function change_row(url) {
       if ($('.contentpanel').width() < 680) {
           
           $(".riga-modifica-operatore,.riga-modifica-apparato").css("cursor", "pointer");

           jQuery('.riga-modifica-operatore').click(function () {
               location.href = url;
           });

           jQuery('.riga-modifica-apparato').click(function () {
               location.href = url;
           });


       }
   }


});