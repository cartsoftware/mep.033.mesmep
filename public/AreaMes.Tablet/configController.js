﻿var app = angular.module("appCfg", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl'])




app.controller('cfgCtrl', ['$scope', '$rootScope', '$cookies', function ($scope, $rootScope, $cookies) {


    $scope.serverIP = "localhost";
    $scope.signalR = "9001";
    $scope.webApi = "9000";
    $scope.stationId = $.cookie("StationId");
    $scope.loginConRfid = $.cookie("loginConRfid");

    // --------------------------------------------------------------------------------------------------------------
    // In fase di avvio, se è presente la chiave come query string allora viene subito memorizzata nel localstorage
    var serverIp = getUrlParameter('serverIp');
    var webApi = getUrlParameter('webApiPort');
    var signalRPort = getUrlParameter('signalRPort');

    if (serverIp) {
        $scope.serverIP = serverIp;
        $scope.webApi = webApi;
        $scope.signalRPort = signalRPort;
        $scope.stationId = 99;
        $scope.loginConRfid = false;

        $.cookie("SigalRc", "http://" + $scope.serverIP + ":" + $scope.signalR, { expires: 70000 });
        $.cookie("WebApiC", "http://" + $scope.serverIP + ":" + $scope.webApi + "/api", { expires: 70000 });
        $.cookie("StationId", $scope.stationId, { expires: 70000 });
        $.cookie("LoginConRfid", $scope.loginConRfid, { expires: 70000 });
        window.location.href = "login.html";
    }
    // --------------------------------------------------------------------------------------------------------------

    $scope.confirm = function () {
        $.cookie("SigalRc", "http://" + $scope.serverIP + ":" + $scope.signalR, { expires: 70000 });
        $.cookie("WebApiC", "http://" + $scope.serverIP + ":" + $scope.webApi + "/api", { expires: 70000 });
        $.cookie("StationId", $scope.stationId, { expires: 70000 });
        $.cookie("LoginConRfid", $scope.loginConRfid, { expires: 70000 });
        window.location.href = "login.html";

    };
}]);


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

