﻿// 2a dichiarazione del caricamento dei moduli
var app = angular.module("AppMacchineRuntime", ['datatables', 'angular.chosen', 'ngCookies', 'treeControl', 'pascalprecht.translate'])
.run(function ($rootScope, $location, $cookies, $http, $window) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookies.getObject('MesAuth');
    if ($rootScope.globals!= null) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        
    }
    var x = $window.location.href;
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in
        if (x.indexOf('/login') == -1 && !$rootScope.globals) {
            $window.location.href = 'login.html';
        }
    });
});

// ------------------------------------------------------------------------
// definizione generale
// ------------------------------------------------------------------------
app.controller('menuCtrl', [
    '$scope', '$http', '$compile',  '$window',
    function ($scope, $http, $compile, $window) {
        $scope.appName = "AreaMES";
        $scope.pagina = $window.location.href;
    }]);


app.constant('Enums', {
    ViewState: {
        R: 'R',
        C: 'C',
        U: 'U',
        D: 'D',
        RO: 'RO'
    }
});



app.config(['$httpProvider', function ($httpProvider) {
    //initialize get headers
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    //disable IE ajax request caching
    //$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    //$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);


//app.directive('prettyp', function(){
//    return function(scope, element, attrs){
//        $("[rel^='prettyPhoto']").prettyPhoto({
//            deeplinking: false,
//            theme: 'dark_rounded',
//            overlay_gallery: false,
//            social_tools: false,
//        });
//    }
//})