﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('SfoDetailCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope) {
    
    $rootScope.signalRconnection = $.cookie("SigalRc");
    urlApi = $.cookie("WebApiC");

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };
    $scope.currentItem = null;
    basicCrud_doInitWatch($scope);

    $scope.batch = {};

    $scope.btnStateConcludi = {};
    $scope.btnStateStart = {};
    $scope.btnStatePause = {};
    $scope.btnStateAbort = {};

    $scope.stateStarted = {};
    $scope.stateFinished = {};
    $scope.stateAborted = {};

    $scope.userLocked = {};

    $scope.stateBatchStart = {};
    $scope.stateBatchPausaS = {};
    $scope.stateBatchEmergenza = {};
    $scope.stateBatchStopProduzione = {};

    $scope.idMacchina = null;
   
    DataFactory.GetStatiBatch().then(function (response) {
        $scope.ListaStatiBatch = response.data;
    });

    // caricamento fasi
    $scope.loadFasi = function () {
        $scope.items = [];
        $scope.idMacchina = $location.search().idMacchina;
        DataFactory.GetBatchFromId($location.search().idBatch).then(function (response) {
            $scope.batch = response.data;
            // abilitazione pulsanti per il batch
            $scope.stateBatchStart = ($scope.batch.stato != "inLavorazione");
            $scope.stateBatchPausaS = ($scope.batch.stato != "inLavorazioneInPausaS") && ($scope.batch.stato != "inLavorazioneInEmergenza");
            $scope.stateBatchEmergenza = ($scope.batch.stato != "inLavorazioneInEmergenza");
            $scope.stateBatchStopProduzione = ($scope.batch.stato != "terminato");

            DataFactory.GetFasiFromBatch($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (response) {
                $scope.items.push.apply($scope.items, response.data);

                angular.forEach($scope.items, function (fase, key) {
                    // mostro il lucchetto se ho un utente diverso da quello dell'avvio
                    $scope.userLocked[fase.numeroFase] = (fase.userLocked != $rootScope.globals.currentUser.username && fase.userLocked != null) && (fase.stato != "inAttesa");
                    // se la fase è cominciata mostro il tempo di start
                    $scope.stateStarted[fase.numeroFase] = (fase.stato != "inAttesa");
                    // se la fase è terminata non faccio vedere più i pulsanti ma mostro un'altra finestra
                    $scope.stateFinished[fase.numeroFase] = (fase.stato == "terminato");
                    // se la fase è abortita non faccio vedere più i pulsanti ma mostro un'altra finestra
                    $scope.stateAborted[fase.numeroFase] = (fase.stato == "abortito");
                    // controlli su abilitazione pulsanti
                    $scope.btnStateConcludi[fase.numeroFase] = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateStart[fase.numeroFase] = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazione") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStatePause[fase.numeroFase] = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazioneInPausa") && (fase.stato != "inAttesa") && (fase.stato != "abortito") && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateAbort[fase.numeroFase] = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "abortito");
                });
            });
        });
        
        
    }

    $scope.loadFasi();

    // funzione per cambiare lo stato della fase
    $scope.changeState = function (item, idStato) {
        $scope.currentItem = item;
        $scope.currentItem.stato = idStato;
        $scope.currentItem.userLocked = $rootScope.globals.currentUser.username;

        var data = {
            idBatch: $location.search().idBatch,
            fase: $scope.currentItem
        }

        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateFase, data).success(function (data) {
            $scope.loadFasi();
        })
        .error(function (e) {
            console.log(e);
        });;
    }

    // funzione per cambiare lo stato del batch
    $scope.changeStateBatch = function (idStato)
    {
        $scope.batch.stato = idStato;

        var data = {
            idBatch :  $scope.batch.id,
            idStato : idStato
        }
        
        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateBatch, data).success(function (data) {
            $scope.loadFasi();
        })
        .error(function (e) {
            console.log(e);
        });
    }
}]);
