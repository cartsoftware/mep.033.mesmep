﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('AnagraficaMacchineCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory) {

    $rootScope.signalRconnection = $.cookie("SigalRc");
    urlApi = $.cookie("WebApiC");

    $scope.items = [];
    $scope.currentItem = null;   


    $scope.showItemDetail = function (item) {

        basicCrud_doShowItemDetail($scope, $http, subUrlApi_Macchina, Enums, item);
    };

    $scope.reload = function () { basicCrud_doReload($scope, $http, subUrlApi_Macchina); };

    $scope.confirm = function () {
        basicCrud_doConfirm($scope, $http, subUrlApi_Macchina, Enums);
    };

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };

    basicCrud_doInitWatch($scope);

    $scope.reload();

}]);
