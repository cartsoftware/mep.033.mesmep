﻿// --------------------------------------------------------------------------------------------------------------------------------
// Main controller
// --------------------------------------------------------------------------------------------------------------------------------
app.controller('AndonDetailCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope',
function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope) {

    $rootScope.signalRconnection = $.cookie("SigalRc");
    urlApi = $.cookie("WebApiC");

    $scope.title = function () { basicCrud_doTitle($scope, Enums); };
    $scope.currentItem = null;
    basicCrud_doInitWatch($scope);

    $scope.batch = {};

    $scope.faseAttuale = {};

    $scope.taktTotale = {};
    $scope.perditaTotale = {};

    $scope.btnStateConcludi = {};
    $scope.btnStateStart = {};
    $scope.btnStatePause = {};
    $scope.btnStateAbort = {};

    $scope.stateStarted = {};
    $scope.stateFinished = {};
    $scope.stateAborted = {};

    $scope.userLocked = {};

    $scope.stateBatchStart = {};
    $scope.stateBatchPausaS = {};
    $scope.stateBatchEmergenza = {};

    $scope.idMacchina = null;

    // caricamento
    $scope.loadPage = function () {
        $scope.items = [];
        $scope.idMacchina = $location.search().idMacchina;
        DataFactory.GetBatchFromId($location.search().idBatch).then(function (response) {
            $scope.batch = response.data;
            $scope.perditaTotale = convertime($scope.batch.tempoTotaleFermi);
            angular.forEach($scope.batch.fasi, function (fase, key) {
                if (fase.numeroFase == $location.search().numeroFase) {
                    $scope.faseAttuale = fase;
                    $scope.taktTotale = convertime(fase.taktTotale);
                    // controlli su abilitazione pulsanti
                    $scope.btnStateConcludi = (fase.userLocked == $rootScope.globals.currentUser.username) && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateStart = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazione") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStatePause = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazioneInPausa") && (fase.stato != "inAttesa") && (fase.stato != "abortito") && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateAbort = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "abortito");
                }
            });

            $scope.stateBatchStart = ($scope.batch.stato != "inLavorazione");
            $scope.stateBatchPausaS = ($scope.batch.stato != "inLavorazioneInPausaS") && ($scope.batch.stato != "inLavorazioneInEmergenza");
            $scope.stateBatchEmergenza = ($scope.batch.stato != "inLavorazioneInEmergenza");
        });
    }

    $scope.loadPage();

    // funzione per aumentare il numero dei pezzi
    $scope.addPieces = function () {

        var data = {
            idBatch : $scope.batch.id
        }

        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_AddPiecesToBatch, $scope.batch).success(function (data) {
            $scope.loadPage();
        })
        .error(function (e) {
            console.log(e);
        });;


    }

    // funzione per cambiare lo stato della fase
    $scope.changeState = function (idStato) {
        $scope.faseAttuale.stato = idStato;
        $scope.faseAttuale.userLocked = $rootScope.globals.currentUser.username;

        var data = {
            idBatch: $location.search().idBatch,
            fase: $scope.faseAttuale
        }

        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateFase, data).success(function (data) {
            $scope.loadPage();
        })
        .error(function (e) {
            console.log(e);
        });;
    }

    // funzione per cambiare lo stato del batch
    $scope.changeStateBatch = function (idStato) {
        $scope.batch.stato = idStato;

        var data = {
            idBatch: $scope.batch.id,
            idStato: idStato
        }

        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateBatch, data).success(function (data) {
            $scope.loadPage();
        })
        .error(function (e) {
            console.log(e);
        });
    }

    function convertime (seconds) {
        
        // multiply by 1000 because Date() requires miliseconds
        var date = new Date(seconds * 1000);
        var hh = date.getUTCHours();
        var mm = date.getUTCMinutes();
        var ss = date.getSeconds();
        // If you were building a timestamp instead of a duration, you would uncomment the following line to get 12-hour (not 24) time
        // if (hh > 12) {hh = hh % 12;}
        // These lines ensure you have two-digits
        if (hh < 10) { hh = "0" + hh; }
        if (mm < 10) { mm = "0" + mm; }
        if (ss < 10) { ss = "0" + ss; }
        // This formats your string to HH:MM:SS
        var t = hh + ":" + mm + ":" + ss;
        return t;
    }
}]);