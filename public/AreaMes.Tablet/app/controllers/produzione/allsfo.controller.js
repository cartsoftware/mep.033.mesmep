﻿app.controller('AllSfoCtrl', ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope', '$window', "$translate", 
    function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope, $window, $translate) {

    $rootScope.signalRconnection = $.cookie("SigalRc");
    urlApi = $.cookie("WebApiC");
    $translate.use($scope.$parent.globals.currentUser.lingua);
   
    $scope.title = function () { basicCrud_doTitle($scope, Enums); };
    $scope.filterIdStato = "-1";
    $scope.lUser = $rootScope.globals.currentUser.username;

    basicCrud_doInitWatch($scope);
    $scope.items = [];
    $scope.idMacchina = null;
    $scope.idMachine = -1;

        $scope.arrayEliminare = [];

    DataFactory.GetStatiBatch().then(function (response) {
        $scope.ListaStatiBatch = response.data;
        });

    DataFactory.getMacchine().then(function (response) {
        $scope.Macchine = response.data;
     });

    $scope.filterState = function (idStato, loggedUser, idmacchinaa) {
        //$rootScope.idStatoPag = idStato;
        if (idmacchinaa != null) $scope.idMachine = idmacchinaa;
        //else $scope.idMachine = -1;

        $scope.idMacchina = $location.search().idMacchina;
        $scope.filterIdStato = idStato;
        DataFactory.GetSfo(idStato, loggedUser).then(function (response) {

            // se l'oggetto è presente nell'array locale, 
            // ma non in quello ricevuto dal server, allora viene cancellato
            angular.forEach($scope.items, function (value, key) {
                var itemFounded = response.data.find(o => o.id == value.id);
                //if (!itemFounded)
                //    $scope.items.splice(list.indexOf(itemFounded), 1);
                if (itemFounded == null) {
                    $scope.items = [];
                }
            });

            // vengono ciclati gli elementi prelevati dal server
            angular.forEach(response.data, function (value, key) {
                var itemFounded = $scope.items.find(o => o.id == value.id);

                angular.forEach(value.fasi, function (value1, key1) {
                    if (value1.numeroFase == "1") $scope.FaseSet = value1.stato;
                    else $scope.Faselav = value1.stato;
                });

                if (!itemFounded) {

                    // se l'elemento non è presente viene inserito
                    if ($scope.idMachine == '-1' || $scope.idMachine == null) {
                        value.FaseLav = $scope.Faselav;
                        value.FaseSet = $scope.FaseSet;
                        value.lunghezza = value.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                        $scope.items.push(value);

                    }
                       
                    else {
                        if (value.fasi[0].macchine[0].macchina.id == $scope.idMachine) {
                            value.FaseLav = $scope.Faselav;
                            value.FaseSet = $scope.FaseSet;
                            value.lunghezza = value.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                            $scope.items.push(value);
                        }
                           
                    }
                   
                } else {
                    if ($scope.idMachine == '-1' || $scope.idMachine == null || itemFounded.fasi[0].macchine[0].macchina.id == $scope.idMachine) {
                        // se l'elemento è presente vengono aggiornate le sue proprietà
                        itemFounded.fasi = value.fasi;
                        itemFounded.stato = value.stato;
                        itemFounded.pezziProdotti = value.pezziProdotti;
                        itemFounded.oee = value.oee;
                        itemFounded.userLockedDate = value.userLockedDate;
                        itemFounded.inizio = value.inizio;
                        itemFounded.percCompletamento = value.percCompletamento;
                        itemFounded.FaseLav = $scope.Faselav;
                        itemFounded.FaseSet = $scope.FaseSet;
                    }
                    else $scope.arrayEliminare.push(value);
                   
                }
            });

            //ciclo per eliminare quelli che non mi servono
            angular.forEach($scope.arrayEliminare, function (value, key) {

                angular.forEach($scope.items, function (value1, key1) {

                    if (value.id == $scope.items[key1].id)
                        $scope.items.splice(key1, 1);

                });
            });
            $scope.arrayEliminare = [];

        });

        $scope.items = $scope.items.sort((a, b) => (a.odP > b.odP ? 1 : -1));
    
    }

    $scope.filterState($scope.filterIdStato, $scope.lUser)

    $scope.getBatchTime = function (idBatch, tType) {
        var t = 0;
        angular.forEach($scope.items, function(item, key){
            if( item.id==idBatch){            
            //Calcolo tempi teorici distinta base
            angular.forEach(item.distintaBase.fasi, function (fase, key) {
                t += fase.manodopera[0].tempo;
                t += fase.macchine[0].tempo;
            });
        }
        });
        var date = new Date(null);
        date.setSeconds(t);
        return date.toISOString().substr(11, 8);
    }

        $scope.linkToBatch = function (id) {
            var path = "sfofasi.html#?idBatch=" + id;
            $window.location.href = path;
        }

    var timer = setInterval(function () {
        $scope.filterState($scope.filterIdStato, $scope.lUser);
    }, 10*1000);

    setTimeout(function () {
        $scope.filterState($scope.filterIdStato, $scope.lUser);
        }, 2 * 1000);
   

}]);
