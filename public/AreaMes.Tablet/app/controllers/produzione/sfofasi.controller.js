﻿app.controller('SfoFasiCtrl',
    ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Enums', '$compile', 'DataFactory', '$location', '$rootScope', '$timeout', "$translate",
        function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Enums, $compile, DataFactory, $location, $rootScope, $timeout, $translate,growl ) {

            var connection;
            var hub;
            var timerFocus;
            var barcode = "";
            var boolean2 = false;
           
            $rootScope.signalRconnection = $.cookie("SigalRc");
            urlApi = $.cookie("WebApiC");
            stationId = $.cookie("StationId");
            $scope.nclick = 1;
            $translate.use($scope.$parent.globals.currentUser.lingua);
            $scope.sidebar2Show = false;
            $scope.sidebarIsCollapsed = false;

            $scope.clickBatchFaseMateriale = function (arg) {
                var ck = arg;   // per futuri utilizzi //
            }

            $scope.materialiPerFase = [];
            $scope.multimediaPerFase = [];
            $scope.selectedPhase = null;
            $scope.selectedBatchPhase = null;

            $scope.btnStateConcludi = {};
            $scope.btnStateStart = {};
            $scope.btnStatePause = {};
            $scope.btnStateAbort = {};

            $scope.stateStarted = {};
            $scope.stateFinished = {};
            $scope.stateAborted = {};

            $scope.userLocked = {};

            $scope.stateBatchStart = {};
            $scope.stateBatchPausaS = {};
            $scope.stateBatchEmergenza = {};
            $scope.stateBatchStopProduzione = {};
            $scope.stateBatchInEmergenza = {};

            $scope.caricoPezziVersati = 0;
            $scope.isLoaderActiveWrite = false;
            $scope.isLoaderActiveRead = false;

            $scope.isVisibleButtonStartStop = false;
            $scope.isVisibleButtonEmergency = false;
            $scope.isVisibleProgressionPerc = false;
            $scope.modalWarningMessage = '';

            $scope.numpadMinus = function () {
                $scope.numpadClose('-1');
            }
            $scope.numpadPlus = function () {
                $scope.numpadClose('1');
            }

            $scope.numpadClose = function (param) {
                var data = {
                    idBatch: $scope.batch.id,
                    pezziVersati: param,
                    numeroFase: $scope.selectedBatchPhase.numeroFase,
                    Username: $rootScope.globals.currentUser.username
                }
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_AddPiecesToBatch, data).success(function (data) {
                })
                    .error(function (e) {
                        console.log(e);
                        $scope.onError(e);
                    });
            }



            connection = $.hubConnection($rootScope.signalRconnection);
            connection.logging = false;

            hub = connection.createHubProxy('AreaMESManager');
            hub.logging = false;



            hub.on('OnFaseChanged', function (batchId, message) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch
                    var inizio = message.Current.Inizio;
                    var numFase = message.Current.NumeroFase;
                    var stato = message.Current.Stato;
                    var statoDecoded = message.Current.Nota;

                    $scope.selectedPhase = null;
                    //if ($scope.selectedPhase.numeroFase != numFase)
                    angular.forEach($scope.currentItem.distintaBase.fasi, function (fase, key) {
                        if (fase.numeroFase == numFase) $scope.selectedPhase = fase;    //$scope.selectedPhase = fase;
                    })
                    $scope.loadFasiAlt();
                }
            });


            // registrazione delle variazioni del batch
            hub.on('OnBatchChanged', function (batchId, message) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch
                    $scope.refreshBatch();
                }
            });


            // registrazione delle variazioni sui materiali
            hub.on('OnBatchMaterialeChanged', function (batchId, response) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch

                    if ($scope.currentItem.batchMateriali == null) return;
                    angular.forEach(response, function (value, key) {
                        var itemFounded = $scope.currentItem.batchMateriali.find(o => (o.barcode == value.barcode) && (o.numeroFase == value.numeroFase));
                        if (!itemFounded)
                            $scope.currentItem.batchMateriali.push(value);
                        else
                            itemFounded = value; 
                    });

                    $scope.materialiPerFase = $scope.getMaterialiPerFase();
                    $scope.loadFasiAlt();
                }
            });



            //NotifyVersamentoFaseChanged -> OnVersamentoFaseChanged
            hub.on('OnVersamentoFaseChanged', function (batchId, message) {
                if (batchId == $scope.currentItem.id) {     //ignora messaggi per altri batch
                    $scope.loadFasiAlt();
                }
            });




            // Viene intercettato l'evento di cambio stato in SignalR
            $(connection).bind("onStateChanged", function (e, data) {
                if (data.newState === $.signalR.connectionState.connected) {
                    doSubscribe($location.search().idBatch);
                }
                else if (data.newState === $.signalR.connectionState.disconnected) {
                    setTimeout(function () { connection.start(); }, 5 * 1000); // Restart connection after 5 seconds.
                }
            });

            connection.start();

            // ----------------------------------------------------------------------

            $scope.checkIfIncompleteBatch = function () {
                var isIncomplete = false;
                angular.forEach($scope.filteredFasi, function (fase, key) {
                    if (fase.stato != 'terminato') {
                        isIncomplete = true;;
                    }
                })
                return isIncomplete;
            }


            $scope.getMaterialiPerFase = function () {
                var ret = [];
                var nFase = $scope.selectedBatchPhase.numeroFase;
                angular.forEach($scope.currentItem.batchMateriali, function (mat, key) {
                    if ((mat.numeroFase == null) || ((mat.numeroFase != null && mat.numeroFase === nFase))) {
                        mat.isOnAction = false;
                        ret.push(mat);
                    }

                })
                return ret.reverse();
            }

            $scope.getMultimediaPerFase = function () {
                var ret = [];
                var nFase = $scope.selectedBatchPhase.numeroFase;
                angular.forEach($scope.currentItem.batchMultimedia, function (mat, key) {
                    if (mat.numeroFase != null && mat.numeroFase == nFase)
                        ret.push(mat);
                })
                angular.forEach($scope.currentItem.batchMultimedia, function (mat, key) {
                    if (mat.numeroFase == null)
                        ret.push(mat);
                })
                return ret;
            }

            //
            $scope.updateProgressBar = function (val) { val = $scope.currentItem.percCompletamento; }

            //
            $scope.loadFasiAlt = function () {
                $scope.items = [];
                $scope.idMacchina = $location.search().idBatch;

                DataFactory.mGetBatchFromId($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (response) {

                    DataFactory.GetFasiFromBatch($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (responseSub) {
                        $scope.filteredFasi = responseSub.data;

                        $scope.batch = response.data;
                        $scope.currentItem = $scope.batch;
                        // abilitazione pulsanti per il batch
                        var st = $scope.batch.stato;
                        $scope.stateBatchStart = (st == "inAttesa" || st == "inLavorazioneInEmergenza" || st == "inLavorazioneInPausaS");
                        $scope.stateBatchPausaS = (st == "inLavorazione");
                        $scope.stateBatchEmergenza = (st == "inLavorazione");
                        $scope.stateBatchStopProduzione = (st == "inLavorazione" || st == "inLavorazioneInEmergenza");
                        $scope.stateBatchInEmergenza = ($scope.batch.stato == "inLavorazioneInEmergenza");
                        $scope.batch.lunghezza = $scope.batch.distintaBase.distintaBaseParametri.find(x => x.codice == "MEP_SETUP_LUNGH").valoreDefault;
                        createOrRefreshTreeView();
                        $scope.updateProgressBar(($scope.currentItem.pezziProdotti / $scope.currentItem.pezzidaProdurre * 100).toFixed(2));

                        // -------------------------------------------------------------------------------------------------
                        // viene selezionata la prima fase in lavorazione, oppure, la prima in elenco
                        try {
                            var sel = null;
                            angular.forEach($scope.currentItem.distintaBase.fasi, function (bfase, key) {
                                if (sel == null) {
                                    if ($scope.selectedPhase != null) {
                                        if ($scope.selectedPhase.numeroFase == bfase.numeroFase)
                                            sel = bfase;
                                    } else if (bfase.stato == 'inLavorazione')
                                        sel = bfase;
                                }
                            });
                            if (sel == null) sel = $scope.currentItem.distintaBase.fasi[0];

                            $scope.showSelected(sel);

                            contsize();
                        }
                        catch (e) { }
                        // -------------------------------------------------------------------------------------------------
                    })
                });
            }


            //
            $scope.refreshBatch = function () {
                DataFactory.mGetBatchFromId($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (response) {
                    $scope.batch = response.data;
                    $scope.currentItem = $scope.batch;
                 
                    // abilitazione pulsanti per il batch
                    var st = $scope.batch.stato;
                    $scope.stateBatchStart = (st == "inAttesa" || st == "inLavorazioneInEmergenza" || st == "inLavorazioneInPausaS");
                    $scope.stateBatchPausaS = (st == "inLavorazione");
                    $scope.stateBatchEmergenza = (st == "inLavorazione");
                    $scope.stateBatchStopProduzione = (st == "inLavorazione" || st == "inLavorazioneInEmergenza");
                    $scope.stateBatchInEmergenza = ($scope.batch.stato == "inLavorazioneInEmergenza");

                    $scope.updateProgressBar(($scope.currentItem.pezziProdotti / $scope.currentItem.pezzidaProdurre * 100).toFixed(2));

                    var fase = $scope.selectedBatchPhase; // refresh selectedPhase buttons
                    $scope.btnStateConcludi = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza") && (fase.stato != "inAttesa");
                    $scope.btnStateStart = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazione") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStatePause = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazioneInPausa") && (fase.stato != "inAttesa") && (fase.stato != "abortito") && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateAbort = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "abortito");
                });
            }

            //
            $scope.refreshFase = function () {
                var changedId = $scope.selectedBatchPhase.numeroFase;

                DataFactory.mGetBatchFromId($location.search().idBatch, $rootScope.globals.currentUser.username).then(function (response) {
                    $scope.currentItem = response.data;
                    angular.forEach($scope.filteredFasi, function (fase, key) {
                        if (fase.numeroFase == changedId) {
                            $scope.showSelected($scope.selectedPhase);
                        }
                    })
                });
                $scope.updateProgressBar(($scope.currentItem.pezziProdotti / $scope.currentItem.pezzidaProdurre * 100).toFixed(2));
            }

            // opzioni per l'albero
            $scope.treeOptions = {
                nodeChildren: "fasi",
                dirSelectable: true,
                injectClasses: {
                    ul: "a1",
                    li: "a2",
                    liSelected: "a7",
                    iExpanded: "a3",
                    iCollapsed: "a4",
                    iLeaf: "a5",
                    label: "a6",
                    labelSelected: "a8"
                }
            }

            //
            $scope.showSelected = function (sel) {
                if (sel == null) return;

                $scope.shDetail = "";
                // inizializzazione dei models
                //resetAllFields();
                $scope.materialiPerFase = [];
                $scope.multimediaPerFase = [];

                //if (sel.macchine != null || sel.manodopera != null) {
                $scope.isEnabledDettagliFase = sel.abilitaAvanzamenti;
                $scope.isEnabledMateriali = sel.abilitaMateriali;
                $scope.isEnabledMultimedia = sel.abilitaMultimedia;
                $scope.selectedNode = sel;
                $scope.numeroFase = sel.numeroFase;
                $scope.nomeFase = sel.nome;
                $scope.descrizioneFase = sel.descrizione;
                $scope.type = sel.type;
                $scope.isAndon = sel.isAndon;
                $scope.isAutomatica = sel.isAutomatica;
                $scope.sidebar2Show = $scope.selectedNode.isAndon;
                $scope.sidebarIsCollapsed = $scope.selectedNode.isAndon;
                $scope.IsVisibleFaseCiclica = (sel.type == "0");
                $scope.abilitabarcode = sel.abilitabarcode;
                $scope.abilitaLetturaRfid = sel.abilitaLetturaRfid;
                $scope.abilitascritturaRfid = sel.abilitascritturaRfid;
                $scope.abilitaAvanzamentiStar = sel.abilitaAvanzamentiStar;
                $scope.abilitaAvanzamentiStop = sel.abilitaAvanzamentiStop;
                $scope.abilitaAvanzamentiPausa = sel.abilitaAvanzamentiPausa;
                $scope.visibilitastazione = sel.visibilitastazione;

                if ($scope.isEnabledMateriali == false) {
                    $scope.switchDetail(null, 2);
                }


                $scope.set_color = function (id, isEnabledMateriali, toogle) {
                    var change = document.getElementById("b3");
                    var change2 = document.getElementById("b2");

                    if (isEnabledMateriali == false && id == '2' || boolean2 == true) {
                        $scope.nclick++;
                        boolean2 = false;
                        return { "background-color": "#428bca", "color": "#ffffff" };
                    }
                        //&& $scope.nclick>0
                    else if (isEnabledMateriali == true && id == '1' && $scope.nclick > 0) {
                        $scope.nclick = 0;
                        return { "background-color": "#428bca", "color": "#ffffff" };
                    }
                    else {
                        return { "background-color": "#fff", "color": "#333" };
                    }
                };

                angular.forEach($scope.currentItem.fasi, function (fase, key) {
                    if (fase.numeroFase == sel.numeroFase) {    // da aggiornare usando fase.ID (da riportare dalla distinta alla creazione del batch //
                        $scope.selectedPhase = sel;             // FASE DISTINTA BASE
                        $scope.selectedBatchPhase = fase;       // FASE BATCH 
                    }
                });

                // caricamento materiali
                if ($scope.isEnabledMateriali) {
                    $scope.materialiPerFase = $scope.getMaterialiPerFase();
                    $scope.shDetail = "1";
                }

                // caricamento multimedia
                if ($scope.isEnabledMultimedia)
                    $scope.multimediaPerFase = $scope.getMultimediaPerFase();


                angular.forEach($scope.dataForTheTree[0].fasi, function (treefase, key) {
                    if (treefase.numeroFase == $scope.selectedBatchPhase.numeroFase)
                        treefase.statoOperativo = $scope.selectedBatchPhase.stato;
                });

                if (sel.abilitaAvanzamenti == true) {
                    
                    // array per abilitazione macchine e competenze macchine
                    $scope.isEnabledMacchinaSelection = {};
                    $scope.isEnabledcompentenzaMacchineSelection = {};

                    // abilitazione checkbox di collegamento all'operazione ciclo
                    $scope.isEnabledLinkedOption = false;

                    angular.forEach($scope.CausaliFase, function (obj, key) {
                        // caso in cui ho a null tutti i gli oggetti macchine e competenze macchinari
                        //if (item.macchine[obj.id].macchina == null && item.macchine[obj.id].macchinaCompetenza == null)
                        $scope.isEnabledMacchinaSelection[obj.id] = true;
                    });


                    // Calcolo tempi teorici distinta base
                    var t = 0;
                    var tf = 0;
                    angular.forEach($scope.currentItem.distintaBase.fasi, function (fase, key) {
                        t += fase.manodopera[0].tempo;
                        t += fase.macchine[0].tempo;
                        if (fase.numeroFase == sel.numeroFase) {
                            tf += fase.manodopera[0].tempo;
                            tf += fase.macchine[0].tempo;
                        }
                    });
                    var date = new Date(null);
                    var datef = new Date(null);
                    date.setSeconds(t);
                    $scope.tempiTeoriciDistintaBase = date.toISOString().substr(11, 8);
                    datef.setSeconds(tf);
                    $scope.tempiTeoriciDistintaBaseFase = datef.toISOString().substr(11, 8);


                    var fase = $scope.selectedBatchPhase;
                    // mostro il lucchetto se ho un utente diverso da quello dell'avvio
                    $scope.userLocked = (fase.userLocked != $rootScope.globals.currentUser.username && fase.userLocked != null) && (fase.stato != "inAttesa");
                    // se la fase è cominciata mostro il tempo di start
                    $scope.stateStarted = (fase.stato != "inAttesa");
                    // se la fase è terminata non faccio vedere più i pulsanti ma mostro un'altra finestra
                    $scope.stateFinished = (fase.stato == "terminato");
                    // se la fase è abortita non faccio vedere più i pulsanti ma mostro un'altra finestra
                    $scope.stateAborted = (fase.stato == "abortito");
                    // controlli su abilitazione pulsanti
                    $scope.btnStateConcludi = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza") && (fase.stato != "inAttesa");
                    $scope.btnStateStart = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazione") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStatePause = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "inLavorazioneInPausa") && (fase.stato != "inAttesa") && (fase.stato != "abortito") && (fase.stato != "terminato") && ($scope.batch.stato != "inLavorazioneInEmergenza");
                    $scope.btnStateAbort = (fase.userLocked == $rootScope.globals.currentUser.username || fase.userLocked == null) && (fase.stato != "abortito");


                }

                contsize();

            };

            $scope.nodeDoubleClick = function () {
                var defExp = [];
                angular.forEach($scope.currentItem.distintaBase, function (fase, key) {
                    if (fase.numeroFase == sel.numeroFase) {
                        fillItemsInView(fase);
                    }
                });
                $scope.selectedNode.expand();
                //$scope.defaultExpanded = defExp;
            }

            // funzione per cambiare lo stato della fase $scope.selectedBatchPhase
            $scope.changeState = function (idStato) {
                $scope.selectedBatchPhase.stato = idStato;
                $scope.selectedBatchPhase.userLocked = $rootScope.globals.currentUser.username;
                //if ($scope.batch.queueId != null)
                //    angular.forEach($scope.batch.queueId.item.batch, function (batchQueued, key) {

                //        var data = {
                //            idBatch: batchQueued.id,
                //            fase: $scope.selectedBatchPhase,
                //            Username: $rootScope.globals.currentUser.username
                //        }
                //        $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateFase, data).success(function (data) {
                //            var x = data;
                //        })
                //            .error(function (e) {
                //                console.log(e);
                //                $scope.onError(e);
                //            });;

                //    });
                //else {
                    var data = {
                        idBatch: $location.search().idBatch,
                        fase: $scope.selectedBatchPhase,
                        Username: $rootScope.globals.currentUser.username
                    }
                    $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateFase, data).success(function (data) {
                        var x = data;
                    })
                        .error(function (e) {
                            console.log(e);
                            $scope.onError(e);
                        });;
                //}
            }

            $scope.changeStateToStart = function (idStato) {
                var num = $scope.selectedBatchPhase.numeroFase;
                var toBeConfirmed = false
                if (num > 1) {
                    var previousPhase = num - 1;
                    angular.forEach($scope.filteredFasi, function (fase, key) {
                        if (fase.numeroFase == (num - 1) && fase.stato == 'inAttesa' && (fase.macchine != null || fase.manodopera != null)) {
                            toBeConfirmed = true;
                        }
                    });
                }
                if (toBeConfirmed) {
                    var element = angular.element('#notSequentialStartFase');
                    element.modal("show");
                }
                else
                    $scope.changeState(idStato);
            }

            // funzione per cambiare lo stato del batch
            $scope.changeStateBatch = function (idStato) {
                $scope.batch.stato = idStato;

                var eventDate = new Date().toISOString();
                try {
                    eventDate = new Date($scope.dataDiRiavvio).toISOString();
                }
                catch (e) { }

                var data = {
                    idBatch: $scope.batch.id,
                    idStato: idStato,
                    causaleRiavvio: $scope.selectedCausaleDiRiavvio,
                    dataRiavvio: eventDate,
                    notaRiavvio: $scope.notaDiRiavvio,
                    Username: $rootScope.globals.currentUser.username,
                    StationId: stationId
                }

                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SetStateBatch, data)
                    .success(function (data) { })
                    .error(function (e) { console.log(e); $scope.onError(e); });
            }

            $scope.onError = function (err) {
                $scope.modalWarningMessage = err.exceptionMessage;
                angular.element(document.querySelector('#modalWarning')).modal('toggle');
            }


            $scope.shDetail = 0;
            $scope.switchDetail = function (btn, arg) {
                if (arg == 2) {
                    boolean2 = true;
                }
                $scope.shDetail = arg;
            };


            $scope.clickSendBarcode = function (barcode, isRfid, item) {

                $scope.isLoaderActiveWrite = true;
                var data = {
                    IdBatch: $scope.batch.id,
                    IdFase: $scope.selectedBatchPhase.numeroFase,
                    Barcode: barcode,
                    Material: (item != null ? item.materiale : null),
                    IsRfid: isRfid,
                    TimeoutRequest: 30,
                    Username: $rootScope.globals.currentUser.username,
                    StationId: stationId
                }

                // reset dello stato prima della spedizione 
                angular.forEach($scope.materialiPerFase, function (mat, key) { if (mat.barcodeRfid === barcode) { mat.statoIngresso = ''; mat.isOnAction = true; } });

                // chiamata al server per la scrittura
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_SendBarcode, data).success(function (res) {
                    $scope.isLoaderActiveWrite = false;

                    if (res.isOnError) {
                        //growl.success(res.errorMessage, config);
                        $scope.modalWarningMessage = res.errorMessage;
                        angular.element(document.querySelector('#modalWarning')).modal('toggle');
                        return;
                    }

                    angular.forEach($scope.materialiPerFase, function (mat, key) {
                        if (mat.barcodeRfid === barcode) {
                            mat.isOnAction = false;
                            mat.batchMaterialiStato = res.isCompleted ? 'completato' : '';
                            //growl.success("Scrittura avvenuta correttamente", config);
                        }

                    });
                })
                    .error(function (e) {
                        $scope.isLoaderActiveWrite = false;
                        //growl.success(e, config);
                        $scope.modalWarningMessage = res.errorMessage;
                        angular.element(document.querySelector('#modalWarning')).modal('toggle');
                        $scope.onError(e);
                    });;
            }
            // end SendBarcode

            $scope.clickReadBarcode = function (barcode, isRfid, item) {

                $scope.isLoaderActiveRead = true;
                var data = {
                    IdBatch: $scope.batch.id,
                    IdFase: $scope.selectedBatchPhase.numeroFase,
                    IsRfid: isRfid,
                    TimeoutRequest: 30,
                    Username: $rootScope.globals.currentUser.username,
                    StationId: stationId
                }

                // chiamata al server per la scrittura
                $http.post(urlApi + '/' + subUrlRuntimeTransportContext + '/' + subUrlAction_ReadBarcode, data).success(function (res) {
                    $scope.isLoaderActiveRead = false;

                    angular.forEach($scope.materialiPerFase, function (mat, key) {
                        if (mat.barcode === res.barcode) {
                            mat.batchMaterialiStato = res.isCompleted ? 'completato' : '';
                            // growl.success("Scrittura avvenuta correttamente", config);
                        }

                    });

                })
                    .error(function (e) {
                        console.log(e);
                        $scope.isLoaderActiveRead = false;
                    });;
            }


            $scope.loadFasiAlt();


            // ----------------------------------------------------------------------------------------
            // GESTIONE LETTURA BARCODE
            // ----------------------------------------------------------------------------------------
            // funzione per attivare il focus sulla casella barcode. Ogni n-secondi viene impostato il focus
            $scope.focusBarcodeEnabled = function () {

                timerFocus = setInterval(function () {
                    $(window).focus();
                    $("#txtBarcode").focus().select();
                }, 10 * 500);

                $("#txtBarcode").focus().select();

                $("#txtbarcode").blur(function () {
                    setTimeout(function () { $("#txtbarcode").focus(); $("#txtbarcode").attr("disabled", true); }, 10 * 500);
                });
            };

            // funzione per disattivare il focus sulla casella barcode
            $scope.focusBarcodeDisabled = function () {
                clearInterval(timerFocus);
            };

            // evento scato alla pressione del pulsnte "Invio" sulla casella barcode
            $scope.onKeyPress = function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    var barcodeFixed = $scope.barcode.replace('\r', '');
                    $scope.clickSendBarcode(barcodeFixed, false, null);
                    $scope.barcode = ""; // reset del barcode
                }
            };
            // ----------------------------------------------------------------------------------------



            // ----------------------------------------------------------------------------------------
            // WATCH di variabili del model
            // ----------------------------------------------------------------------------------------

            // watch della variabile per gestire l'indice di TAB dei pulsanti "MULTIMEDIA", "MATERIALI"
            $scope.$watch('shDetail', function (newValue, oldValue, scope) {
                // disabilitazione del focus automatico
                if (newValue == "1") {
                    $scope.focusBarcodeEnabled();
                    $scope.materialiPerFase = $scope.getMaterialiPerFase();
                } else $scope.focusBarcodeDisabled();

                contsize();
            }, true);

            // ----------------------------------------------------------------------------------------
            function createOrRefreshTreeView() {
                $scope.defaultExpanded = [];
                var oNodeSelected = $scope.selectedNode;

                var batchFasi = angular.copy($scope.filteredFasi);
                $scope.dataForTheTree = []; //$scope.dataForTheTree[0].fasi = [];

                if ($scope.filteredFasi != null) {
                    var parentNode = 0;
                    var i = 0;
                    
                    $scope.dataForTheTree.push($scope.currentItem.distintaBase);
                   

                    //***********************************************************************************************************************************************************
                    angular.forEach($scope.dataForTheTree[0].fasi, function (fase, key) {
                       
                        var result = $.grep(batchFasi, function (e) { return e.numeroFase == fase.numeroFase; });
                            if (result.length > 0) {
                                fase.statoOperativo = result[0].stato
                                fase.tipo = (fase.isCiclica ? "1" : "0");
                            }
                    });

                    for (var i = $scope.dataForTheTree[0].fasi.length - 1; i >= 0; i--) {
                        var faseProva = $scope.dataForTheTree[0].fasi[i];
                        var stazioniAbilitate = faseProva.visibilitastazione.trim();
                        var elencoStazioniAbilitate = stazioniAbilitate.split(',');
                        var stazioneCorrenteAbilitata = $.grep(elencoStazioniAbilitate, function (e) { return e == stationId; });

                        if (stazioniAbilitate == "" || stazioniAbilitate == stationId || stazioniAbilitate == "0" || stazioneCorrenteAbilitata > 0) {
                            var fase = $scope.dataForTheTree[0].fasi[i];
                            var result = $.grep(batchFasi, function (e) { return e.numeroFase == fase.numeroFase; });
                            if (result.length == 0) {
                                $scope.dataForTheTree[0].fasi.splice(i, 1); //ELIMINA da TreeView la fase "Batch.DistintaBase.Fasi" GHOST
                            }
                        }
                        else { $scope.dataForTheTree[0].fasi.splice(i, 1); }

                    }//fine for

                }
                $scope.defaultExpanded = [$scope.dataForTheTree[0], $scope.dataForTheTree[0].fasi[0]];
                $scope.showSelected(oNodeSelected);

            }

            // ordinamento di json per campo richiesto
            function orderById(items, field, reverse) {
                var filtered = [];
                angular.forEach(items, function (item) {
                    filtered.push(item);
                });
                filtered.sort(function (a, b) {
                    return (a[field] > b[field] ? 1 : -1);
                });
                if (reverse) filtered.reverse();
                return filtered;
            };



            // **********************************************************************
            // 2016-02-12 Metodo per sottoscrivere la postazione locale al server
            // **********************************************************************
            function doSubscribe(batchId) {

                hub.invoke('SubscribeToBatch', batchId).done(function (res) {
                    // caricamento di ulteriori informazioni
                    var x = res;

                }).fail(function (error) {
                    // errore nella sottoscrizione
                });
            }

            function doUnSubscribe(batchId) {

                hub.invoke('UnSubscribeToBatch', batchId).done(function (res) {
                    // caricamento di ulteriori informazioni
                    var x = res;
                }).fail(function (error) {
                    // errore nella sottoscrizione
                });
            }


            $(document).ready(function () {

                $scope.selectedCausaleDiRiavvio = "";
                $scope.causaliRiavvio = null;
                var d = new Date();
                //$scope.dataDiRiavvio = d.toLocaleDateString();
                $scope.notaDiRiavvio = "";
                DataFactory.GetCausaliRiavvio().then(function (response) {
                    $scope.causaliRiavvio = angular.copy(response.data);
                });

            });


            $scope.openingModalSetCausaleEmergenza = function () {
                $scope.selectedCausaleDiRiavvio = $scope.causaliRiavvio[0].codice;
                var d = new Date();
                $scope.dataDiRiavvio = d.toLocaleString();  //null;//d.toDatetimeString();
                $scope.notaDiRiavvio = "";
            }


        }]);





