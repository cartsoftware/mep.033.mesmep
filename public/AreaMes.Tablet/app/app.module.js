﻿var urlApi = 'http://localhost:9000/api';
var urlLanguageDataTable = './app/localization/datatable/It.txt';
var subUrlApi_Macchina = "macchina";
var subUrlApi_TabelleTipoMacchina = "TipiMacchina";
var subUrlApi_MacchinaCompetenza = "macchinacompetenza";

var subUrlApi_CausaliRiavvio = "causaliriavvio";

var subUrlApi_Operatore = "operatore";
var subUrlApi_OperatoreCompetenza = "operatorecompetenza";

var subUrlApi_UnitaDiMisura = "unitadimisura";

var subUrlApi_Materiali = "materiale";

var subUrlOperazioniCiclo = "operazioniciclo";

var subUrlDistintaBase = "distintabase";

var subUrlBatch = "batch";

var subUrlApi_Enums = "enum";

var subUrlRuntimeTransportContext = "RuntimeTransportContext";

// action specificche per caricare le enum
var subUrlAction_GetLivelliAccesso = "GetLivelliAccesso";
var subUrlAction_GetTipiMateriale = "GetTipiMateriale";
var subUrlAction_GetCausaliFase = "GetCausaliFase";
var subUrlAction_GetStatiDistintaBase = "GetStatiDistintaBase";
var subUrlAction_GetStatiBatch = "GetStatiBatch";
var subUrlAction_GetFasiFromBatch = "GetFasiFromBatch";
var subUrlAction_GetBatch = "GetBatch";

var subUrlAction_GetSfo = "GetSfo";
var subUrlAction_GetSfoFromMachine = "GetSfoFromMachine";
var subUrlAction_SetStateFase = "SetFase";
var subUrlAction_SetStateBatch = "SetBatch";
var subUrlAction_AddPiecesToBatch = "AddPiecesToBatch";
var subUrlAction_SendBarcode = "SendBarcode";
var subUrlAction_ReadBarcode = "ReadBarcode";
var subUrlAction_GetTranslations = "GetTranslations";
var customerName = "AreaMES";

var subUrlAction_Login = "Login";

// 1a dichiarazione del caricamento dei moduli 
var app = angular.module("AppMacchineRuntime", ['$rootScope', '$location', '$cookies', '$http', '$window', 'angular-growl', 'ngAnimate', 'pascalprecht.translate'])
    .run(function ($window, $compile, $rootScope, $location, $cookies, $http, $window) {

    /*
        * Function to 'Angular-fy' dynamically loaded content
        * by JQuery. This compiles the new html code and injects it
        * into the DOM so Angular 'knows' about the new code.
        */
    $window.angularfy = function (target, newHtml) {
        // must use JQuery to query for element by id.
        // must wrap as angular element otherwise 'scope' function is not defined.
        var targetScope = angular.element($(target)).scope();

        //        elem.replaceWith( $compile( newHtml )(elemScope)); Displays html wrong.
        //        elem.html( $compile( newHtml )(elemScope)); Does not work.
        $(target)['html']($compile(newHtml)(targetScope)); // Does work!
        targetScope.$apply();
    }

    
    });

app.config(['growlProvider', function (growlProvider) {
    growlProvider.globalTimeToLive(8000);
    growlProvider.globalPosition('bottom-right');
}]);

//app.config(['$translateProvider', function ($translateProvider) {
//     add translation table
//    $translateProvider.useUrlLoader(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTranslations);
//    $translateProvider.preferredLanguage('it');
//}]);




function basicCrud_doTitle($scope, Enums) {

    switch ($scope.viewState) {
        case Enums.ViewState['R']:
            $scope.titolo = "Dettaglio " + $scope.currentItem.codice;
            break;
        case Enums.ViewState['U']:
            $scope.titolo = "Modifica " + $scope.currentItem.codice;
            break;
        case Enums.ViewState['C']:
            $scope.titolo = "Inserimento ";
            break;
    }
};

function basicCrud_doReload($scope, $http, subUrlApi) {
    $http.get(urlApi + '/' + subUrlApi)
               .success(function (data) {
                   $scope.items = [];
                   $scope.items.push.apply($scope.items, data);

                   setTimeout(function () {
                       if (($scope.dtInstance.dataTable) && ($scope.currentItem)) {
                           var o = $scope.dtInstance.dataTable.fnFindFullCellRowIndexes($scope.currentItem.id);
                           var $currentPage = Math.ceil(o[0] / $scope.dtInstance.dataTable.fnPagingInfo().iLength);
                           $scope.dtInstance.dataTable.fnPageChange($currentPage - 1);
                       }
                   }, 200);

               }).error(function (e) {
                   console.log(e);
               });
}

function basicCrud_doConfirm($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
               .success(function (data) {
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem.id = data;
                       $scope.reload();
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   $scope.viewState = Enums.ViewState['R'];
               })
               .error(function (e) {
                   console.log(e);
               });
}

function basicCrud_doConfirmWithOutReload($scope, $http, subUrlApi, Enums) {
    $http.post(urlApi + '/' + subUrlApi, $scope.currentItem)
               .success(function (data) {
                   if ($scope.viewState === Enums.ViewState['C']) {
                       $scope.currentItem.id = data;
                       //$scope.reload();
                   }
                   else if ($scope.viewState === Enums.ViewState['U']) {
                       var elementPos = $scope.items.map(function (x) { return x.id; }).indexOf($scope.currentItem.id);
                       $scope.items[elementPos] = $scope.currentItem;
                   }

                   //$scope.viewState = Enums.ViewState['R'];
               })
               .error(function (e) {
                   console.log(e);
               });
}

function basicCrud_doShowItemDetail($scope, $http, subUrlApi, Enums, item) {
    if (($scope.currentItem == null) || (item.id !== $scope.currentItem.id)) {
        $http.get(urlApi + '/' + subUrlApi + '/' + item.id)
            .success(function (data) {
                $scope.currentItem = data;
                $scope.viewState = Enums.ViewState['R'];
                
            })
        .error(function(e) {
                console.log(e);
            });
    }
}

function basicCrud_doInitWatch($scope) {
    $scope.$watch('viewState', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.title();
    });
    $scope.$watch('currentItem', function (newValue, oldValue) {
        if (newValue === oldValue) return;
        $scope.title();
    });
}

function basicCrud_doDelete($scope, $http, subUrlApi, Enums) {
    $http.delete(urlApi + '/' + subUrlApi, $scope.currentItem)
               .success(function (data) {
                   $scope.reload();
                   $scope.viewState = Enums.ViewState['R'];
                   angular.element(document.querySelector('#modalConfirmDelete')).modal('hide');
               })
               .error(function (e) {
                   console.log(e);
               });
}

