﻿//...
app.factory('DataFactory', ['$http', '$rootScope', '$timeout', '$cookies', '$window', function ($http, $rootScope, $timeout, $cookies, $window) {
    var datafactory = {};

    // metodo generale di ritorno
    datafactory.getAll = function (baseUrl) {
        
        return $http.get(baseUrl);
    }

    // metodo per ritornare tutti i tipi di macchine
    datafactory.getTipiMacchine = function () {

        return $http.get(urlApi + "/" + subUrlApi_TabelleTipoMacchina);
    }

    // metodo per ritornare le macchine
    datafactory.getMacchine = function () {

        return $http.get(urlApi + "/" + subUrlApi_Macchina);
    }


    // metodo per ritornare tutte le competenze degli operatori
    datafactory.getCompetenzeOperatori = function () {

        return $http.get(urlApi + "/" + subUrlApi_OperatoreCompetenza);
    }

    // metodo per ritornare tutte le competenze delle macchine
    datafactory.getCompetenzeMacchina = function () {

        return $http.get(urlApi + "/" + subUrlApi_MacchinaCompetenza);
    }

    // metodo per ritornare tutti materiali
    datafactory.getMateriali = function () {

        return $http.get(urlApi + "/" + subUrlApi_Materiali);
    }

    // metodo per ritornare le unità di misura
    datafactory.getUnitaDiMisura = function () {

        return $http.get(urlApi + "/" + subUrlApi_UnitaDiMisura);
    }

    // metodo per ritornare le operazioni ciclo
    datafactory.GetOperazioniCiclo = function () {
        return $http.get(urlApi + "/" + subUrlOperazioniCiclo);
    }

    // metodo per ritornare le distinte basi
    datafactory.GetDistinteBasi = function () {
        return $http.get(urlApi + "/" + subUrlDistintaBase);
    }

    // metodo per ritornare ordini 
    datafactory.GetSfo = function (idStato, loggedUser) {
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/" + subUrlAction_GetSfo + "?idStato=" + idStato + "&Username=" + loggedUser);
    }

    // metodo per ritornare ordini per macchina
    datafactory.GetSfoFromMachine = function (idMacchina, idStato, loggedUser) {
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/" + subUrlAction_GetSfoFromMachine + "?idMacchina=" + idMacchina + "&idStato=" + idStato + "&Username=" + loggedUser);
    }

    datafactory.GetFasiFromBatch = function (idBatch, loggedUser) {
        return $http.get(urlApi + "/" + subUrlRuntimeTransportContext + "/" + subUrlAction_GetFasiFromBatch + "?idBatch=" + idBatch + "&Username=" + loggedUser, { headers: { 'Cache-Control': 'no-cache' } });
    }

    datafactory.GetBatchFromId = function (idBatch) {
        return $http.get(urlApi + '/' + subUrlBatch + "/" + idBatch, { headers: { 'Cache-Control' : 'no-cache' } } );
    }

    datafactory.mGetBatchFromId = function (idBatch, loggedUser) {
        return $http.get(urlApi + '/' + subUrlRuntimeTransportContext + "/" + subUrlAction_GetBatch + "?idBatch=" + idBatch + "&Username=" + loggedUser, { headers: { 'Cache-Control': 'no-cache' } });
    }


    /***********************************************************/
    /*****Enums*************************************************/
    /***********************************************************/
    // otteniamo i livelli di accesso per gli utenti
    datafactory.GetLivelliAccesso = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetLivelliAccesso);
    }

    // otteniamo i tipi di materiale
    datafactory.GetTipiMateriale = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTipiMateriale);
    }

    // otteniamo i tipi di causale
    datafactory.GetCausaliFase = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetCausaliFase);
    }

    // otteniamo gli stati della distinta fase
    datafactory.GetStatiDistintaBase = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetStatiDistintaBase);
    }

    // otteniamo gli stati del batch
    datafactory.GetStatiBatch = function () {

        return $http.get(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetStatiBatch);
    }

    // otteniamo le causali di riavvio
    datafactory.GetCausaliRiavvio = function () {
        return $http.get(urlApi + "/" + subUrlApi_CausaliRiavvio);
    }

    // otteniamo le traduzioni 
    datafactory.GetTranslations = function (languageId) {

        var languageInfo = localStorage.getItem(languageId);

        if (languageInfo != null)
            return JSON.parse(languageInfo).items;
    }

    // metodo per logout
    datafactory.ClearCredentials = function () {
        $rootScope.globals = {};
        $cookies.remove('MesAuth');
        $http.defaults.headers.common.Authorization = 'Basic ';
        $window.location.href = 'login.html';
    }

    // metodo di autenticazione
    datafactory.Login = function (username, password) {

        /*----------------------------------------------*/
        //var response = { success: username === 'test' && password === 'test' };
        //if (response)
        //    return {username : "test", password : "password", nome : "nome1" , cognome : "cognome1"};
        return $http.get(urlApi + "/" + subUrlApi_Operatore + "/" + 'login?username=' + username + '&password=' + password);
    }

    // metodo per settare le credenziali
    datafactory.SetCredentials = function (id, username, password, nome, cognome, lingua, extra) {
        var authdata = username + ':' + password; //Base64.encode(username + ':' + password);

        $rootScope.globals = {
            currentUser: {
                idUser: id,
                username: username,
                authdata: authdata,
                nome: nome,
                cognome: cognome,
                lingua: lingua,
                extra: extra
               
            }
        };

        $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; 
        $cookies.put('MesAuth', JSON.stringify($rootScope.globals));
        //$window.location.href = 'elenco-macchine.html';  // ORIGINALE //

        // TO DO: href pagina dipendente da parametro di configurazione // 

        $window.location.href = 'allsfo.html';
    }

    return datafactory;
}]);

app.config(['$translateProvider', function ($translateProvider) {
    // add translation table
    urlApi = $.cookie("WebApiC");
    $translateProvider.useUrlLoader(urlApi + "/" + subUrlApi_Enums + "/" + subUrlAction_GetTranslations);
    $translateProvider.preferredLanguage('it');
}]);

