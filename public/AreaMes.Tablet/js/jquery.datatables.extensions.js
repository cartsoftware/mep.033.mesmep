﻿

jQuery.fn.dataTableExt.oApi.fnFindFullCellRowIndexes = function (oSettings, sSearch, iColumn) {
    var
        i, iLen, j, jLen, val,
        aOut = [], aData,
        columns = oSettings.aoColumns;

    for (i = 0, iLen = oSettings.aoData.length ; i < iLen ; i++) {
        aData = oSettings.aoData[i]._aData;

        if (aData.DT_RowId == sSearch)
            aOut.push(oSettings.aiDisplayMaster.indexOf(i));
        else {
            if (iColumn === undefined) {
                for (j = 0, jLen = columns.length ; j < jLen ; j++) {
                    val = this.fnGetData(i, j);

                    if (val == sSearch) {
                        aOut.push(i);
                    }
                }
            }
            else if (this.fnGetData(i, iColumn) == sSearch) {
                aOut.push(i);
            }
        }

    }

    return aOut;
};

jQuery.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};