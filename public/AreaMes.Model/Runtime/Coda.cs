﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Runtime
{
    public class Coda : BaseDoc, IDoc
    {
        public string Nome { get; set; }
        public string Commessa { get; set; }
        public StatoBatch Stato { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Inizio { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Fine { get; set; }
        public int PercCompletamento { get; set; }
        public string Id { get; set; }
        public ExternalDoc<Macchina> Macchina { get; set; }
        public List<ExternalDoc<Batch>> Batch { get; set; }
        public ExternalDoc<Materiale> Materiale { get; set; }
    }

}
