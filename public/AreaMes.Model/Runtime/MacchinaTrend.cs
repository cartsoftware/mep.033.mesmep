﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Runtime
{
    public  class TrendMacchina
    {
        //public ExternalDoc<Macchina> Macchina { get; set; }
        public Macchina Macchina { get; set; }
        public List<TrendMacchinaVariable> Variabili { get; set; }
    }


    public class TrendMacchinaVariable
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public object UltimoValore { get; set; }
        public List<TrendMacchinaVariabileValoreGiorno> Valori { get; set; }
    }


    public class TrendMacchinaVariabileValoreGiorno
    {
        public DateTime Data { get; set; }

        public List<TrendMacchinaVariabileValoreOra> Valori { get; set; }
        
    }



    public class TrendMacchinaVariabileValoreOra
    {
        public DateTime Ora { get; set; }
        public object Valore { get; set; }
    }
}
