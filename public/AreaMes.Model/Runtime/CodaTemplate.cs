﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using System.Collections.Generic;


namespace AreaMes.Model.Runtime
{
    public class CodaTemplate : BaseDoc, IDoc
    {

        public string NomeTemplate { get; set; }
        public string Nome { get; set; }
        public string Commessa { get; set; }
        public StatoBatch Stato { get; set; }
        public string Id { get; set; }
        public ExternalDoc<Materiale> Materiale { get; set; }
        public ExternalDoc<Macchina> Macchina { get; set; }
        public List<Batch> Batch { get; set; }
    }
    }
