﻿using System;
using System.Collections.Generic;

namespace AreaMes.Model.Runtime
{
    public class BatchCampionatura
    {
        public string Codice { get; set; }
        public DateTime Inizio { get; set; }
        public DateTime Fine { get; set; }
        public List<BatchCampionaturaRisposta> Risposte { get; set; }
    }
}
