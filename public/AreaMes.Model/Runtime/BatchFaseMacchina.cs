﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;

namespace AreaMes.Model.Runtime
{
    public class BatchFaseMacchina
    {
        public Macchina Macchina { get; set; }
        public CausaleFase CausaleFase { get; set; }
         [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Inizio { get; set; }
         [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Fine { get; set; }
    }
}
