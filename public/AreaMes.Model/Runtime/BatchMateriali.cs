﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Runtime
{
    public class BatchMateriali
    {
        public String NumeroFase { get; set; }
        public Materiale Materiale { get; set; }
        public double QntPrevista { get; set; }
        public double QntEffettiva { get; set; }
        public string Lotto { get; set; }
        public string Barcode { get; set; }
        public string BarcodeRfid { get; set; }
        public string Cammpo1 { get; set; }
        public string Cammpo2 { get; set; }
        public string Cammpo3 { get; set; }
        public string Cammpo4 { get; set; }
        public string Cammpo5 { get; set; }
        public string Cammpo6 { get; set; }
        public BatchMaterialiTagAzione TagAzione { get; set; }
        public TipoGestione TipoGestione { get; set; }
        public BatchMaterialiStato BatchMaterialiStato { get; set; }
        public BatchMaterialiTipo BatchMaterialiTipo { get; set; }
    }

    public enum BatchMaterialiTipo {
        Carico = 0,
        Scarico = 1
    }

    public enum BatchMaterialiStato
    {
        InAttesa = 0,
        Completato = 1,
        Nd = 9,
    }

    public enum BatchMaterialiTagAzione
    {
        Lettura = 0, Scrittura = 1, Nd = 9
    }

}
