﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Runtime
{
    public class BatchMultimedia
    {
        public String NumeroFase { get; set; }

        public String Url { get; set; }

        public String Codice { get; set; }

        public String Descrizione { get; set; }

        public BatchMultimediaTipo Tipo { get;set; }
    }

    public enum BatchMultimediaTipo {
        None,
        Immagine,
        Pdf,
        Video
    }
}
