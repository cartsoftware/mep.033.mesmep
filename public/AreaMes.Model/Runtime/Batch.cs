﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Enums;
using AreaMes.Model.Design;
using MongoDB.Bson.Serialization.Attributes;
using AreaMes.Meta;

namespace AreaMes.Model.Runtime
{
    public class Batch : BaseDoc, IDoc
    {
        public string OdP { get; set; }
        public string OdPParziale { get; set; }
        public string OdV { get; set; }
        public DateTime OdV_DataConsegna { get; set; }
        public string OdV_DescrizioneCliente { get; set; }
        public string Revisione { get; set; }
        public string Commessa { get; set; }
        public Materiale Materiale { get; set; }
        public int PezziDaProdurre { get; set; }
        public int PezziProdotti { get; set; }
        public StatoBatch Stato { get; set; }
        public DistintaBase DistintaBase { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Inizio { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Fine { get; set; }
        public int PercCompletamento { get; set; }
        public List<BatchFermi> Fermi { get; set; }
        public List<BatchCampionatura> Campionature { get; set; }
        public List<BatchFase> Fasi { get; set; }
        public string Id { get; set; }
        public int TotaleFermi { get; set; }
        public int PezziScartati { get; set; }
        public int OEE { get; set; }
        public double TempoTotaleFermi { get; set; }
        public int OEEPrecedente { get; set; }
        public AndamentoValore OEEAndamento { get; set; }
        public List<BatchParametri> BatchParametri { get; set; }
        public List<BatchAvanzamento> BatchAvanzamenti { get; set; }
        public List<BatchMateriali> BatchMateriali { get; set; }
        public List<BatchMultimedia> BatchMultimedia { get; set; }
        public ExternalDoc<Coda> QueueId { get; set; }

        public bool QueueAsFirst { get; set; }

    }

    public enum AndamentoValore
    {
        Stabile = 0,
        InSalita = 1,
        InDiscesa = 2,
    }



}
