﻿using AreaMes.Model.Enums;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Runtime
{
    public class BatchAvanzamento
    {
        public String Id { get; set; }
        public string NumeroFase { get; set; }
        public string CodiceFase { get; set; }
        public string BarcodeFase { get; set; }
        public StatoFasi Stato { get; set; }
        public BatchFaseMacchina BatchFaseMacchina { get; set; }
        public BatchFaseManoDopera BatchFaseManoDopera { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Inizio { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? Fine { get; set; }

        public string Nota { get; set; }

        public string UserLocked { get; set; }

        public object Campo1 { get; set; }

        public object Campo2 { get; set; }

        public object Campo3 { get; set; }

        public object Campo4 { get; set; }

        public object Campo5 { get; set; }

        public object Campo6 { get; set; }

        public object Campo7 { get; set; }

        public object Campo8 { get; set; }
    }

}
