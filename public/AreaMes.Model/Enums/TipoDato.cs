﻿namespace AreaMes.Model.Enums
{
    public enum TipoDato
    {
        Bool,
        Numeric,
        String,
        Date,
        DateTime,
        Time,
        Foto,
        List
    }



    public enum UmList
    {
        Kilogrammi, Pezzi, Metri, Litri
    }


}
