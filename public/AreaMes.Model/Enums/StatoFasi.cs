﻿namespace AreaMes.Model.Enums
{
    public enum StatoFasi
    {
        InModifica = 0,
        InAttesa = 1,
        InLavorazione = 2,
        InLavorazioneInPausa = 3,
        InLavorazioneInPausaS = 4,
        InLavorazioneInEmergenza = 5,
        InPausa = 6,
        Terminato = 7,
        Abortito = 8,
        Eliminato = 9
    }
}
