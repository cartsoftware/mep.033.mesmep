﻿namespace AreaMes.Model.Enums
{
    public enum StatoBatch
    {
        InModifica = 0,
        InAttesa = 1,
        InLavorazione = 2,
        InLavorazioneInPausa = 3,
        InLavorazioneInPausaS = 4,
        InLavorazioneInEmergenza = 5,
        InPausa = 6,
        Terminato = 7,
        TerminatoAbortito = 8,
        Eliminato = 9,
        InCreazione=10
    }

    public enum StatoMacchinaDriver
    {
        Disconnesso = 0,
        InConnessione = 1,
        Connesso = 2,
        InRiconnessione =3
    }
}
