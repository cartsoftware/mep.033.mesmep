﻿namespace AreaMes.Model.Enums
{
    public enum Severita
    {
        Bassa,
        Media,
        Alta,
        Critica
    }
}
