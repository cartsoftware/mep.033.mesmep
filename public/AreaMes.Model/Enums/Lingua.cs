﻿namespace AreaMes.Model.Enums
{
    public enum Lingua
    {
        It,
        En, 
        Es,
        Fr,
        De
    }
}
