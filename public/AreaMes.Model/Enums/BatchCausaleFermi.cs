﻿namespace AreaMes.Model.Enums
{
    public enum BatchCausaleFermi
    {
        None,
        Pausa,
        PausaS,
        Emergenza,
    }
}
