﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public interface ISettings
    {
        Dictionary<string, object> Values { get; }
    }
}
