﻿using AreaMes.Model.Enums;

namespace AreaMes.Model
{
    public class DistintaBaseFasiManoDopera
    {
        public  ExternalDoc<OperatoreCompetenza> OperatoreCompetenza { get; set; }

        public CausaleFase CausaleFase { get; set; }

        public double Tempo { get; set; }

        public string Descrizione { get; set; }
    }
}
