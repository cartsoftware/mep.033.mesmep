﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Enums;

namespace AreaMes.Model
{
    public class DistintaBaseFasiCampionaturaDomanda
    {
        public string Codice { get; set; }
        public string Domanda { get; set; }
        public string Descrizione { get; set; }
        public bool IsDisabled { get; set; }
        public bool AsserisciNegativa { get; set; }
        public TipoDato TipoDato { get; set; }
        public List<Tuple<Comparazione, object, Severita>> ValoriConfronto { get; set; }

    }
}
