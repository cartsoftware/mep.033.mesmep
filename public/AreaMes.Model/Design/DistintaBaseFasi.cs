﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using AreaMes.Meta;

namespace AreaMes.Model
{
    public class DistintaBaseFasi : BaseDoc, IDoc
    {
        public string NumeroFase { get; set; }
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public string Codice { get; set; }
        public string Barcode { get; set; }
        public bool IsDisabled { get; set; }
        public List<DistintaBaseFasiMacchina> Macchine { get; set; }
        public Tuple<CausaleFase, double> TotaleTempiMacchina { get; set; }
        public List<DistintaBaseFasiManoDopera> Manodopera { get; set; }
        public Tuple<CausaleFase, double> TotaleTempiManodopera { get; set; }
        public bool IsAutomatica { get; set; }
        public bool IsAndon { get; set; }
        public double TaktMacchina { get; set; }
        public double TaktManodopera { get; set; }
        public double TaktTotale { get; set; }
        public bool IsCiclica { get; set; }
        public bool AbilitaAvanzamenti { get; set; } = true;
        public bool AbilitaVersamentiManuali { get; set; } = false;
        public bool AbilitaVersamentiAutomatici { get; set; } = false;
        public bool AbilitaMultimedia { get; set; } = false;
        public bool AbilitaMateriali { get; set; } = false;

        public bool Abilitabarcode { get; set; } = false;
        public bool AbilitaLetturaRfid { get; set; } = false;
        public bool AbilitascritturaRfid { get; set; } = false;
        public bool AbilitaAvanzamentiStar { get; set; } = false;
        public bool AbilitaAvanzamentiStop { get; set; } = false;
        public bool AbilitaAvanzamentiPausa { get; set; } = false;
        public string Visibilitastazione { get; set; }
        public int StileDiVisualizzazioneVersamenti { get; set; }
        public ExternalDoc<OperazioneFase> OperazioneFase { get; set; }
        public string Id { get; set; }
        public List<DistintaBaseFasi> Fasi { get; set; }

        public object Campo1 { get; set; }
        public object Campo2 { get; set; }
        public object Campo3 { get; set; }
        public object Campo4 { get; set; }
        public object Campo5 { get; set; }
        public object Campo6 { get; set; }
        public object Campo7 { get; set; }
        public object Campo8 { get; set; }
        public object Campo9 { get; set; }
        public object Campo10 { get; set; }
        public DistintaBaseFasi()
        {
            this.Fasi = new List<DistintaBaseFasi>();
        }
    }
}
