﻿using AreaMes.Meta;

namespace AreaMes.Model
{
    public class Parametri : IDoc
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public string ValoreDefault { get; set; }
        public string Tipo { get; set; }
        public bool Visibilita { get; set; }
        public string UnitaDiMisura { get; set; }
        public string Id { get; set; }
    }

    public class DistintaBaseParametri
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public object ValoreDefault { get; set; }
        public AreaMes.Model.Enums.TipoDato Tipo { get; set; } 
        public bool Visibilita { get; set; }
        public bool IsTrend { get; set; }
        public int OrdineIdx { get; set; }
        public ExternalDoc<UnitaDiMisura> UnitaDiMisura { get; set; }
    }
}
