﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using MongoDB.Driver;


namespace AreaMes.Model
{
    public class Macchina2 : BaseDoc, IDoc
    {

        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public bool IsEsterna { get; set; }

        public bool IsDisable { get; set; }

        public string Barcode { get; set; }

        public string TipologiaRefId { get; set; }

        public MongoDBRef Tipologia { get; set; }

        public string Id { get; set; }


        //public TipiMacchina TipoMacchina { get; set; }

        //[BsonRepresentation(BsonType.String)]
        //public Guid Id { get; set; }
        //public long __DocVersion { get; set; }


    }



    //[JsonConverter(typeof(StringEnumConverter))]
    public enum TipoMacchina
    {
        Tipo1,
        Tipo2
    }
}
