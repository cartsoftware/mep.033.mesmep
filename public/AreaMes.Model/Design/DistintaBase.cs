﻿using System.Collections.Generic;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using AreaMes.Meta;
using System;

namespace AreaMes.Model
{
    public class DistintaBase : BaseDoc, IDoc
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public string Barcode { get; set; }
        public string Descrizione { get; set; }
        public string Revisione { get; set; }
        public ExternalDoc<Materiale> MaterialeInUscita { get; set; }
        public ExternalDoc<UnitaDiMisura> UnitaDiMisura { get; set; }
        public List<DistintaBaseFasi> Fasi { get; set; }
        public List<DistintaBaseMateriale> Materiali { get; set; }
        public List<DistintaBaseFasiCampionatura> Campionatura { get; set; }
        public StatoDistintaBase StatoDistintaBase { get; set; }
        public bool IsDisabled { get; set; }
        public string Id { get; set; }
        public List<DistintaBaseParametri> DistintaBaseParametri { get; set; }

        public bool AbilitaStopProduzione { get; set; } = true;
        public bool AbilitaAutoStartProduzione { get; set; } = true;
        public bool AbilitaEmergenza { get; set; } = true;

        public DistintaBase()
        {
            this.Fasi = new List<DistintaBaseFasi>();
            this.DistintaBaseParametri = new List<Model.DistintaBaseParametri>();
        }


        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!String.IsNullOrWhiteSpace(this.Nome))
                    res += (String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }

    }
}
