﻿using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public class Licence: IDoc
    {
        public bool IsValid { get;  set; }
        public int MaxMachines { get;  set; }
        public string LicencedAt_VatCode { get;  set; }
        public string LicencedAt_Name { get;  set; }
        public string LicencedAt_Key { get;  set; }
        public int LicencedAt_Type { get;  set; }
        public DateTime? LastValidationAt { get;  set; }
        public DateTime? ActivatedAt { get;  set; }
        public string Id { get; set; }
    }
}
