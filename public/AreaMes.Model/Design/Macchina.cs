﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;

namespace AreaMes.Model
{

    public class Macchina : BaseDoc, IDoc
    {
        public string Id { get; set; }

        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public bool IsEsterna { get; set; }

        public bool IsDisable { get; set; }

        public string Barcode { get; set; }

        public string IndirizzoIp { get; set; }

        public string SerialNumber { get; set; }

        public bool isActiveRealtime { get; set; }

        public ExternalDoc<TipiMacchina> Tipologia { get; set; }

        public ExternalDoc<MacchinaCompetenza> MacchinaCompetenza { get; set; }

        public bool IsRegisteredToM2M { get; set; }

        public DateTime? RegisteredToM2MAt { get; set; }

        public int RegisteredToM2MDeviceId { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!String.IsNullOrWhiteSpace(this.Nome))
                    res += (String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }

    }

}
