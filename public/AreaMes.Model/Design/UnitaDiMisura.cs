﻿using AreaMes.Meta;

namespace AreaMes.Model
{
    public class UnitaDiMisura : IDoc
    {
        public string Codice { get; set; }
 
        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public string Id { get; set; }

        //public Guid Id { get; set; }

        //public long __DocVersion { get; set; }
    }
}
