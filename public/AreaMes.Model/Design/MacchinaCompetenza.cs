﻿using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Design
{
    public class MacchinaCompetenza : IDoc
    {
        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public bool IsDisable { get; set; }

        public string Id { get; set; }

    }
}
