﻿using AreaMes.Meta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Design
{
    public class Licenza : IDoc
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public string Vat { get; set; }

        public string Name { get; set; }

        public string SecurityToken { get; set; }


        /// <summary>
        /// 0 = Solo Agent/Demone
        /// 1 = Agent/Demone + Mes locale
        /// 2 = Agent/Demone + Mes cloud
        /// </summary>
        public int LicenceType { get; set; }

        public DateTime? ActivateAt { get; set; }

        public string MacAddress { get; set; }

        public int MaxMachines { get; set; }

        public int MaxUsers { get; set; }

        public bool IsEnabled { get; set; }

        public DateTime? LastServerValidationAt { get; set; }

        public string Description1 { get; set; }

        public string Description2 { get; set; }

        public string Description3 { get; set; }


    }
}
