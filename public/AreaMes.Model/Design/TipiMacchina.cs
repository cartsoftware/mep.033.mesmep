﻿using AreaMes.Meta;
using System;
using AreaMes.Model.Design;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace AreaMes.Model.Design
{
    public class TipiMacchina: IDoc
    {
        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public bool IsDisable { get; set; }

        public string Id { get; set; }

        public string Driver { get; set; }

        public bool IsEnableLine { get; set; }

        //[BsonRepresentation(BsonType.String)]
        //public Guid Id { get; set; }

        //public long __DocVersion { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!String.IsNullOrWhiteSpace(this.Nome))
                    res += (String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }
    }
}
