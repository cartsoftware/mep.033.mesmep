﻿using AreaMes.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Design
{
    public class TotalizzatoriPerCausale
    {
        public CausaleFase CausaleFase { get; set; }  
        public double Totale {get;set;}
    }
}
