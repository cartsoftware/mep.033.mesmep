﻿using AreaMes.Model.Design;
using AreaMes.Model.Enums;

namespace AreaMes.Model
{
    public class DistintaBaseFasiMacchina
    {
        public ExternalDoc<Macchina> Macchina { get; set; }

        public ExternalDoc<MacchinaCompetenza> MacchinaCompetenza { get; set; }

        public CausaleFase CausaleFase { get; set; }

        public double Tempo { get; set; }
    }
}
