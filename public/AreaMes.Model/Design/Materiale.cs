﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Design;
using AreaMes.Meta;

namespace AreaMes.Model
{
    public class Materiale : BaseDoc, IDoc
    {

        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public string Barcode { get; set; }

        public bool IsDisable { get; set; }

        public TipoMateriale TipoMateriale { get; set; }

        public double PesoNetto { get; set; }

        public double PesoLordo { get; set; }

        public List<Foto> Foto { get; set; }

        public string Categoria { get; set; }

        public string Id { get; set; }

        public string Um { get; set; }

        public TipoGestione TipoGestione { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!String.IsNullOrWhiteSpace(this.Nome))
                    res += (String.IsNullOrWhiteSpace(res) ? "": " - " ) + this.Nome;

                if (String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }
    }


    public enum TipoMateriale
    {
        Ingresso, Semilavorato, ProdottoFinito
    }

    public enum TipoGestione
    {
        Nessuno,
        Rfid,
        Barcode,
    }
}
