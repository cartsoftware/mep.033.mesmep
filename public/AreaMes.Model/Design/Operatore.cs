﻿using AreaMes.Meta;
using AreaMes.Model.Design;
using AreaMes.Model.Enums;
using System;

namespace AreaMes.Model
{
    public class Operatore : BaseDoc, IDoc
    {

        public string Codice { get; set; }

        public string Nome { get; set; }

        public string Cognome { get; set; }

        public Foto Foto { get; set; }

        public bool IsDisable { get; set; }

        public string Lingua { get; set; }

        public string Mail { get; set; }

        public bool IsAbilityNotice { get; set; }

        public string Barcode { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Id { get; set; }

        public ExternalDoc<OperatoreCompetenza> OperatoreCompetenza { get; set; }

        public OperatoreLivelloLogin OperatoreLivelloLogin { get; set; }

        //public Guid Id { get; set; }

        //public long __DocVersion { get; set; }

        public bool AreaDiSicurezza_Tabelle { get; set; }

        public bool AreaDiSicurezza_Anagrafica { get; set; }

        public bool AreaDiSicurezza_Designer { get; set; }

        public bool AreaDiSicurezza_Produzione { get; set; }

        public bool AreaDiSicurezza_IsSuperAdmin { get; set; }

        public new string ToString
        {
            get
            {
                string res = this.Codice;
                if (!String.IsNullOrWhiteSpace(this.Nome))
                    res += (String.IsNullOrWhiteSpace(res) ? "" : " - ") + this.Nome;

                if (String.IsNullOrWhiteSpace(res))
                    res = this.Id;

                return res;
            }
        }
    }
}
