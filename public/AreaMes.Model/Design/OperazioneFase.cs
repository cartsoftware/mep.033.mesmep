﻿using System;
using System.Collections.Generic;
using AreaMes.Model.Enums;
using AreaMes.Meta;

namespace AreaMes.Model.Design
{
    public class OperazioneFase : BaseDoc, IDoc
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public bool IsDisabled { get; set; }
        public List<DistintaBaseFasiMacchina> Macchine { get; set; }
        public TotalizzatoriPerCausale TotaleTempiMacchina { get; set; }
        public List<DistintaBaseFasiManoDopera> Manodopera { get; set; }
        public TotalizzatoriPerCausale TotaleTempiManodopera { get; set; }
        public bool IsAutomatica { get; set; }
        public bool IsAndon { get; set; }
        public double TaktMacchina { get; set; }
        public double TaktManodopera { get; set; }
        public double TaktTotale { get; set; }
        public bool IsCiclica { get; set; }
        public string Barcode { get; set; }

        public string Id { get; set; }

        //public Guid Id { get; set; }
        //public long __DocVersion { get; set; }
    }
}
