﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model.Dto
{
    public class RegisterMachineToM2MResult
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        public bool IsRegisteredToM2M { get; set; }
        public DateTime? RegisteredToM2MAt { get; set; }
        public int RegisteredToM2MDeviceId { get; set; }
    }

    public class RegisterMachineToM2MRequest
    {
    }


    public class RealTimeActivateResult
    {
        public bool Success { get; set; }
        public string Error { get; set; }
    }

    public class RealTimeDeactivateResult
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        public string TokenId { get; set; }
    }
}
