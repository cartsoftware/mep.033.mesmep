﻿using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public interface IRuntimeTransportContextController
    {
        List<Batch> GetSfo(string idStato, string loggedUser);

        List<Batch> GetSfoFromMachine(string idMacchina, string idStato, string loggedUser);

        List<BatchFase> GetFasiFromBatch(string idBatch, string loggedUser);

        Task<string> SetFase(SetFaseInfo utBatch );

        Task<string> SetBatch(SetBatchInfo utBatch);

        string AddPiecesToBatch(VersamentoBatch vBatch);

        Task<string> SetBatchMateriale(string idBatch, List<BatchMateriali> materiali);
        
    }

    /// <summary>
    /// classe di utility per web api contentente idbatch e la fase
    /// </summary>
    public class SetFaseInfo
    {
        public string idBatch { get; set; }
        public BatchFase fase { get; set; }
        public string Username { get; set; }

        public bool NoSetupQueue { get; set; }
    }

    public class SetBatchInfo
    {
        public string idBatch { get; set; }
        public StatoBatch idStato { get; set; }
        public string causaleRiavvio { get; set; }
        public DateTime dataRiavvio { get; set; }
        public string notaRiavvio { get; set; }
        public string Username { get; set; }
    }
    public class VersamentoBatch
    {
        public string idBatch { get; set; }
        public string pezziVersati { get; set; }
        public string numeroFase { get; set; }
        public string Username { get; set; }
    }


    public class BaseRequest
    {
        public string IdBatch { get; set; }
        public string IdFase { get; set; }
        public string Barcode { get; set; }
        public string StationId { get; set; }
        public int TimeoutRequest { get; set; }
        public string Username { get; set; }
    }

    public class SendBarcodeRequest : BaseRequest
    {
        public bool IsRfid { get; set; }

        public Materiale Material { get; set; }
    }

    public class SendBarcodeResponse
    {
        public string ErrorMessage { get; set; }

        public bool IsOnError { get; set; }

        public bool IsCompleted { get; set; }
    }


    public class ReadBarcodeRequest : BaseRequest
    {
        public bool IsRfid { get; set; }
    }

    public class ReadBarcodeResponse 
    {
        public string ErrorMessage { get; set; }
        public bool IsOnError { get; set; }
        public bool IsCompleted { get; set; }
        public string Barcode { get; set; }
    }
}
