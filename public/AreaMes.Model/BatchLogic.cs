﻿using AreaMes.Meta;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public class BatchLogic
    {
        protected log4net.ILog Logger;

        protected IDataContainer _data;
        protected IRepository _repository;
        protected IMessageServices _messages;
        protected IRealTimeManager _areaMesClient;
        protected IAreaM2MClient _areaM2MClient;
        protected IRuntimeTransportContextController _runtimeContext;
        private ISettings _settings;
        protected string _batchId;
        protected Batch _batch { get; set; }

        protected  Dictionary<string, BatchParametri> _listOfBatchParams = new Dictionary<string, BatchParametri>();

        private RegistrationInfo subscrb1;
        private RegistrationInfo subscrb2;
        private RegistrationInfo subscrb3;

        public Batch Get()
        {
            return this._batch;
        }

        public void Prepare(ISettings settings)
        {
            _settings = settings;
        }
        public object GetSettingValue(string key)
        {
            return _settings.Values[key];
        }

        public virtual async Task Init(IDataContainer data,
                               IRepository repository,
                               IMessageServices messages,
                               IRealTimeManager areaMESClient,
                               IAreaM2MClient areaM2MClient,
                               IRuntimeTransportContextController runtimeContext,
                               Batch batch)
        {
            await Task.Run(() => {

                Logger = log4net.LogManager.GetLogger("Odp." + batch.OdP);

                try
                {
                    _data = data; _repository = repository; _messages = messages; _batchId = batch.Id; _areaMesClient = areaMESClient; _batch = batch; _areaM2MClient = areaM2MClient;
                    _runtimeContext = runtimeContext;
                    subscrb1 = _messages.Subscribe<DeviceValueChangedMessage>(DeviceValueChangedMessage.Name, OnDeviceValueChangedMessage);
                    subscrb2 = _messages.Subscribe<BatchChangedMessage>(_batchId, OnBatchChanged);
                    subscrb3 = _messages.Subscribe<FaseChangedInfo>(_batchId, OnFaseChanged);

                    Logger.InfoFormat("Batch '{0}' subscribed to messages", _batchId);

                    if (_batch.BatchParametri == null)
                    {
                        _batch.BatchParametri = new List<BatchParametri>();
                        foreach (var par in batch.DistintaBase.DistintaBaseParametri)
                            _batch.BatchParametri.Add(new BatchParametri { Parametro = par, Trend = new List<BatchTrendParametri>() });
                    }

                    foreach (var item in _batch.BatchParametri)
                        _listOfBatchParams.Add(item.Parametro.Codice, item);
                }
                catch (Exception exc)
                {
                    Logger.Error(exc);
                }

            });

        }

        public virtual void Start()
        {

        }

        public virtual void Stop()
        {
            if (subscrb1 != null)
                _messages.UnSubscribe(subscrb1);

            if (subscrb2 != null)
                _messages.UnSubscribe(subscrb2);

            if (subscrb3 != null)
                _messages.UnSubscribe(subscrb3);

            _data = null;
            _repository = null;
            _messages = null;
            _areaMesClient = null;
            _areaM2MClient = null;
            _runtimeContext = null;
        }

        protected virtual void OnFaseChanged(object arg1, FaseChangedInfo arg2)
        {

        }

        protected virtual void OnBatchChanged(object arg1, BatchChangedMessage arg2)
        {

        }

        protected virtual void OnDeviceValueChangedMessage(object arg1, DeviceValueChangedMessage arg2)
        {

        }


        protected void registerTrend(DeviceValueChangedMessage message, string systemVariableName, BatchParametri parametro)
        {
            var oThing = message.FirstOrDefault(o => o.Name == systemVariableName);
            if (oThing == null) return;

            lock (parametro.Trend)
            {
                var oLastPar = parametro.Trend.LastOrDefault();
                var oNewPar = new BatchTrendParametri { DataOra = DateTime.Now, Valore = oThing.Value };
                if (oLastPar != null)
                {
                    oNewPar.DataOraPrecedente = oLastPar.DataOra;
                    oNewPar.ValorePrecedente = oLastPar.Valore;
                }

                parametro.Trend.Add(oNewPar);
            }
            calculateStatistics(parametro);
        }

        protected void registerTrend(object value, BatchParametri parametro)
        {
            lock (parametro.Trend)
            {
                var oLastPar = parametro.Trend.LastOrDefault();
                var oNewPar = new BatchTrendParametri { DataOra = DateTime.Now, Valore = value };
                if (oLastPar != null)
                {
                    oNewPar.DataOraPrecedente = oLastPar.DataOra;
                    oNewPar.ValorePrecedente = oLastPar.Valore;
                }

                parametro.Trend.Add(oNewPar);
            }


            calculateStatistics(parametro);
        }

        private void calculateStatistics(BatchParametri parametro)
        {
            if (parametro.Parametro.Tipo == Enums.TipoDato.Numeric)
            {
                lock (parametro.Trend)
                {
                    var temp = parametro.Trend.ToList();

                    parametro.Avg = temp.Average(o => o.Valore == null ? 0 : Convert.ToDouble(o.Valore));
                    parametro.Min = temp.Min(o => o.Valore == null ? 0 : Convert.ToDouble(o.Valore));
                    parametro.Max = temp.Max(o => o.Valore == null ? 0 : Convert.ToDouble(o.Valore));
                }
            }
        }
    }


    public class SupervisorBatchLogicBase
    {

        private ISettings _settings;
        private IRepository _repository;
        protected IRuntimeTransportContextController _runtimeContext;
        protected log4net.ILog Default;

        // protected  StartBatchInfo Start { get; private set; }

        public Func<List<BatchLogic>> BatchLogicInMemory { get;  set; }
        protected Action<Batch> Start { get; private set; }
        protected Func<List<Batch>> BatchInMemory { get; private set; }

        public void Prepare(ISettings settings, Action<Batch> startAction, IRepository repository, Func<List<Batch>> batchInMemory, IRuntimeTransportContextController runtimeContext)
        {
            Default = log4net.LogManager.GetLogger("SPRV");
            _settings = settings;
            Start = startAction;
            BatchInMemory = batchInMemory;
            _runtimeContext = runtimeContext;
            _repository = repository;
        }

        public object GetSettingValue(string key)
        {
            object res = null;
            _settings.Values.TryGetValue(key, out res);

            return res;
        }

        public virtual void Init()
        {

        }

        public virtual void OnBatchAdd(BatchLogic batchLogic)
        {

        }

        protected IRepository Repository
        {
            get { return _repository; }
        }
    }


    public static class BatchUtils
    {
        public static async Task PrepareBatchTo_InAttesa(Batch item, IRepository db)
        {
            item.UserLockedDate = DateTime.Now;
            // creo le fasi
            List<BatchFase> listOfFasi = new List<BatchFase>();
            foreach (var oFase in item.DistintaBase.Fasi)
            {
                List<BatchFaseMacchina> bfm = new List<BatchFaseMacchina>();
                List<BatchFaseManoDopera> bfman = new List<BatchFaseManoDopera>();

                if (oFase.Macchine != null)
                    foreach (var oMacchina in oFase.Macchine)
                    {
                        Macchina machine = new Macchina();
                        if (oMacchina.Macchina != null)
                            machine = await db.GetAsync<Macchina>(oMacchina.Macchina.Id);
                        bfm.Add(new BatchFaseMacchina { Macchina = machine, CausaleFase = oMacchina.CausaleFase, Fine = null, Inizio = null });
                    }

                if (oFase.Manodopera != null)
                    foreach (var oManodopera in oFase.Manodopera)
                        bfman.Add(new BatchFaseManoDopera { Operatore = null, CausaleFase = oManodopera.CausaleFase, Fine = null, Inizio = null, Descrizione = oManodopera.Descrizione});


                listOfFasi.Add(new BatchFase
                {
                    DistintaBaseFase = oFase,
                    Macchine = bfm,
                    Manodopera = bfman,
                    NumeroFase = oFase.NumeroFase,
                    Stato = StatoFasi.InAttesa,
                    UserLocked = null,
                    UserLockedDate = item.UserLockedDate,
                    IsAndon = oFase.IsAndon,
                    IsCiclica = oFase.IsCiclica,
                    TaktMacchina = oFase.TaktMacchina,
                    TaktManodopera = oFase.TaktManodopera,
                    TaktTotale = oFase.TaktTotale
                });
            }

            item.Fasi = listOfFasi;

            item.BatchParametri = new List<BatchParametri>();
            foreach (var par in item.DistintaBase.DistintaBaseParametri)
                item.BatchParametri.Add(new BatchParametri { Parametro = par, Trend = new List<BatchTrendParametri>() });

        }

        public static int CalculateRuntimeEfficiency(Batch pBatch)
        {
            if ((pBatch.Inizio.HasValue) && (pBatch.Inizio.Value.Year > 2000))
            {
                double tFermi = 0;
                if (pBatch.Fermi != null)
                    foreach (var singoloStop in pBatch.Fermi)
                    {
                        DateTime dtFineStop = DateTime.Now;
                        if (singoloStop.Fine.HasValue)
                            dtFineStop = singoloStop.Fine.Value;

                        var diff = (dtFineStop - singoloStop.Inizio.Value).TotalSeconds;
                        if (diff > 0)
                        tFermi += diff;
                    }


                double tTempoTotale = 0;
                DateTime dtFineBatch = DateTime.Now;
                if (pBatch.Fine.HasValue)
                    dtFineBatch = pBatch.Fine.Value;

                tTempoTotale = (dtFineBatch - pBatch.Inizio.Value).TotalSeconds;

                double x1 = (tTempoTotale - tFermi);
                double x = x1 / tTempoTotale;
                return Convert.ToInt32(x * 100);
            }

            return 0;
        }

        public static async Task<BatchAutoCounter> GetAutoCounterInfo(IRepository db, bool getNew)
        {
            var ret = new BatchAutoCounter { IsEnabled = false };
            var allpa = await db.All<Parametri>();
            var CNT_BATCH_IS_ENABLED = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_IS_ENABLED");

            if ((CNT_BATCH_IS_ENABLED != null) && (CNT_BATCH_IS_ENABLED.ValoreDefault == "1"))
            {

                var CNT_BATCH_GLOBALE = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_GLOBALE");
                var CNT_BATCH_PARZIALE = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PARZIALE");
                var CNT_BATCH_PARZIALE_RESET_OGNI = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PARZIALE_RESET_OGNI");
                var CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE = allpa.FirstOrDefault(o => o.Codice == "CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE");

                var _cntParziale = Convert.ToInt32(CNT_BATCH_PARZIALE.ValoreDefault);
                var _cntGlobale = Convert.ToInt32(CNT_BATCH_GLOBALE.ValoreDefault);


                if (CNT_BATCH_PARZIALE_RESET_OGNI != null)
                {
                    DateTime _dataUltimaAssegnazione = DateTime.Now;
                    DateTime.TryParseExact(CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE.ValoreDefault, "dd-MM-yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out _dataUltimaAssegnazione);

                    if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "gg")
                    {
                        // reset ogni giorni
                        if (DateTime.Now.Subtract(_dataUltimaAssegnazione).TotalDays > 1)
                            _cntParziale = 0;

                    }
                    else if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "mm")
                    {
                        // reset ogni mese
                        if (DateTime.Now.Month != _dataUltimaAssegnazione.Month)
                            _cntParziale = 0;
                    }
                    else if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "aa")
                    {
                        // reset ogni anno
                        if (DateTime.Now.Year != _dataUltimaAssegnazione.Year)
                            _cntParziale = 0;
                    }
                    else if (CNT_BATCH_PARZIALE_RESET_OGNI.ValoreDefault == "HH")
                    {
                        // reset ogni ora
                        if (DateTime.Now.Hour != _dataUltimaAssegnazione.Hour)
                            _cntParziale = 0;
                    }
                }

                if (getNew)
                {
                    _cntGlobale += 1;
                    _cntParziale += 1;
                }

                CNT_BATCH_GLOBALE.ValoreDefault = Convert.ToString(_cntGlobale);
                CNT_BATCH_PARZIALE.ValoreDefault = Convert.ToString(_cntParziale);
                CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE.ValoreDefault = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");

                if (getNew)
                {
                    await db.SetAsync(CNT_BATCH_GLOBALE);
                    await db.SetAsync(CNT_BATCH_PARZIALE);
                    await db.SetAsync(CNT_BATCH_PARZIALE_DATA_ULTIMA_ASSEGNAZIONE);
                }

                ret.IsEnabled = true;
                ret.Global = _cntGlobale;
                ret.Partial = _cntParziale;
                ret.LastAssign = DateTime.Now;
            }

            return ret;

        }

        public static bool IsInLavorazione(Batch item)
        {
            return (item.Stato == StatoBatch.InLavorazione) ||
                   (item.Stato == StatoBatch.InLavorazioneInEmergenza) ||
                   (item.Stato == StatoBatch.InLavorazioneInPausa) ||
                   (item.Stato == StatoBatch.InLavorazioneInPausaS);
        }

        public static bool IsInLavorazione(BatchFase item)
        {
            return (item.Stato == StatoFasi.InLavorazione) ||
                   (item.Stato == StatoFasi.InLavorazioneInEmergenza) ||
                   (item.Stato == StatoFasi.InLavorazioneInPausa) ||
                   (item.Stato == StatoFasi.InLavorazioneInPausaS);
        }

        public class BatchAutoCounter
        {
            public bool IsEnabled { get; set; }
            public int Global { get; set; }
            public  int Partial { get; set; }
            public DateTime? LastAssign { get; set; }

        }
    }

    public class DailyTimer : IDisposable
    {
        private DateTime _lastRunDate;
        private TimeSpan _time;
        private Timer _timer;
        private Action _callback;
        private Action _callbackAfterDelay;
        private double _delaySecondCallback;
        private DateTime _createTime;
        public DailyTimer(TimeSpan time, Action callback, Action callbackAfterDelay = null, double delaySecondCallback = 0)
        {
            _time = time;
            _timer = new Timer(CheckTime, null, TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30));
            _callback = callback;
            _callbackAfterDelay = callbackAfterDelay;
            _delaySecondCallback = delaySecondCallback;
            _createTime = DateTime.Now;
        }

        private void CheckTime(object state)
        {
            // 2016-08-02 Modifica se _lastRunDate a null perchè altrimenti mi carica la sequenza
            if (_lastRunDate == DateTime.MinValue)
                if ((_createTime.TimeOfDay > _time) && (_createTime.Date == DateTime.Now.Date))
                    return;
            if (_lastRunDate == DateTime.Today)
                return;
            if (DateTime.Now.TimeOfDay < _time)
                return;
            _lastRunDate = DateTime.Today;
            _callback();
            if (_callbackAfterDelay != null)
                Task.Delay(Convert.ToInt32(_delaySecondCallback * 1000)).ContinueWith((_) => { _callbackAfterDelay(); });
        }

        public void Dispose()
        {
            if (_timer == null)
                return;
            _timer.Dispose();
            _timer = null;
        }

    }
}


    //public delegate  void StartBatchInfo(Batch batch, bool prepare = false);

