﻿using AreaMes.Meta;
using AreaMes.Model.Enums;
using AreaMes.Model.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaMes.Model
{
    public class DeviceValueChangedMessage : List<DataItem>
    {
        public static string Name = "DeviceValueChangedMessage";

        public string DeviceId { get; set; }

        public string DeviceSerialNumber { get; set; }
    }

    public class DeviceValueChangedMessageAlarm : List<DataItem>
    {
        public static string Name = "DeviceValueChangedMessageAlarm";

        public string DeviceId { get; set; }

        public string DeviceSerialNumber { get; set; }
    }

    public class BatchChangedMessage
    {
        public static string Name = "BatchChangedMessage";
    }

    public class FaseChangedInfo
    {
        public static string Name = "BatchAvanzamentoChanged";

        public BatchAvanzamento Current { get; set; }

        public string Username { get; set; }
        public bool NoSetupQueue { get; set; }
    }


    public class Logon
    {
        public static string Name = "Logon";

        public string Username { get; set; }
    }

    public class Logoff
    {
        public static string Name = "Logoff";

        public string Username { get; set; }
    }


    public class SendBarcodeMessage
    {
        public  SendBarcodeRequest Data { get; set; }

        public SendBarcodeResponse Response { get; set; }

        public static string Name = "SendBarcodeMessage";
    }

    public class ReadBarcodeMessage
    {
        public ReadBarcodeRequest Data { get; set; }

        public ReadBarcodeResponse Response { get; set; }

        public static string Name = "ReadBarcodeMessage";
    }


    public class BatchMaterialeMessage
    {
        public BatchMateriali Data { get; set; }

        public string BatchId { get; set; }

        public static string Name = "BatchMaterialeMessage";
    }


    public class StatoMacchinaChangedMessage
    {
        public StatoMacchinaDriver Data { get; set; }

        public string MacchinaId { get; set; }

        public static string Name = "StatoMacchinaDriverMessage";
    }


    public class MacchinaChangedMessage  
    {
        public static string Name = "MacchinaChangedMessage";
        public Macchina Data { get; set; }
    }
}
