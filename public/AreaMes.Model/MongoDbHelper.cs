﻿using AreaMes.Meta;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;


namespace AreaMes.Model
{
    public class MongoDBRef<T> : MongoDBRef 
        where T : IDoc
    {

        [BsonIgnore]
        public T Item { get; set; }


        public MongoDBRef( BsonValue id) 
            : base( (typeof(T).Name), id)
        {
     
        }

        public MongoDBRef(string databaseName, BsonValue id)
            : base(databaseName, (typeof(T).Name), id)
        {
           
        }

        public MongoDBRef(T item)
            : base((typeof(T).Name), item.Id)
        {
            Item = item;
        }


        public static MongoDBRef Create(string id) 
        {
            return new MongoDBRef(typeof(T).Name, id);
        }

    }
}
